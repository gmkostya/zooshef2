<?php
class Cache {
	private $cache;

	public function __construct($driver, $expire = 3600) {
		$class = 'Cache\\' . $driver;

		if (class_exists($class)) {
			$this->cache = new $class($expire);
		} else {
			exit('Error: Could not load cache driver ' . $driver . ' cache!');
		}
	}

	public function get($key) {
		return $this->cache->get($key);
	}

	public function set($key, $value) {
		return $this->cache->set($key, $value);
	}

	public function delete($key) {
		return $this->cache->delete($key);
	}
	public function getsearch($key) {
		return $this->cache->getsearch($key);
	}

	public function setsearch($key, $value) {
		return $this->cache->setsearch($key, $value);
	}

	public function deletesearch($key) {
		return $this->cache->deletesearch($key);
	}
	public function getgetproducts($key) {
		return $this->cache->getgetproducts($key);
	}

	public function setgetproducts($key, $value) {
		return $this->cache->setgetproducts($key, $value);
	}

	public function deletegetproducts($key) {
		return $this->cache->deletegetproducts($key);
	}
}
