/*
SQLyog Community Edition- MySQL GUI v6.56
MySQL - 5.5.25 : Database - zooshefnew
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `brands_categories_shablon` */

DROP TABLE IF EXISTS `brands_categories_shablon`;

CREATE TABLE `brands_categories_shablon` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `single_name` varchar(255) DEFAULT NULL,
  `menu_name` varchar(255) NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `meta_description` varchar(500) NOT NULL,
  `seo_description` text,
  `seo_description_up` text,
  `seo_description_middle` text,
  PRIMARY KEY (`category_id`,`brand_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;