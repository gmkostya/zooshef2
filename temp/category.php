<script>
    window.dataLayer = window.dataLayer || [];
</script><!-- Google Tag Manager --><noscript><iframe height="0" src="//www.googletagmanager.com/ns.html?id=GTM-M37VWC" style="display:none;visibility:hidden" width="0"></iframe></noscript>
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-M37VWC');
</script><!-- End Google Tag Manager --><!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {   (w[c] = w[c] || []).push(function() {   try {     w.yaCounter33677824 = new Ya.Metrika({     id:33677824,     clickmap:true,     trackLinks:true,     accurateTrackBounce:true,     ecommerce:"dataLayer",     webvisor: true,     trackHash: true,     });   } catch(e) { }   });    var n = d.getElementsByTagName("script")[0],   s = d.createElement("script"),   f = function () { n.parentNode.insertBefore(s, n); };   s.type = "text/javascript";   s.async = true;   s.src = "https://mc.yandex.ru/metrika/watch.js";    if (w.opera == "[object Opera]") {   d.addEventListener("DOMContentLoaded", f, false);   } else { f(); }   })(document, window, "yandex_metrika_callbacks");
</script>
<script>
    var is_moscow_region = false;
</script><noscript>
    <div><img alt="" src="https://mc.yandex.ru/watch/33677824" style="position:absolute; left:-9999px;"></div></noscript>
<div class="footer-down">
    <div class="header-wrapper">
        <header class="main-header center-wrapper clearfix">
            <div class="header-elements-block clearfix">
                <a class="logo" href="/">ЗООшеф</a>
                <div class="search_mobile_button">
                    <i aria-hidden="true" class="fa fa-search"></i>
                </div>
                <div class="cart-block full-cart fl-right">
                    <div class="cart-info">
                        <div class="products-count">
                            <span id="total_ammount">9</span> товаров
                        </div>
                        <div class="price-count">
                            на <span id="total_products_price">42921</span> руб.
                        </div>
                    </div>
                    <div class="cart-drop">
                        <div class="cart-drop-wrapper" id="cart-drop-wrapper">
                            <div class="cart-items-wrapper clearfix" id="top-cart-items">
                                <div class="clearfix">
                                    <div class="cart-item header-cart-item" data-brand="Belcando (Белькандо)" data-category="Собаки / Корм для собак / Сухой корм для собак" data-id="1135" data-name="Belcando Puppy Gravy для Щенков, Беременных и Кормящих Собак" data-price="4769.00" data-quantity="9" data-variant="Упаковка 15кг" id="top-cart-item-1135">
                                        <div class="image fl-left"><img alt="" src="/files/products/3270_small.jpg"></div>
                                        <div class="cart-item-info">
                                            <div class="name">
                                                Упаковка 15кг
                                            </div>
                                            <div class="art">
                                                Артикул 0247103
                                            </div>
                                            <div class="price-count clearfix">
                                                <div class="price-value fl-right">
                                                    на <span class="price-text"><span class="price-value">42921</span> руб.</span>
                                                </div>
                                                <div class="items-count fl-left" data-price="4769" data-trigger="spinner" data-variant="1135">
                                                    <input class="counter" name="count" type="text" value="9">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-block">
                                <div id="discount-block-container">
                                    Ваша скидка: <strong>0</strong> руб.
                                </div><a class="btn btn-primary" href="/cart">Оформить заказ</a>
                            </div>
                            <div id="cart-drop-fade"></div>
                        </div>
                    </div>
                </div>
                <div class="info-line fl-left">
                    <div class="custom-select fl-left">
                        <div class="select-mask" id="selected-city">
                            Хмельницкий
                        </div>
                        <div class="custom-select-content"></div><select class="custom-select-hidden" id="user_location_select" name="">
                            <option data-city="Москва" value="Москва">
                                Москва
                            </option>
                            <option data-city="Выбрать регион" value="0">
                                Выбрать регион
                            </option>
                        </select>
                    </div>
                    <div class="contact-data fl-left">
                        <span class="top-contact-phone" id="contact-all-phone">8 800 550 85 86</span><span class="top-contact-phone" id="contact-moscow-phone">(495) 544 85 85</span>
                        <div class="tooltip-pop">
                            <div class="tip-body">
                                <div class="heading">
                                    График работы Call-центра
                                </div>
                                <div class="line">
                                    <span class="days">Пн—Пт:</span> с 9:00 до 18:00
                                </div>
                                <div class="line">
                                    <span class="days">Сб—Вс:</span> с 10:00 до 18:00
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contact-data fl-left">
                        Skype: Zooshef.ru
                    </div>
                    <div class="clearfix"></div><a class="default-link fl-left" href="/delivery.htm">Доставка и оплата</a> <span class="default-link func-link fl-left">Перезвоните мне</span>
                    <div class="tooltip-pop call-us">
                        <span class="default-link func-link fl-left"></span>
                        <div class="tip-body">
                            <span class="default-link func-link fl-left"></span>
                            <div class="close-btn">
                                <span class="default-link func-link fl-left"></span>
                            </div>
                            <div class="zag3">
                                <span class="default-link func-link fl-left">Заказать обратный звонок</span>
                            </div>
                            <div id="form-callback-block">
                                <span class="default-link func-link fl-left"></span>
                                <form id="form-callback" method="post" name="form-callback">
                                    <span class="default-link func-link fl-left"><label class="text-label" for="">Телефон</label></span>
                                    <div class="placeholder-container">
                                        <span class="default-link func-link fl-left"><input class="contact-field phone-number" id="phone-number" name="phone" type="text"><label class="placeholder-text" for="phone-number"></label></span>
                                    </div><span class="default-link func-link fl-left"><label class="text-label" for="">Имя</label></span>
                                    <div class="placeholder-container">
                                        <span class="default-link func-link fl-left"><input class="contact-field" id="user-name" name="name" placeholder="Сидоров Михаил" type="text"></span>
                                    </div>
                                    <div class="sub-text">
                                        <span class="default-link func-link fl-left"><em>Время работы операторов с 9:00 до 18:00, кроме субботы и воскресенья.</em></span>
                                    </div><span class="default-link func-link fl-left"><button class="btn btn-secondary phone" type="submit"><span class="default-link func-link fl-left">позвоните мне</span><input name="action" type="hidden" value="callback"></button></span>
                                </form>
                            </div>
                            <div id="form-callback-block-sended" style="display: none;">
                                <div class="zag4" style="margin-top: 50px;">
                                    Спасибо!
                                </div>
                                <p>Мы свяжемся с вами в ближайшее время!</p>
                            </div>
                        </div>
                    </div><a class="default-link fl-left" href="/contact.htm">Контактная информация</a>
                </div>
                <ul class="header-func-links fl-left">
                    <li>
                        <div class="default-link func-link data-enter">
                            Вход в личный кабинет
                            <div class="tooltip-pop log-in">
                                <div class="tip-body">
                                    <div class="close-btn"></div>
                                    <div class="zag3">
                                        Вход в личный кабинет
                                    </div><label class="text-label" for="">Через социальные сети</label><!--div class="social-links clearfix"><a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="gplus"></a><a href="#" class="pinterest"></a><a href="#" class="email"></a></div-->
                                    <script src="//ulogin.ru/js/ulogin.js">
                                    </script>
                                    <div data-ulogin="display=panel;fields=first_name,last_name,email,phone;verify=1;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2Fzooshef.ru%2Fuloginhandler" id="uLogin"></div>
                                    <form action="/login" id="form-login" method="post" name="form-login">
                                        <label class="text-label" for="">Электронная почта</label>
                                        <div class="placeholder-container">
                                            <input class="contact-field" id="user-email" name="email" placeholder="familia.imia@gmail.com" type="text">
                                        </div><label class="text-label" for="">Пароль</label>
                                        <div class="placeholder-container">
                                            <input class="contact-field" id="user-password" name="password" placeholder="********" type="password">
                                        </div>
                                        <div class="buttons-block clearfix">
                                            <button class="btn btn-secondary txt-up fl-left" id="button-login">войти</button><a class="bordered-link default-link fl-left" href="/registration">Регистрация</a>
                                        </div>
                                        <div class="buttons-block clearfix">
                                            <a class="bordered-link default-link fl-left" href="/forgotpassword">Забыли пароль?</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="default-link func-link data-money" href="#">Накопительная система скидок
                            <div class="tooltip-pop discount-pop">
                                <div class="tip-body">
                                    <div class="close-btn"></div>
                                    <div class="zag3">
                                        Накопительная система скидок
                                    </div>
                                    <p class="text-block">При общей сумме заказов более 25 000 руб. получаете скиду 2% Больше покупаете — больше экномите!</p>
                                    <div class="sustem_bonus">
                                        <!--         -->
                                        <div class="one">
                                            <!--           -->
                                            <div class="elem">
                                                <strong>Скидка 2%</strong>
                                            </div><!--           -->
                                            <div class="elem one">
                                                от 25 000 руб.
                                            </div><!--         -->
                                        </div><!--         -->
                                        <div class="two">
                                            <!--           -->
                                            <div class="elem">
                                                <strong>Скидка 3%</strong>
                                            </div><!--           -->
                                            <div class="elem two">
                                                от 40 000 руб.
                                            </div><!--         -->
                                        </div><!--         -->
                                        <div class="three">
                                            <!--           -->
                                            <div class="elem">
                                                <strong>Скидка 4%</strong>
                                            </div><!--           -->
                                            <div class="elem three">
                                                от 70 000 руб.
                                            </div><!--         -->
                                        </div><!--         -->
                                        <div class="four">
                                            <!--           -->
                                            <div class="elem">
                                                <strong>Скидка 5%</strong>
                                            </div><!--           -->
                                            <div class="elem four">
                                                от 90 000 руб.
                                            </div><!--         -->
                                        </div><!--         -->
                                    </div>
                                    <p class="text-block">При единовременном заказе на сумму от 20 000 руб. Вы также получаете единовременную скидку 2% с покупки. Единовременная скидка не суммируется с накопительной.</p>
                                    <div class="heading">
                                        <strong>Скидки не действут на продукцию</strong>
                                    </div>
                                    <p class="text-block">Belcando, Brit, Cat Chow, CESAR, Chappi, Dog Chow, Dreamies, Edel Cat, Edel Dog, Eukanuba, Felix, Ferplast, Friskies, Golden Eagle, Gourmet, Happy Cat, Happy Dog, Hill's, Kitekat, Leonardo, Pedigree, Perfect Fit, Pro Plan, Pronature, Purina, Sheba, Whiskas, Родные Корма, Родные Места</p>
                                </div>
                            </div></a>
                    </li>
                </ul>
            </div>
            <div class="search-container">
                <form action="/search" id="search_form" method="get" name="search_form">
                    <div class="placeholder-container">
                        <input class="search-field" id="site-search" name="keyword" required="required" type="text"><label class="placeholder-text" for="site-search">Быстрый поиск товаров и брендов</label>
                    </div><button class="btn btn-secondary search-btn txt-up" onclick="if($('#site-search').val().length &lt; 3) {return false;}" type="submit">Найти</button>
                </form>
            </div>
            <section class="main-menu">
                <div class="burgers"></div>
                <ul class="site-menu clearfix">
                    <li class="menu-holder left-side dogs">
                        <a class="menu-text" href="/tovary_dlya_sobak/">Собаки</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/korm-dlya-sobak/">Корм для собак</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/korm-dlya-sobak/suhoy-korm-dlya-sobak/">Сухой корм для собак</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/konservy/">Консервы для собак</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/profilakticheskie_korma/">Лечебные корма для собак</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/profilakticheskij_korm_dlja_sobak/">Профилактические корма для собак</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/zamenitkli-moloka/">Заменители молока</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/kashi_dlja_sobak/">Каши для собак</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/dlya-kastrirovannyh-i-sterilizovannyh-sobak/">Для кастрированных и стерилизованных собак</a><a href="/tovary_dlya_sobak/korm-dlya-sobak/dlya-beremennyh-i-kormyaschih-sobak/">Для беременных и кормящих собак</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Корм для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/suhoy-korm-dlya-sobak/">
                                                    Сухой корм для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/konservy/">
                                                    Консервы для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/profilakticheskie_korma/">
                                                    Лечебные корма для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/profilakticheskij_korm_dlja_sobak/">
                                                    Профилактические корма для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/zamenitkli-moloka/">
                                                    Заменители молока
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/kashi_dlja_sobak/">
                                                    Каши для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/dlya-kastrirovannyh-i-sterilizovannyh-sobak/">
                                                    Для кастрированных и стерилизованных собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/korm-dlya-sobak/dlya-beremennyh-i-kormyaschih-sobak/">
                                                    Для беременных и кормящих собак
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovary_dlya_sobak/kosti-i-lakomstva/">Кости и лакомства</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/vitaminy/">Витамины</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/vitaminy/dlja_kostej_i_sustavov/">Для костей и суставов</a><a href="/tovary_dlya_sobak/vitaminy/dlya-zubov/">Для зубов</a><a href="/tovary_dlya_sobak/vitaminy/dlja_kozhi_i_shersti/">Для кожи и шерсти</a><a href="/tovary_dlya_sobak/vitaminy/obshheukrepljajushhie/">Общеукрепляющие</a><a href="/tovary_dlya_sobak/vitaminy/dlja_uluchshenija_immuniteta/">Для улучшения иммунитета</a><a href="/tovary_dlya_sobak/vitaminy/multivitamini_dlja_sobak/">Мультивитамины для собак</a><a href="/tovary_dlya_sobak/vitaminy/dlja_snizhenija_vesa/">Для снижения веса</a><a href="/tovary_dlya_sobak/vitaminy/dlja_jkt/">Витамины и пищевые добавки для ЖКТ</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Витамины
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/dlja_kostej_i_sustavov/">
                                                    Для костей и суставов
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/dlya-zubov/">
                                                    Для зубов
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/dlja_kozhi_i_shersti/">
                                                    Для кожи и шерсти
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/obshheukrepljajushhie/">
                                                    Общеукрепляющие
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/dlja_uluchshenija_immuniteta/">
                                                    Для улучшения иммунитета
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/multivitamini_dlja_sobak/">
                                                    Мультивитамины для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/dlja_snizhenija_vesa/">
                                                    Для снижения веса
                                                </option>
                                                <option value="/tovary_dlya_sobak/vitaminy/dlja_jkt/">
                                                    Витамины и пищевые добавки для ЖКТ
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/">Аксессуары для выгула собак</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/osheiniki/">Ошейники</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/strogie_oshejniki/">Строгие ошейники</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/shlejki_dlja_sobak/">Шлейки для собак</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/povodki/">Поводки</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/ruletki_i_aksessuari/">Рулетки и аксессуары</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/namordniki/">Намордники</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/adresniki_i_brelki_dlja_sobak/">Адресники и брелки для собак</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/vreznie_dveri_dlja_sobak/">Врезные двери для собак</a><a href="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/vjuchnie_sumki_dlja_sobak/">Полезные аксессуары</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Аксессуары для выгула собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/osheiniki/">
                                                    Ошейники
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/strogie_oshejniki/">
                                                    Строгие ошейники
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/shlejki_dlja_sobak/">
                                                    Шлейки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/povodki/">
                                                    Поводки
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/ruletki_i_aksessuari/">
                                                    Рулетки и аксессуары
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/namordniki/">
                                                    Намордники
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/adresniki_i_brelki_dlja_sobak/">
                                                    Адресники и брелки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/vreznie_dveri_dlja_sobak/">
                                                    Врезные двери для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/aksessuari_dlja_vigula_sobak/vjuchnie_sumki_dlja_sobak/">
                                                    Полезные аксессуары
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/">Миски и аксессуары для кормления</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/miski/">Миски</a><a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/kormushki_i_poilki/">Кормушки и поилки</a><a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/podstavki_s_miskami/">Подставки с мисками</a><a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/dorozhnie_miski/">Дорожные миски</a><a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/kontejneri_dlja_korma/">Контейнеры для корма</a><a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/kovriki_dlja_misok/">Коврики для мисок</a><a href="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/sumki_dlja_lakomstv/">Сумки для лакомств</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Миски и аксессуары для кормления
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/miski/">
                                                    Миски
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/kormushki_i_poilki/">
                                                    Кормушки и поилки
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/podstavki_s_miskami/">
                                                    Подставки с мисками
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/dorozhnie_miski/">
                                                    Дорожные миски
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/kontejneri_dlja_korma/">
                                                    Контейнеры для корма
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/kovriki_dlja_misok/">
                                                    Коврики для мисок
                                                </option>
                                                <option value="/tovary_dlya_sobak/miski-i-aksessuary-dlya-kormleniya/sumki_dlja_lakomstv/">
                                                    Сумки для лакомств
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/">Средства от блох и клещей</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/oshejniki_ot_parazitov_/">Ошейники от паразитов</a> <a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/kapli_ot_parazitov/">Капли от паразитов</a><a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/tabletki_ot_bloh_i_kleshei/">Таблетки от блох и клещей</a><a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/sprei_ot_parazitov/">Спреи от паразитов</a><a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/shampuni_ot_parazitov/">Шампуни от паразитов</a><a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/antigelmitiki/">Антигельминтики</a><a href="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/udaliteli_kleshhej/">Удалители клещей</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства от блох и клещей
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/oshejniki_ot_parazitov_/">
                                                    Ошейники от паразитов
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/kapli_ot_parazitov/">
                                                    Капли от паразитов
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/tabletki_ot_bloh_i_kleshei/">
                                                    Таблетки от блох и клещей
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/sprei_ot_parazitov/">
                                                    Спреи от паразитов
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/shampuni_ot_parazitov/">
                                                    Шампуни от паразитов
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/antigelmitiki/">
                                                    Антигельминтики
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva-ot-bloh-i-kleschey/udaliteli_kleshhej/">
                                                    Удалители клещей
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/">Средства по уходу и груминг</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/shampuni_i_kondicioneri/">Шампуни и кондиционеры</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/aksessuari_dlja_mitiya/">Аксессуары для мытья</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/instrumenti_dlja_gruminga/">Инструменты для груминга</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/uhod_za_ushami/">Уход за ушами</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/kosmetika_dlja_vistavok/">Косметика для выставок</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/uhod_za_glazami/">Уход за глазами</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/sredstva_dlja_polosti_pasti/">Средства для полости пасти</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/uhod_za_lapami/">Уход за лапами</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/otpugivajushhie_sredstva/">Отпугивающие средства</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/tualety_i_pelenki/">Туалеты и пеленки для собак</a><a href="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/sredstva_dlja_uborki/">Средства для уборки</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства по уходу и груминг
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/shampuni_i_kondicioneri/">
                                                    Шампуни и кондиционеры
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/aksessuari_dlja_mitiya/">
                                                    Аксессуары для мытья
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/instrumenti_dlja_gruminga/">
                                                    Инструменты для груминга
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/uhod_za_ushami/">
                                                    Уход за ушами
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/kosmetika_dlja_vistavok/">
                                                    Косметика для выставок
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/uhod_za_glazami/">
                                                    Уход за глазами
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/sredstva_dlja_polosti_pasti/">
                                                    Средства для полости пасти
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/uhod_za_lapami/">
                                                    Уход за лапами
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/otpugivajushhie_sredstva/">
                                                    Отпугивающие средства
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/tualety_i_pelenki/">
                                                    Туалеты и пеленки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/sredstva_po_uhodu_i_gruming/sredstva_dlja_uborki/">
                                                    Средства для уборки
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/igrushki_dlya_sobak/">Игрушки для собак</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_iz_rezini/">Игрушки из резины</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/frisbi/">Фрисби</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/razvivajushhie_igrushki/">Развивающие игрушки</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_zubochistki/">Игрушки зубочистки</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/kanati_i_vervochnie_igrushki/">Канаты и верёвочные игрушки</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_iz_lateksa/">Игрушки из латекса</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/myachiki/">Мячики для собак</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/myagkie_igrushki/">Мягкие игрушки</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_dlja_aporta/">Игрушки для апорта</a><a href="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_dlja_aktivnih_igr/">Игрушки для активных игр</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Игрушки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_iz_rezini/">
                                                    Игрушки из резины
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/frisbi/">
                                                    Фрисби
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/razvivajushhie_igrushki/">
                                                    Развивающие игрушки
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_zubochistki/">
                                                    Игрушки зубочистки
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/kanati_i_vervochnie_igrushki/">
                                                    Канаты и верёвочные игрушки
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_iz_lateksa/">
                                                    Игрушки из латекса
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/myachiki/">
                                                    Мячики для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/myagkie_igrushki/">
                                                    Мягкие игрушки
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_dlja_aporta/">
                                                    Игрушки для апорта
                                                </option>
                                                <option value="/tovary_dlya_sobak/igrushki_dlya_sobak/igrushki_dlja_aktivnih_igr/">
                                                    Игрушки для активных игр
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/perenoski_i_kletki/">Переноски и клетки</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/perenoski_i_kletki/kletki_dlya_sobak/">Клетки для собак</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/aksessuari_dlja_velosipeda/">Аксессуары для велосипеда</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/zagoni_dlja_shhenkov/">Загоны для щенков</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/palatki/">Палатки</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/budki_dlja_sobak/">Будки для собак</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/perenoski_dlja_sobak/">Переноски для собак</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/sumki_dlja_sobak/">Сумки для собак</a><a href="/tovary_dlya_sobak/perenoski_i_kletki/avtomobilnie_aksessuari/">Автомобильные аксессуары</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Переноски и клетки
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/kletki_dlya_sobak/">
                                                    Клетки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/aksessuari_dlja_velosipeda/">
                                                    Аксессуары для велосипеда
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/zagoni_dlja_shhenkov/">
                                                    Загоны для щенков
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/palatki/">
                                                    Палатки
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/budki_dlja_sobak/">
                                                    Будки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/perenoski_dlja_sobak/">
                                                    Переноски для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/sumki_dlja_sobak/">
                                                    Сумки для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/perenoski_i_kletki/avtomobilnie_aksessuari/">
                                                    Автомобильные аксессуары
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/">Лежаки и домики для собак</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/lezhaki_dlja_cobak/">Лежаки для cобак</a><a href="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/divani_i_sofi/">Диваны и софы</a><a href="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/domiki_dlja_sobak/">Домики для собак</a><a href="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/podstilki_dlja_sobak/">Подстилки для собак</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Лежаки и домики для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/lezhaki_dlja_cobak/">
                                                    Лежаки для cобак
                                                </option>
                                                <option value="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/divani_i_sofi/">
                                                    Диваны и софы
                                                </option>
                                                <option value="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/domiki_dlja_sobak/">
                                                    Домики для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/lezhaki_i_domiki_dlja_sobak/podstilki_dlja_sobak/">
                                                    Подстилки для собак
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/odezhda_dlja_sobak/">Одежда для собак</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/odezhda_dlja_sobak/obuv_dlja_sobak/">Обувь для собак</a><a href="/tovary_dlya_sobak/odezhda_dlja_sobak/svitera/">Свитера для собак</a><a href="/tovary_dlya_sobak/odezhda_dlja_sobak/sportivnie_kostjumi/">Спортивные костюмы для собак</a><a href="/tovary_dlya_sobak/odezhda_dlja_sobak/dozhdeviki/">Дождевики для собак</a><a href="/tovary_dlya_sobak/odezhda_dlja_sobak/kombinezoni/">Комбинезоны для собак</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Одежда для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/odezhda_dlja_sobak/obuv_dlja_sobak/">
                                                    Обувь для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/odezhda_dlja_sobak/svitera/">
                                                    Свитера для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/odezhda_dlja_sobak/sportivnie_kostjumi/">
                                                    Спортивные костюмы для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/odezhda_dlja_sobak/dozhdeviki/">
                                                    Дождевики для собак
                                                </option>
                                                <option value="/tovary_dlya_sobak/odezhda_dlja_sobak/kombinezoni/">
                                                    Комбинезоны для собак
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_sobak/veterinarnie_preparati/">Ветеринарные препараты</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_sobak/veterinarnie_preparati/vakcini_i_sivorotki/">Вакцины и сыворотки</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/glaznie_preparati/">Глазные препараты</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/ushnie_preparati/">Ушные препараты</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/uspokoitelnie_sredstva/">Успокоительные средства</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/gomeopaticheskie_sredstva/">Гомеопатические средства</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/imunnie_preparati/">Иммунные препараты</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/preparati_dlja_lechenija_opornodvigatelnogo_apparata/">Препараты для лечения опорно-двигательного аппарата</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/pri_dermaticheskih_zabolevanijah/">При дерматических заболеваниях</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/pri_zabolevanijah_mochepolovoj_sistemi/">При заболеваниях мочеполовой системы</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/pri_zabolevanijah_pishhevaritelnoj_sistemi/">При заболеваниях пищеварительной системы</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/ranozazhivljajushhie_preparati/">Ранозаживляющие препараты</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/protivomikrobnie_preparati/">Противомикробные препараты</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/biogennie_stimuljatori/">Биогенные стимуляторы</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/rastvori/">Растворы</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/kontraceptivi/">Контрацептивы</a><a href="/tovary_dlya_sobak/veterinarnie_preparati/aksessuari/">Аксессуары</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Ветеринарные препараты
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/vakcini_i_sivorotki/">
                                                    Вакцины и сыворотки
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/glaznie_preparati/">
                                                    Глазные препараты
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/ushnie_preparati/">
                                                    Ушные препараты
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/uspokoitelnie_sredstva/">
                                                    Успокоительные средства
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/gomeopaticheskie_sredstva/">
                                                    Гомеопатические средства
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/imunnie_preparati/">
                                                    Иммунные препараты
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/preparati_dlja_lechenija_opornodvigatelnogo_apparata/">
                                                    Препараты для лечения опорно-двигательного аппарата
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/pri_dermaticheskih_zabolevanijah/">
                                                    При дерматических заболеваниях
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/pri_zabolevanijah_mochepolovoj_sistemi/">
                                                    При заболеваниях мочеполовой системы
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/pri_zabolevanijah_pishhevaritelnoj_sistemi/">
                                                    При заболеваниях пищеварительной системы
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/ranozazhivljajushhie_preparati/">
                                                    Ранозаживляющие препараты
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/protivomikrobnie_preparati/">
                                                    Противомикробные препараты
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/biogennie_stimuljatori/">
                                                    Биогенные стимуляторы
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/rastvori/">
                                                    Растворы
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/kontraceptivi/">
                                                    Контрацептивы
                                                </option>
                                                <option value="/tovary_dlya_sobak/veterinarnie_preparati/aksessuari/">
                                                    Аксессуары
                                                </option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="menu-product" style="display:none;">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Pro Plan Акция Корм для Кошек и Собак + Подарок на Новый Год!
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_35.jpg"></div>
                                    <div class="description">
                                        Акция! Купите корм Pro Plan для кошек или собак на сумму от 3000 рублей и получите подарочный сертификат на 500 рублей! Сертификат можно обменять на презент для своего любимца на сайте proplan.daripodarki.ru.
                                    </div><a class="btn btn-primary" href="/actions/pro_plan_akcija_korm_dlja_koshek_i_sobak__podarok_na_novij_god">Купить сейчас</a>
                                </div>
                                <div class="menu-product" style="display:none;">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Hill's Акция Два Мешка Корма для Собак со Скидкой 15%
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_33.jpg"></div>
                                    <div class="description">
                                        Акция! Купите 2 больших мешка корма для собак Hill's Science Plan или Natures Best со скидкой 15%!
                                    </div><a class="btn btn-primary" href="/actions/hills_akcija_dva_meshka_korma_dlja_sobak_so_skidkoj_15">Купить сейчас</a>
                                </div>
                                <div class="menu-product" style="display:none;">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Hill's Акция 2 Упаковки Корма для Щенков и Котят со Скидкой 15%
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_32.jpg"></div>
                                    <div class="description">
                                        Акция! Купите 2 меленьких упаковки корма Hill's для щенков или котят с приятной скидкой в 15%!
                                    </div><a class="btn btn-primary" href="/actions/hills_akcija_2_upakovki_korma_dlja_shhenkov_i_kotjat_so_skidkoj_15">Купить сейчас</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder left-side cats">
                        <a class="menu-text" href="/tovary_dlya_koshek/">Кошки</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/korm_dlja_koshek/">Корм для кошек</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/korm_dlja_koshek/suhoj_korm_dlja_koshek/">Сухой корм для кошек</a><a href="/tovary_dlya_koshek/korm_dlja_koshek/dlja_kastrirovannih_i_sterilizovannih_koshek/">Для кастрированных и стерилизованных кошек</a><a href="/tovary_dlya_koshek/korm_dlja_koshek/konservy_dlya_koshek/">Консервы для кошек</a><a href="/tovary_dlya_koshek/korm_dlja_koshek/profilakticheskij_korm_dlja_koshek/">Профилактический корм для кошек</a><a href="/tovary_dlya_koshek/korm_dlja_koshek/korma_profilakticheskie/">Лечебные корма для кошек</a><a href="/tovary_dlya_koshek/korm_dlja_koshek/dlja_beremennih_i_kormjashhih_koshek/">Для беременных и кормящих кошек</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Корм для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/korm_dlja_koshek/suhoj_korm_dlja_koshek/">
                                                    Сухой корм для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/korm_dlja_koshek/dlja_kastrirovannih_i_sterilizovannih_koshek/">
                                                    Для кастрированных и стерилизованных кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/korm_dlja_koshek/konservy_dlya_koshek/">
                                                    Консервы для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/korm_dlja_koshek/profilakticheskij_korm_dlja_koshek/">
                                                    Профилактический корм для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/korm_dlja_koshek/korma_profilakticheskie/">
                                                    Лечебные корма для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/korm_dlja_koshek/dlja_beremennih_i_kormjashhih_koshek/">
                                                    Для беременных и кормящих кошек
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovary_dlya_koshek/lakomstva_dlya_koshek/">Лакомства для кошек</a>
                                        </li>
                                        <li>
                                            <a href="/tovary_dlya_koshek/pishevye_dobavki_i_vitaminy/">Пищевые добавки и Витамины</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/">Средства от блох и клещей</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/kapli/">Капли</a><a href="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/oshejniki/">Ошейники</a><a href="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/sprei/">Спреи</a><a href="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/shampuni_ot_bloh/">Шампуни от блох</a><a href="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/antigelmintiki_ot_vnutrennih_parazitov/">Антигельминтики (от внутренних паразитов)</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства от блох и клещей
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/kapli/">
                                                    Капли
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/oshejniki/">
                                                    Ошейники
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/sprei/">
                                                    Спреи
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/shampuni_ot_bloh/">
                                                    Шампуни от блох
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/antigelmintiki_ot_vnutrennih_parazitov/">
                                                    Антигельминтики (от внутренних паразитов)
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/aksessuary_dlya_koshek/">Аксессуары для кошек</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/aksessuary_dlya_koshek/oshejniki_dlja_koshek/">Ошейники для кошек</a><a href="/tovary_dlya_koshek/aksessuary_dlya_koshek/povodki_i_shlejki/">Поводки и шлейки</a><a href="/tovary_dlya_koshek/aksessuary_dlya_koshek/adresniki_i_brelki_dlja_koshek/">Адресники и брелки для кошек</a><a href="/tovary_dlya_koshek/aksessuary_dlya_koshek/vreznie_dveri_dlja_koshek/">Врезные двери для кошек</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Аксессуары для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/aksessuary_dlya_koshek/oshejniki_dlja_koshek/">
                                                    Ошейники для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/aksessuary_dlya_koshek/povodki_i_shlejki/">
                                                    Поводки и шлейки
                                                </option>
                                                <option value="/tovary_dlya_koshek/aksessuary_dlya_koshek/adresniki_i_brelki_dlja_koshek/">
                                                    Адресники и брелки для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/aksessuary_dlya_koshek/vreznie_dveri_dlja_koshek/">
                                                    Врезные двери для кошек
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/posuda_dlja_koshek/">Посуда для кошек</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/posuda_dlja_koshek/avtomaticheskie_kormushki/">Автоматические кормушки</a><a href="/tovary_dlya_koshek/posuda_dlja_koshek/miski/">Миски для кошек</a><a href="/tovary_dlya_koshek/posuda_dlja_koshek/poilki_dlja_koshek/">Поилки для кошек</a><a href="/tovary_dlya_koshek/posuda_dlja_koshek/kovriki_pod_misku/">Коврики под миску для кошек</a><a href="/tovary_dlya_koshek/posuda_dlja_koshek/pitevie_fontanchiki/">Питьевые фонтанчики</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Посуда для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/posuda_dlja_koshek/avtomaticheskie_kormushki/">
                                                    Автоматические кормушки
                                                </option>
                                                <option value="/tovary_dlya_koshek/posuda_dlja_koshek/miski/">
                                                    Миски для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/posuda_dlja_koshek/poilki_dlja_koshek/">
                                                    Поилки для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/posuda_dlja_koshek/kovriki_pod_misku/">
                                                    Коврики под миску для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/posuda_dlja_koshek/pitevie_fontanchiki/">
                                                    Питьевые фонтанчики
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/tualeti_i_lotki/">Туалеты и лотки</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/tualeti_i_lotki/tualeti_lotki/">Туалеты - лотки</a><a href="/tovary_dlya_koshek/tualeti_i_lotki/tualeti_domiki/">Туалеты - домики</a><a href="/tovary_dlya_koshek/tualeti_i_lotki/lopatki_dlja_tualetov/">Лопатки для туалетов</a><a href="/tovary_dlya_koshek/tualeti_i_lotki/kovriki_dlja_tualetov/">Коврики для туалетов</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Туалеты и лотки
                                                </option>
                                                <option value="/tovary_dlya_koshek/tualeti_i_lotki/tualeti_lotki/">
                                                    Туалеты - лотки
                                                </option>
                                                <option value="/tovary_dlya_koshek/tualeti_i_lotki/tualeti_domiki/">
                                                    Туалеты - домики
                                                </option>
                                                <option value="/tovary_dlya_koshek/tualeti_i_lotki/lopatki_dlja_tualetov/">
                                                    Лопатки для туалетов
                                                </option>
                                                <option value="/tovary_dlya_koshek/tualeti_i_lotki/kovriki_dlja_tualetov/">
                                                    Коврики для туалетов
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovary_dlya_koshek/napolniteli_dlya_tualeta/">Наполнители для туалета</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/">Когтеточки и игровые площадки</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/kogtetochki/">Когтеточки</a><a href="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/igrovie_ploshhadki/">Игровые площадки</a><a href="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/tunneli_dlja_koshek/">Туннели для кошек</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Когтеточки и игровые площадки
                                                </option>
                                                <option value="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/kogtetochki/">
                                                    Когтеточки
                                                </option>
                                                <option value="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/igrovie_ploshhadki/">
                                                    Игровые площадки
                                                </option>
                                                <option value="/tovary_dlya_koshek/kogtetochki_i_igrovie_ploshhadki/tunneli_dlja_koshek/">
                                                    Туннели для кошек
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/lezhaki_i_domiki/">Лежаки и домики</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/lezhaki_i_domiki/lezhanki_dlja_koshek/">Лежанки для кошек</a><a href="/tovary_dlya_koshek/lezhaki_i_domiki/sofi_dlja_koshek/">Софы для кошек</a><a href="/tovary_dlya_koshek/lezhaki_i_domiki/domiki_dlja_koshek/">Домики для кошек</a><a href="/tovary_dlya_koshek/lezhaki_i_domiki/gamaki_dlja_koshek/">Гамаки для кошек</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Лежаки и домики
                                                </option>
                                                <option value="/tovary_dlya_koshek/lezhaki_i_domiki/lezhanki_dlja_koshek/">
                                                    Лежанки для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/lezhaki_i_domiki/sofi_dlja_koshek/">
                                                    Софы для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/lezhaki_i_domiki/domiki_dlja_koshek/">
                                                    Домики для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/lezhaki_i_domiki/gamaki_dlja_koshek/">
                                                    Гамаки для кошек
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/igrushki_dlja_koshek/">Игрушки для кошек</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/igrushki_dlja_koshek/zhevatelnie_igrushki/">Жевательные игрушки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/zavodnie_igrushki/">Заводные игрушки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/igrushki_s_koshachej_mjatoj/">Игрушки с кошачей мятой</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/igrushkikogtetochki/">Игрушки-когтеточки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/mishki/">Мышки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/matreshki_dlja_lakomstva/">Матрешки для лакомства</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/razvivajushhie_igrushki/">Развивающие игрушки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/cilindr/">Цилиндр</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/interaktivnie_igrushki/">Интерактивные игрушки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/mjagkie_igrushki/">Мягкие игрушки</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/draznilki/">Дразнилки для кошек</a><a href="/tovary_dlya_koshek/igrushki_dlja_koshek/shari_i_mjachi/">Шары и мячи</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Игрушки для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/zhevatelnie_igrushki/">
                                                    Жевательные игрушки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/zavodnie_igrushki/">
                                                    Заводные игрушки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/igrushki_s_koshachej_mjatoj/">
                                                    Игрушки с кошачей мятой
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/igrushkikogtetochki/">
                                                    Игрушки-когтеточки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/mishki/">
                                                    Мышки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/matreshki_dlja_lakomstva/">
                                                    Матрешки для лакомства
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/razvivajushhie_igrushki/">
                                                    Развивающие игрушки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/cilindr/">
                                                    Цилиндр
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/interaktivnie_igrushki/">
                                                    Интерактивные игрушки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/mjagkie_igrushki/">
                                                    Мягкие игрушки
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/draznilki/">
                                                    Дразнилки для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/igrushki_dlja_koshek/shari_i_mjachi/">
                                                    Шары и мячи
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/sredstva_gigieni/">Средства гигиены</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/sredstva_gigieni/sredstva_dlja_chistki_zubov/">Средства для чистки зубов</a><a href="/tovary_dlya_koshek/sredstva_gigieni/sprei_i_kapli/">Спреи и капли</a><a href="/tovary_dlya_koshek/sredstva_gigieni/salfetki/">Салфетки</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства гигиены
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_gigieni/sredstva_dlja_chistki_zubov/">
                                                    Средства для чистки зубов
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_gigieni/sprei_i_kapli/">
                                                    Спреи и капли
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_gigieni/salfetki/">
                                                    Салфетки
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/sredstva_po_uhodu/">Средства по уходу</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/sredstva_po_uhodu/pelenki_i_podstilki/">Пеленки и подстилки</a><a href="/tovary_dlya_koshek/sredstva_po_uhodu/dezodaranti_dlja_tualeta/">Дезодаранты для туалета</a><a href="/tovary_dlya_koshek/sredstva_po_uhodu/zashhitnie_kovriki/">Защитные коврики</a><a href="/tovary_dlya_koshek/sredstva_po_uhodu/sredstva_dlja_obrabotki_i_uborki/">Средства для обработки и уборки</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства по уходу
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_po_uhodu/pelenki_i_podstilki/">
                                                    Пеленки и подстилки
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_po_uhodu/dezodaranti_dlja_tualeta/">
                                                    Дезодаранты для туалета
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_po_uhodu/zashhitnie_kovriki/">
                                                    Защитные коврики
                                                </option>
                                                <option value="/tovary_dlya_koshek/sredstva_po_uhodu/sredstva_dlja_obrabotki_i_uborki/">
                                                    Средства для обработки и уборки
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/gruming/">Груминг</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/gruming/shampuni_i_kondicioneri/">Шампуни и кондиционеры</a><a href="/tovary_dlya_koshek/gruming/instrumenti_dlja_gruminga/">Инструменты для груминга</a><a href="/tovary_dlya_koshek/gruming/kosmetika_dlja_vistavki/">Косметика для выставки для кошек</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Груминг
                                                </option>
                                                <option value="/tovary_dlya_koshek/gruming/shampuni_i_kondicioneri/">
                                                    Шампуни и кондиционеры
                                                </option>
                                                <option value="/tovary_dlya_koshek/gruming/instrumenti_dlja_gruminga/">
                                                    Инструменты для груминга
                                                </option>
                                                <option value="/tovary_dlya_koshek/gruming/kosmetika_dlja_vistavki/">
                                                    Косметика для выставки для кошек
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/perenoski_dlja_koshek/">Переноски для кошек</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/perenoski_dlja_koshek/kontejneri_perenoski/">Контейнеры-переноски</a><a href="/tovary_dlya_koshek/perenoski_dlja_koshek/sumki_dlja_koshek/">Сумки для кошек</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Переноски для кошек
                                                </option>
                                                <option value="/tovary_dlya_koshek/perenoski_dlja_koshek/kontejneri_perenoski/">
                                                    Контейнеры-переноски
                                                </option>
                                                <option value="/tovary_dlya_koshek/perenoski_dlja_koshek/sumki_dlja_koshek/">
                                                    Сумки для кошек
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovary_dlya_koshek/veterinarnie_preparati/">Ветеринарные препараты</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovary_dlya_koshek/veterinarnie_preparati/glaznie_preparati/">Глазные препараты</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/ushnie_preparati/">Ушные препараты</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/uspokoitelnie_sredstva/">Успокоительные средства</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/gomeopaticheskie_sredstva/">Гомеопатические средства</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/imunnie_preparati/">Имунные препараты</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/preparati_dlja_lechenija_opornodvigatelnogo_apparata/">Препараты для лечения опорно-двигательного аппарата</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/pri_dermaticheskih_zabolevanijah/">При дерматических заболеваниях</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/pri_zabolevanijah_mochepolovoj_sistemi/">При заболеваниях мочеполовой системы</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/pri_zabolevanijah_pishhevaritelnoj_sistemi/">При заболеваниях пищеварительной системы</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/ranozazhivljajushhie_preparati/">Ранозаживляющие препараты</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/protivomikrobnie_preparati/">Противомикробные препараты</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/biogennie_stimuljatori/">Биогенные стимуляторы</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/rastvori/">Растворы</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/vakcini_i_sivorotki/">Вакцины и сыворотки</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/kontraceptivi/">Контрацептивы</a><a href="/tovary_dlya_koshek/veterinarnie_preparati/aksessuari/">Аксессуары</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Ветеринарные препараты
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/glaznie_preparati/">
                                                    Глазные препараты
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/ushnie_preparati/">
                                                    Ушные препараты
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/uspokoitelnie_sredstva/">
                                                    Успокоительные средства
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/gomeopaticheskie_sredstva/">
                                                    Гомеопатические средства
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/imunnie_preparati/">
                                                    Имунные препараты
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/preparati_dlja_lechenija_opornodvigatelnogo_apparata/">
                                                    Препараты для лечения опорно-двигательного аппарата
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/pri_dermaticheskih_zabolevanijah/">
                                                    При дерматических заболеваниях
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/pri_zabolevanijah_mochepolovoj_sistemi/">
                                                    При заболеваниях мочеполовой системы
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/pri_zabolevanijah_pishhevaritelnoj_sistemi/">
                                                    При заболеваниях пищеварительной системы
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/ranozazhivljajushhie_preparati/">
                                                    Ранозаживляющие препараты
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/protivomikrobnie_preparati/">
                                                    Противомикробные препараты
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/biogennie_stimuljatori/">
                                                    Биогенные стимуляторы
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/rastvori/">
                                                    Растворы
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/vakcini_i_sivorotki/">
                                                    Вакцины и сыворотки
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/kontraceptivi/">
                                                    Контрацептивы
                                                </option>
                                                <option value="/tovary_dlya_koshek/veterinarnie_preparati/aksessuari/">
                                                    Аксессуары
                                                </option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="menu-product" style="display:none;">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Pro Plan Акция Корм для Кошек и Собак + Подарок на Новый Год!
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_35.jpg"></div>
                                    <div class="description">
                                        Акция! Купите корм Pro Plan для кошек или собак на сумму от 3000 рублей и получите подарочный сертификат на 500 рублей! Сертификат можно обменять на презент для своего любимца на сайте proplan.daripodarki.ru.
                                    </div><a class="btn btn-primary" href="/actions/pro_plan_akcija_korm_dlja_koshek_i_sobak__podarok_na_novij_god">Купить сейчас</a>
                                </div>
                                <div class="menu-product" style="display:none;">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Hill's Акция Две Упаковки Корма Prescription Diet для Кошек со Скидкой 15%
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_34.jpg"></div>
                                    <div class="description">
                                        Акция! Купите две 1,5-килограммовых упаковки корма Hill's Prescription Diet для кошек со скидкой 15%!
                                    </div><a class="btn btn-primary" href="/actions/hills_akcija_dve_upakovki_korma_prescription_diet_dlja_koshek_so_skidkoj_15">Купить сейчас</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder left-side rats">
                        <a class="menu-text" href="/tovari_dlja_grizunov/">Грызуны</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_grizunov/korm_dlja_grizunov/">Корм для грызунов</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_degu/">Корм для дегу</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korma_universalnie_dlja_grizunov/">Корма универсальные</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_horkov/">Корм для хорьков</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_homjakov/">Корм для хомяков</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_mishej_i_kris/">Корм для мышей и крыс</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_krolikov/">Корм для кроликов</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_morskih_svinok/">Корм для морских свинок</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_shinshill/">Корм для шиншилл</a><a href="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_ezhej/">Корм для ежей</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Корм для грызунов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_degu/">
                                                    Корм для дегу
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korma_universalnie_dlja_grizunov/">
                                                    Корма универсальные
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_horkov/">
                                                    Корм для хорьков
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_homjakov/">
                                                    Корм для хомяков
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_mishej_i_kris/">
                                                    Корм для мышей и крыс
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_krolikov/">
                                                    Корм для кроликов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_morskih_svinok/">
                                                    Корм для морских свинок
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_shinshill/">
                                                    Корм для шиншилл
                                                </option>
                                                <option value="/tovari_dlja_grizunov/korm_dlja_grizunov/korm_dlja_ezhej/">
                                                    Корм для ежей
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovari_dlja_grizunov/lakomstva_dlja_grizunov/">Лакомства для Грызунов</a>
                                        </li>
                                        <li>
                                            <a href="/tovari_dlja_grizunov/napolniteli_dlja_grizunov/">Наполнители для грызунов</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/">Клетки для мелких животных</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_homjakov/">Клетки для хомяков</a><a href="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_morskih_svinok/">Клетки для морских свинок</a><a href="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_krolikov/">Клетки для кроликов</a><a href="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_horkov/">Клетки для хорьков</a><a href="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_mishej_i_kris/">Клетки для мышей и крыс</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Клетки для мелких животных
                                                </option>
                                                <option value="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_homjakov/">
                                                    Клетки для хомяков
                                                </option>
                                                <option value="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_morskih_svinok/">
                                                    Клетки для морских свинок
                                                </option>
                                                <option value="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_krolikov/">
                                                    Клетки для кроликов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_horkov/">
                                                    Клетки для хорьков
                                                </option>
                                                <option value="/tovari_dlja_grizunov/kletki_dlja_melkih_zhivotnih/kletki_dlja_mishej_i_kris/">
                                                    Клетки для мышей и крыс
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovari_dlja_grizunov/gnezda_i_domiki/">Гнезда и домики</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/">Миски и поилки для грызунов</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/miski/">Миски</a><a href="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/poilki/">Поилки</a><a href="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/kormushki/">Кормушки</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Миски и поилки для грызунов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/miski/">
                                                    Миски
                                                </option>
                                                <option value="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/poilki/">
                                                    Поилки
                                                </option>
                                                <option value="/tovari_dlja_grizunov/miski_i_poilki_dlja_grizunov/kormushki/">
                                                    Кормушки
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/">Игрушки для грызунов</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/kolesa/">Колеса</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/kacheli/">Качели</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/lestnici/">Лестницы</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/progulochnie_shari/">Прогулочные шары</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/begovie_dorozhki/">Беговые дорожки</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/zhevatelnie_igrushki/">Жевательные игрушки</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/derevjannie_igrushki/">Деревянные игрушки</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/igrushki_dlja_lakomstv/">Игрушки для лакомств</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/gamaki/">Гамаки</a><a href="/tovari_dlja_grizunov/igrushki_dlja_grizunov/tonneli/">Тоннели</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Игрушки для грызунов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/kolesa/">
                                                    Колеса
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/kacheli/">
                                                    Качели
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/lestnici/">
                                                    Лестницы
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/progulochnie_shari/">
                                                    Прогулочные шары
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/begovie_dorozhki/">
                                                    Беговые дорожки
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/zhevatelnie_igrushki/">
                                                    Жевательные игрушки
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/derevjannie_igrushki/">
                                                    Деревянные игрушки
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/igrushki_dlja_lakomstv/">
                                                    Игрушки для лакомств
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/gamaki/">
                                                    Гамаки
                                                </option>
                                                <option value="/tovari_dlja_grizunov/igrushki_dlja_grizunov/tonneli/">
                                                    Тоннели
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovari_dlja_grizunov/amunicija_dlja_grizunov/">Амуниция для грызунов</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_grizunov/sredstva_po_uhody/">Средства по уходу</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_grizunov/sredstva_po_uhody/tualeti_dlja_grizunov/">Туалеты для грызунов</a><a href="/tovari_dlja_grizunov/sredstva_po_uhody/puhoderki_i_rascheski_dlja_grizunov/">Пуходерки и расчески для грызунов</a><a href="/tovari_dlja_grizunov/sredstva_po_uhody/shampuni_i_sprei_dlja_grizunov/">Шампуни и спреи для грызунов</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства по уходу
                                                </option>
                                                <option value="/tovari_dlja_grizunov/sredstva_po_uhody/tualeti_dlja_grizunov/">
                                                    Туалеты для грызунов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/sredstva_po_uhody/puhoderki_i_rascheski_dlja_grizunov/">
                                                    Пуходерки и расчески для грызунов
                                                </option>
                                                <option value="/tovari_dlja_grizunov/sredstva_po_uhody/shampuni_i_sprei_dlja_grizunov/">
                                                    Шампуни и спреи для грызунов
                                                </option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder left-side birds">
                        <a class="menu-text" href="/tovari_dlja_ptic/">Птицы</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_ptic/korm_dlja_ptic/">Корм для птиц</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_ptic/korm_dlja_ptic/dlja_ptencov/">Для птенцов</a><a href="/tovari_dlja_ptic/korm_dlja_ptic/universalnij/">Универсальный</a><a href="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_volnistih_popugaev/">Для волнистых попугаев</a><a href="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_srednih_i_krupnih_popugaev/">Для средних и крупных попугаев</a><a href="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_kanareek/">Для канареек</a><a href="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_jekzoticheskih_ptic/">Для экзотических птиц</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Корм для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/korm_dlja_ptic/dlja_ptencov/">
                                                    Для птенцов
                                                </option>
                                                <option value="/tovari_dlja_ptic/korm_dlja_ptic/universalnij/">
                                                    Универсальный
                                                </option>
                                                <option value="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_volnistih_popugaev/">
                                                    Для волнистых попугаев
                                                </option>
                                                <option value="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_srednih_i_krupnih_popugaev/">
                                                    Для средних и крупных попугаев
                                                </option>
                                                <option value="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_kanareek/">
                                                    Для канареек
                                                </option>
                                                <option value="/tovari_dlja_ptic/korm_dlja_ptic/korm_dlja_jekzoticheskih_ptic/">
                                                    Для экзотических птиц
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/">Средства по уходу за птицами</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/napolniteli/">Наполнители</a><a href="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/zhidkost_dlja_udalenija_zapaha/">Жидкость для удаления запаха</a><a href="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/antistress_dlja_ptic/">Антистресс для птиц</a><a href="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/sredstva_dlja_ochishhenija_perev/">Средства для очищения перьев</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Средства по уходу за птицами
                                                </option>
                                                <option value="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/napolniteli/">
                                                    Наполнители
                                                </option>
                                                <option value="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/zhidkost_dlja_udalenija_zapaha/">
                                                    Жидкость для удаления запаха
                                                </option>
                                                <option value="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/antistress_dlja_ptic/">
                                                    Антистресс для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/sredstva_po_uhodu_za_pticami/sredstva_dlja_ochishhenija_perev/">
                                                    Средства для очищения перьев
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_ptic/kletki_dlja_ptic/">Клетки для птиц</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_ptic/kletki_dlja_ptic/kletki_dlja_ptic_malie/">Клетки для птиц малые</a><a href="/tovari_dlja_ptic/kletki_dlja_ptic/kletki_dlja_ptic_srednie/">Клетки для птиц средние</a><a href="/tovari_dlja_ptic/kletki_dlja_ptic/voleri_dlja_ptic/">Вольеры для птиц</a><a href="/tovari_dlja_ptic/kletki_dlja_ptic/podstavki_dlja_kletok/">Подставки для клеток</a><a href="/tovari_dlja_ptic/kletki_dlja_ptic/perenoska_dlja_ptic/">Переноска для птиц</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Клетки для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/kletki_dlja_ptic/kletki_dlja_ptic_malie/">
                                                    Клетки для птиц малые
                                                </option>
                                                <option value="/tovari_dlja_ptic/kletki_dlja_ptic/kletki_dlja_ptic_srednie/">
                                                    Клетки для птиц средние
                                                </option>
                                                <option value="/tovari_dlja_ptic/kletki_dlja_ptic/voleri_dlja_ptic/">
                                                    Вольеры для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/kletki_dlja_ptic/podstavki_dlja_kletok/">
                                                    Подставки для клеток
                                                </option>
                                                <option value="/tovari_dlja_ptic/kletki_dlja_ptic/perenoska_dlja_ptic/">
                                                    Переноска для птиц
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_ptic/aksessuari_dlja_kletok/">Аксессуары для клеток</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/kormushki_dlja_ptic/">Кормушки для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/zherdochki_dlja_ptic/">Жердочки для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/kamni_i_pesok_dlja_ptic/">Камни и песок для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/poilki_dlja_ptic/">Поилки для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/igrushki_dlja_ptic/">Игрушки для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/kacheli_dlja_ptic/">Качели для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/lestnici_dlja_ptic/">Лестницы для птиц</a><a href="/tovari_dlja_ptic/aksessuari_dlja_kletok/vanni_dlja_ptic/">Ванны для птиц</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Аксессуары для клеток
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/kormushki_dlja_ptic/">
                                                    Кормушки для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/zherdochki_dlja_ptic/">
                                                    Жердочки для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/kamni_i_pesok_dlja_ptic/">
                                                    Камни и песок для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/poilki_dlja_ptic/">
                                                    Поилки для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/igrushki_dlja_ptic/">
                                                    Игрушки для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/kacheli_dlja_ptic/">
                                                    Качели для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/lestnici_dlja_ptic/">
                                                    Лестницы для птиц
                                                </option>
                                                <option value="/tovari_dlja_ptic/aksessuari_dlja_kletok/vanni_dlja_ptic/">
                                                    Ванны для птиц
                                                </option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder left-side reptiles">
                        <a class="menu-text" href="/reptilii/">Рептилии</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a href="/reptilii/korm_dlja_reptilij/">Корм для рептилий</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/reptilii/terrariumi_i_aksessuari/">Террариумы и аксессуары</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/reptilii/terrariumi_i_aksessuari/terrariumi/">Террариумы</a><a href="/reptilii/terrariumi_i_aksessuari/napolniteli_dlja_terrariuma/">Наполнители для террариума</a><a href="/reptilii/terrariumi_i_aksessuari/teplo_i_svet_dlja_terrariumov/">Тепло и свет для террариумов</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Террариумы и аксессуары
                                                </option>
                                                <option value="/reptilii/terrariumi_i_aksessuari/terrariumi/">
                                                    Террариумы
                                                </option>
                                                <option value="/reptilii/terrariumi_i_aksessuari/napolniteli_dlja_terrariuma/">
                                                    Наполнители для террариума
                                                </option>
                                                <option value="/reptilii/terrariumi_i_aksessuari/teplo_i_svet_dlja_terrariumov/">
                                                    Тепло и свет для террариумов
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/reptilii/terrariumnie_dekoracii/">Террариумные декорации</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/reptilii/paljudariumi_i_aksessuari/">Палюдариумы и аксессуары</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/reptilii/paljudariumi_i_aksessuari/paljudariumi/">Палюдариумы</a><a href="/reptilii/paljudariumi_i_aksessuari/aksessuari/">Аксессуары</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Палюдариумы и аксессуары
                                                </option>
                                                <option value="/reptilii/paljudariumi_i_aksessuari/paljudariumi/">
                                                    Палюдариумы
                                                </option>
                                                <option value="/reptilii/paljudariumi_i_aksessuari/aksessuari/">
                                                    Аксессуары
                                                </option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder left-side fish">
                        <a class="menu-text" href="/tovari_dlja_rib/">Рыбы</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_rib/akvariumi/">Аквариумы</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_rib/akvariumi/akvariumi_dlja_rib/">Аквариумы для рыб</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Аквариумы
                                                </option>
                                                <option value="/tovari_dlja_rib/akvariumi/akvariumi_dlja_rib/">
                                                    Аквариумы для рыб
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="/tovari_dlja_rib/tumbi_pod_akvarium/">Тумбы под аквариум</a>
                                        </li>
                                        <li>
                                            <a href="/tovari_dlja_rib/korm_dlja_akvariumnih_rib/">Корм для рыб</a>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_rib/akvariumnie_dekoracii/">Аквариумные декорации</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_rib/akvariumnie_dekoracii/dekoracii_dlja_akvariumov/">Декорации для аквариумов</a><a href="/tovari_dlja_rib/akvariumnie_dekoracii/rastenija_dlja_akvariumov/">Растения для аквариумов</a><a href="/tovari_dlja_rib/akvariumnie_dekoracii/grunt_dlja_akvariumov/">Грунт для аквариумов</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Аквариумные декорации
                                                </option>
                                                <option value="/tovari_dlja_rib/akvariumnie_dekoracii/dekoracii_dlja_akvariumov/">
                                                    Декорации для аквариумов
                                                </option>
                                                <option value="/tovari_dlja_rib/akvariumnie_dekoracii/rastenija_dlja_akvariumov/">
                                                    Растения для аквариумов
                                                </option>
                                                <option value="/tovari_dlja_rib/akvariumnie_dekoracii/grunt_dlja_akvariumov/">
                                                    Грунт для аквариумов
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="more_items" href="/tovari_dlja_rib/aksessuari_dlja_akvariuma/">Аксессуары для аквариума</a>
                                            <div class="menu_lvl_3_bg">
                                                <div class="menu_lvl_3">
                                                    <a href="/tovari_dlja_rib/aksessuari_dlja_akvariuma/sredstva_dlya_vodi/">Средства для воды</a><a href="/tovari_dlja_rib/aksessuari_dlja_akvariuma/filtri_i_komplektujushhie/">Фильтры и комплектующие</a><a href="/tovari_dlja_rib/aksessuari_dlja_akvariuma/tehnicheskie_komponeti/">Технические компонеты</a><a href="/tovari_dlja_rib/aksessuari_dlja_akvariuma/vse_dlja_xbcnrb_frdfhbevjd/">Все для чистки аквариумов</a>
                                                </div>
                                            </div><select class="menu_mobile_select" name="hero">
                                                <option selected>
                                                    Аксессуары для аквариума
                                                </option>
                                                <option value="/tovari_dlja_rib/aksessuari_dlja_akvariuma/sredstva_dlya_vodi/">
                                                    Средства для воды
                                                </option>
                                                <option value="/tovari_dlja_rib/aksessuari_dlja_akvariuma/filtri_i_komplektujushhie/">
                                                    Фильтры и комплектующие
                                                </option>
                                                <option value="/tovari_dlja_rib/aksessuari_dlja_akvariuma/tehnicheskie_komponeti/">
                                                    Технические компонеты
                                                </option>
                                                <option value="/tovari_dlja_rib/aksessuari_dlja_akvariuma/vse_dlja_xbcnrb_frdfhbevjd/">
                                                    Все для чистки аквариумов
                                                </option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder left-side horses">
                        <a class="menu-text" href="/loshadi/">Лошади</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <ul class="links-list">
                                        <li>
                                            <a href="/loshadi/geli_i_krema/">Гели и крема</a>
                                        </li>
                                        <li>
                                            <a href="/loshadi/shampuni_dlja_loshadej/">Шампуни для лошадей</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-holder right-side actions">
                        <a class="menu-text" href="/actions">Акции</a>
                        <div class="menu-drop">
                            <div class="menu-products-wrapper clearfix">
                                <div class="menu-product">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Pro Plan Акция Корм для Кошек и Собак + Подарок на Новый Год!
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_35.jpg"></div>
                                    <div class="description">
                                        Акция! Купите корм Pro Plan для кошек или собак на сумму от 3000 рублей и получите подарочный сертификат на 500 рублей! Сертификат можно обменять на презент для своего любимца на сайте proplan.daripodarki.ru.
                                    </div><a class="btn btn-primary" href="/actions/pro_plan_akcija_korm_dlja_koshek_i_sobak__podarok_na_novij_god">Купить сейчас</a>
                                </div>
                                <div class="menu-product">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Hill's Акция Две Упаковки Корма Prescription Diet для Кошек со Скидкой 15%
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_34.jpg"></div>
                                    <div class="description">
                                        Акция! Купите две 1,5-килограммовых упаковки корма Hill's Prescription Diet для кошек со скидкой 15%!
                                    </div><a class="btn btn-primary" href="/actions/hills_akcija_dve_upakovki_korma_prescription_diet_dlja_koshek_so_skidkoj_15">Купить сейчас</a>
                                </div>
                                <div class="menu-product">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Hill's Акция Два Мешка Корма для Собак со Скидкой 15%
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_33.jpg"></div>
                                    <div class="description">
                                        Акция! Купите 2 больших мешка корма для собак Hill's Science Plan или Natures Best со скидкой 15%!
                                    </div><a class="btn btn-primary" href="/actions/hills_akcija_dva_meshka_korma_dlja_sobak_so_skidkoj_15">Купить сейчас</a>
                                </div>
                                <div class="menu-product">
                                    <div class="prod-link-container">
                                        <div class="name">
                                            Hill's Акция 2 Упаковки Корма для Щенков и Котят со Скидкой 15%
                                        </div>
                                    </div>
                                    <div class="image"><img alt="" src="/files/actions/action_32.jpg"></div>
                                    <div class="description">
                                        Акция! Купите 2 меленьких упаковки корма Hill's для щенков или котят с приятной скидкой в 15%!
                                    </div><a class="btn btn-primary" href="/actions/hills_akcija_2_upakovki_korma_dlja_shhenkov_i_kotjat_so_skidkoj_15">Купить сейчас</a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </section><a href="/delivery.htm">
                <div class="delivery_block_mobile"></div></a>
            <div class="call_to_me_mobile default-link func-link"></div>
            <div class="bonus_mobile default-link func-link data-money"></div><a href="/contact.htm">
                <div class="contacts_mobile"></div></a>
        </header>
    </div>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <a href="/">Главная</a><span class="separator">/</span>
                </div>
                <h1 class="prod-link-container">Контакты</h1>
            </div>
            <div class="contact-us-wrapper clearfix">
                <div class="contact-right fl-right">
                    <div id="question-block">
                        <div class="ask-question-text">
                            <div class="heading">
                                Задать вопрос
                            </div>
                            <p>Если у вас остались вопросы — напишите,<br>
                                наш консультант свяжется с вами в течение рабочего дня.</p>
                        </div>
                        <div class="send-form">
                            <form action="/contact" id="form-question" method="post" name="form-question">
                                <div class="form-block">
                                    <div class="form-row">
                                        <div class="full-input">
                                            <div class="label">
                                                Ваше сообщение
                                            </div>
                                            <textarea cols="30" id="" name="message" rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="full-input">
                                            <div class="label">
                                                Имя
                                            </div><input name="name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="full-input">
                                            <div class="label">
                                                Электронная почта для ответа
                                            </div><input name="email" type="text">
                                        </div>
                                    </div>
                                    <div class="button-block">
                                        <button class="btn btn-secondary send-message txt-up" type="submit">Задать вопрос</button>
                                    </div>
                                </div><input name="ajax" type="hidden" value="1">
                            </form>
                        </div>
                    </div>
                    <div id="question-block-sended">
                        Ваше сообщение отправлено!
                    </div>
                </div>
                <div class="contact-info">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 543px; height: 277px;">
                        <tbody>
                        <tr>
                            <td width="40"><img alt="" border="0" height="22" src="/files/images/emailButton.png" width="16"></td>
                            <td>
                                <a href="mailto:info@zooshef.ru">info@zooshef.ru</a>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 20px;"><strong>Москва</strong></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                            <td style="height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="40"><img alt="" border="0" height="16" src="/files/images/con_tel.png" width="16"></td>
                            <td style="height: 25px;">+7 (495)&nbsp;544-85-85</td>
                        </tr>
                        <tr>
                            <td><img alt="" border="0" height="16" src="/files/images/skype.jpg" width="16"></td>
                            <td>Skype: zooshef.ru</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><strong>Время работы</strong>:</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>понедельник-четверг: с 9-00 до 20-00</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>пятница-воскресенье: с 9-00 до 18-00</td>
                        </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 543px;">
                        <tbody>
                        <tr>
                            <td valign="top" width="40">&nbsp;</td>
                            <td>
                                <p>ООО "Вега"<br>
                                    ОГРН: 1167746399479</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table border="0">
                        <tbody>
                        <tr>
                            <td style="width: 40px;">&nbsp;</td>
                            <td><strong>г. Москва</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <p>улица 4-я Марьиной Рощи, 12<br>
                                    <span style="font-size: small;"><span style="color: #ff0000;">Пожалуйста, обратите внимание, у нас нет самовывоза!</span></span></p>
                                <p>
                                    <script async charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=QmSi5iosNwYrlJa8u5pfsxWst6raXRjg&amp;width=650&amp;height=460&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true" type="text/javascript">
                                    </script></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p>&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
</div><!--noindex><div class="social-bookmarks"><a class="vk" title="Вконтакте" target="_blank" href="/go?url=http://vk.com/zooshef" rel="nofollow"><i></i></a><a class="fb" title="Facebook" target="_blank" href="/go?url=https://www.facebook.com/zooshef?ref=hl" rel="nofollow"><i></i></a><a class="ok" title="Одноклассники" target="_blank" href="/go?url=http://odnoklassniki.ru/group/52383908430010" rel="nofollow"><i></i></a><a class="ig" title="Instagramm" target="_blank" href="/go?url=http://instagram.com/zooshef" rel="nofollow"><i></i></a><a class="gp" title="Google Plus" target="_blank" href="/go?url=https://plus.google.com/u/0/b/114067795207431978215/114067795207431978215/about" rel="nofollow"><i></i></a><a class="tw" title="Twitter" target="_blank" href="/go?url=https://twitter.com/zooshef" rel="nofollow"><i></i></a></div></noindex-->
<footer class="main-footer">
    <div class="center-wrapper clearfix">
        <div class="copyright-block fl-right">
            <!--noindex--><!--LiveInternet counter-->
            <script type="text/javascript">
                <!--     document.write("<a href='http://www.liveinternet.ru/click;zooshef' " +       "target=_blank><img src='//counter.yadro.ru/hit;zooshef?t26.6;r" +       escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :       ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?       screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +       ";" + Math.random() +       "' alt='' title='LiveInternet: показано число посетителей за" +       " сегодня' " +       "border='0' width='88' height='15'><\/a>")     //-->
            </script><!--/LiveInternet--><!--/noindex-->
            <div class="socials">
                <a class="vk" href="/go?url=http://vk.com/zooshef" rel="nofollow" target="_blank" title="Вконтакте"><i></i></a><a class="fb" href="/go?url=https://www.facebook.com/zooshef?ref=hl" rel="nofollow" target="_blank" title="Facebook"><i></i></a><a class="ok" href="/go?url=http://odnoklassniki.ru/group/52383908430010" rel="nofollow" target="_blank" title="Одноклассники"><i></i></a><a class="ig" href="/go?url=http://instagram.com/zooshef" rel="nofollow" target="_blank" title="Instagramm"><i></i></a><a class="gp" href="/go?url=https://plus.google.com/u/0/b/114067795207431978215/114067795207431978215/about" rel="nofollow" target="_blank" title="Google Plus"><i></i></a><a class="tw" href="/go?url=https://twitter.com/zooshef" rel="nofollow" target="_blank" title="Twitter"><i></i></a>
            </div>Copyright zooshef.ru 2008—2016
        </div>
        <div class="text-block fl-left">
            <ul>
                <li>
                    <a class="default-link" href="/delivery.htm">Доставка</a>
                </li>
                <li>
                    <a class="default-link" href="/how_to_pay.htm">Оплата</a>
                </li>
                <li>
                    <a class="default-link" href="/sistema_skidok.htm">Система скидок</a>
                </li>
                <li>
                    <a class="default-link" href="/contact.htm">Контакты</a>
                </li>
                <li>
                    <a class="default-link" href="/o_nas.htm">О нас</a>
                </li>
                <li>
                    <a class="default-link" href="/FAQ.htm">FAQ</a>
                </li>
            </ul>
            <div class="phone">
                Телефон офиса:<br>
                +7 (495) 544-85-85
            </div>
            <div class="skype">
                Skype: zooshef.ru
            </div>
            <div class="icq">
                ICQ: 579913575
            </div>
        </div>
        <div class="services-block fl-left">
            <div>
                Доставка по всей России
            </div><img alt="" src="/assets/images/footer-1.png"></div>
        <div class="services-block fl-left">
            <div>
                Любые способы оплаты
            </div><img alt="" src="/assets/images/footer-2.png"></div>
        <div class="services-block fl-left">
            <div>
                Гарантия безопасности
            </div><img alt="" src="/assets/images/footer-3.png"></div>
    </div><span id="totop"><span></span></span>
</footer>
<div id="location_dialog">
    <div class="top-bar">
        <div class="countries"></div><span class="close" onclick="$( '#location_dialog' ).dialog('close');">x</span>
    </div>
    <div class="middle-bar">
        <div class="my-city-bar">
            <div class="name" id="selected-city3">
                Хмельницкий
            </div>
            <div class="descr">
                <span id="selected-country1">Украина</span>, <span id="selected-region1">Хмельницкая область</span>, <span id="selected-city5">Хмельницкий</span>
            </div>
        </div>
        <div class="search-box">
            <input name="city" onkeyup="getCitiesForFirst(this);" placeholder="Выберите свой город или регион" type="text" value="">
            <div class="hint">
                Например: Москва
            </div>
        </div>
        <div class="regions-row clearfix">
            <div class="regions-col-1">
                <div class="regions">
                    <div class="ttl">
                        Регион
                    </div>
                    <div class="inner"></div>
                </div>
            </div>
            <div class="regions-col-2">
                <div class="cities">
                    <div class="ttl">
                        Населенный пункт
                    </div>
                    <div class="inner"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bar"></div>
    <form action="/setlocation" id="reg_location_form" method="post" name="reg_location_form">
        <input id="reg_location_input" name="user_location" type="hidden" value="other"><input id="reg_location_input_id" name="user_location_id" type="hidden" value="0"><input id="reg_location_input_region" name="user_location_region" type="hidden" value=""><input id="reg_location_input_country" name="user_location_country" type="hidden" value=""><input id="reg_location_input_area" name="user_location_area" type="hidden" value="">
    </form>
</div>
<div id="cart_renew_dialog">
    Ваша корзина обновлена!
</div>
<script src="/assets/js/noUiSlider.7.0.10/jquery.nouislider.all.min.js">
</script>
<script src="/assets/js/bxslider.js">
</script>
<script src="/assets/js/infieldLabel.js">
</script>
<script src="/assets/js/jquery.mask.js">
</script>
<script src="/assets/js/jquery.icheck.min.js">
</script>
<script src="/assets/js/jquery.bootstrap-touchspin.min.js">
</script>
<script src="/assets/js/elevatezoom-master/jquery.elevateZoom-3.0.8.min.js">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript">
</script>
<script src="/assets/js/jquery.center.min.js">
</script>
<script src="/assets/js/jquery.nicescroll.min.js">
</script>
<script src="/assets/js/plugins.js?v=1482153292">
</script>
<script src="/assets/js/regions.js?v=1477316980">
    ">
</script>
<script src="/assets/js/jquery.flexslider.min.js">
</script>
<script src="/assets/js/jquery.jcarousel.min.js">
</script>
<script src="/assets/js/scripts.js?v=1480601490">
</script>
<script src="/assets/js/wonderful.min.js?v=1482153292">
</script>
<script src="/assets/js/lightbox.js">
</script>