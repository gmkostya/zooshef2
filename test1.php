<?php
/*
* @copyright  Copyright (C) 2008 - 2009 All rights reserved.
* @license
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$menu = &JSite::getMenu();
$isMainPage = $menu->getActive() == $menu->getDefault();
$menuItem = $menu->getActive();
$flypage = JRequest::getVar('flypage', '');
$virtue = JRequest::getVar('option', '');
$view = JRequest::getVar('view', '');
$user = & JFactory::getUser();
$option = JRequest::getVar('option', null);
$Itemid = JRequest::getVar('Itemid', null);
$formId = JRequest::getVar('formId', null);
$virtuemart_category_id=JRequest::getVar('virtuemart_category_id', null);
$id = JRequest::getVar('id', null);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<?php if ($id == '29') : ?>
    <?php
    $curtitle = "Таблица размеров JP-australia";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>


<?php if ($id == '35') : ?>
    <?php
    $curtitle = "Таблица размеров NP";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>



<?php if ($id == '37') : ?>
    <?php
    $curtitle = "Таблица размеров NeilPryde";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>


<?php if ($id == '39') : ?>
    <?php
    $curtitle = "Таблица размеров T293";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>

<?php if ($id == '40') : ?>
    <?php
    $curtitle = "Таблица размеров RS:X";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>


<?php if ($id == '56') : ?>
    <?php
    $curtitle = "Таблица размеров BIC SUP";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>

<?php if ($id == '58') : ?>
    <?php
    $curtitle = "Таблица размеров Каяки BIC";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>


<?php if ($id == '60') : ?>
    <?php
    $curtitle = "Таблица размеров Jobe";
    $document = JFactory::GetDocument();
    $document->setTitle($curtitle) ;?>
<?php endif; ?>








<head>
    <?php JHTML::_('behavior.modal'); ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript"> jQuery = jQuery.noConflict();</script>


    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ;?>/templates/system/css/system.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ;?>/templates/system/css/general.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/css/template.css" type="text/css" />


    <?php if($this->direction == 'rtl') : ?>
        <link href="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/css/template_rtl.css" rel="stylesheet" type="text/css" />
    <?php endif; ?>
    <link id="JTKLocalCss" href="../vtsport/css/template.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" src="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/js/jquery.placeholder.min.js"></script>
    <script src="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/js/select.js" type="text/javascript"></script>
    <script type="text/javascript">jQuery(document).ready(function(){jQuery('.selectBlock').sSelect();});</script>



    <script>
        jQuery(document).ready(function() {
            jQuery("#mainmenu ul li.active ul").addClass("second-level");
        });
    </script>

    <script>
        jQuery(document).ready(function() {
            if(jQuery(".second-level").length>0) {
                jQuery(".blue_line").addClass("vis");
            }
        });
    </script>

    <script type="text/javascript">

        jQuery(document).ready(function() {
            jQuery("#mainmenu ul li.item-102").hover(function() {
                jQuery(".blue_line").addClass("visible");

            }, function() {
                jQuery(".blue_line").removeClass("visible");

            });


            jQuery("#mainmenu ul li.item-103").hover(function() {
                jQuery(".blue_line").addClass("visible");

            }, function() {
                jQuery(".blue_line").removeClass("visible");

            });


            jQuery("#mainmenu ul li.item-101").hover(function() {
                jQuery(".blue_line").addClass("visible");

            }, function() {
                jQuery(".blue_line").removeClass("visible");

            });



        });



    </script>




    <script>
        var filterByBrand = function(items) {
            var checkboxes = jQuery('#check_sport input.check');
            var checked = checkboxes.filter(':checked');
            if(checked.length === 0) {
                return items;
            }
            var toReturn = jQuery(items).filter(function(i,v) {
                var itemBrands = jQuery(v).find('span.brands a').map(function() {
                    return jQuery(this).text().toLowerCase();
                });
                for(var i = 0;i < checked.length;i++) {
                    var brand = jQuery(checked[i]).val().toLowerCase();
                    if(jQuery.inArray(brand, itemBrands) !== -1) {
                        return true;
                    }
                }
                return false;
            });
            return toReturn;
        };
        var getFilteredCity = function() {
            return jQuery('select[name=group] option:selected').text().toLowerCase();
        };
        var filterByCity = function(items) {
            var toReturn = [];
            var city = getFilteredCity();
            jQuery(items).find('span.address').each(function(i,v) {
                if(jQuery(v).text().toLowerCase().indexOf(city) !== -1) {
                    toReturn.push(jQuery(v).closest('div.item'));
                }
            });
            return toReturn;
        };
        var filter = function() {
            var items = jQuery('#column_center div.item');
            items.hide();

            var checkboxes = jQuery('#check_sport input.check');
            var checked = checkboxes.filter(':checked');
            var filteredItems = items;
            if(checked.length !== 0) {
                filteredItems = filterByBrand(items);
            }
            if('город' !== getFilteredCity()) {
                filteredItems = filterByCity(filteredItems);
            }
            jQuery(filteredItems).each(function(){
                jQuery(this).show();
            });
        };

        jQuery(document).ready(function() {
            jQuery('#check_sport input.check').change(filter);
            jQuery('select[name=group]').change(filter);
        });
    </script>



    <?php if (($id=='29') || ($id=='37') || ($id=='39') || ($id=='40') || ($id=='56') || ($id=='58')) :?>
        <script>
            jQuery(document).ready(function(){

                jQuery(".tab_category").each(function(){
                    var id = jQuery(this).find("h3").attr("id");
                    console.log(id);jQuery(this).next().find("input").off();
                    jQuery(this).next().find("input").click(function(e){
                        e.preventDefault();
                        window.location.href = "/index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id="+id.substr(3);
                    });
                });

            });
        </script>
    <?php endif; ?>


    <!------------------------------------------------------------------------------------------------------------------->

    <?php if ($id=='60') :?>
        <script>
            jQuery(document).ready(function(){

                jQuery(".tab_category").each(function(){
                    var id = jQuery(this).find("h3").attr("id");
                    console.log(id);jQuery(this).next().find("input").off();
                    jQuery(this).next().find("input").click(function(e){
                        e.preventDefault();
                        window.location.href = "/index.php?option=com_virtuemart&view=category&virtuemart_category_id="+id.substr(3);
                    });
                });

            });
        </script>
    <?php endif; ?>


    <!---------------------------------------------------------------->
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.razmeri_np tr:odd').addClass('odd');
            jQuery('.razmeri_np tr:even').addClass('even');
        });
    </script>
    <!---------------------------------------------------------------->



    <?php if(($Itemid == '187')) :?>
        <script>
            var filterByBrand = function(items) {
                var checkboxes = jQuery('#check_class input.check');
                var checked = checkboxes.filter(':checked');
                if(checked.length === 0) {
                    return items;
                }
                var toReturn = jQuery(items).filter(function(i,v) {
                    var itemBrands = jQuery(v).find('span.y_classes a').map(function() {
                        return jQuery(this).text().toLowerCase();
                    });
                    for(var i = 0;i < checked.length;i++) {
                        var brand = jQuery(checked[i]).val().toLowerCase();
                        if(jQuery.inArray(brand, itemBrands) !== -1) {
                            return true;
                        }
                    }
                    return false;
                });
                return toReturn;
            };


            var filter = function() {
                var items = jQuery('#column_center div.item');
                items.hide();

                var checkboxes = jQuery('#check_class input.check');
                var checked = checkboxes.filter(':checked');
                var filteredItems = items;
                if(checked.length !== 0) {
                    filteredItems = filterByBrand(items);
                }
                jQuery(filteredItems).each(function(){
                    jQuery(this).show();
                });
            };

            jQuery(document).ready(function() {
                jQuery('#check_class input.check').change(filter);

            });
        </script>
    <?php endif; ?>







    <!-- fancybox на страницах материалов яхтинга -->
    <?php if((($view=='article') and ($id=="45")) or (($view=='article') and ($id=="46")) or (($view=='article') and ($id=="47")) or (($view=='article') and ($id=="48")) or (($view=='article') and ($id=="49")) or (($view=='article') and ($id=="50")) or (($view=='article') and ($id=="51")) or (($view=='article') and ($id=="52"))) :?>
        <link rel="stylesheet" href="/components/com_virtuemart/assets/css/jquery.fancybox-1.3.4.css" type="text/css" />
        <script src="/components/com_virtuemart/assets/js/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>

        <script type="text/javascript">


            jQuery(document).ready(function() {
                jQuery("a[rel=vm-additional-images]").fancybox({
                    "titlePosition"   : "inside",
                    "transitionIn"  :  "elastic",
                    "transitionOut"  :  "elastic"
                });
                jQuery(".additional-images .product-image").click(function() {
                    jQuery(".main-image img").attr("src",this.src );
                    jQuery(".main-image img").attr("alt",this.alt );
                    jQuery(".main-image a").attr("href",this.src );
                    jQuery(".main-image a").attr("title",this.alt );
                });
            });

        </script>


    <?php endif ;?>

    <!---------------------------------------------->











</head>

<body>





<!-- Yandex.Metrika informer -->
<div style="position:absolute; left:-9999px;">
    <a href="http://metrika.yandex.ru/stat/?id=22985173&amp;from=informer"
       target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/22985173/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                           style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:22985173,lang:'ru'});return false}catch(e){}"/></a>
    <!-- /Yandex.Metrika informer -->
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22985173 = new Ya.Metrika({id:22985173,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/22985173" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->




<div id="top">

    <div class="wrapper">

        <div id="t1">
            <a href="/index.php"><img src="/images/logo.png" /></a>
        </div><!-- t1 -->

        <div id="t2">
            <jdoc:include type="modules" name="contact" style="xhtml" />
        </div><!-- t2 -->

        <div id="t3">

            <div id="topmenu">
                <jdoc:include type="modules" name="topmenu" style="xhtml" />
            </div><!-- topmenu -->

            <div id="search">
                <jdoc:include type="modules" name="search" style="xhtml" />
            </div><!-- search -->

        </div><!-- t3 -->

    </div><!-- wrapper -->

</div><!-- top -->
<div style="clear:both"></div>


<div id="middle">


    <div id="menu_block">

        <div class="wrapper menu">

            <div id="mainmenu">

                <jdoc:include type="modules" name="mainmenu" style="xhtml" />
            </div><!-- mainmenu -->



        </div><!-- wrapper -->


        <div class="blue_line" style="display:none"></div>


    </div><!-- end menu_block-->


    <div id="content">
        <div class="wrapper">

            <div id="column_left"<?php if(($view == 'article') and ($id == 37)) :?> style="display:none;"<?php  endif; ?>>

                <jdoc:include type="modules" name="smart_search" style="xhtml" />



                <!-- скрываю фильтр подбора для страниц яхтинга -->
                <?php if(($Itemid == '187') || ($id =='45') || ($id =='46') || ($id =='47') || ($id =='48') || ($id =='49') || ($id =='49') || ($id =='50') || ($id =='51') || ($id =='52')) :?>
                <div style="display:none">
                    <?php endif; ?>

                    <jdoc:include type="modules" name="filter" style="xhtml" />


                    <?php if(($Itemid == '187') || ($id =='45') || ($id =='46') || ($id =='47') || ($id =='48') || ($id =='49') || ($id =='49') || ($id =='50') || ($id =='51') || ($id =='52')) :?>
                </div>
            <?php endif; ?>




                <jdoc:include type="modules" name="buyfilter" style="xhtml" />


                <?php if(($id== '44') and ($Itemid == '187')) :?>
                    <jdoc:include type="modules" name="yahtingfilter" style="xhtml" />
                <?php endif; ?>




                <!--                <div id="brands">
                                  <jdoc:include type="modules" name="slider_manufacturers" style="xhtml" />
                               </div><!-- brands -->


                <!--<div id="brands">
                    <div class="moduletable_slider_manufacturers">

                        <div class="manuf_container">

                            <a href="#" class="down"></a>

                            <div class="vmgroup_slider_manufacturers">




                                <ul class="vmmanufacturer_slider_manufacturers">
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/bic-sport.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/logotip-brenda_150x300_150x300.png" height="80" alt="logotip-brenda_150x300"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/jp-australia.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/logo_jp-australia_header_150x300_150x300.jpg" height="80" alt="logo_jp-australia_header_150x300"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/mfc.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/logo_150x300_150x300.png" height="80" alt="logo_150x300"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/neilpryde.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/np_logo_black_150x300.jpg" height="80" alt="np_logo_black"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/neilpryde-sailing.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/logotip_150x300_150x300.jpg" height="80" alt="logotip_150x300"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/np.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/np_logo_outline_black_150x300.jpg" height="80" alt="np_logo_outline_black"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/unifiber.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/unifiber-logo-white-background_150x300_150x300.jpg" height="80" alt="unifiber-logo-white-background_150x300"  />        </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>

                                            <a href="/katalog-brendov/zhik.html">
                                                <img src="/images/stories/virtuemart/manufacturer/resized/273_150x300.jpg" height="80" alt="273"  />        </a>
                                        </div>
                                    </li>
                                </ul>



                            </div>
                            <a href="#" class="up"></a>
                        </div>


                    </div>

                </div>--><!-- brands -->






            </div><!-- column_left -->


            <?php if ($this->countModules('special_offer')!='0') : ?>
                <div id="column_right"<?php if(($view == 'article') and ($id == 29)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 35)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 37)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 39)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 40)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 56)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 58)) :?> style="display:none;"<?php  endif; ?><?php if(($view == 'article') and ($id == 60)) :?> style="display:none;"<?php  endif; ?>>
                    <jdoc:include type="modules" name="special_offer" style="xhtml" />
                </div><!-- column_right -->

            <?php  endif; ?>







            <!-- Главная страница f <?php echo $this->countModules('special_offer') ?> -->
            <?php if((JMenuSite::getInstance('site')->getActive()->home) and ($view=='featured')) :?>



                <div id="column_center" >

                    <div id="slidershow">
                        <jdoc:include type="modules" name="slider" style="xhtml" />
                    </div><!--slidershow-->

                    <div id="news">

                        <jdoc:include type="modules" name="home_news" style="xhtml" />


                    </div><!-- news -->

                    <!------------------------------------------>

                    <div class="content">

                        <jdoc:include type="component" />

                    </div><!-- content -->



                </div><!-- column_center -->

            <?php  endif; ?>



            <!-- субстраница----------------------------------------------->
            <?php if((!JMenuSite::getInstance('site')->getActive()->home) and ($view<>'featured')) :?>


                <div id="column_center"<?php if(($view == 'article') and ($id == 29)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 35)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 37)) :?> style="padding:0px; width:940px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 39)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 40)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 56)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 58)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?><?php if(($view == 'article') and ($id == 60)) :?> style="padding-right:0px; width:730px;"<?php  endif; ?> <?php if ($this->countModules('special_offer')=='0') : ?> class="no-right-column" <?php endif; ?>>


                    <?php if (($option=='com_virtuemart') and ($view=='category')) :?>
                        <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
                    <?php endif; ?>


                    <?php if (($option=='com_virtuemart') and ($view=='productdetails')) :?>
                        <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
                    <?php endif; ?>











                    <?php if (($option=="com_virtuemart") and ($view=="virtuemart")) :?>
                        <div id="slidershow">
                            <jdoc:include type="modules" name="slider" style="xhtml" />
                        </div><!--slidershow-->
                    <?php endif; ?>


                    <jdoc:include type="component" />


                    <?php if (($option=="com_virtuemart") and ($view=="category") and ($Itemid==104)) :?>
                        <div id="team_news">
                            <jdoc:include type="modules" name="team_news" style="xhtml" />
                        </div>
                        <!-- end -->
                    <?php endif; ?>


                </div><!-- column_center -->

            <?php  endif; ?>





        </div><!-- wrapper -->
    </div><!-- content -->


</div><!-- middle -->


<div style="clear:both"></div>


<div id="bottom">

    <div id="bottom1">

        <div class="wrapper">

            <div id="footermenu">
                <jdoc:include type="modules" name="fmenu" style="xhtml" />
            </div><!-- footermenu -->

            <div id="footsoc1">
                <jdoc:include type="modules" name="soc1" style="xhtml" />
            </div><!-- footcoc1 -->

            <div id="footsoc2">

                <jdoc:include type="modules" name="soc2" style="xhtml" />
            </div><!-- footcoc2 -->

            <div id="footinfo">
                <jdoc:include type="modules" name="contact" style="xhtml" />
                <span class="www">Создание и поддержка - <a target="_blank" href="http://web-systems.com.ua">Web-systems</a></span>
            </div><!-- footinfo -->

        </div><!-- wrapper -->

    </div><!-- bottom1 -->

    <div id="bottom2">
        <jdoc:include type="modules" name="copy" style="xhtml" />
    </div><!-- bottom2 -->

</div><!-- bottom -->

<script src="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/js/jcarousellite_1.0.1.min.js" type="text/javascript"></script>
<script type='text/javascript' >
    jQuery(document).ready(function() {
        jQuery(function() {
            jQuery(".vmgroup_slider_manufacturers").jCarouselLite({
                btnNext: ".manuf_container .down",
                btnPrev: ".manuf_container .up",
                vertical: true,
                visible: 3,
                auto: 5000,
                speed: 500,
                circular: true
            });
        });
    });
</script>



<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'ukuXgyqmcv';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


</body>
</html>