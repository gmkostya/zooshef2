<?php
class ModelToolImportCustomer extends Model {
	public function backup($product_category) {

	   // var_dump($product_category);

        /**проход по всем закзаам  старой базы*/

        $sql = "SELECT  o.user_id AS customer_id, o.name, o.phone AS telephone,  o.email,  op.product_id, o.name, ptc.category_id, op.product_name  
FROM orders o 
LEFT JOIN orders_products op ON (op.order_id = o.order_id) 
LEFT JOIN oc_product_to_category ptc ON (ptc.product_id = op.product_id) 
WHERE ptc.category_id IN (".$product_category.") GROUP BY o.user_id";




        $query = $this->db->query($sql);
        $result_array = array();

        foreach ($query->rows AS $result) {
            $result_array[$result['customer_id']] = array(
                'name' => $result['name'],
                'telephone' => str_replace(";","",$result['telephone']),
                'email' => $result['email'],
                'product_id' => $result['product_id'],
                'category_id' => $result['category_id'],
                'product_name' => $result['product_name'],
            );

        }





        /**проход по всем закзаам  новой базы*/

        $sql = "SELECT  o.customer_id, o.firstname, o.lastname,  o.telephone ,  o.email,  op.product_id,  ptc.category_id, op.name AS name
FROM oc_order o 
LEFT JOIN oc_order_product op ON (op.order_id = o.order_id) 
LEFT JOIN oc_product_to_category ptc ON (ptc.product_id = op.product_id) 
WHERE ptc.category_id IN (".$product_category.") GROUP BY o.customer_id";


        $query = $this->db->query($sql);

        foreach ($query->rows AS $result) {
            $result_array[$result['user_id']] = array(
                'name' => $result['firstname']. ' (' .$result['lastname'].')',
                'telephone' => str_replace(";","",$result['telephone']),
                'email' => $result['email'],
                'product_id' => $result['product_id'],
                'category_id' => $result['category_id'],
                'product_name' => $result['product_name'],
            );

        }


        $output ='';
        foreach ($result_array AS $key => $value) {

          $cat_arr = array("1","294","67","68","71","483","295","120","297","296","72","79","190","191","189","302","300","192","73","106","303","217","107","74","482","481","98","261","220","471","244","100","243","247","260","307","310","308","312","304","263","311","102","168","167","484","169","305","170","306","81","75","480","99","320","193","321","315","319","317","313","323","318","316","314","322","181","333","331","329","332","330","171","327","324","326","325","328","182","334","337","335","336","183","341","338","339","340","188","346","348","345","343","342","344","187","353","354","355","108","351","349","352","350","184","361","360","359","362","356","357","358","85","369","470","368","367","109","110","111","112","235","242","122","236","372","370","371","222","237","238","239","472","474","475","473","248","119","249","250","256","123","128","363","366","364","365","124","127","125","126","273","386","374","375","378","379","380","381","382","383","385","384","387","377","376","176","274");

            in_array($value['category_id'], $cat_arr)? $cat = '1' : $cat = '2';


          //  $output .= $key.';';
            $output .= str_replace(';','',$value['name']).';';
            $output .= str_replace(';','',$value['telephone']).';';
            $output .= str_replace(';','',$value['email']).';';
           // $output .= str_replace(';','',$value['product_id']).';';
            $output .= str_replace(';','',$cat).';';
           // $output .= str_replace(';','',$value['product_name'])."\r\n";
            $output .= "\r\n";
        }

        $output = @iconv( "utf-8", "windows-1251//ignore", $output );

        $file_link = DIR_DOWNLOAD ."customer_export_" . date('Y-m-d_H-i-s', time()) . ".csv";
        file_put_contents($file_link, $output, LOCK_EX);
      //  echo($output);

        return $output;


	}
    public function getCustomers($data = array()) {
        $sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "c.email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "c.newsletter = '" . (int)$data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "c.customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'c.email',
            'customer_group',
            'c.status',
            'c.approved',
            'c.ip',
            'c.date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
}
