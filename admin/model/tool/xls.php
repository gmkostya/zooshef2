<?php
class ModelToolXls extends Model
{
    public function restore($sql)
    {
        foreach (explode(";\n", $sql) as $sql) {
            $sql = trim($sql);

            if ($sql) {
                $this->db->query($sql);
            }
        }

        $this->cache->delete('*');
    }

    public function getTables()
    {
        $table_data = array();

        $query = $this->db->query("SHOW TABLES FROM `" . DB_DATABASE . "`");

        foreach ($query->rows as $result) {
            if (utf8_substr($result['Tables_in_' . DB_DATABASE], 0, strlen(DB_PREFIX)) == DB_PREFIX) {
                if (isset($result['Tables_in_' . DB_DATABASE])) {
                    $table_data[] = $result['Tables_in_' . DB_DATABASE];
                }
            }
        }

        return $table_data;
    }

    public function export($categories)
    {

        $count_info_fields = '60';
        $start_data = $this->config->get('config_data_start_export');
        $end_data = $this->config->get('config_data_end_export');

        $difference = intval(abs(
            strtotime($start_data) - strtotime($end_data)
        ));
// Количество дней
        $count_data = $difference / (3600 * 24);

        $dates = array();
        $date = new DateTime($start_data);
        for ($d = 1; $d <= $count_data+1; $d++) {
            $dates[] = $date->format('Y-m-d'); // 2014-01-04

            $date->modify('+1 day');
        }
//var_dump($dates);


        $xls = new PHPExcel();
// Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
// Получаем активный лист
        $sheet = $xls->getActiveSheet();
// Подписываем лист
        $sheet->setTitle('UBC ЕКСПОРТ');

        $row = 2; // ряд
        $col = 0; //колонка

        $total_j = $count_info_fields + $count_data;


        $sheet->setCellValueByColumnAndRow($col++, '1', 'ЖБК');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'ID');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'об’єкт');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Під`їзд');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Тип приміщення (Список)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Тип планування');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Поверх');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'к-ть кімнат');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', '№ приміщення');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Площа, м2 (проектна');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Площа, м2 (фактична)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Хто продав (ЦП/партнер)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Менеджер (ПІБ відповідального)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Особливі (хто взяв)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Особливі (коментар)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Облік (БУ/УО, УО, БО)');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Закрито для редагування');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'План/факт');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Тип договору (ДПУ, ФК та ДПУ)');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Дата первинного продажу');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Дата договору');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Курс USD, в договорі');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Договірна ціна, UAH/м2');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Договірна ціна, USD/м2');
        $sheet->setCellValueByColumnAndRow($col++, '1', '№ дог');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'ПІБ кінцевого власника');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Контакти покупця');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Коментар менеджера');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Сума договору (всього), UAH');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Оплат (всього), UAH');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Борг (всього), UAH');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Сума договору (осн), UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Оплачено (по осн), UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Борг (по осн), UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Сума договору (благ), UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Оплачено (по благ), UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Борг (по благ), UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Сума договору (всього), USD');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Оплат (всього), USD');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'Борг (всього), USD');
        $sheet->setCellValueByColumnAndRow($col++, '1', 'сума договору (осн), USD');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', 'Оплачено (по осн), USD');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' Борг (по осн), USD');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' Сума договору (благ), USD');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' Оплачено (по благ), USD');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' Борг (по благ), USD');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' РОЗРАХУНОК (Доплата/повернення)');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' Фактично виручені кошти, UAH');
        $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);

        $sheet->setCellValueByColumnAndRow($col++, '1', ' Фактично виручені кошти, USD');
     ///   $sheet->getStyleByColumnAndRow($col, '1')->getAlignment()->setTextRotation(90);


        //стилі для заголовка
        $arHeadStyle = array(
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => 'ffffff'),
                'size' => 10,
              //  'name' => 'Verdana'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                //  'rotation' => 0,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            ),
            // FILL
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '009966')
            )
        );

        //стилі для заголовка сині
        $arHeadStyle2 = array(
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => 'ffffff'),
                'size' => 10,
              //  'name' => 'Verdana'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                //  'rotation' => 0,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            ),
            // FILL
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '0066CC')
            )
        );



        //стилі для заголовка дати
        $arHeadStyle3 = array(
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => '000000'),
                'size' => 10,
                //'name' => 'Verdana'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                //  'rotation' => 0,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            ),
            // FILL
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF00')
            )
        );

        $sheet->getRowDimension(1)->setRowHeight(80);
        for ($st = 0; $st <= 18; $st++) {
            $sheet->getStyleByColumnAndRow($st, '1')->applyFromArray($arHeadStyle);
        }
        $sheet->getRowDimension(1)->setRowHeight(80);
        for ($st = 18; $st <= 48; $st++) {
            $sheet->getStyleByColumnAndRow($st, '1')->applyFromArray($arHeadStyle2);
        }


        for ($head_date = 1; $head_date <= $count_data+1; $head_date++) {
            $sheet->setCellValueByColumnAndRow(($col + $head_date-1), '1', $dates[$head_date - 1]);
            $sheet->getStyleByColumnAndRow(($col + $head_date-1), '1')->applyFromArray($arHeadStyle3);
        }

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

$category_list = array();


        foreach ($categories as $key=>$category) {
            $category_list[] = $category;
            $categories2 = $this->model_catalog_category->getCategoriesByParentId($category);
            if ($categories2) {
                foreach ($categories2 as $category2) {
                    $category_list[] = $category2['category_id'];
                    $categories3 = $this->model_catalog_category->getCategoriesByParentId($category2['category_id']);
                    if ($categories3) {
                        foreach ($categories3 as $category3) {
                            $category_list[] = $category3['category_id'];
                            $categories4 = $this->model_catalog_category->getCategoriesByParentId($category3['category_id']);
                            foreach ($categories4 as $category4) {
                                $category_list[] = $category4['category_id'];

                            }
                        }
                    }

                }
            }

        }

        //foreach ($categories as $key=>$category) {
        foreach (array_unique($category_list) as $category) {


            $categoty_data = $this->model_catalog_category->getCategory($category);

            $filter_data = array(
                'filter_category' => $categoty_data['category_id'],
                'filter_sub_category' => '0',
            );

             $products= $this->model_catalog_product->getProducts($filter_data);


            foreach ($products as $product) {

              //  echo $product['product_id'].'/';

                $col= 0;
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['gbk']);
                //$sheet->setCellValueByColumnAndRow($col++, $row, $product['id']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['model']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['obekt']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['pidyizd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['typ_prymischenya']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['typ_planu']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['poverh']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['qty_kimnat']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['nomer']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['ploscha_proektna']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['ploscha_fakt']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['prodavec']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['manager']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['osoblyvi_pokupec']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['osoblyvi_komentar']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oblik']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['edit']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['plan_fakt']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['typ_dogovoru']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['data_perv_prodgu']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['data_dogovoru']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['kurs']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['cina_dogovirna_uah']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['cina_dogovirna_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['number_dogovoru']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['pib']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['contacts']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['kommentar']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['suma_dogovoru']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oplat_vsego']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['borg']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['suma_dogovoru_osn']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oplata_osn']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['borg_osn']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['suma_dogovoru_bl']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oplata_bl']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['borg_bl']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['suma_dogovoru_vsego_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oplata_vsego_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['borg_vsego_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['suma_dogovoru_osn_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oplata_osn_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['borg_osn_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['suma_dogovoru_bl_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['oplata_bl_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['borg_bl_usd']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['rozrahunok']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['rozrahunok_fakt']);
                $sheet->setCellValueByColumnAndRow($col++, $row, $product['rozrahunok_fakt_usd']);

                $product_discounts = $this->model_catalog_product->getProductDiscounts($product['product_id']);
                foreach ($product_discounts as $discount) {
                  $date_col_position=array_search($discount['date_start'], $dates);

                   // $sheet->setCellValueByColumnAndRow(49+$date_col_position, $row, $discount['date_start']);
                    $sheet->setCellValueByColumnAndRow(49+$date_col_position, $row, $discount['price']);

                   // var_dump($discount);
                }

                $row++;
            }

            //var_dump($product_data);

            /*for ($k = 1; $k < 10; $k++) {
                $i++;
                $sheet->setCellValueByColumnAndRow('0', ($j + $k), $k);
                $sheet->getStyleByColumnAndRow('0', ($j + $k))->getFill()->setFillType(
                    PHPExcel_Style_Fill::FILL_SOLID);
                $sheet->getStyleByColumnAndRow('0', ($j + $k))->getFill()->getStartColor()->setRGB('FFFF00');
            }*/


        }


        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ubc_" . date("Y-m-d_H-i-s", time()) . ".xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($xls);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

    }
}
