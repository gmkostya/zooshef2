<?php
class ControllerToolXls extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('tool/xls');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/xls');


		$data['heading_title'] = $this->language->get('heading_title');
        $data['text_select_all'] = $this->language->get('text_select_all');
        $data['text_unselect_all'] = $this->language->get('text_unselect_all');


		$data['entry_export'] = $this->language->get('entry_export');

		$data['button_export'] = $this->language->get('button_export');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/xls', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['backup'] = $this->url->link('tool/xls/export', 'token=' . $this->session->data['token'], 'SSL');


        // Categories
        $this->load->model('catalog/category');
        $filter_data = array(
            'sort'        => 'name',
            'order'       => 'ASC'
        );

        $data['categories'] = $this->model_catalog_category->getCategories($filter_data);





        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/xls.tpl', $data));
	}

	public function export() {
		$this->load->language('tool/xls');

		if (!isset($this->request->post['product_category'])) {
			$this->session->data['error'] = $this->language->get('error_xls');

			$this->response->redirect($this->url->link('tool/xls', 'token=' . $this->session->data['token'], 'SSL'));
		} elseif ($this->user->hasPermission('modify', 'tool/xls')) {
         /*   $this->response->addheader('Expires: 0');
            $this->response->addheader ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
            $this->response->addheader ( "Cache-Control: no-cache, must-revalidate" );
            $this->response->addheader ( "Pragma: no-cache" );
            $this->response->addheader ( "Content-type: application/vnd.ms-excel" );
            $this->response->addheader('Content-Disposition: attachment; filename=' . DB_DATABASE . '_' . date('Y-m-d_H-i-s', time()) . '_xls.sql');*/
           // $this->response->addheader('Content-Transfer-Encoding: binary');




//var_dump($this->request->post['product_category']);
			$this->load->model('tool/xls');

			$this->response->setOutput($this->model_tool_xls->export($this->request->post['product_category']));
		} else {
			$this->session->data['error'] = $this->language->get('error_permission');

			$this->response->redirect($this->url->link('tool/xls', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
}
