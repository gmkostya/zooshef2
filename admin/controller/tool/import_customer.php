<?php
class ControllerToolImportCustomer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('tool/import_customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/import_customer');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$data['entry_export'] = $this->language->get('entry_export');
		$data['entry_import'] = $this->language->get('entry_import');

		$data['button_export'] = $this->language->get('button_export');
		$data['button_import'] = $this->language->get('button_import');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/import_customer', 'token=' . $this->session->data['token'], 'SSL')
		);


		$data['backup'] = $this->url->link('tool/import_customer/backup', 'token=' . $this->session->data['token'], 'SSL');

// Categories
        $this->load->model('catalog/category');
        $filter_data = array(
            'sort'        => 'name',
            'order'       => 'ASC'
        );

        $data['categories'] = $this->model_catalog_category->getCategories($filter_data);


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/import_customer.tpl', $data));
	}

	public function backup() {
		$this->load->language('tool/import_customer');

		if (!isset($this->request->post['product_category'])) {
			$this->session->data['error'] = 'Не уазаны категории';

			$this->response->redirect($this->url->link('tool/import_customer', 'token=' . $this->session->data['token'], 'SSL'));
		} elseif ($this->user->hasPermission('modify', 'tool/import_customer')) {
			$this->response->addheader('Pragma: public');
			$this->response->addheader('Expires: 0');
			$this->response->addheader('Content-Description: File Transfer');
			$this->response->addheader('Content-Type: application/octet-stream');
			$this->response->addheader('Content-Disposition: attachment; filename=customer_' . date('Y-m-d_H-i-s', time()) . '_export.csv');
			$this->response->addheader('Content-Transfer-Encoding: binary');
			$this->load->model('tool/import_customer');

            //$product_category =  $this->request->post['product_category'];
            $this->load->model('catalog/category');
            foreach ($this->request->post['product_category'] as $key=>$category) {


                    $category_list[] = $category;
                    $categories2 = $this->model_catalog_category->getCategoriesByParentId($category);
                    if ($categories2) {
                        foreach ($categories2 as $category2) {
                            $category_list[] = $category2['category_id'];
                            $categories3 = $this->model_catalog_category->getCategoriesByParentId($category2['category_id']);
                            if ($categories3) {
                                foreach ($categories3 as $category3) {
                                    $category_list[] = $category3['category_id'];
                                    $categories4 = $this->model_catalog_category->getCategoriesByParentId($category3['category_id']);
                                    foreach ($categories4 as $category4) {
                                        $category_list[] = $category4['category_id'];

                                    }
                                }
                            }

                        }
                    }

            }


            foreach (array_unique($category_list) as $key => $category) {

                if ($key == '0') {
                    $product_category = $category;
                } else {
                    $product_category .= ',' . $category;
                }
            }


            //var_dump($product_category);

			$this->response->setOutput($this->model_tool_import_customer->backup($product_category));
		} else {
			$this->session->data['error'] = $this->language->get('error_permission');

			$this->response->redirect($this->url->link('tool/import_customer', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
}
