<?php
class ControllerToolImport extends Controller {
	private $error = array();
	private $category_id = 0;
	private $path = array();

	public function index() {
		$this->language->load('tool/import');

		echo $this->document->setTitle($this->language->get('heading_title'));

		//$this->load->model('tool/import');

		//$this->getList();
       /*
        update brand
        admin/index.php?route=tool/import/importbrand&target=brand&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
        импорт категорий (после очистить кеш)
       admin/index.php?route=tool/import/importcat&target=cat&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
        update product
       /admin/index.php?route=tool/import/importcat2&target=prod&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
       товары для главных категорий
       /admin/index.php?route=tool/import/setparentcategory&target=parent&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
       проверить метод . работает локально
       /admin/index.php?route=tool/import/importcat2category&target=prod_upd&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
        товары для категории ALL (ghjdthbnm ID для категории ALL 487)
       /admin/index.php?route=tool/import/setallcategory&target=all&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om

       К этому товару подходят
       /admin/index.php?route=tool/import/importrelated&target=rel&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om

       варианты
       /admin/index.php?route=tool/import/importvariant&target=var&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
       атрибути
       /admin/index.php?route=tool/import/importattribute&target=attr&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
       значення атрибутыв
       admin/index.php?route=tool/import/importattributevaluesproduct&target=attrp&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
        Коментарии  товаров
       /admin/index.php?route=tool/import/imporcomments&target=comments&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
        Фото товаров
       /admin/index.php?route=tool/import/importimages&target=images&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
Статьи
       admin/index.php?route=tool/import/importarticle&target=article&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
       Акции
       /admin/index.php?route=tool/import/importactions&target=actions&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
       Новости
       admin/index.php?route=tool/import/importnews&target=importnews&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW

       Акции фильтр
       /admin/index.php?route=tool/import/importactionsfilter&target=actionsfilter&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
       костумерс
       /admin/index.php?route=tool/import/importcustomer&target=customer&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW
       банера
       /admin/index.php?route=tool/import/importbanners&target=banners&token=RXjljvSG6tScSIHhmzp4QTDFSSIA5vkW


        update all_sku for search
        admin/index.php?route=tool/import/addallsku&target=addallsku&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om
        create manufacturer review alias
        admin/index.php?route=tool/import/createmanufacreviewalias&target=createmanufacreviewalias&token=jVXBJhwhPQos8ZnIfZPnaLnHRS6tN4Om

       */


	}

    public function createmanufacreviewalias() {

        $this->load->model('tool/import');

        if (isset($this->request->get['target']) && $this->request->get['target']=='createmanufacreviewalias') {


            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE '%manufacreview_id%'");

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import->getManufacturerAlias($start);
                $start= $start+100;
                set_time_limit(20);

                if(empty($old_data))break;

                //  echo '<pre>';
                // var_dump($old_data);


                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    $put_data[] = array(
                        'manufacturer_id' => $result['manufacturer_id'],
                        'query' => 'manufacreview_id='.$result['manufacturer_id'],
                        'keyword' => $result['old_url'].'_reviews'
                    );
                }

                foreach ($put_data as $result) {
                    echo $result['manufacturer_id'].'/';
                    $this->model_tool_import->addManufacturerAlias($result);
                }

            }

        } else {
            echo 'Не указан парметр';
        }
   }
    public function addallsku() {

        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target']=='addallsku') {


            $this->db->query("UPDATE  oc_product SET sku_all=''");

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import2->getOptionSku($start);
                $start= $start+100;
                set_time_limit(20);

                if(empty($old_data))break;

                //  echo '<pre>';
                // var_dump($old_data);


                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    $put_data[] = array(
                        'product_id' => $result['product_id'],
                        'sku' => $result['sku']
                    );
                }

                foreach ($put_data as $result) {
                    echo $result['product_id'].'/';
                    $this->model_tool_import2->addProductSku($result);
                }

            }

        } else {
            echo 'Не указан парметр';
        }
   }


	public function importbrand() {
		$this->language->load('tool/import');

		$this->load->model('tool/import');

		if (isset($this->request->get['target']) && $this->request->get['target']=='brand') {


            $this->db->query("TRUNCATE `oc_manufacturer`");
            $this->db->query("TRUNCATE `oc_manufacturer_description`");
            $this->db->query("TRUNCATE `oc_manufacturer_to_store`");
            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE '%manufacturer_id%'");


            /*данные старой базы*/
           $old_data = $this->model_tool_import->getOldBrands();

            /*array(10) {
                'manufacturer_description'=>array (
    [1]=>  array(
      'name'=>
      'description'=>
      'meta_title'=>
      'meta_h1'=>
      'meta_description'=>
      'meta_keyword'=>
    )
  ),
  'manufacturer_store'=>
  array(1) (
    [0]=> '0'
  )
  'keyword'=> ''
  'image'=>
  'sort_order'=>
            }*/


           /*данные для добавления*/
           $put_data = array();
            foreach ($old_data as $result) {
                $put_data[] = array(
                    'manufacturer_id'=> $result['brand_id'],
                    'name'=> $result['name'],
                    'manufacturer_description'=> array (
                         '1'=>  array(
                              'name'=>$result['name'],
                              'description'=>$result['description'],
                              'meta_title'=> $result['meta_title'],
                              'meta_h1'=>$result['name'],
                              'meta_description'=>$result['meta_description'],
                              'meta_keyword'=>$result['meta_keywords'],
                         )
                    ),
                    'manufacturer_store'=>array (
                                '0'=> '0'
                  ),
                  'keyword'=> $result['url'],
                  'image'=> 'catalog/brands/'.$result['image'],
                  'sort_order'=> '0',

                  'anonce'=> $result['anonce'],
                  'old_url'=> $result['url'],
                  'seo_description'=> $result['seo_description'],
                  'meta_title_review'=> $result['meta_title_review'],
                  'meta_keywords_review'=> $result['meta_keywords_review'],
                  'meta_description_review'=> $result['meta_description_review'],
                  'discount_available'=> $result['discount_available'],
                  'part'=> $result['part'],
                  'in_home'=> $result['in_home']


                );


            }

            foreach ($put_data as $result) {
                $this->model_tool_import->addManufacturer($result);

            }


          echo 'ololo brands';
		} else {
            echo 'Не указан парметр';
        }


	}
	public function importcat() {
		$this->language->load('tool/import');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/import');

		if (isset($this->request->get['target']) && $this->request->get['target']='cat') {

            $this->db->query("TRUNCATE `oc_category`");
            $this->db->query("TRUNCATE `oc_category_description`");
            $this->db->query("TRUNCATE `oc_category_filter`");
            $this->db->query("TRUNCATE `oc_category_path`");
            $this->db->query("TRUNCATE `oc_category_to_layout`");
            $this->db->query("TRUNCATE `oc_category_to_store`");
            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE '%category_id%'");



            /*данные старой базы*/
           $old_data = $this->model_tool_import->getOldCategories();


            /*array(10) {
                ["category_description"]=> array(1) {
                    [1]=> array(6) {
                        ["name"]=> string(16) "іваіваів"
                        ["description"]=> string(41) "<p>аваіва<br></p>"
                        ["meta_title"]=> string(8) "івав"
                        ["meta_h1"]=> string(10) "аваіа"
                        ["meta_description"]=> string(0) ""
                        ["meta_keyword"]=> string(0) "" }
                }
                ["parent_id"]=> string(2) "34"
                ["filter"]=> string(0) ""
                ["category_store"]=> array(1) {
                    [0]=> string(1) "0"
                }
                ["keyword"]=> string(0) ""
                ["image"]=> string(0) ""
                ["column"]=> string(1) "1"
                ["sort_order"]=> string(1) "0"
                ["status"]=> string(1) "1"
                ["category_layout"]=> array(1) {
                    [0]=> string(0) ""
                }
            }*/


           /*данные для добавления*/
           $put_data = array();
            foreach ($old_data as $result) {
                $put_data[] = array(
                    'old_category_id' => $result['category_id'],
                    'category_id' => $result['category_id'],
                    'category_description' => array(
                        '1' => array (
                            'name' => $result['name'],
                            'meta_h1'        => $result['cat_h1'],
                            'single_name'        => $result['single_name'],
                            'meta_title'        => $result['meta_title'],
                            'meta_keyword'        => $result['meta_keywords'],
                            'meta_description'        => $result['meta_description'],
                            'description'        => $result['description'],
                            )
                    ),
                    'parent_id' => $result['parent'],
                    'filter'        => '',
                    'category_store'        => array(
                        '0' => '0'
                    ),
                    'keyword'        => $result['url'],
                    'image'        => 'catalog/categories/'.$result['image'],
                    'column'        => '',
                    'top'        => '1',
                    'sort_order'        => $result['order_num'],
                    'status'        => $result['enabled'],
                    'category_layout'        => array(
                        '0' => ''
                    ),

                );


            }

            foreach ($put_data as $result) {
                $this->model_tool_import->addCategory($result);

            }


          echo 'ololo';
		} else {
            echo 'Не указан парметр';
        }


	}

	/*товары*/
	public function importcat2() {
		$this->language->load('tool/import');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/import2');

		if (isset($this->request->get['target']) && $this->request->get['target']='prod') {



            $this->db->query("TRUNCATE `oc_product`");
            $this->db->query("TRUNCATE `oc_product_attribute`");
            $this->db->query("TRUNCATE `oc_product_description`");
            $this->db->query("TRUNCATE `oc_product_discount`");
            $this->db->query("TRUNCATE `oc_product_filter`");
            $this->db->query("TRUNCATE `oc_product_image`");
            $this->db->query("TRUNCATE `oc_product_option`");
            $this->db->query("TRUNCATE `oc_product_option_value`");
            $this->db->query("TRUNCATE `oc_product_recurring`");
            $this->db->query("TRUNCATE `oc_product_related`");
            $this->db->query("TRUNCATE `oc_product_special`");
            $this->db->query("TRUNCATE `oc_product_to_category`");
            $this->db->query("TRUNCATE `oc_product_to_download`");
            $this->db->query("TRUNCATE `oc_product_to_layout`");
            $this->db->query("TRUNCATE `oc_product_to_store`");
            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE '%product_id%'");




            $total = $this->model_tool_import2->getTotalOldProducts();
            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import2->getOldProducts($start);
                $start= $start+100;
                set_time_limit(20);

                if(empty($old_data))break;

                // echo "<pre>";
                // var_dump($old_data);

                /*array(39) {
      ["product_description"]=>
      array(1) {
        [1]=>
        array(7) {
          ["name"]=>
          string(24) "Наименование"
          ["description"]=>
          string(29) "<p><br></p>"
          ["meta_title"]=>
          string(0) ""
          ["meta_h1"]=>
          string(0) ""
          ["meta_description"]=>
          string(0) ""
          ["meta_keyword"]=>
          string(0) ""
          ["tag"]=>
          string(0) ""
        }
      }
      ["image"]=>
      string(16) "catalog/cart.png"
      ["model"]=>
      string(12) "Модель"
      ["sku"]=>
      string(0) ""
      ["upc"]=>
      string(0) ""
      ["ean"]=>
      string(0) ""
      ["jan"]=>
      string(0) ""
      ["isbn"]=>
      string(0) ""
      ["mpn"]=>
      string(0) ""
      ["location"]=>
      string(0) ""
      ["price"]=>
      string(0) ""
      ["tax_class_id"]=>
      string(1) "0"
      ["quantity"]=>
      string(1) "1"
      ["minimum"]=>
      string(1) "1"
      ["subtract"]=>
      string(1) "1"
      ["stock_status_id"]=>
      string(1) "7"
      ["shipping"]=>
      string(1) "1"
      ["keyword"]=>
      string(0) ""
      ["date_available"]=>
      string(10) "2016-12-09"
      ["length"]=>
      string(0) ""
      ["width"]=>
      string(0) ""
      ["height"]=>
      string(0) ""
      ["length_class_id"]=>
      string(1) "1"
      ["weight"]=>
      string(0) ""
      ["weight_class_id"]=>
      string(1) "1"
      ["status"]=>
      string(1) "1"
      ["sort_order"]=>
      string(1) "1"
      ["manufacturer_id"]=>
      string(1) "0"
      ["main_category_id"]=>
      string(1) "0"
      ["filter"]=>
      string(0) ""
      ["product_store"]=>
      array(1) {
        [0]=>
        string(1) "0"
      }
      ["download"]=>
      string(0) ""
      ["related"]=>
      string(0) ""
      ["product_attribute"]=>
      array(1) {
        [0]=>
        array(3) {
          ["name"]=>
          string(6) "test 1"
          ["attribute_id"]=>
          string(1) "4"
          ["product_attribute_description"]=>
          array(1) {
            [1]=>
            array(1) {
              ["text"]=>
              string(8) "аыва"
            }
          }
        }
      }
      ["option"]=>
      string(2) "у"
      ["product_option"]=>
      array(1) {
        [0]=>
        array(6) {
          ["product_option_id"]=>
          string(0) ""
          ["name"]=>
          string(16) "Упаковка"
          ["option_id"]=>
          string(1) "5"
          ["type"]=>
          string(9) "input_qty"
          ["required"]=>
          string(1) "1"
          ["product_option_value"]=>
          array(1) {
            [0]=>
            array(16) {
              ["option_value_id"]=>
              string(2) "39"
              ["product_option_value_id"]=>
              string(0) ""
              ["name"]=>
              string(31) "азвание варианта"
              ["image"]=>
              string(0) ""
              ["sku"]=>
              string(17) "Артикул в"
              ["quantity"]=>
              string(3) "120"
              ["subtract"]=>
              string(1) "1"
              ["price_prefix"]=>
              string(1) "+"
              ["price"]=>
              string(3) "120"
              ["price_old"]=>
              string(0) ""
              ["points_prefix"]=>
              string(1) "+"
              ["points"]=>
              string(0) ""
              ["weight_prefix"]=>
              string(1) "+"
              ["weight"]=>
              string(0) ""
              ["action"]=>
              string(1) "0"
              ["preorder"]=>
              string(1) "0"
            }
          }
        }
      }
      ["points"]=>
      string(0) ""
      ["product_reward"]=>
      array(1) {
        [1]=>
        array(1) {
          ["points"]=>
          string(0) ""
        }
      }
      ["product_layout"]=>
      array(1) {
        [0]=>
        string(0) ""
      }
    }
    */
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    $put_data[] = array(
                        'product_description' => array(
                            '1' => array(
                                'name' => $result['model'],
                                'description' => $result['body'],
                                'description2' => $result['description'],
                                'meta_title' => $result['meta_title'],
                                'meta_h1' => '',
                                'meta_description' => $result['meta_description'],
                                'meta_keyword' => $result['meta_keywords'],
                                'tag' => $result['meta_keywords'],
                            )
                        ),
                        'image' => 'catalog/products/' . $result['large_image'],
                        'model' => $result['product_id'],
                        'sku' => '',
                        'upc' => '',
                        'ean' => '',
                        'jan' => '',
                        'isbn' => '',
                        'mpn' => '',
                        'location' => '',
                        'price' => '',
                        'tax_class_id' => '0',
                        'quantity' => '999',
                        'minimum' => '1',
                        'subtract' => '1',
                        'stock_status_id' => '7',
                        'shipping' => '0',
                        'keyword' => $result['url'],
                        'date_available' => '',
                        'length' => '',
                        'width' => '',
                        'height' => '',
                        'length_class_id' => '1',
                        'weight' => '',
                        'weight_class_id' => '1',
                        'status' => $result['enabled'],
                        'sort_order' => $result['order_num'],
                        'manufacturer_id' => $result['brand_id'],
                        'main_category_id' => $result['category_id'],
                        'filter' => '',
                        'product_store' => array(
                            '0' => '0',
                        ),
                        'download' => '',
                        'related' => '',
                        /* 'product_attribute'=>  array(
                             '0'=> array(
                                 'name'=> "test 1",
                                 'attribute_id'=> '4'
                                 'product_attribute_description'=> array(
                                     '1'=> array(
                                         'text'=> 'аыва'
                                        )
                                 )
                             )
                         )*/

                        /* ["option"]=>
                         string(2) "у"
                                   ["product_option"]=>
                         array(1) {
                                       [0]=>
                           array(6) {
                                           ["product_option_id"]=>
                             string(0) ""
                                           ["name"]=>
                             string(16) "Упаковка"
                                           ["option_id"]=>
                             string(1) "5"
                                           ["type"]=>
                             string(9) "input_qty"
                                           ["required"]=>
                             string(1) "1"
                                           ["product_option_value"]=>
                             array(1) {
                                               [0]=>
                               array(16) {
                                                   ["option_value_id"]=>
                                 string(2) "39"
                                                   ["product_option_value_id"]=>
                                 string(0) ""
                                                   ["name"]=>
                                 string(31) "азвание варианта"
                                                   ["image"]=>
                                 string(0) ""
                                                   ["sku"]=>
                                 string(17) "Артикул в"
                                                   ["quantity"]=>
                                 string(3) "120"
                                                   ["subtract"]=>
                                 string(1) "1"
                                                   ["price_prefix"]=>
                                 string(1) "+"
                                                   ["price"]=>
                                 string(3) "120"
                                                   ["price_old"]=>
                                 string(0) ""
                                                   ["points_prefix"]=>
                                 string(1) "+"
                                                   ["points"]=>
                                 string(0) ""
                                                   ["weight_prefix"]=>
                                 string(1) "+"
                                                   ["weight"]=>
                                 string(0) ""
                                                   ["action"]=>
                                 string(1) "0"
                                                   ["preorder"]=>
                                 string(1) "0"
                               }
                             }
                           }
                         }
                        */
                        'points' => '',
                        'product_reward' => array(
                            '1' => array(
                                'points' => ''
                            )
                        ),
                        'product_layout' => array(
                            '0' => ''
                        ),
                        'top' => $result['top'],
                        'hit' => $result['hit'],
                        'special' => $result['special'],
                        'vk' => $result['vk'],
                        'max_price' => $result['max_price'],
                        'min_price' => $result['min_price'],
                        'date_added' => $result['created'],
                        'date_modified' => $result['modified'],
                        'old_product_id' => $result['product_id'],
                        'product_status' => $result['product_status'],
                        'date_added' => $result['created'],

                    );


                }


                //  var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import2->addProduct($result);

                }

              //  $start=$start+300;
               // echo 'ololo'.$start.'<br/>';
            }

		} else {
            echo 'Не указан парметр';
        }


	}


    /*товары*/
    public function importplacedescription()
    {
        $this->load->model('tool/import2');
        if (isset($this->request->get['target']) && $this->request->get['target']=='prod_place') {

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import2->getOldPlaceDescriptionProducts($start);
                $start= $start+100;
                set_time_limit(20);

                if(empty($old_data))break;

                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    $put_data[] = array(
                        'category_id' => $result['category_id'],
                        'product_id' => $result['product_id']
                    );


                }
                foreach ($put_data as $result) {
                    $this->model_tool_import2->addPlaceDescription($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }
    }



	/**дубли категорий*/
	public function importcat2category() {
		$this->language->load('tool/import');

		$this->load->model('tool/import2');

		if (isset($this->request->get['target']) && $this->request->get['target']=='prod_upd') {

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import2->getOldProducts2($start);
                $start= $start+100;
                set_time_limit(20);

                if(empty($old_data))break;

              //  echo '<pre>';
               // var_dump($old_data);


                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    $put_data[] = array(
                        'category_id' => $result['category_id'],
                        'product_id' => $result['product_id']
                    );


                }
                foreach ($put_data as $result) {
                    $this->model_tool_import2->addProductCategory($result);

                }

            }

		} else {
            echo 'Не указан парметр';
        }


	}
	/**категории для  главных*/
	public function setparentcategory() {
		$this->load->model('tool/import2');

		if (isset($this->request->get['target']) && $this->request->get['target']=='parent') {

		    $data =array(
                '294'=> array(67,68,71,483,295,120,297,296),
                '79'=> array(190,299,191,198,302,300,301,192),
                '65'=> array(17,61,64,94,92,19),
                '163'=> array(207,203,205,204,208),
                '138'=> array(453,454,140,141,142,143),
                '486'=> array(134),
                '244'=> array(311,263,304,260,247,243,100),
                /***гризуни*/
                '223'=> array(224,286,226),
                '166'=> array(449,451,450,452,446,445,447,448,230,209),
                '163'=> array(207,206,205,204,208),
                '148'=> array(441,149,150,151,152,153,154,162,291),
                '165'=> array(444,442,443),
                '147'=> array(155,156,227,219,224,286,226,449,451,450,452,446,445,447,448,230,209,207,206,205,204,208,441,149,150,151,152,153,154,162,291,444,442,443),
                /***pticy*/
                '138'=> array(453,454,140,141,142,143),
                '139'=> array(457,456,455,458),
                '145'=> array(215,216,214,469,468),
                '228'=> array(197,229,144,231,233,234),
                '137'=> array(453,454,140,141,142,143,457,456,455,458,215,216,214,469,468,197,229,144,231,233,234),
                /***reptilii*/
                '157'=> array(158,212,277),
                /***ryby*/
                '130'=> array(486,465,132),
                /***ryby*/
                '268'=> array(269,270),
                /***собаки второго уровня*/
                '73'=> array(106,303,217,107,74,98,261,260,220,471),
                '102'=> array(168,167,484,169,305,170,306),
                '81'=> array(75,480,99,18,171,182,183,188,187,108,184),
                '85'=> array(369,470,368,367,109,110,111,112,235,242),
                '122'=> array(236,370,371,222,237,238,239),
                '248'=> array(119,249,250,256),
                '123'=> array(128,124,127,125,126),
                '273'=> array(386,374,375,378,374,380,381,382,383,385,384,387,274),
                /***коты второго уровня*/
                '103'=> array(423,388,384,390,391),
                '82'=> array(484,397,262,221),
                '392'=> array(394,101,393,259,278),
                '113'=> array(264,265,266,267),
                '240'=> array(118,241,467),
                '252'=> array(253,117,254,255),
                '121'=> array(421,419,415,429,416,414,179),
                '395'=> array(394,129,398),
                '80'=> array(401,402,400,403),
                '396'=> array(83,116,172),
                '105'=> array(257,258),
                '174'=> array(439,424,425,431,432,433,434,435,436,438,437,429,175,283),
            );

            foreach ($data as $key=>$value) {
                   $this->model_tool_import2->setParentProductCategory($key, $value);
            }


		} else {
            echo 'Не указан парметр';
        }


	}

    /**товары для ALL*/
    public function setallcategory()
    {
        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target'] == 'all') {

                $this->model_tool_import2->setAllProductCategory();
        } else {
            echo 'Не указан парметр';
        }
    }

/*опции*/
    public function importvariant() {
        $this->language->load('tool/import');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target']='var') {

            $this->db->query("TRUNCATE `oc_product_option`");
            $this->db->query("TRUNCATE `oc_product_option_value`");


            // echo '<pre>';
            $start = 0;
            for ( ; ;) {
                $productids = $this->model_tool_import2->getProductIds($start);
                $start= $start+100;
                set_time_limit(20);
                if(empty($productids))break;



                foreach ($productids as $product_id) {

                    $old_data_variants_one_product = $this->model_tool_import2->getOldVariants($product_id['product_id']);
                    // var_dump($old_data_variants_one_product);
                    $option_id = "";
                    $option_value_id = "";
                    $product_option = array();
                    $product_option_value = array();

                    foreach ($old_data_variants_one_product as $variant) {


                        if ($variant['weight'] > 0) {
                            $option_id = "5";
                            $option_value_id = "39";
                        } else {
                            $option_id = "6";
                            $option_value_id = "49";
                        }

                        $product_option_value[] = array(
                            'option_value_id' => $option_value_id,
                            'option_id' => $option_id,
                            'old_variant_id' => $variant['variant_id'],
                            'name' => $variant['name'],
                            'name2' => $variant['name'],
                            'image' => "",
                            'sku' => $variant['sku'],
                            'quantity' => $variant['stock'],
                            'subtract' => "1",
                            'price_prefix' => "+",
                            'price' => $variant['price'],
                            'price_old' => $variant['price_old'],
                            'points_prefix' => "+",
                            'points' => "",
                            'weight_prefix' => "+",
                            'weight' => $variant['weight'],
                            'sort' => $variant['position'],
                            'action' => $variant['variant_special'],
                            'preorder' => $variant['booking'],
                            'accesable' => $variant['accesable'],
                            'volume' => $variant['volume'],
                            'color' => $variant['color'],
                            'color_text' => $variant['color_text'],
                            'product_id' => $product_id['product_id'],


                        );

                    }
                    $product_option[] = array(
                        'option_id' => $option_id,
                        'required' => "1",
                        'product_id' => $variant['product_id'],
                        'product_option_value' => $product_option_value,
                    );

                    //   var_dump($product_option);
                    $this->model_tool_import2->addProductOption($product_option);
                }

            }


        } else {
            echo 'Не указан парметр';
        }


    }
/*importrelated*/
    public function importrelated() {
        $this->language->load('tool/import');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target']='rel') {

            $this->db->query("TRUNCATE `oc_product_related`");


            // echo '<pre>';
            $start = 0;
            for ( ; ;) {
                $productids = $this->model_tool_import2->getLinkedProducts($start);
                $start= $start+100;
                set_time_limit(20);
                if(empty($productids))break;



                foreach ($productids as $product) {
                    $this->model_tool_import2->addRelatedproduct($product['product_id'], $product['related_id']);

                }

            }


        } else {
            echo 'Не указан парметр';
        }


    }
/*атрибути*/

    public function importattribute(){
        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target']='attr') {
            echo 'ddd';

            $old_attributes = $this->model_tool_import2->getOldAttributes();
            foreach ($old_attributes as $attributes) {
                $data[]=array(
                    'attribute_group_id' => '4',
                    'attribute_id' => $attributes['id'],

                    'old_id' => $attributes['id'],
                    'old_url' => $attributes['url'],
                    'old_type' => $attributes['type'],
                    'sort_order' => $attributes['pos'],

                    'attribute_description' => array(
                        '1' => array(
                            'name' => $attributes['name'],
                    )
                    )

                );

            }
            // echo "<pre>";
            // var_dump($data);
            $this->db->query("DELETE FROM " . DB_PREFIX . "attribute_description ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "attribute ");

            foreach ($data as $new_data) {
                $this->model_tool_import2->addAttribute($new_data);
            }

        }


    }
    public function importattributevaluesproduct() {
        $this->load->model('tool/import2');


       // SELECT pf.name, pf.id, pfv.name as value_name,  pfa.value_id,  pfa.product_id  from products_filters_assigned pfa  LEFT JOIN products_filters_values pfv ON (pfa.value_id=pfv.id)  LEFT JOIN products_filters pf ON (pf.id=pfv.filter_id)  where pfa.product_id='6307';


        if (isset($this->request->get['target']) && $this->request->get['target']='attrp') {

            // echo '<pre>';
            $start = 0;
            for ( ; ;) {
                $productids = $this->model_tool_import2->getOldAttrProd($start);
                $start= $start+100;
                set_time_limit(20);
                if(empty($productids))break;

echo $start;

                foreach ($productids as $product_attr) {
                    $data =array(
                        'product_id'=>$product_attr['product_id'],
                        'old_value_id'=>$product_attr['value_id'],
                        'old_url'=>$product_attr['url'],
                        'old_name_filter'=>$product_attr['name_filter'],
                        'attribute_id'=>$product_attr['id'],
                        'language_id'=>'1',
                        'text'=>$product_attr['value_name'],
                );

                  //  echo '<pre>';
                   // var_dump($data);
                    $this->model_tool_import2->addAttributeToProduct($data);

                }
            }


        } else {
            echo 'Не указан парметр';
        }


    }


/*коментарии*/
    public function imporcomments() {
        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target']=='comments') {

            $this->db->query("TRUNCATE `oc_review`");

            //copyComments
            $this->model_tool_import2->copyComments();
        } else {
            echo 'Не указан парметр';
        }

    }
    /*коментарии*/

    /*изображения дополнительные*/
    public function importimages() {
        $this->load->model('tool/import2');

        if (isset($this->request->get['target']) && $this->request->get['target']=='images') {

            $this->db->query("TRUNCATE `oc_product_image`");
            //copyImages
            $this->model_tool_import2->copyImages();

        } else {
            echo 'Не указан парметр';
        }

    }
    /*коментарии*/


    /*
     *
    array(24) {
  ["news_description"]=>
  array(1) {
    [1]=>
    array(11) {
      ["title"]=>
      string(82) "In photos: Racine's Holiday Parade and community Christmas tree lighting ceremony "
      ["description"]=>
      string(180) "<p>Downtown Racine Corporation kicked off the holiday season with its
annual Holiday Parade and community Christmas tree lighting ceremony on
Saturday, Nov. 12.</p>"
      ["description2"]=>
      string(29) "<p><br></p>"
      ["ctitle"]=>
      string(0) ""
      ["meta_desc"]=>
      string(0) ""
      ["meta_key"]=>
      string(0) ""
      ["ntags"]=>
      string(0) ""
      ["cfield1"]=>
      string(0) ""
      ["cfield2"]=>
      string(0) ""
      ["cfield3"]=>
      string(0) ""
      ["cfield4"]=>
      string(0) ""
    }
  }
  ["image"]=>
  string(36) "catalog/demo/5823be2ab9009.image.jpg"
  ["status"]=>
  string(1) "1"
  ["nauthor_id"]=>
  string(1) "0"
  ["keyword"]=>
  string(81) "In-photos-Racines-Holiday-Parade-and-community-Christmas-tree-lighting-ceremony-9"
  ["date_added"]=>
  string(19) "2016-10-13 19:09:38"
  ["date_updated"]=>
  string(19) "2016-12-15 14:52:31"
  ["date_pub"]=>
  string(19) "2016-11-12 19:09:38"
  ["image2"]=>
  string(0) ""
  ["acom"]=>
  string(1) "0"
  ["sort_order"]=>
  string(1) "1"
  ["news_ncategory"]=>
  array(1) {
    [0]=>
    string(2) "59"
  }
  ["news_store"]=>
  array(1) {
    [0]=>
    string(1) "0"
  }
  ["nrelated"]=>
  string(0) ""
  ["news_nrelated"]=>
  array(1) {
    [0]=>
    string(1) "8"
  }
  ["related"]=>
  string(0) ""
  ["gal_thumb_w"]=>
  string(3) "150"
  ["gal_thumb_h"]=>
  string(3) "150"
  ["gal_popup_w"]=>
  string(3) "700"
  ["gal_popup_h"]=>
  string(3) "700"
  ["gal_slider_t"]=>
  string(1) "1"
  ["gal_slider_w"]=>
  string(3) "980"
  ["gal_slider_h"]=>
  string(3) "400"
  ["news_layout"]=>
  array(1) {
    [0]=>
    string(0) ""
  }
}


     */

    /*импорт статей*/
    public function importarticle() {

        $this->load->model('tool/import3');


        if (isset($this->request->get['target']) && $this->request->get['target']=='article') {


            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE '%news_id%'");

                $this->db->query("TRUNCATE `oc_nauthor`");
                $this->db->query("TRUNCATE `oc_nauthor_description`");
               // $this->db->query("TRUNCATE `oc_ncategory`");
             //   $this->db->query("TRUNCATE `oc_ncategory_description`");
               // $this->db->query("TRUNCATE `oc_ncategory_to_layout`");
              //  $this->db->query("TRUNCATE `oc_ncategory_to_store`");
                $this->db->query("TRUNCATE `oc_ncomments`");
                $this->db->query("TRUNCATE `oc_news`");
                $this->db->query("TRUNCATE `oc_news_description`");
                $this->db->query("TRUNCATE `oc_news_gallery`");
                $this->db->query("TRUNCATE `oc_news_related`");
                $this->db->query("TRUNCATE `oc_news_to_layout`");
                $this->db->query("TRUNCATE `oc_news_to_ncategory`");
                $this->db->query("TRUNCATE `oc_news_to_store`");
                $this->db->query("TRUNCATE `oc_news_video`");

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import3->getOldArticle($start);
                $start= $start+100;
                set_time_limit(20);

               if(empty($old_data))break;
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    //echo $result['url'];

                    $put_data[] = array(
                        'news_description' => array(
                            '1' => array(
                                'title' => $result['name'],
                                'description' => $result['description'],
                                'description2' => $result['annotation'],
                                'ctitle' => $result['meta_title'],
                                'meta_h1' => '',
                                'meta_desc' => $result['meta_description'],
                                'meta_key' => $result['meta_keywords'],
                                'ntags' => '',
                                'cfield1' => '',
                                'cfield2' => '',
                                'cfield3' => '',
                                'cfield4' => ''
                            )
                        ),
                        'image' => 'catalog/articles/' . $result['image'],
                        'status' => $result['enabled'],
                        'nauthor_id' => '0',
                        'keyword' => $result['url'],
                        'date_added' => $result['date_add'],
                        'date_updated' => $result['date_edit'],
                        'date_pub' => '',
                        'image2' => '',
                        'acom' => '',
                        'sort_order' => '',
                        'news_ncategory' => array(
                            '0' => '1',
                        ),
                        'news_store' => array(
                            '0' => '0'
                        ),
                        'news_nrelated' => array(

                        ),
                        'nrelated' => '',

                        'gal_thumb_w' => '',
                        'gal_thumb_h' => '',
                        'gal_popup_w' => '',
                        'gal_popup_h' => '',
                        'gal_slider_t' => '',
                        'gal_slider_w' => '',
                        'gal_slider_h' => '',
                        'news_layout' => array(
                            '0' => '0'
                        ),
                        'old_keyword' => $result['url'],
                        'old_id' => $result['id'],
                        'atype' => $result['atype'],
                        'id' => $result['id'],
                        'old_category_id' => $result['category_id'],

                    );
                }
                 // var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import3->addNews($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }
    }
    /*импорт новостей*/
    public function importnews() {

        $this->load->model('tool/import3');


        if (isset($this->request->get['target']) && $this->request->get['target']=='importnews') {


            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import3->getOldNews($start);
                $start= $start+100;
                set_time_limit(20);

               if(empty($old_data))break;
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    //echo $result['url'];

                    $put_data[] = array(
                        'news_description' => array(
                            '1' => array(
                                'title' => $result['header'],
                                'description' => $result['body'],
                                'description2' => str_replace('/files/images/', '/image/catalog/', $result['annotation']),
                                'ctitle' => $result['meta_title'],
                                'meta_h1' => '',
                                'meta_desc' => $result['meta_description'],
                                'meta_key' => $result['meta_keywords'],
                                'ntags' => '',
                                'cfield1' => '',
                                'cfield2' => '',
                                'cfield3' => '',
                                'cfield4' => ''
                            )
                        ),
                        'image' => '',
                        'status' => $result['enabled'],
                        'nauthor_id' => '0',
                        'keyword' => $result['url'],
                        'date_added' => $result['created'],
                        'date_updated' => $result['modified'],
                        'date_pub' => $result['date'],
                        'image2' => '',
                        'acom' => '',
                        'sort_order' => '',
                        'news_ncategory' => array(
                            '0' => '3',
                        ),
                        'news_store' => array(
                            '0' => '0'
                        ),
                        'news_nrelated' => array(

                        ),
                        'nrelated' => '',

                        'gal_thumb_w' => '',
                        'gal_thumb_h' => '',
                        'gal_popup_w' => '',
                        'gal_popup_h' => '',
                        'gal_slider_t' => '',
                        'gal_slider_w' => '',
                        'gal_slider_h' => '',
                        'news_layout' => array(
                            '0' => '0'
                        ),
                        'old_keyword' => $result['url'],
                        'old_id' => $result['news_id'],
                        'atype' => 10,
                        'id' => $result['news_id'],
                        'old_category_id' => '',

                    );
                }
                 // var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import3->addNews2($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }
    }


    /*импорт статей акций*/
    public function importactions() {

        $this->load->model('tool/import3');


        if (isset($this->request->get['target']) && $this->request->get['target']=='actions') {

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import3->getOldArticle2($start);
                $start= $start+100;
                set_time_limit(20);

               if(empty($old_data))break;
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    //echo $result['url'];

                    $put_data[] = array(
                        'news_description' => array(
                            '1' => array(
                                'title' => $result['name'],
                                'description' => $result['description'],
                                'description2' => $result['annotation'],
                                'ctitle' => $result['meta_title'],
                                'meta_h1' => '',
                                'meta_desc' => $result['meta_description'],
                                'meta_key' => $result['meta_keywords'],
                                'ntags' => '',
                                'cfield1' => '',
                                'cfield2' => '',
                                'cfield3' => '',
                                'cfield4' => ''
                            )
                        ),
                        'image' => 'catalog/articles/' . $result['image'],
                        'status' => $result['enabled'],
                        'nauthor_id' => '0',
                        'keyword' => $result['url'],
                        'date_added' => $result['date_add'],
                        'date_updated' => $result['date_edit'],
                        'date_pub' => '',
                        'image2' => '',
                        'acom' => '',
                        'sort_order' => '',
                        'news_ncategory' => array(
                            '0' => '2',
                        ),
                        'news_store' => array(
                            '0' => '0'
                        ),
                        'news_nrelated' => array(

                        ),
                        'nrelated' => '',

                        'gal_thumb_w' => '',
                        'gal_thumb_h' => '',
                        'gal_popup_w' => '',
                        'gal_popup_h' => '',
                        'gal_slider_t' => '',
                        'gal_slider_w' => '',
                        'gal_slider_h' => '',
                        'news_layout' => array(
                            '0' => '0'
                        ),
                        'old_keyword' => $result['url'],
                        'old_id' => $result['id'],
                        'atype' => 0,
                        'id' => $result['id'],
                        'old_category_id' => '',

                    );
                }
                 // var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import3->addNews2($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }
    }

    /*импорт категорий для акций для фильтра*/
    public function importactionsfilter() {

        $this->load->model('tool/import3');


        if (isset($this->request->get['target']) && $this->request->get['target']=='actionsfilter') {

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import3->getOldArticle3($start);
                $start= $start+100;
                set_time_limit(20);

               if(empty($old_data))break;
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {
                    //echo $result['url'];

                    $put_data[] = array(
                        'news_id' => $result['news_id'],
                        'category_id' => $result['category_id']

                    );
                }
                //echo "<pre>";
                 // var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import3->addActionFilter($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }
    }


    /*импорт покупателей*/
    public function importcustomer() {

        $this->load->model('tool/import4');


        if (isset($this->request->get['target']) && $this->request->get['target']=='customer') {

                $this->db->query("TRUNCATE `oc_customer`");
                $this->db->query("TRUNCATE `oc_address`");
                $this->db->query("TRUNCATE `oc_customer_asklist`");
                $this->db->query("TRUNCATE `oc_customer_wishlist`");

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import4->getOldUsers($start);
                $start= $start+100;
                set_time_limit(20);

               if(empty($old_data))break;
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {

                    $put_data[] = array(
                        'customer_id' =>  $result['user_id'],
                        'old_customer_id' =>  $result['user_id'],
                        'customer_group_id' =>  $result['group_id'],
                        'postcode' => '',
                        'country_id' => '',
                        'zone_id' => '',
                        'register' => '',
                        'captcha' => '',
                        'fax' => '',
                        'firstname' =>  $result['firstname'],
                        'telephone' => $result['phones'],
                        'lastname' => $result['lastname'].' '.$result['patronymic'],
                        'password' => $result['password'],
                        'email' => $result['email'],
                        'newsletter' => $result['spam_enable'],
                        'address_1' => $result['address'],
                        'address_2' => '',
                        'company' => '',
                        'city' => '',
                        'default' => '',
                        'country' => '',
                        'iso_code_2' => '',
                        'iso_code_3' => '',
                        'address_format' => '',
                        'zone' => '',
                        'zone_code' => '',
                        'custom_field' => '',
                        'current_address_id' => '',
                        'date_added' => $result['date'],


                        'old_gender' => $result['gender'],
                        'old_birth_date' => $result['birth_date'],
                        'old_remission_group_id' => $result['remission_group_id'],
                        'old_ulogin' => $result['ulogin'],
                        'old_via_mob_version' => $result['via_mob_version'],
                        'old_via_first_visit' => $result['via_first_visit'],
                    );
                }
                 // var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import4->addCustomer($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }


    }




    /*импорт банеров*/
    public function importbanners() {

        $this->load->model('tool/import5');


        if (isset($this->request->get['target']) && $this->request->get['target']=='banners') {

            $this->db->query("TRUNCATE `oc_banner_image`");
            $this->db->query("TRUNCATE `oc_banner_image_description`");

            $start = 0;
            for ( ; ;) {

                /*данные старой базы*/
                $old_data = $this->model_tool_import5->getOldBanners($start);
                $start= $start+100;
                set_time_limit(20);



                if(empty($old_data))break;
                /*данные для добавления*/
                $put_data = array();
                foreach ($old_data as $result) {

                    $put_data[] = array(
                        'banner_image_id' =>  $result['banner_id'],
                        'banner_id' =>  $result['banner_id'],
                        'link' =>  $result['ref_url'],
                        'image' =>  'catalog/sliders/'.$result['img_url'],
                        'sort_order' =>  $result['range'],
                        'language_id' =>  '1',
                        'title' =>  $result['banner_id'],
                        'old_target_blank' =>  $result['target_blank'],
                        'old_enabled' =>  $result['enabled'],
                        'old_final_date' =>  $result['final_date'],

                    );
                }
                // var_dump($put_data);
                foreach ($put_data as $result) {
                    $this->model_tool_import5->addBanner($result);

                }

            }

        } else {
            echo 'Не указан парметр';
        }


    }






    public function edit() {
		$this->language->load('catalog/category');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_category->editCategory($this->request->get['category_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/category');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $category_id) {
				$this->model_catalog_category->deleteCategory($category_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function repair() {
		$this->language->load('catalog/category');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		if ($this->validateRepair()) {
			$this->model_catalog_category->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('catalog/category/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('catalog/category/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['repair'] = $this->url->link('catalog/category/repair', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$category_total = $this->model_catalog_category->getTotalCategories();

		$results = $this->model_catalog_category->getCategories($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'category_id' => $result['category_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'edit'        => $this->url->link('catalog/category/edit', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'] . $url, 'SSL'),
				'delete'      => $this->url->link('catalog/category/delete', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'] . $url, 'SSL')
			);
		}

		if (isset($this->request->get['path'])) {
			if ($this->request->get['path'] != '') {
				$this->path = explode('_', $this->request->get['path']);
				$this->category_id = end($this->path);
				$this->session->data['path'] = $this->request->get['path'];
			} else {
				unset($this->session->data['path']);
			}
		} elseif (isset($this->session->data['path'])) {
			$this->path = explode('_', $this->session->data['path']);
			$this->category_id = end($this->path);
		}

		$data['categories'] = $this->getCategories(0);

		$category_total = count($data['categories']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$data['sort_sort_order'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['path'])) {
			$url .= '&path=' . $this->request->get['path'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/category_list.tpl', $data));
	}

	protected function getForm() {
    //CKEditor
    if ($this->config->get('config_editor_default')) {
        $this->document->addScript('view/javascript/ckeditor/ckeditor.js');
        $this->document->addScript('view/javascript/ckeditor/ckeditor_init.js');
    }

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_h1'] = $this->language->get('entry_meta_h1');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (!isset($this->request->get['category_id'])) {
			$data['action'] = $this->url->link('catalog/category/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('catalog/category/edit', 'token=' . $this->session->data['token'] . '&category_id=' . $this->request->get['category_id'] . $url, 'SSL');
		}

		$data['cancel'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_category->getCategory($this->request->get['category_id']);
		}

		$data['token'] = $this->session->data['token'];
		$data['ckeditor'] = $this->config->get('config_editor_default');

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['lang'] = $this->language->get('lang');

		if (isset($this->request->post['category_description'])) {
			$data['category_description'] = $this->request->post['category_description'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_description'] = $this->model_catalog_category->getCategoryDescriptions($this->request->get['category_id']);
		} else {
			$data['category_description'] = array();
		}

		// Categories
		$categories = $this->model_catalog_category->getAllCategories();

		$data['categories'] = $this->getAllCategories($categories);

		if (isset($category_info)) {
			unset($data['categories'][$category_info['category_id']]);
		}

		if (isset($this->request->post['parent_id'])) {
			$data['parent_id'] = $this->request->post['parent_id'];
		} elseif (!empty($category_info)) {
			$data['parent_id'] = $category_info['parent_id'];
		} else {
			$data['parent_id'] = 0;
		}

		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_category->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_category->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($category_info)) {
			$data['keyword'] = $category_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}

		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_category->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/category_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['category_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if (utf8_strlen($this->request->post['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

			if ($url_alias_info && isset($this->request->get['category_id']) && $url_alias_info['query'] != 'category_id=' . $this->request->get['category_id']) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['category_id'])) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($this->error && !isset($this->error['warning'])) {
				$this->error['warning'] = $this->language->get('error_warning');
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	private function getCategories($parent_id, $parent_path = '', $indent = '') {
		$category_id = array_shift($this->path);

		$output = array();

		static $href_category = null;
		static $href_action = null;

		if ($href_category === null) {
			$href_category = $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . '&path=', 'SSL');
			$href_action = $this->url->link('catalog/category/update', 'token=' . $this->session->data['token'] . '&category_id=', 'SSL');
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$results = $this->model_catalog_category->getCategoriesByParentId($parent_id);

		foreach ($results as $result) {
			$path = $parent_path . $result['category_id'];

			$href = ($result['children']) ? $href_category . $path : '';

			$name = $result['name'];

			if ($category_id == $result['category_id']) {
				$name = '<b>' . $name . '</b>';

				$data['breadcrumbs'][] = array(
					'text'      => $result['name'],
					'href'      => $href,
					'separator' => ' :: '
				);

				$href = '';
			}

			$selected = isset($this->request->post['selected']) && in_array($result['category_id'], $this->request->post['selected']);

			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $href_action . $result['category_id']
			);

			$output[$result['category_id']] = array(
				'category_id' => $result['category_id'],
				'name'        => $name,
				'sort_order'  => $result['sort_order'],
				'selected'    => $selected,
				'action'      => $action,
				'edit'        => $this->url->link('catalog/category/edit', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'] . $url, 'SSL'),
				'delete'      => $this->url->link('catalog/category/delete', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'] . $url, 'SSL'),
				'href'        => $href,
				'indent'      => $indent
			);

			if ($category_id == $result['category_id']) {
				$output += $this->getCategories($result['category_id'], $path . '_', $indent . str_repeat('&nbsp;', 8));
			}
		}

		return $output;
	}

	private function getAllCategories($categories, $parent_id = 0, $parent_name = '') {
		$output = array();

		if (array_key_exists($parent_id, $categories)) {
			if ($parent_name != '') {
				//$parent_name .= $this->language->get('text_separator');
				$parent_name .= ' &gt; ';
			}

			foreach ($categories[$parent_id] as $category) {
				$output[$category['category_id']] = array(
					'category_id' => $category['category_id'],
					'name'        => $parent_name . $category['name']
				);

				$output += $this->getAllCategories($categories, $category['category_id'], $parent_name . $category['name']);
			}
		}

    uasort($output, array($this, 'sortByName'));
    
		return $output;
	}

  function sortByName($a, $b) {
    return strcmp($a['name'], $b['name']);
  }

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/category');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_category->getCategories($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
