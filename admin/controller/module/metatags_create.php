<?php
class ControllerModuleMetatagsCreate extends Controller {
	
		
	
	private $error = array();
	private $form;
	private $data = array();

  	
  	public function index() {
		$this->load->language('module/metatags_create');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');

    	if ($this->request->server['REQUEST_METHOD'] == 'POST') {
    		if ($this->validate($this->request->post)) {
				$this->model_setting_setting->editSetting('metatags_create', $this->request->post);
				
				$this->session->data['success'] = $this->language->get('text_success');
	
				$this->response->redirect($this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL'));
				//$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
    		}
    	}

		$data['heading_title'] = $this->language->get('heading_title');
      	$data['breadcrumbs'] = $this->getBreadCrumbs();
      	$data['button_save'] = $this->language->get('button_save');
      	$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['token'] = $this->session->data['token'];
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_get_key'] = $this->language->get('text_get_key');
		$data['text_unselect'] = $this->language->get('text_unselect');
		
		
		
		
		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['lang'] = $this->language->get('lang');
		
		$data['filds_parse'] = array(
				'name',
				'description',
				'meta_title',
				'meta_h1',
				'meta_description',
				'meta_keyword',
				'tag'
			);
		
		
		
 
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		$data['_error'] = $this->error;
		
    	$data['action'] = $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		foreach($data['languages'] as $language){
			
			
			$defaults["metatags_create"][$language['language_id']] = array(
						'product_1' => '',
						'product_2' => '',
						'product_3' => '',
						'product_1_select_field' => '',
						'product_2_select_field' => '',	
						'product_3_select_field' => '',
					);
		}
		
		foreach ($defaults as $key=>$value) {
			if (isset($this->request->post[$key])) {
				$defautls[$key] = $this->request->post[$key];
			}
			else {
				$defautls[$key] = $this->config->get($key);
			}
			$data[$key] = $defautls[$key];
			$data['entry_'.$key] = $this->language->get('entry_'.$key);
			if ($this->language->get('entry_'.$key.'_help')) {
				$data['entry_'.$key.'_help'] = $this->language->get('entry_'.$key.'_help');
			}
		}

		$template = 'module/metatags_create.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view($template, $data));				
  	}
	
	public function install() {
		
	}

	public function uninstall() {
		
	}

  	private function validate($post_data) {
		if (!$this->user->hasPermission('modify', 'module/metatags_create')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		
    	if (!$this->error || !sizeof($this->error)) {
      		return true;
    	} else {
      		return false;
    	}
  	}
	
	private function getBreadCrumbs() {
		$breadcrumbs = array();

   		$breadcrumbs[] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$breadcrumbs[] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$breadcrumbs[] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL'),      		
      		'separator' => ' :: '
   		);
   		
      	return $breadcrumbs;
	}
	
	function parse() {
		$json = array();
			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
			$this->load->model('catalog/manufacturer');
			$this->load->model('tool/metatags_create');
            $setting = $this->config->get("metatags_create");
            $categories = $this->model_catalog_category->getAllCategories2();
            $manufactureres = $this->model_catalog_manufacturer->getManufacturers();

				//var_dump($setting);

        if(1){
				foreach($categories as $category){
                    $i = 0;
                    $j = 0;
                    $z = 0;


                    foreach($manufactureres as $manufacturer){

                        $search = array("%BRAND%","%CATEGORY%");
                        $replace   = array($manufacturer['name'], $category['name']);

                        $product_1 = $setting[1]['product_1'];
                        $product_1_new_srting = str_replace($search, $replace, $product_1);

                        preg_match_all('/{{.*?}}/', $product_1, $m_string);
                        if(isset($m_string[0][0])){
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var = count($var_string);

                                if(!isset($var_string[$i])){ $i = 0; }

                                $product_1_new_srting = str_replace($find, $var_string[$i], $product_1_new_srting);
                            }
                        }
                        $product_2 = $setting[1]['product_2'];
                        $product_2_new_srting = str_replace($search, $replace, $product_2);
                        preg_match_all('/{{.*?}}/', $product_2, $m_string);
                        if(isset($m_string[0][0])){
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var = count($var_string);

                                if(!isset($var_string[$j])){ $j = 0; }
                                $jj[] = $j;

                                $product_2_new_srting = str_replace($find, $var_string[$j], $product_2_new_srting);
                            }
                        }
                        $product_3 = $setting[1]['product_3'];
                        $product_3_new_srting = str_replace($search, $replace, $product_3);
                        preg_match_all('/{{.*?}}/', $product_3, $m_string);
                        if(isset($m_string[0][0])){
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var = count($var_string);

                                if(!isset($var_string[$z])){ $z = 0; }

                                $product_3_new_srting = str_replace($find, $var_string[$z], $product_3_new_srting);
                            }
                        }
                        $insert_data = array(
                            'category_id' => $category['category_id'],
                            'brand_id' => $manufacturer['manufacturer_id'],
                            'enabled' => '1',
                            'single_name' => $category['name'].'/'.$manufacturer['name'] ,
                            'menu_name' => $category['name'].'/'.$manufacturer['name'] ,
                            'meta_title' => $category['name'].'/'.$manufacturer['name'] ,
                            'meta_keywords' => $category['name'].', '.$manufacturer['name'] ,
                            'meta_description' => $category['name'].', '.$manufacturer['name'] ,
                            'seo_description' => $product_1_new_srting,
                            'seo_description_up' => $product_2_new_srting,
                            'seo_description_middle' => $product_3_new_srting,
                        );
                        $this->model_tool_metatags_create->InsertCategoryBrandDescription($insert_data);

                        $i++; $j++;$z++;
                       // if ($i==6) {exit; echo 'exit';}
                    }


				}
				}
	}
	function parse_rus() {
		$json = array();
		$step = 20;
		if(isset($this->request->get['page_parse'])){
			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
			$this->load->model('catalog/manufacturer');
			$this->load->model('tool/metatags_create');
			$total_products = $this->model_catalog_product->getTotalProducts();

			$pages = $total_products/$step;
			$page = $this->request->get['page_parse'];

			if($page <= ($pages+1)){


				$filter_data = array(
					'start'		=> ($page - 1) * $step,
					'limit'		=> $step
				);
				$products = $this->model_catalog_product->getProducts($filter_data);
				$setting = $this->config->get("metatags_create");
				//var_dump($setting);
				$i = 0;
				$j = 0;
				foreach($products as $product){

					$product_id = $product['product_id'];

					$this->load->model('localisation/language');
					$languages = $this->model_localisation_language->getLanguages();
					$data['lang'] = $this->language->get('lang');

					$data_product_description = $this->model_catalog_product->getProductDescriptions($product_id);
					$product_main_category_id = $this->model_catalog_product->getProductMainCategoryId($product_id);
					$product_caregory_description = $this->model_catalog_category->getCategoryDescriptions($product_main_category_id);

					$product_manufacturer_description = $this->model_catalog_manufacturer->getManufacturerDescriptions($product['manufacturer_id']);

					$model = $product['model'];
					$price = $this->currency->format((float)$product['price']);

					//var_dump($price); exit;
					$product_description_update = array();
					foreach($languages as $language){
						$name 				= $data_product_description[$language['language_id']]['name'];
						$description 		= $data_product_description[$language['language_id']]['description'];
						$meta_title 		= $data_product_description[$language['language_id']]['meta_title'];
						$meta_h1 			= $data_product_description[$language['language_id']]['meta_h1'];
						$meta_description 	= $data_product_description[$language['language_id']]['meta_description'];
						$meta_keyword 		= $data_product_description[$language['language_id']]['meta_keyword'];
						$tag 				= $data_product_description[$language['language_id']]['tag'];

						if(isset($product_caregory_description[$language['language_id']]['name'])){
						$caregory_name		= $product_caregory_description[$language['language_id']]['name'];
						} else {
						$caregory_name = "";
						}

						if(isset($product_manufacturer_description[$language['language_id']]['name'])){
						$manufacturer_name		= $product_manufacturer_description[$language['language_id']]['name'];
						} else {
						$manufacturer_name = "";
						}

						$search = array("%NAME%","%BRAND%","%CATEGORY%","%MODEL%","%PRICE%");
						$replace   = array($name, "$manufacturer_name", $caregory_name,$model,$price);

						$product_1 = $setting[$language['language_id']]['product_1'];
						$product_1_new_srting = str_replace($search, $replace, $product_1);

						preg_match_all('/{{.*?}}/', $product_1, $m_string);
						if(isset($m_string[0][0])){
							foreach($m_string[0] as $key => $find){
								$string = str_replace(array("{{","}}"),"",$find);
								$var_string = explode("|",$string);
								$count_var = count($var_string);

								if(!isset($var_string[$i])){ $i = 0; }

								$product_1_new_srting = str_replace($find, $var_string[$i], $product_1_new_srting);
							}
						}

						$product_2 = $setting[$language['language_id']]['product_2'];
						$product_2_new_srting = str_replace($search, $replace, $product_2);

						preg_match_all('/{{.*?}}/', $product_2, $m_string);
						if(isset($m_string[0][0])){
							foreach($m_string[0] as $key => $find){
								$string = str_replace(array("{{","}}"),"",$find);
								$var_string = explode("|",$string);
								$count_var = count($var_string);

								if(!isset($var_string[$j])){ $j = 0; }

								$product_2_new_srting = str_replace($find, $var_string[$j], $product_2_new_srting);
							}
						}

						$product_1_select_field = $setting[$language['language_id']]['product_1_select_field'];
						if(($product_1_select_field != "0") && ($product_1_new_srting != "")){
							$data_product_description[$language['language_id']][$product_1_select_field] = $product_1_new_srting;
						}

						$product_2_select_field = $setting[$language['language_id']]['product_2_select_field'];
						if(($product_2_select_field != "0") && ($product_2_new_srting != "")){
							$data_product_description[$language['language_id']][$product_2_select_field] = $product_2_new_srting;
						}
					}
					$this->model_tool_metatags_create->updateProductDescription($product_id,$data_product_description);
					$i++; $j++;
				}


				$json['text'] = "Страница ".$page." загружена";
			    $json['total'] = ($page+1);
			} else {
				 $json['text'] = "Обновлено";
			     $json['total'] = 0;
			}


			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

}
