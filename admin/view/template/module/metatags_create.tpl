<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-latest" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-latest" class="form-horizontal">
     <div class="tab-content">
            <div class="tab-pane active" id="tab-product">
              <ul class="nav nav-tabs" id="language_advert">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language_advert<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
		<?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language_advert<?php echo $language['language_id']; ?>">
		
		
			<div class="form-group">
			
			<table id="pr_links" class="table table-striped table-bordered table-hover">
				 <thead>
					<td width="70%" class="text-left">Шаблон</td>
                    <td width="10%" class="text-right">Привязать</td>
                    <td width="20%" class="text-right">Описание</td>
				 </thead>
				<tr>
					<td class="text-right">
					<textarea type="text" name="metatags_create[<?php echo $language['language_id']; ?>][product_1]"   class="form-control"><?php echo  $metatags_create[$language['language_id']]['product_1'] ; ?></textarea>
					</td>
					<td>
					<select id="metatags_create[<?php echo $language['language_id']; ?>][product_1_select_field]" name="metatags_create[<?php echo $language['language_id']; ?>][product_1_select_field]" class="form-control">
                    <option value="0" selected="selected">--selected--</option>
                    <?php foreach($filds_parse as $fild) { ?>
                    <?php if($fild == $metatags_create[$language['language_id']]['product_1_select_field']) { ?>
                    <option value="<?php echo $fild; ?>" selected="selected"><?php echo $fild; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $fild; ?>"><?php echo $fild; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
					</td>
					<td rowspan="3">
					<b>Название товара </b>%NAME%</br>
					<b>Производитель </b>%BRAND%</br>
					<b>Главная категория </b>%CATEGORY%</br>
					<b>Модель </b>%MODEL%</br>
					<b>Цена </b>%PRICE% <i>(в зависимости от выбраной валюты на фронте сайта)</i></br>
					<b>Варианты </b>{{дешево|супердешево|дешевле}}</br>
					</td>
				</tr>
				<tr>
					<td class="text-right">
					<textarea type="text" name="metatags_create[<?php echo $language['language_id']; ?>][product_2]" class="form-control"><?php echo  $metatags_create[$language['language_id']]['product_2'] ; ?></textarea>
					</td>
					<td>
					<select id="metatags_create[<?php echo $language['language_id']; ?>][product_2_select_field]" name="metatags_create[<?php echo $language['language_id']; ?>][product_2_select_field]" class="form-control">
                    <option value="0" selected="selected">--selected--</option>
                    <?php foreach($filds_parse as $fild) { ?>
                    <?php if($fild == $metatags_create[$language['language_id']]['product_2_select_field']) { ?>
                    <option value="<?php echo $fild; ?>" selected="selected"><?php echo $fild; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $fild; ?>"><?php echo $fild; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
					</td>
				</tr>
				<tr>
					<td class="text-right">
					<textarea type="text" name="metatags_create[<?php echo $language['language_id']; ?>][product_3]" class="form-control"><?php echo  $metatags_create[$language['language_id']]['product_3'] ; ?></textarea>
					</td>
					<td>
					<select id="metatags_create[<?php echo $language['language_id']; ?>][product_3_select_field]" name="metatags_create[<?php echo $language['language_id']; ?>][product_3_select_field]" class="form-control">
                    <option value="0" selected="selected">--selected--</option>
                    <?php foreach($filds_parse as $fild) { ?>
                    <?php if($fild == $metatags_create[$language['language_id']]['product_3_select_field']) { ?>
                    <option value="<?php echo $fild; ?>" selected="selected"><?php echo $fild; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $fild; ?>"><?php echo $fild; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
					</td>
				</tr>

			</table>
            </div>
         
         
         
         
          </div>
         
         <?php } ?>
          </div>
        
         </div>
       
     
        </form>
      
      
      
      
        <div class="form-group">
		<div class="col-md-4">
			<a class="btn btn-primary" onclick="pr_download('Старт',1)"><?php echo "Спарсить"; ?></a> перед парсингом кликните сохранить модуль
		</div>
		
		</div>
		
		 <div class="form-group ">
		<div class="col-md-12 text-primary" id="status_download">
		</div>
		</div>
		
		
      
      
      
      
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript"><!--
$('#language_advert a:first').tab('show');
//--></script>

<script>

	function pr_download(text,total){
		
		var status =  document.getElementById('status_download');
		status.innerText = text+"\r\n"+status.innerText;
		
		if(total>0){
			$.ajax({
			url: 'index.php?route=module/metatags_create/parse&page_parse='+total+'&token=<?php echo $token; ?>',
			dataType: 'json',
			success: function(json) {
				var mass = JSON.stringify(json);
				 mass = JSON.parse(mass);
				 pr_download(mass.text,mass.total);
			
			}
		});
			
		} 	
	}
</script>
<?php echo $footer; ?>
