<?php
// Heading
$_['heading_title']     = 'Поиск';
$_['heading_tag']		= 'Тег - ';

// Text
$_['text_search']       = 'Товары, соответствующие критериям поиска';
$_['text_keyword']      = 'Ключевые слова';
$_['text_category']     = 'Все категории';
$_['text_sub_category'] = 'Искать в подкатегориях';
$_['text_empty']        = 'Нет товаров, соответствующих критериям поиска.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусные баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без НДС:';
$_['text_reviews']      = '%s отзывов.';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировать:';
$_['text_default']      = 'Автоматически';
$_['text_name_asc']     = 'По Имени (A - Я)';
$_['text_name_desc']    = 'По Имени (Я - A)';
$_['text_price_asc']    = 'Дешевле';
$_['text_price_desc']   = 'Дороже';
$_['text_hit_desc']     = 'Хиты продаж';
$_['text_action_desc']  = 'Акции';
$_['text_rating_asc']   = 'По Рейтингу (возрастанию)';
$_['text_rating_desc']  = 'По Рейтингу (убыванию)';
$_['text_model_asc']    = 'По Модели (A - Я)';
$_['text_model_desc']   = 'По Модели (Я - A)';
$_['text_limit']        = 'Показывать:';

// Entry
$_['entry_search']      = 'Критерии поиска';
$_['entry_description'] = 'Искать в описании товара';
