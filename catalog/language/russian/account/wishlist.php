<?php
// Heading 
$_['heading_title'] = 'Список избраного';

// Text
$_['text_account']  = 'Личный Кабинет';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Иизбраного (%s)';
$_['text_login']    = 'Необходимо войти в <a href="%s">Личный Кабинет</a> или <a href="%s">создать учетную запись</a>, чтобы добавить товар <a href="%s">%s</a> в свои <a href="%s">закладки</a>!';
$_['text_success']  = 'Товар <a href="%s">%s</a> успешно добавлен в <a href="%s">избраное</a>!';
$_['text_remove']   = 'Список избраного успешно обновлен!';
$_['text_empty']    = 'Ваш список избраного пуст';

// Column
$_['column_image']  = 'Изображение';
$_['column_name']   = 'Наименование товара';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'На складе';
$_['column_price']  = 'Цена';
$_['column_action'] = 'Действие';
