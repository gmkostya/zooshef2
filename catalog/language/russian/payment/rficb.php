<?php
// Text
$_['text_title'] = 'Оплата банковской картой';
$_['text_description'] = '<p>Мы поддерживаем следующие платежные системы</p><div class="online_payment_logo_list"><img src="/image/add_logos/mc_logo.gif" width="55"><img src="/image/add_logos/visa_logo.gif" width="55"><img src="/image/add_logos/webmoney.jpg" height="55"></div>';

$_['text_response']				= 'Ответ сервиса РФИ банка:';
$_['text_success']				= '... платеж выполнен успешно.';
$_['text_success_wait']			= '<b><span style="color: #FF0000">Please wait...</span></b> whilst we finish processing your order.<br>If you are not automatically re-directed in 10 seconds, please click <a href="%s">here</a>.';
$_['text_failure']				= '... платеж отменен!';
$_['text_failure_wait']			= '<b><span style="color: #FF0000">Please wait...</span></b><br>If you are not automatically re-directed in 10 seconds, please click <a href="%s">here</a>.';
$_['text_pw_mismatch']			= 'Ошибка проверки платежа';