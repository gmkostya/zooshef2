<?php
// Text
$_['text_signin_register']    = 'Вход/Регистрация';

$_['text_new_customer']       = 'Новый покупатель';
$_['text_returning']          = 'Возврат';
$_['text_returning_customer'] = 'Я вернулся';
$_['text_details']            = 'Ваши персональные даные';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Имя';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Восстановить пароль';
$_['text_agree']              = 'Я ознавомлен с правилами <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Логин';

//Error
$_['error_name']           = 'Имя должно иметь от 3 джо 32 символов';
$_['error_email']          = 'E-Mail указан не коректно!';
$_['error_telephone']      = 'Телефон должен иметь от 3 до 32 символов!';
$_['error_password']       = 'Пароль должен иметь от 4 до 20 символов!';
$_['error_exists']         = 'Внимание: E-Mail уже зарегистрирован!';
$_['error_agree']          = 'Внимание: Вы должныв подтвердить %s!';
$_['error_warning']        = 'Внимание: Пожалуйстав проверте форму на ошибки!';
$_['error_approved']       = 'Внимание: Ваша учетная запись требует одобрения, прежде чем вы можете войти.';
$_['error_login']          = 'Внимание: Не верно указан пароль или логин.';