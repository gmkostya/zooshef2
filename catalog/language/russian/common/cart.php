<?php
// Text
$_['text_items']     = '<div class="products-count"><span id="cart-total">%s</span> товаров</div><div class="price-count">на <span id="total_products_price">%s</span></div>';
$_['text_empty']     = 'В корзине пусто!';
$_['text_cart']      = 'Открыть Корзину';
$_['text_checkout']  = 'Оформить Заказ';
$_['text_recurring'] = 'Профиль платежа';

