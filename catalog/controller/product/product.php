<?php
class ControllerProductProduct extends Controller {
	private $error = array();
    public function view()
    {

        $shablon_price = '';

        $this->load->language('product/product');

        $text_after_title = $this->language->get('text_after_title');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $this->load->model('catalog/category');

        if (isset($this->request->get['path'])) {
            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $path)
                    );
                }
            }

            // Set the last category breadcrumb
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $url = '';

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                if (isset($this->request->get['limit'])) {
                    $url .= '&limit=' . $this->request->get['limit'];
                }

                $data['breadcrumbs'][] = array(
                    'text' => $category_info['name'],
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
                );

                /*for brand breadcrumbs*/
                $category_current_url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url);
                $data['category_info_name'] = $category_current_name = $category_info['name'];

            }
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->get['manufacturer_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_brand'),
                'href' => $this->url->link('product/manufacturer')
            );

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

            if ($manufacturer_info) {
                $data['breadcrumbs'][] = array(
                    'text' => $manufacturer_info['name'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
                );
            }
        }

        if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
            $url = '';

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('product/search', $url)
            );
        }
        /*
                $this->load->model('catalog/information');
                $information_info = $this->model_catalog_information->getInformation(9);

                $data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
        */

        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');
        $this->load->model('catalog/review');


        $data['text_delivery'] = $this->language->get('text_delivery');


        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {

            $product_info['reviews'] = (int)$this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }


            if (isset($product_info['manufacturer']) && !empty($product_info['manufacturer']) && (isset($category_current_url))) {

                $manufacturer_key = $this->model_catalog_manufacturer->getManufacturersAliasById($product_info['manufacturer_id']);
                if ($manufacturer_key && isset($manufacturer_key)) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_current_name . ' ' . $product_info['manufacturer'],
                        'href' => $category_current_url . $manufacturer_key . '/'
                    );
                }
            }


            $data['breadcrumbs'][] = array(
                'text' => $product_info['name'],
                'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
            );


            $this->document->setDescription($product_info['meta_description']);
            $this->document->setKeywords($product_info['meta_keyword']);
            $this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/locale/' . $this->session->data['language'] . '.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

            if ($product_info['meta_h1']) {
                $data['heading_title'] = $product_info['meta_h1'];
            } else {
                $data['heading_title'] = $product_info['name'];
            }

            $data['text_select'] = $this->language->get('text_select');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_stock'] = $this->language->get('text_stock');
            $data['text_discount'] = $this->language->get('text_discount');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
            $data['text_write'] = $this->language->get('text_write');
            $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
            $data['text_note'] = $this->language->get('text_note');
            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_related'] = $this->language->get('text_related');
            $data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
            $data['text_loading'] = $this->language->get('text_loading');

            $data['entry_qty'] = $this->language->get('entry_qty');
            $data['entry_name'] = $this->language->get('entry_name');
            $data['entry_review'] = $this->language->get('entry_review');
            $data['entry_rating'] = $this->language->get('entry_rating');
            $data['entry_good'] = $this->language->get('entry_good');
            $data['entry_bad'] = $this->language->get('entry_bad');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_upload'] = $this->language->get('button_upload');
            $data['button_continue'] = $this->language->get('button_continue');

            $this->load->model('catalog/review');

            $data['tab_description'] = $this->language->get('tab_description');
            $data['tab_attribute'] = $this->language->get('tab_attribute');
            $data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

            $data['product_id'] = (int)$this->request->get['product_id'];
            $data['manufacturer'] = $product_info['manufacturer'];
            $data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
            $data['model'] = $product_info['model'];
            $data['reward'] = $product_info['reward'];
            $data['points'] = $product_info['points'];
            $data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
            $data['description2'] = html_entity_decode($product_info['description2'], ENT_QUOTES, 'UTF-8');


            /*статус ствара*/

            $product_statuses = array(
                '0' => array(
                    'kod' => '0',
                    'name' => 'НЕТ УКАЗАН'
                ),
                '1' => array(
                    'kod' => '1',
                    'name' => 'Снято с производства'
                ),
                '2' => array(
                    'kod' => '2',
                    'name' => 'Выведен из ассортимента'
                ),
                '3' => array(
                    'kod' => '3',
                    'name' => 'Акциия завершена'
                )
            );

            $data['product_status'] = $product_info['product_status'];

            if (empty($product_info['product_status'])) $product_info['product_status'] = '0';

            $data['product_status_text'] = $product_statuses[$product_info['product_status']]['name'];


            if (isset($this->session->data['compare']) and in_array($product_id, $this->session->data['compare'])) {

                $data['compare_status'] = '1';
            } else {
                $data['compare_status'] = false;
            }

            $data['description_places'] = array();

            $results = $this->model_catalog_product->getProductDescriptionPlaces($product_id);

            foreach ($results as $result) {
                $data['description_places'][$result['place']] = array(
                    'descr' => html_entity_decode($result['descr'], ENT_QUOTES, 'UTF-8'),
                    'name' => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'),
                );
            }


            if ($product_info['quantity'] <= 0) {
                $data['stock'] = $product_info['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $data['stock'] = $product_info['quantity'];
            } else {
                $data['stock'] = $this->language->get('text_instock');
            }

            $this->load->model('tool/image');

            if ($product_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
            } else {
                $data['popup'] = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));;
            }

            if ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
                $this->document->setOgImage($data['thumb']);
            } else {
                $data['thumb'] = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            }

            $data['images'] = array();

            $results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

            foreach ($results as $result) {
                $data['images'][] = array(
                    'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
                    'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
                );
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $data['price'] = false;
            }

            if ((float)$product_info['special']) {
                $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $data['special'] = false;
            }

            if ($this->config->get('config_tax')) {
                $data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
            } else {
                $data['tax'] = false;
            }

            $discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

            $data['discounts'] = array();

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
                );
            }

            $data['options'] = array();

            $data['is_qty_option'] = '0';
            foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                $product_option_value_data = array();


                $owq_has_stock = false;
                $owq_has_image = false;
                $owq_has_sku = false;
                $owq_discounts = array();
                $shablon_price = 99999999999999999999999999;

                if ($option['type'] == 'input_qty_td') {
                    $data['discounts'] = array();
                    $data['dicounts_unf'] = array();

                    foreach ($discounts as $discount) {
                        $owq_discounts[] = $this->currency->format($this->tax->calculate($discount['quantity'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    }
                }


                foreach ($option['product_option_value'] as $option_value) {


                    if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                        if ($option_value['quantity'] > 0) {
                            $data['is_qty_option'] = '1';
                        }

                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                        } else {
                            $price = false;
                        }

                        /*min price*/

                        if (($option_value['price'] < $shablon_price) AND ($option_value['price'] > 0)) {
                            $shablon_price = $option_value['price'];
                        }


                        if ($option_value['subtract']) $owq_has_stock = true;

                        $option_full_price = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];

                        switch ($option_value['price_prefix']) {
                            case '+':
                                $option_full_price += $option_value['price'];
                                break;
                            case '-':
                                $option_full_price -= $option_value['price'];
                                break;
                            case '*':
                                $option_full_price *= $option_value['price'];
                                break;
                            case '=':
                                $option_full_price = $option_value['price'];
                                break;
                            case 'u':
                                $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                break;
                            case 'd':
                                $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                break;
                        }

                        $option_value_discounts = array();
                        if ($option['type'] == 'input_qty_td') {
                            foreach ($discounts as $discount) {
                                $option_value_discounts[] = $this->currency->format($this->tax->calculate($option_full_price * (1.0 - $discount['price'] / 100.0), $product_info['tax_class_id'], $this->config->get('config_tax')));
                            }
                        }


                        $with_group_discount = '0';


                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                            $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $option_full_price_text = false;
                        }

                        if (!empty($option_value['sku'])) $owq_has_sku = true;
                        if (!empty($option_value['image'])) {
                            $owq_has_image = true;
                            $image_index = 0;

                            $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                            $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));

                            foreach ($data['images'] as $key => $add_image_data) {
                                if ($opt_thumb == $add_image_data['thumb']) {
                                    $image_index = $key + 1;
                                    break;
                                }
                            }

                            if ($image_index == 0) {
                                $data['images'][] /**/ = array(
                                    'thumb' => $opt_thumb,
                                    'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    'popup' => $opt_popup,
                                    'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                );
                                $image_index = count($data['images']);
                            }
                        }

                        if ($option_value['price_old'] > $option_value['price']) {
                            $bonus = $option_value['price_old'] - $option_value['price'];
                        } else {
                            $bonus = '';
                        }

                        $product_option_value_data[] = array(
                            'owq_full_price' => $option_full_price,
                            'owq_full_price_text' => $option_full_price_text,
                            'owq_title' => $option_value['title'],
                            'owq_title2' => $option_value['title2'],
                            'owq_price_old_value' => $option_value['price_old'],
                            'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
                            'owq_value_price' => $option_value['price'],
                            'owq_price_bonus' => $bonus,


                            'with_group_discount' => $with_group_discount,
                            'owq_action' => $option_value['action'],
                            'owq_preorder' => $option_value['preorder'],
                            'owq_quantity' => $option_value['quantity'],
                            'owq_has_stock' => $option_value['subtract'],
                            'owq_sku' => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                            'owq_discounts' => $option_value_discounts,


                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                //echo "<pre>";
                //var_dump($option);

                $data['options'][] = array(
                    'owq_has_stock' => $owq_has_stock,
                    'owq_has_image' => $owq_has_image,
                    'owq_has_sku' => $owq_has_sku,
                    'owq_discounts' => $owq_discounts,

                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'view' => $option['view'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }


            $data['description_shablon'] = sprintf($this->language->get('description_shablon'), $product_info['name'], round($shablon_price));

            if ($product_info['meta_title']) {
                //$this->document->setTitle($product_info['meta_title'].' '.$text_after_title. ' от ' . round($shablon_price) .' рублей.' );
                $this->document->setTitle($product_info['meta_title'] . ' ' . $text_after_title);
            } else {
                //$this->document->setTitle($product_info['name'].' '.$text_after_title. ' от ' . round($shablon_price) .' рублей.' );
                $this->document->setTitle($product_info['name'] . ' ' . $text_after_title);
            }


            if ($product_info['minimum']) {
                $data['minimum'] = $product_info['minimum'];
            } else {
                $data['minimum'] = 1;
            }

            $data['review_status'] = $this->config->get('config_review_status');

            if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
                $data['review_guest'] = true;
            } else {
                $data['review_guest'] = false;
            }

            if ($this->customer->isLogged()) {
                $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
            } else {
                $data['customer_name'] = '';
            }

            $data['reviews'] = sprintf($this->language->get('text_reviews'), $product_info['reviews']);
            $data['rating'] = (int)$product_info['rating'];

            $data['rating2'] = $this->model_catalog_review->getTotalReviewsByProductId2($this->request->get['product_id']);

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }

            $data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

            /*склеиваем одинаковые атрибуты*/
            $product_attribute_data = array();
            foreach ($data['attribute_groups'] as $attribute_group) {
                $k = '';
                foreach ($attribute_group['attribute'] as $attribute) {
                    $k++;
                    $product_attribute_data[$attribute['name']][] = array(
                        'text' => $attribute['text']
                    );

                }

            }

            $data['product_attribute_data'] = $product_attribute_data;


            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            //  $data['footer'] = $this->load->controller('common/footer');
            // $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/productview.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/productview.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/product/productview.tpl', $data));
            }

        }
    }
	public function index() {

        /**add for front edit*/
        $this->user = new User($this->registry);
        if ($this->user->isLogged()) {
            $data['userLogged'] = true;
            $data['token'] = $this->session->data['token'];
        } //$this->user->isLogged()
        else {
            $data['userLogged'] = false;
            $data['token'] = false;
        }
        $data['admin_path'] = 'admin/';



        $shablon_price = '';

        $this->load->language('product/product');

        $text_after_title = $this->language->get('text_after_title');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);
            $category_to_metrika ='';
			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

                $category_to_metrika .= $category_info['name'].'/';

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

            $data['category_to_metrika'] = $category_to_metrika;

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);

				/*for brand breadcrumbs*/
				$category_current_url =$this->url->link('product/category', 'path=' . $this->request->get['path'] . $url);
                $category_current_name =$category_info['name'];

			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}
/*
        $this->load->model('catalog/information');
        $information_info = $this->model_catalog_information->getInformation(9);

        $data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
*/

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');
        $this->load->model('catalog/review');


        $data['text_delivery'] = $this->language->get('text_delivery');


        $product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {

            $product_info['reviews'] = (int)$this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }


            if (isset($product_info['manufacturer']) && !empty($product_info['manufacturer']) && (isset($category_current_url))) {

                $manufacturer_key = $this->model_catalog_manufacturer->getManufacturersAliasById($product_info['manufacturer_id']);
                    if ($manufacturer_key && isset($manufacturer_key)) {
                        $data['breadcrumbs'][] = array(
                            'text' => $category_current_name.' '.$product_info['manufacturer'],
                            'href' => $category_current_url .  $manufacturer_key .'/'
                        );
                    }
            }


            $data['breadcrumbs'][] = array(
                'text' => $product_info['name'],
                'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
            );


            $this->document->setDescription($product_info['meta_description']);
            $this->document->setKeywords($product_info['meta_keyword']);
            $this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/locale/' . $this->session->data['language'] . '.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

            if ($product_info['meta_h1']) {
                $data['heading_title'] = $product_info['meta_h1'];
            } else {
                $data['heading_title'] = $product_info['name'];
            }

            $data['text_select'] = $this->language->get('text_select');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_stock'] = $this->language->get('text_stock');
            $data['text_discount'] = $this->language->get('text_discount');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
            $data['text_write'] = $this->language->get('text_write');
            $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
            $data['text_note'] = $this->language->get('text_note');
            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_related'] = $this->language->get('text_related');
            $data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
            $data['text_loading'] = $this->language->get('text_loading');

            $data['entry_qty'] = $this->language->get('entry_qty');
            $data['entry_name'] = $this->language->get('entry_name');
            $data['entry_review'] = $this->language->get('entry_review');
            $data['entry_rating'] = $this->language->get('entry_rating');
            $data['entry_good'] = $this->language->get('entry_good');
            $data['entry_bad'] = $this->language->get('entry_bad');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_upload'] = $this->language->get('button_upload');
            $data['button_continue'] = $this->language->get('button_continue');

            $this->load->model('catalog/review');

            $data['tab_description'] = $this->language->get('tab_description');
            $data['tab_attribute'] = $this->language->get('tab_attribute');
            $data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

            $data['product_id'] = (int)$this->request->get['product_id'];
            $data['manufacturer'] = $product_info['manufacturer'];
            $data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
            $data['model'] = $product_info['model'];
            $data['reward'] = $product_info['reward'];
            $data['points'] = $product_info['points'];
            $data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
            $data['description2'] = html_entity_decode($product_info['description2'], ENT_QUOTES, 'UTF-8');






            /*статус ствара*/

            $product_statuses = array(
                '0'=>  array(
                    'kod' => '0',
                    'name' => 'НЕТ УКАЗАН'
                ),
                '1'=>  array(
                    'kod' => '1',
                    'name' => 'Снято с производства'
                ),
                '2'=>  array(
                    'kod' => '2',
                    'name' => 'Выведен из ассортимента'
                ),
                '3'=>  array(
                    'kod' => '3',
                    'name' => 'Акциия завершена'
                )
            );

            $data['product_status'] = $product_info['product_status'];

            if (empty($product_info['product_status'])) $product_info['product_status']='0';

            $data['product_status_text'] = $product_statuses[$product_info['product_status']]['name'];






            if (isset($this->session->data['compare']) and in_array($product_id, $this->session->data['compare'])) {

                $data['compare_status'] = '1';
            } else {
                $data['compare_status'] = false;
            }

            $data['description_places'] = array();

            $results = $this->model_catalog_product->getProductDescriptionPlaces($product_id);

            foreach ($results as $result) {
                $data['description_places'][$result['place']] = array(
                    'descr' => html_entity_decode($result['descr'], ENT_QUOTES, 'UTF-8'),
                    'name' => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'),
                );
            }


            if ($product_info['quantity'] <= 0) {
                $data['stock'] = $product_info['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $data['stock'] = $product_info['quantity'];
            } else {
                $data['stock'] = $this->language->get('text_instock');
            }

            $this->load->model('tool/image');

            if ($product_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
            } else {
                $data['popup'] = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));;
            }

            if ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
                $this->document->setOgImage($data['thumb']);
            } else {
                $data['thumb'] = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            }

            $data['images'] = array();

            $results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

            foreach ($results as $result) {
                $data['images'][] = array(
                    'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
                    'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
                );
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $data['price'] = false;
            }

            if ((float)$product_info['special']) {
                $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $data['special'] = false;
            }

            if ($this->config->get('config_tax')) {
                $data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
            } else {
                $data['tax'] = false;
            }

            $discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

            $data['discounts'] = array();

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
                );
            }

            $data['options'] = array();

            $data['is_qty_option'] = '0';
            foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                $product_option_value_data = array();


                $owq_has_stock = false;
                $owq_has_image = false;
                $owq_has_sku = false;
                $owq_discounts = array();
                $shablon_price = 99999999999999999999999999;

                if ($option['type'] == 'input_qty_td') {
                    $data['discounts'] = array();
                    $data['dicounts_unf'] = array();

                    foreach ($discounts as $discount) {
                        $owq_discounts[] = $this->currency->format($this->tax->calculate($discount['quantity'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    }
                }


                foreach ($option['product_option_value'] as $option_value) {


                    if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                        if ($option_value['quantity'] > 0) {
                            $data['is_qty_option'] = '1';
                        }

                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                        } else {
                            $price = false;
                        }

                        /*min price*/

                        if (($option_value['price'] < $shablon_price) AND ($option_value['price']> 0)) {
                            $shablon_price = $option_value['price'];
                        }


                        if ($option_value['subtract']) $owq_has_stock = true;

                        $option_full_price = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];

                        switch ($option_value['price_prefix']) {
                            case '+':
                                $option_full_price += $option_value['price'];
                                break;
                            case '-':
                                $option_full_price -= $option_value['price'];
                                break;
                            case '*':
                                $option_full_price *= $option_value['price'];
                                break;
                            case '=':
                                $option_full_price = $option_value['price'];
                                break;
                            case 'u':
                                $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                break;
                            case 'd':
                                $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                break;
                        }

                        $option_value_discounts = array();
                        if ($option['type'] == 'input_qty_td') {
                            foreach ($discounts as $discount) {
                                $option_value_discounts[] = $this->currency->format($this->tax->calculate($option_full_price * (1.0 - $discount['price'] / 100.0), $product_info['tax_class_id'], $this->config->get('config_tax')));
                            }
                        }


                        $with_group_discount = '0';


                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                            $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $option_full_price_text = false;
                        }

                        if (!empty($option_value['sku'])) $owq_has_sku = true;
                        if (!empty($option_value['image'])) {
                            $owq_has_image = true;
                            $image_index = 0;

                            $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                            $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));

                            foreach ($data['images'] as $key => $add_image_data) {
                                if ($opt_thumb == $add_image_data['thumb']) {
                                    $image_index = $key + 1;
                                    break;
                                }
                            }

                            if ($image_index == 0) {
                                $data['images'][] /**/ = array(
                                    'thumb' => $opt_thumb,
                                    'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    'popup' => $opt_popup,
                                    'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                );
                                $image_index = count($data['images']);
                            }
                        }

                        if ($option_value['price_old']>$option_value['price']) {
                            $bonus = $option_value['price_old'] - $option_value['price'];
                        } else {
                            $bonus ='';
                        }

                        $product_option_value_data[] = array(
                            'owq_full_price' => $option_full_price,
                            'owq_full_price_text' => $option_full_price_text,
                            'owq_title' => $option_value['title'],
                            'owq_title2' => $option_value['title2'],
                            'owq_price_old_value' => $option_value['price_old'],
                            'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
                            'owq_value_price' => $option_value['price'],
                            'owq_price_bonus' => $bonus,


                            'with_group_discount' => $with_group_discount,
                            'owq_action' => $option_value['action'],
                            'owq_preorder' => $option_value['preorder'],
                            'owq_quantity' => $option_value['quantity'],
                            'owq_has_stock' => $option_value['subtract'],
                            'owq_sku' => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                            'owq_discounts' => $option_value_discounts,


                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                //echo "<pre>";
                //var_dump($option);

                $data['options'][] = array(
                    'owq_has_stock' => $owq_has_stock,
                    'owq_has_image' => $owq_has_image,
                    'owq_has_sku' => $owq_has_sku,
                    'owq_discounts' => $owq_discounts,

                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'view' => $option['view'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }


            $data['description_shablon'] =   sprintf($this->language->get('description_shablon'), $product_info['name'], round($shablon_price));
            $data['shablon_price'] = round($shablon_price);
            if ($product_info['meta_title']) {
                //$this->document->setTitle($product_info['meta_title'].' '.$text_after_title. ' от ' . round($shablon_price) .' рублей.' );
                $this->document->setTitle($product_info['meta_title'].' '.$text_after_title );
            } else {
                //$this->document->setTitle($product_info['name'].' '.$text_after_title. ' от ' . round($shablon_price) .' рублей.' );
                $this->document->setTitle($product_info['name'].' '.$text_after_title );
            }



            if ($product_info['minimum']) {
                $data['minimum'] = $product_info['minimum'];
            } else {
                $data['minimum'] = 1;
            }

            $data['review_status'] = $this->config->get('config_review_status');

            if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
                $data['review_guest'] = true;
            } else {
                $data['review_guest'] = false;
            }

            if ($this->customer->isLogged()) {
                $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
            } else {
                $data['customer_name'] = '';
            }

            $data['reviews'] = sprintf($this->language->get('text_reviews'), $product_info['reviews']);
            $data['rating'] = (int)$product_info['rating'];

            $data['rating2'] =   $this->model_catalog_review->getTotalReviewsByProductId2($this->request->get['product_id']);

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }

            $data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

            /*склеиваем одинаковые атрибуты*/
            $product_attribute_data = array();
            foreach ($data['attribute_groups'] as $attribute_group) {
                 $k='';
                 foreach ($attribute_group['attribute'] as $attribute) {
                     $k++;
                     $product_attribute_data[$attribute['name']][] = array(
                         'text'        => $attribute['text']
                     );

                 }

             }

            $data['product_attribute_data']=$product_attribute_data;

            /*product from category*/

            if (isset($category_id)) {

                $data['products_category'] = array();

                $filter_data = array(

                    'filter_category_id' => $category_id,
                    'start' => 0,
                    'limit' => 5
                );

                $results = $this->model_catalog_product->getProducts($filter_data);

                if ($results) {

                    foreach ($results as $result) {
                        if ($result['image']) {
                            $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                        }

                        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                            $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                        } else {
                            $price = false;
                        }

                        if ((float)$result['special']) {
                            $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                        } else {
                            $special = false;
                        }

                        if ($this->config->get('config_tax')) {
                            $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                        } else {
                            $tax = false;
                        }

                        if ($this->config->get('config_review_status')) {
                            $rating = (int)$result['rating'];
                        } else {
                            $rating = false;
                        }

                        /*option*/
                        $options = array();
                        $data_price_min = 0;
                        $lable_action = 0;
                        // var_dump($this->model_catalog_product->getProductOptions($result['product_id']));

                        foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {

                            $product_option_value_data = array();

                            $owq_has_stock = false;
                            $owq_has_image = false;
                            $owq_has_sku = false;
                            $owq_discounts = array();


                            foreach ($option['product_option_value'] as $option_value) {


                                // if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                                    if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                        $price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                    } else {
                                        $price = false;
                                    }


                                    if ($data_price_min == 0 || $data_price_min > $option_value['price']) {
                                        $data_price_min = $option_value['price'];
                                    }


                                    if ($option_value['subtract']) $owq_has_stock = true;

                                    $option_full_price = (float)$result['special'] ? $result['special'] : $result['price'];

                                    switch ($option_value['price_prefix']) {
                                        case '+':
                                            $option_full_price += $option_value['price'];
                                            break;
                                        case '-':
                                            $option_full_price -= $option_value['price'];
                                            break;
                                        case '*':
                                            $option_full_price *= $option_value['price'];
                                            break;
                                        case '=':
                                            $option_full_price = $option_value['price'];
                                            break;
                                        case 'u':
                                            $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                            break;
                                        case 'd':
                                            $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                            break;
                                    }

                                    $option_value_discounts = array();


                                    if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                                        $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                    } else {
                                        $option_full_price_text = false;
                                    }

                                    if (!empty($option_value['sku'])) $owq_has_sku = true;
                                    if (!empty($option_value['image'])) {
                                        $owq_has_image = true;
                                        $image_index = 0;

                                        $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                                        $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));


                                        if ($image_index == 0) {
                                            $data['images'][] /**/ = array(
                                                'thumb' => $opt_thumb,
                                                'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                                'popup' => $opt_popup,
                                                'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                            );
                                            $image_index = count($data['images']);
                                        }
                                    }

                                    if ($option_value['action'] == '1') $lable_action = 1;

                                    /**Опредление имени для закладок*/

                                    if (count($option['product_option_value']) < 5) {
                                        $q_del_lettet = 8;
                                    } else {
                                        $q_del_lettet = 4;
                                    }

                                    $clear_words = array('Упаковка', 'Размер', 'Объем');
                                    if (!empty($option_value['name'])) {
                                        $owq_title_short = utf8_substr(str_replace($clear_words, '', $option_value['title']), 0, $q_del_lettet);
                                    }

                                    if (!empty($option_value['title2'])) {
                                        $owq_title_short = $option_value['title2'];

                                    }
                                    if (!empty($option_value['weight']) AND $option_value['weight'] != '0') {
                                        $owq_title_short = $option_value['weight'] . 'кг';
                                    }


                                    $product_option_value_data[] = array(
                                        'owq_full_price' => $option_full_price,
                                        'owq_full_price_text' => $option_full_price_text,
                                        'owq_title_short' => $owq_title_short,
                                        'owq_price_old_value' => $option_value['price_old'],
                                        'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $result['tax_class_id'], $this->config->get('config_tax'))),
                                        'owq_action' => $option_value['action'],
                                        'owq_preorder' => $option_value['preorder'],
                                        'owq_quantity' => $option_value['quantity'],
                                        'owq_has_stock' => $option_value['subtract'],
                                        'owq_sku' => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                                        'owq_discounts' => $option_value_discounts,
                                        'owq_title2' => $option_value['title2'],
                                        'owq_title' => $option_value['title'],


                                        'product_option_value_id' => $option_value['product_option_value_id'],
                                        'option_value_id' => $option_value['option_value_id'],
                                        'name' => $option_value['name'],
                                        'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                        'price' => $price,
                                        'price_prefix' => $option_value['price_prefix']
                                    );

                                }
                            }


                            $options[] = array(
                                'owq_has_stock' => $owq_has_stock,
                                'owq_has_image' => $owq_has_image,
                                'owq_has_sku' => $owq_has_sku,
                                'owq_discounts' => $owq_discounts,

                                'product_option_id' => $option['product_option_id'],
                                'product_option_value' => $product_option_value_data,
                                'option_id' => $option['option_id'],
                                'name' => $option['name'],
                                'view' => $option['view'],
                                'type' => $option['type'],
                                'value' => $option['value'],
                                'required' => $option['required']
                            );

                            // echo $option['view'];
                        }


                        /*стикер*/
                        $sticker_statuses = array(
                            '0' => array(
                                'kod' => '0',
                                'name' => 'Нет'
                            ),
                            '1' => array(
                                'kod' => '1',
                                'name' => 'Хит'
                            ),
                            '2' => array(
                                'kod' => '2',
                                'name' => 'Новинка'
                            )
                        );

                        if ($result['hit'] == 1) $sticker_key = '1';
                        elseif (!empty($result['new_date'])) $sticker_key = '2';
                        else $sticker_key = '0';

                        $sticker_status = $sticker_statuses[$sticker_key];

                        /*статус ствара*/
                        if (empty($result['ean'])) $result['ean'] = '0';


                        $ean_statuses = array(
                            '0' => array(
                                'kod' => '0',
                                'name' => 'НЕТ УКАЗАН'
                            ),
                            '1' => array(
                                'kod' => '1',
                                'name' => 'Снято с производства'
                            ),
                            '2' => array(
                                'kod' => '2',
                                'name' => 'Выведен из ассортимента'
                            ),
                            '3' => array(
                                'kod' => '3',
                                'name' => 'Акциия завершена'
                            )
                        );

                        $ean_status = $ean_statuses[$result['ean']];

                        /*type of view product in list*/
                        $result_view_type = $this->model_catalog_product->getProductViewType($result['product_id']);

                        if (isset($this->session->data['compare']) and in_array($result['product_id'], $this->session->data['compare'])) {
                            $compare_status = '1';
                        } else {
                            $compare_status = false;
                        }


                        $data['products_category'][] = array(
                            'product_id' => $result['product_id'],
                            'view' => $result_view_type,
                            'compare_status' => $compare_status,
                            'thumb' => $image,
                            'name' => $result['name'],
                            'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                            'price' => $price,
                            'options' => $options,
                            'data_price_min' => $data_price_min,
                            'manufacturer' => $result['manufacturer'],
                            'model' => $result['model'],
                            'upc' => $result['upc'],
                            'sticker_status' => $sticker_status,
                            'lable_action' => $lable_action,
                            'ean_status' => $ean_status,
                            'special' => $special,
                            'tax' => $tax,
                            'rating' => $rating,
                            'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                            'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                        );
                    }
                }

            }





            $data['products'] = array();

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

			//var_dump($this->request->get['product_id']);
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }

                /*option*/
                $options = array();
                $data_price_min = 0;
                $lable_action = 0;
                // var_dump($this->model_catalog_product->getProductOptions($result['product_id']));


                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {

                    $product_option_value_data = array();

                    $owq_has_stock = false;
                    $owq_has_image = false;
                    $owq_has_sku = false;
                    $owq_discounts = array();


                    foreach ($option['product_option_value'] as $option_value) {


                        // if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {


                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $price = false;
                            }


                            if ($data_price_min == 0 || $data_price_min > $option_value['price']) {
                                $data_price_min = $option_value['price'];
                            }


                            if ($option_value['subtract']) $owq_has_stock = true;

                            $option_full_price = (float)$result['special'] ? $result['special'] : $result['price'];

                            switch ($option_value['price_prefix']) {
                                case '+':
                                    $option_full_price += $option_value['price'];
                                    break;
                                case '-':
                                    $option_full_price -= $option_value['price'];
                                    break;
                                case '*':
                                    $option_full_price *= $option_value['price'];
                                    break;
                                case '=':
                                    $option_full_price = $option_value['price'];
                                    break;
                                case 'u':
                                    $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                    break;
                                case 'd':
                                    $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                    break;
                            }

                            $option_value_discounts = array();


                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                                $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $option_full_price_text = false;
                            }

                            if (!empty($option_value['sku'])) $owq_has_sku = true;
                            if (!empty($option_value['image'])) {
                                $owq_has_image = true;
                                $image_index = 0;

                                $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                                $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));


                                if ($image_index == 0) {
                                    $data['images'][] /**/ = array(
                                        'thumb' => $opt_thumb,
                                        'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                        'popup' => $opt_popup,
                                        'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    );
                                    $image_index = count($data['images']);
                                }
                            }

                            if ($option_value['action'] == '1') $lable_action = 1;




                            /**Опредление имени для закладок*/

                            if (count($option['product_option_value']) < 5){
                                $q_del_lettet = 8;
                            }
                            else {
                                $q_del_lettet = 4;
                            }

                            $clear_words=array('Упаковка', 'Размер', 'Объем');
                            if(!empty($option_value['name'])){
                                $owq_title_short =  utf8_substr(str_replace($clear_words, '', $option_value['title']), 0, $q_del_lettet);
                            }

                            if(!empty($option_value['title2'])){
                                $owq_title_short  =  $option_value['title2'];

                            }
                            if(!empty($option_value['weight']) AND $option_value['weight']!='0') {
                                $owq_title_short =  $option_value['weight'].'кг';
                            }




                            $product_option_value_data[] = array(
                                'owq_full_price'      => $option_full_price,
                                'owq_full_price_text' => $option_full_price_text,
                                'owq_title_short'           => $owq_title_short,
                                'owq_price_old_value' => $option_value['price_old'],
                                'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $result['tax_class_id'], $this->config->get('config_tax'))),
                                'owq_action'           => $option_value['action'],
                                'owq_preorder'           => $option_value['preorder'],
                                'owq_quantity'        => $option_value['quantity'],
                                'owq_has_stock'       => $option_value['subtract'],
                                'owq_sku'             => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                                'owq_discounts'       => $option_value_discounts,
                                'owq_title2'           => $option_value['title2'],
                                'owq_title'           => $option_value['title'],


                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id'         => $option_value['option_value_id'],
                                'name'                    => $option_value['name'],
                                'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price'                   => $price,
                                'price_prefix'            => $option_value['price_prefix']
                            );
                        }
                    }


                    $options[] = array(
                        'owq_has_stock' => $owq_has_stock,
                        'owq_has_image' => $owq_has_image,
                        'owq_has_sku' => $owq_has_sku,
                        'owq_discounts' => $owq_discounts,

                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'view' => $option['view'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );

                    // echo $option['view'];
                }


                //var_dump($options);

                /*стикер*/
                $sticker_statuses = array(
                    '0'=>  array(
                        'kod' => '0',
                        'name' => 'Нет'
                    ),
                    '1'=>  array(
                        'kod' => '1',
                        'name' => 'Хит'
                    ),
                    '2'=>  array(
                        'kod' => '2',
                        'name' => 'Новинка'
                    )
                );

                if ($result['hit']==1) $sticker_key='1';
                elseif (!empty($result['new_date'])) $sticker_key='2';
                else $sticker_key='0';

                $sticker_status= $sticker_statuses[$sticker_key];
                /*статус ствара*/
                if (empty($result['ean'])) $result['ean'] = '0';


                $ean_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'НЕТ УКАЗАН'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Снято с производства'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'Выведен из ассортимента'
                    ),
                    '3' => array(
                        'kod' => '3',
                        'name' => 'Акциия завершена'
                    )
                );

                $ean_status = $ean_statuses[$result['ean']];

                /*type of view product in list*/
                $result_view_type = $this->model_catalog_product->getProductViewType($result['product_id']);

                if (isset($this->session->data['compare']) and in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'view' => $result_view_type,
                    'compare_status'       => $compare_status,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price' => $price,
                    'options' => $options,
                    'data_price_min' => $data_price_min,
                    'manufacturer' => $result['manufacturer'],
                    'model' => $result['model'],
                    'upc' => $result['upc'],
                    'sticker_status' => $sticker_status,
                    'lable_action' => $lable_action,
                    'ean_status' => $ean_status,
                    'special' => $special,
                    'tax' => $tax,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                    'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                );
            }
			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}

			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/product.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/review.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/review.tpl', $data));
		}
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}


            if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match($this->config->get('config_mail_regexp'), $this->request->post['email'])) {
                $this->error['email'] = $this->language->get('error_email');
            }

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function writeStar() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $this->load->model('catalog/review');
            $product_id = $this->request->post['product_id'];
            $rating = $this->request->post['rating'];

            if (!isset($this->session->data['product_reviews'])) $this->session->data['product_reviews'] =array();

            if (!array_search($product_id, $this->session->data['product_reviews']))
            {

                $is_review = $this->model_catalog_review->getReviewsByEmailProductId($product_id, $rating);

                if (empty($is_review)) {
                   $json['total'] = $this->model_catalog_review->addReviewStar($product_id, $rating);

                    $this->session->data['product_reviews'][$product_id]=$product_id;



                    $json['success'] = $this->language->get('text_success2');
                } else {
                    $json['success'] = $this->language->get('text_success2');
                }

            } else {
                $json['success'] = $this->language->get('text_success_isset');
            }

        }

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->language->load('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
