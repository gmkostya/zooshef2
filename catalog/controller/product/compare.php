<?php
class ControllerProductCompare extends Controller {
	public function index() {
		$this->load->language('product/compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}

		if (isset($this->request->get['remove'])) {
			$key = array_search($this->request->get['remove'], $this->session->data['compare']);

			if ($key !== false) {
				unset($this->session->data['compare'][$key]);
			}

			$this->session->data['success'] = $this->language->get('text_remove');

			$this->response->redirect($this->url->link('product/compare'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/compare')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_product'] = $this->language->get('text_product');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_image'] = $this->language->get('text_image');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_availability'] = $this->language->get('text_availability');
		$data['text_rating'] = $this->language->get('text_rating');
		$data['text_summary'] = $this->language->get('text_summary');
		$data['text_weight'] = $this->language->get('text_weight');
		$data['text_dimension'] = $this->language->get('text_dimension');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['review_status'] = $this->config->get('config_review_status');

		$data['products'] = array();

		$data['attribute_groups'] = array();

		foreach ($this->session->data['compare'] as $key => $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_compare_width'), $this->config->get('config_image_compare_height'));
				} else {
					$image = false;
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($product_info['quantity'] <= 0) {
					$availability = $product_info['stock_status'];
				} elseif ($this->config->get('config_stock_display')) {
					$availability = $product_info['quantity'];
				} else {
					$availability = $this->language->get('text_instock');
				}

				$attribute_data = array();

				$attribute_groups = $this->model_catalog_product->getProductAttributes($product_id);

				foreach ($attribute_groups as $attribute_group) {
					foreach ($attribute_group['attribute'] as $attribute) {
						$attribute_data[$attribute['attribute_id']] = $attribute['text'];
					}
				}



                //echo $result_view;


                /*option*/
                $options = array();
                $data_price_min = 0;
                $lable_action = 0;
                // var_dump($this->model_catalog_product->getProductOptions($result['product_id']));

                foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {

                    $product_option_value_data = array();

                    $owq_has_stock = false;
                    $owq_has_image = false;
                    $owq_has_sku = false;
                    $owq_discounts = array();


                    foreach ($option['product_option_value'] as $option_value) {


                        // if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $price = false;
                            }


                            if ($data_price_min == 0 || $data_price_min > $option_value['price']) {
                                $data_price_min = $option_value['price'];
                            }


                            if ($option_value['subtract']) $owq_has_stock = true;

                            $option_full_price = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];

                            switch ($option_value['price_prefix']) {
                                case '+':
                                    $option_full_price += $option_value['price'];
                                    break;
                                case '-':
                                    $option_full_price -= $option_value['price'];
                                    break;
                                case '*':
                                    $option_full_price *= $option_value['price'];
                                    break;
                                case '=':
                                    $option_full_price = $option_value['price'];
                                    break;
                                case 'u':
                                    $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                    break;
                                case 'd':
                                    $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                    break;
                            }

                            $option_value_discounts = array();


                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                                $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $option_full_price_text = false;
                            }

                            if (!empty($option_value['sku'])) $owq_has_sku = true;
                            if (!empty($option_value['image'])) {
                                $owq_has_image = true;
                                $image_index = 0;

                                $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                                $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));


                                if ($image_index == 0) {
                                    $data['images'][] /**/ = array(
                                        'thumb' => $opt_thumb,
                                        'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                        'popup' => $opt_popup,
                                        'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    );
                                    $image_index = count($data['images']);
                                }
                            }

                            if ($option_value['action'] == '1') $lable_action = 1;

                            /**Опредление имени для закладок*/

                            if (count($option['product_option_value']) < 5) {
                                $q_del_lettet = 8;
                            } else {
                                $q_del_lettet = 4;
                            }

                            $clear_words = array('Упаковка', 'Размер', 'Объем');
                            if (!empty($option_value['name'])) {
                                $owq_title_short = utf8_substr(str_replace($clear_words, '', $option_value['title']), 0, $q_del_lettet);
                            }

                            if (!empty($option_value['title2'])) {
                                $owq_title_short = $option_value['title2'];

                            }
                            if (!empty($option_value['weight']) AND $option_value['weight'] != '0') {
                                $owq_title_short = $option_value['weight'] . 'кг';
                            }


                            $product_option_value_data[] = array(
                                'owq_full_price' => $option_full_price,
                                'owq_full_price_text' => $option_full_price_text,
                                'owq_title_short' => $owq_title_short,
                                'owq_price_old_value' => $option_value['price_old'],
                                'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
                                'owq_action' => $option_value['action'],
                                'owq_preorder' => $option_value['preorder'],
                                'owq_quantity' => $option_value['quantity'],
                                'owq_has_stock' => $option_value['subtract'],
                                'owq_sku' => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                                'owq_discounts' => $option_value_discounts,
                                'owq_title2' => $option_value['title2'],
                                'owq_title' => $option_value['title'],


                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }


                    $options[] = array(
                        'owq_has_stock' => $owq_has_stock,
                        'owq_has_image' => $owq_has_image,
                        'owq_has_sku' => $owq_has_sku,
                        'owq_discounts' => $owq_discounts,

                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'view' => $option['view'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );


                }

                /*стикер*/
                $sticker_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'Нет'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Хит'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'Новинка'
                    )
                );

                if ($product_info['hit'] == 1) $sticker_key = '1';
                elseif (!empty($product_info['new_date'])) $sticker_key = '2';
                else $sticker_key = '0';

                $sticker_status = $sticker_statuses[$sticker_key];

                /*статус ствара*/

                $ean_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'НЕТ УКАЗАН'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Снято с производства'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'Выведен из ассортимента'
                    ),
                    '3' => array(
                        'kod' => '3',
                        'name' => 'Акциия завершена'
                    )
                );
                if (empty($product_info['ean'])) $product_info['ean'] = '0';

                $ean_status = $ean_statuses[$product_info['ean']];

                /*type of view product in list*/
                $result_view_type = $this->model_catalog_product->getProductViewType($product_info['product_id']);


                if (isset($this->session->data['compare']) && in_array($product_info['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                //  var_dump($this->session->data['compare']);
                //echo $result_view_type.'-'.$product_info['product_id'].'/';


				$data['products'][$product_id] = array(
					'product_id'   => $product_info['product_id'],
					'name'         => $product_info['name'],
					'thumb'        => $image,
                    'options' => $options,
					'price'        => $price,
					'special'      => $special,
					'description'  => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
					'model'        => $product_info['model'],
					'manufacturer' => $product_info['manufacturer'],
					'availability' => $availability,
					'minimum'      => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
					'rating'       => (int)$product_info['rating'],
					'reviews'      => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'weight'       => $this->weight->format($product_info['weight'], $product_info['weight_class_id']),
					'length'       => $this->length->format($product_info['length'], $product_info['length_class_id']),
					'width'        => $this->length->format($product_info['width'], $product_info['length_class_id']),
					'height'       => $this->length->format($product_info['height'], $product_info['length_class_id']),
					'attribute'    => $attribute_data,
					'href'         => $this->url->link('product/product', 'product_id=' . $product_id),
					'remove'       => $this->url->link('product/compare', 'remove=' . $product_id)
				);

				foreach ($attribute_groups as $attribute_group) {
					$data['attribute_groups'][$attribute_group['attribute_group_id']]['name'] = $attribute_group['name'];

					foreach ($attribute_group['attribute'] as $attribute) {
						$data['attribute_groups'][$attribute_group['attribute_group_id']]['attribute'][$attribute['attribute_id']]['name'] = $attribute['name'];
					}
				}
			} else {
				unset($this->session->data['compare'][$key]);
			}
		}

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/compare.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/compare.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/compare.tpl', $data));
		}
	}

	public function add() {
		$this->load->language('product/compare');

		$json = array();

		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (!in_array($this->request->post['product_id'], $this->session->data['compare'])) {
				if (count($this->session->data['compare']) >= 4) {
					array_shift($this->session->data['compare']);
				}

				$this->session->data['compare'][] = $this->request->post['product_id'];
			}

			$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('product/compare'));

			$json['total'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
