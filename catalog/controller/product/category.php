<?php
class ControllerProductCategory extends Controller {
	public function index() {

	    /**add for front edit*/
	    $this->user = new User($this->registry);
        if ($this->user->isLogged()) {
            $data['userLogged'] = true;
            $data['token'] = $this->session->data['token'];
            //echo  $data['userLogged'];
        } //$this->user->isLogged()
        else {
            $data['userLogged'] = false;
            $data['token'] = false;
        }


        $data['admin_path'] = 'admin/';


        $this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}



		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
            /**myocfilter**/
            if ($filter_ocfilter) {
                $url .= '&filter_ocfilter=' . (string)$filter_ocfilter;
            }
            /***/


            $path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {

            $data['seo_description_up'] = '';
            $data['seo_description_middle'] = '';


            if ($category_info['meta_title']) {
                $this->document->setTitle($category_info['meta_title']);
            } else {
                $this->document->setTitle($category_info['name']);
            }

            $this->document->setDescription($category_info['meta_description']);
            $this->document->setKeywords($category_info['meta_keyword']);

            if ($category_info['meta_h1']) {
                $data['heading_title'] = $category_info['meta_h1'];
            } else {
                $data['heading_title'] = $category_info['name'];
            }

            $data['category_name'] = $category_info['name'];

            $data['text_refine'] = $this->language->get('text_refine');
            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_quantity'] = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list'] = $this->language->get('button_list');
            $data['button_grid'] = $this->language->get('button_grid');


            if ($category_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                $this->document->setOgImage($data['thumb']);
            } else {
                $data['thumb'] = '';
            }

            $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
            $data['description_up'] = html_entity_decode($category_info['description_up'], ENT_QUOTES, 'UTF-8');
            $data['compare'] = $this->url->link('product/compare');

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['categories'] = array();

            $results = $this->model_catalog_category->getCategories($category_id);

            foreach ($results as $result) {
                $filter_data = array(
                    'filter_category_id' => $result['category_id'],
                    'filter_sub_category' => true
                );

                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                }


                // $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));

                $data['categories'][] = array(
                    'image' => $image,
                    'name' => $result['name'],
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
                );
            }

            $data['products'] = array();

            $filter_data = array(
                'filter_category_id' => $category_id,
                'filter_filter' => $filter,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit,
                // 'filter_sub_category' => true
            );

            $product_total = $this->model_catalog_product->getTotalProducts($filter_data);

            $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }

                //echo $result_view;


                /*option*/
                $options = array();
                $data_price_min = 0;
                $lable_action = 0;
                // var_dump($this->model_catalog_product->getProductOptions($result['product_id']));

                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {

                    $product_option_value_data = array();

                    $owq_has_stock = false;
                    $owq_has_image = false;
                    $owq_has_sku = false;
                    $owq_discounts = array();


                    foreach ($option['product_option_value'] as $option_value) {


                        // if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $price = false;
                            }


                            if ($data_price_min == 0 || $data_price_min > $option_value['price']) {
                                $data_price_min = $option_value['price'];
                            }


                            if ($option_value['subtract']) $owq_has_stock = true;

                            $option_full_price = (float)$result['special'] ? $result['special'] : $result['price'];

                            switch ($option_value['price_prefix']) {
                                case '+':
                                    $option_full_price += $option_value['price'];
                                    break;
                                case '-':
                                    $option_full_price -= $option_value['price'];
                                    break;
                                case '*':
                                    $option_full_price *= $option_value['price'];
                                    break;
                                case '=':
                                    $option_full_price = $option_value['price'];
                                    break;
                                case 'u':
                                    $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                    break;
                                case 'd':
                                    $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                    break;
                            }

                            $option_value_discounts = array();


                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                                $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $option_full_price_text = false;
                            }

                            if (!empty($option_value['sku'])) $owq_has_sku = true;
                            if (!empty($option_value['image'])) {
                                $owq_has_image = true;
                                $image_index = 0;

                                $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                                $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));


                                if ($image_index == 0) {
                                    $data['images'][] /**/ = array(
                                        'thumb' => $opt_thumb,
                                        'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                        'popup' => $opt_popup,
                                        'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    );
                                    $image_index = count($data['images']);
                                }
                            }

                            if ($option_value['action'] == '1') $lable_action = 1;

                            /**Опредление имени для закладок*/

                            if (count($option['product_option_value']) < 5) {
                                $q_del_lettet = 8;
                            } else {
                                $q_del_lettet = 4;
                            }

                            $clear_words = array('Упаковка', 'Размер', 'Объем');
                            if (!empty($option_value['name'])) {
                                $owq_title_short = utf8_substr(str_replace($clear_words, '', $option_value['title']), 0, $q_del_lettet);
                            }

                            if (!empty($option_value['title2'])) {
                                $owq_title_short = $option_value['title2'];

                            }
                            if (!empty($option_value['weight']) AND $option_value['weight'] != '0') {
                                $owq_title_short = $option_value['weight'] . 'кг';
                            }


                            $product_option_value_data[] = array(
                                'owq_full_price' => $option_full_price,
                                'owq_full_price_text' => $option_full_price_text,
                                'owq_title_short' => $owq_title_short,
                                'owq_price_old_value' => $option_value['price_old'],
                                'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $result['tax_class_id'], $this->config->get('config_tax'))),
                                'owq_action' => $option_value['action'],
                                'owq_preorder' => $option_value['preorder'],
                                'owq_quantity' => $option_value['quantity'],
                                'owq_has_stock' => $option_value['subtract'],
                                'owq_sku' => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                                'owq_discounts' => $option_value_discounts,
                                'owq_title2' => $option_value['title2'],
                                'owq_title' => $option_value['title'],


                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }


                    $options[] = array(
                        'owq_has_stock' => $owq_has_stock,
                        'owq_has_image' => $owq_has_image,
                        'owq_has_sku' => $owq_has_sku,
                        'owq_discounts' => $owq_discounts,

                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'view' => $option['view'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );


                }

                /*стикер*/
                $sticker_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'Нет'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Хит'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'Новинка'
                    )
                );

                if ($result['hit'] == 1) $sticker_key = '1';
                elseif (!empty($result['new_date'])) $sticker_key = '2';
                else $sticker_key = '0';

                $sticker_status = $sticker_statuses[$sticker_key];

                /*статус ствара*/

                $ean_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'НЕТ УКАЗАН'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Снято с производства'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'Выведен из ассортимента'
                    ),
                    '3' => array(
                        'kod' => '3',
                        'name' => 'Акциия завершена'
                    )
                );
                if (empty($result['ean'])) $result['ean'] = '0';

                $ean_status = $ean_statuses[$result['ean']];

                /*type of view product in list*/
                $result_view_type = $this->model_catalog_product->getProductViewType($result['product_id']);


                if (isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                //  var_dump($this->session->data['compare']);
                //echo $result_view_type.'-'.$result['product_id'].'/';
                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'view' => $result_view_type,
                    'thumb' => $image,
                    'compare_status' => $compare_status,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price' => $price,
                    'options' => $options,
                    'data_price_min' => $data_price_min,
                    'manufacturer' => $result['manufacturer'],
                    'model' => $result['model'],
                    'upc' => $result['upc'],
                    'sticker_status' => $sticker_status,
                    'lable_action' => $lable_action,
                    'ean_status' => $ean_status,
                    'special' => $special,
                    'tax' => $tax,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                    'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                );
            }

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'default',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=default&order=DESC' . $url)
            );
            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_hit_desc'),
                'value' => 'p.hit-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.hit&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_action_desc'),
                'value' => 'action-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=action&order=DESC' . $url)
            );


            /*
                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_name_asc'),
                            'value' => 'pd.name-ASC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
                        );

                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_name_desc'),
                            'value' => 'pd.name-DESC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
                        );

                        if ($this->config->get('config_review_status')) {
                            $data['sorts'][] = array(
                                'text'  => $this->language->get('text_rating_desc'),
                                'value' => 'rating-DESC',
                                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
                            );

                            $data['sorts'][] = array(
                                'text'  => $this->language->get('text_rating_asc'),
                                'value' => 'rating-ASC',
                                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
                            );
                        }

                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_model_asc'),
                            'value' => 'p.model-ASC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
                        );

                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_model_desc'),
                            'value' => 'p.model-DESC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
                        );
            */
            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get('config_product_limit'), 48));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
                );
            }

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $pagination2 = new Pagination2();
            $pagination2->total = $product_total;
            $pagination2->page = $page;
            $pagination2->limit = $limit;
            $pagination2->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination2'] = $pagination2->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));


            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;

            $data['continue'] = $this->url->link('common/home');


            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html


            // OCFilter canonical to mabufacturer

            $url_canonical = '';
            if (isset($this->request->get['filter_ocfilter'])) {
                $og_url = (isset($this->request->server['HTTPS']) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI']) - 1));

                $a = explode('?', $og_url);
                $url_canonical = $a[0];
                $this->document->setKeywords('');
            }
            // OCFilter end

            if (empty($url_canonical)) {
               $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');
            } else {
                $this->document->addLink($url_canonical, 'canonical');
            }

            if ($page == 1) {

            } elseif ($page == 2) {
                $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'prev');
               // $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');
            } else {
                $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page - 1).$url, 'SSL'), 'prev');
            }

            if ($limit && ceil($product_total / $limit) > $page) {
                $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page + 1).$url, 'SSL'), 'next');
            }


            $data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');




            // Set the last category breadcrumb
            $data['breadcrumbs'][] = array(
                'text' => $data['heading_title'],
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
            );




            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
}
