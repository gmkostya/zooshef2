<?php
class ControllerModuleManufacturerCategory extends Controller {
	public function index($manufacturer_id) {

	   // echo $manufacturer_id;
        $this->load->language('module/manufacturer_category');
        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('module/manufacturer_category');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');


        if (isset($manufacturer_id)) {
            $manufacturer_id = (int)$manufacturer_id;
        } else {
            $manufacturer_id = 0;
        }

        if (!empty($manufacturer_id)) {

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

            $manCategories = $this->model_module_manufacturer_category->getCategories($manufacturer_id);

           // var_dump($manCategories);

            $data['description'] = html_entity_decode($manufacturer_info['description'], ENT_QUOTES, 'UTF-8');

            $manCategories = $this->model_module_manufacturer_category->getCategories($manufacturer_id);

            //var_dump($manCategories);
            foreach ($manCategories as $manCat) {
               /**костыль исключения категории все товары ID 487 */
               if ($manCat!=487) {
                   $manSeoUrl = $this->model_module_manufacturer_category->getSeoUrl($manCat, $manufacturer_id);
                   $categoryInfo = $this->model_catalog_category->getCategory($manCat);
                   $data['man_categories'][$manCat] = array(
                       'url' => $manSeoUrl,
                       'image' => $this->model_tool_image->resize($categoryInfo['image'], '50', '50'),
                       'name' => $categoryInfo['name'],
                       'man_name' => $manufacturer_info['name'],
                   );
               }
            }
        }

     //   var_dump($data['man_categories']);

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer_category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer_category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/manufacturer_category.tpl', $data);
		}

	}
}
