<?php
class ControllerModuleManufacturer extends Controller {
	public function index($setting) {
		$this->load->language('module/manufacturer');
        $this->load->language('product/manufacturer');

        $this->load->model('catalog/manufacturer');

        $this->load->model('tool/image');


        $data['heading_title'] = $this->language->get('heading_title');

        $data['categories'] = array();
        $abc ='';
        foreach (range(chr(0xC0), chr(0xDF)) as $b)
            $abc .= iconv('CP1251', 'UTF-8', $b).'_';

        $letter_scopes = array (
            'A_B_C_D_E' => 'A_B_C_D_E',
            'F_G_H_I_J' => 'F_G_H_I_J',
            'K_L_M_N_O' => 'K_L_M_N_O',
            'P_Q_R_S_T' => 'P_Q_R_S_T',
            'U_V_W_X_Y_Z' => 'U_V_W_X_Y_Z',
            'А_Я' => $abc
        );



        $results = $this->model_catalog_manufacturer->getManufacturers();

        if ($results) {
            foreach ($results as $result) {
                $name = $result['name'];

                if (is_numeric(utf8_substr($name, 0, 1))) {
                    $key = '0-9';
                } else {

                        $first_letter = utf8_substr(utf8_strtoupper($name), 0, 1);

                    foreach ($letter_scopes AS $k=>$skope) {
                         $skope;

                        if (stristr($skope, $first_letter)) {
                           $key = $k;

                        }
                    }
                }

                if (!isset($data['categories'][$key])) {
                    $data['categories'][$key]['name'] = $key;
                }

                $data['categories'][$key]['manufacturer'][] = array(
                    'name' => $name,
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
                );
            }        }


       // krsort($data['categories']);

//var_dump($data['categories']);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer.tpl', $data);
			} else {
				return $this->load->view('default/template/module/manufacturer.tpl', $data);
			}
	}
}
