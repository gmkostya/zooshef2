<?php
class ControllerModuleManufacturerPopular extends Controller {
	public function index($setting) {
		$this->load->language('module/manufacturer_popular');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');
        $data['heading_title'] = $this->language->get('heading_title');

        //var_dump($setting);
        $filter_data = array(
            'in_home'  => '1',
            'limit' => $setting['limit'],
            'start' => '0',
            'sort' => 'sort_order'
        );

        $results = $this->model_catalog_manufacturer->getManufacturers($filter_data);
        //var_dump($results);
        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = 'image/'.$result['image'];
                } else {
                    $image = 'image/no_image.png';
                }
                $data['manufacturers'][] = array(
                    'image' => $image,
                    'name' => $result['name'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
                );
            }
        }

//       / var_dump($data['manufacturers']);
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer_popular.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer_popular.tpl', $data);
			} else {
				return $this->load->view('default/template/module/manufacturer_popular.tpl', $data);
			}
	}
}
