<?php
class ControllerModuleLatest extends Controller {
	public function index($setting) {

        /**add for front edit*/
        $this->user = new User($this->registry);
        if ($this->user->isLogged()) {
            $data['userLogged'] = true;
            $data['token'] = $this->session->data['token'];
        } //$this->user->isLogged()
        else {
            $data['userLogged'] = false;
            $data['token'] = false;
        }
        $data['admin_path'] = 'admin/';



        $this->load->language('module/latest');

		$data['heading_title'] = $this->language->get('heading_title');
        $data['heading_title'] = $setting['name'];

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_product->getLatestProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }

                /*option*/
                $options = array();
                $data_price_min =0;
                $lable_action = 0;
                // var_dump($this->model_catalog_product->getProductOptions($result['product_id']));

                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {

                    $product_option_value_data = array();

                    $owq_has_stock = false;
                    $owq_has_image = false;
                    $owq_has_sku = false;
                    $owq_discounts = array();


                    foreach ($option['product_option_value'] as $option_value) {


                        // if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $price = false;
                            }


                            if ($data_price_min==0 || $data_price_min > $option_value['price'] ) {
                                $data_price_min = $option_value['price'];
                            }


                            if ($option_value['subtract']) $owq_has_stock = true;

                            $option_full_price = (float)$result['special'] ? $result['special'] : $result['price'];

                            switch ($option_value['price_prefix']) {
                                case '+':
                                    $option_full_price += $option_value['price'];
                                    break;
                                case '-':
                                    $option_full_price -= $option_value['price'];
                                    break;
                                case '*':
                                    $option_full_price *= $option_value['price'];
                                    break;
                                case '=':
                                    $option_full_price = $option_value['price'];
                                    break;
                                case 'u':
                                    $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                    break;
                                case 'd':
                                    $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                    break;
                            }

                            $option_value_discounts = array();


                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/ ) {
                                $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $option_full_price_text = false;
                            }

                            if (!empty($option_value['sku'])) $owq_has_sku = true;
                            if (!empty($option_value['image'])) {
                                $owq_has_image = true;
                                $image_index = 0;

                                $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                                $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));



                                if ($image_index == 0) {
                                    $data['images'][] /**/ = array(
                                        'thumb' => $opt_thumb,
                                        'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                        'popup' => $opt_popup,
                                        'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    );
                                    $image_index = count($data['images']);
                                }
                            }

                            if ($option_value['action']=='1') $lable_action=1;

                            /**Опредление имени для закладок*/

                            if (count($option['product_option_value']) < 5){
                                $q_del_lettet = 8;
                            }
                            else {
                                $q_del_lettet = 4;
                            }

                            $clear_words=array('Упаковка', 'Размер', 'Объем');
                            if(!empty($option_value['name'])){
                                $owq_title_short =  utf8_substr(str_replace($clear_words, '', $option_value['title']), 0, $q_del_lettet);
                            }

                            if(!empty($option_value['title2'])){
                                $owq_title_short  =  $option_value['title2'];

                            }
                            if(!empty($option_value['weight']) AND $option_value['weight']!='0') {
                                $owq_title_short =  $option_value['weight'].'кг';
                            }




                            $product_option_value_data[] = array(
                                'owq_full_price'      => $option_full_price,
                                'owq_full_price_text' => $option_full_price_text,
                                'owq_title_short'           => $owq_title_short,
                                'owq_price_old_value' => $option_value['price_old'],
                                'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $result['tax_class_id'], $this->config->get('config_tax'))),
                                'owq_action'           => $option_value['action'],
                                'owq_preorder'           => $option_value['preorder'],
                                'owq_quantity'        => $option_value['quantity'],
                                'owq_has_stock'       => $option_value['subtract'],
                                'owq_sku'             => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                                'owq_discounts'       => $option_value_discounts,
                                'owq_title2'           => $option_value['title2'],
                                'owq_title'           => $option_value['title'],


                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id'         => $option_value['option_value_id'],
                                'name'                    => $option_value['name'],
                                'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price'                   => $price,
                                'price_prefix'            => $option_value['price_prefix']
                            );
                        }
                    }


                    $options[] = array(
                        'owq_has_stock' => $owq_has_stock,
                        'owq_has_image' => $owq_has_image,
                        'owq_has_sku'   => $owq_has_sku,
                        'owq_discounts' => $owq_discounts,

                        'product_option_id'    => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id'            => $option['option_id'],
                        'name'                 => $option['name'],
                        'view'                 => $option['view'],
                        'type'                 => $option['type'],
                        'value'                => $option['value'],
                        'required'             => $option['required']
                    );

                    // echo $option['view'];
                }


                /*стикер*/
                $sticker_statuses = array(
                    '0'=>  array(
                        'kod' => '0',
                        'name' => 'Нет'
                    ),
                    '1'=>  array(
                        'kod' => '1',
                        'name' => 'Хит'
                    ),
                    '2'=>  array(
                        'kod' => '2',
                        'name' => 'Новинка'
                    )
                );

                if ($result['hit']==1) $sticker_key='1';
                elseif (!empty($result['new_date'])) $sticker_key='2';
                else $sticker_key='0';

                $sticker_status= $sticker_statuses[$sticker_key];

                /*статус ствара*/
                if (empty($result['ean'])) $result['ean']='0';



                $ean_statuses = array(
                    '0'=>  array(
                        'kod' => '0',
                        'name' => 'НЕТ УКАЗАН'
                    ),
                    '1'=>  array(
                        'kod' => '1',
                        'name' => 'Снято с производства'
                    ),
                    '2'=>  array(
                        'kod' => '2',
                        'name' => 'Выведен из ассортимента'
                    ),
                    '3'=>  array(
                        'kod' => '3',
                        'name' => 'Акциия завершена'
                    )
                );

                $ean_status= $ean_statuses[$result['ean']];
                /*type of view product in list*/
                $result_view_type = $this->model_catalog_product->getProductViewType($result['product_id']);


                if (isset($this->session->data['compare']) and in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                $data['products'][] = array(
                    'product_id'  => $result['product_id'],
                    'view' => $result_view_type,
                    'compare_status'       => $compare_status,


                    'thumb'       => $image,
                    'name'        => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'options'       => $options,
                    'data_price_min' => $data_price_min,
                    'manufacturer' => $result['manufacturer'],
                    'model' => $result['model'],
                    'upc' => $result['upc'],
                    'sticker_status' => $sticker_status,
                    'lable_action' => $lable_action,
                    'ean_status' =>  $ean_status,
                    'special'     => $special,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'reviews'      => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                    'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/latest.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/latest.tpl', $data);
			} else {
				return $this->load->view('default/template/module/latest.tpl', $data);
			}
		}
	}
}
