<?php
class ControllerCommonDiscountInfo extends Controller {
	public function index($setting) {




		$this->language->load('commom/discountinfo');
		
		$this->load->model('catalog/discountinfo');
		$this->load->model('catalog/manufacturer');
		$this->load->model('account/customer_group');
        $data['text_empty'] = $this->language->get('text_empty');
		$results = $this->model_catalog_manufacturer->getManufacturersWithDiscount();
		$data['manufacturers'] = array();

		//var_dump($results);

		foreach ($results as $result) {
			$data['manufacturers'][] = array(
				'name'  => $result['name'],

			);

		}

		$customer_groups = $this->model_account_customer_group->getCustomerGroups();
		$data['customer_groups'] = array();
		$key = array(
			0 => 'one',
			1 => 'two',
			2 => 'three',
			3 => 'four',
			);
		$k=0;
		foreach ($customer_groups as $customer_group) {

			if ($customer_group['name']!='Default') {
				$data['customer_groups'][] = array(
					'key' => $key[$k],
					'name' => $customer_group['name'],
					'description' => $customer_group['description'],
				);
				$k++;
			}

		}




		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/discountinfo.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/discountinfo.tpl', $data);
		} else {
			return $this->load->view('default/template/common/discountinfo.tpl', $data);
		}
	
	}
}
?>
