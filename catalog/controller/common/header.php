<?php
class ControllerCommonHeader extends Controller {
	public function index() {


        // Analytics
           if(($_SERVER['REQUEST_URI'])=='/reptilii/paljudariumi_i_aksessuari/aksessuari/'){
               header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
               $this->response->redirect('/reptilii/paljudariumi_i_aksessuari/acsessuari/', 301);
           }
           if(($_SERVER['REQUEST_URI'])=='/reptilii/paljudariumi_i_aksessuari/aksessuari/ferplast'){
               header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
               $this->response->redirect('/reptilii/paljudariumi_i_aksessuari/acsessuari/ferplast', 301);
           }

        if(($_SERVER['REQUEST_URI'])=='/hills_koshki/'){
            header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
            $this->response->redirect('/tovary_dlya_koshek/korm_dlja_koshek/hills/', 301);
        }
        if(($_SERVER['REQUEST_URI'])=='/hills_sobaki/'){
            header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
            $this->response->redirect('/tovary_dlya_sobak/korm-dlya-sobak/hills/', 301);
        }




        /* if(($_SERVER['REQUEST_URI'])=='/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/oshejniki/s-brand-is-bayer/'){
             header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
             $this->response->redirect('/tovary_dlya_koshek/sredstva_ot_bloh_i_kleshhej/oshejniki/bayer/', 301);
         }
         if(($_SERVER['REQUEST_URI'])=='/tovary_dlya_sobak/korm-dlya-sobak/suhoy-korm-dlya-sobak/s-brand-is-nutra_gold_nutra_gold/vozrast-jivotnogo-is-vzroslie/'){
             header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
             $this->response->redirect('/tovary_dlya_sobak/korm-dlya-sobak/suhoy-korm-dlya-sobak/nutra_gold_nutra_gold/vozrast-jivotnogo/vzroslie/', 301);
         }*/

/*
        "url_alias_id"	"query"	"keyword"
"69430"	"product_id=5869"	"dr._alders_my_lady_kitten"
"68748"	"product_id=5191"	"dr.clauders_s_pechenu"
"68746"	"product_id=5189"	"dr.clauders_s_lososem"
"68747"	"product_id=5190"	"dr.clauders_s_lososem_i_forelu"
"68745"	"product_id=5188"	"dr.clauders_s_kuricei_i_utkoi"
"68744"	"product_id=5187"	"dr.clauders_s_krolikom_i_pechenu"
"68743"	"product_id=5186"	"dr.clauders_s_krolikom"
"68742"	"product_id=5185"	"dr.clauders_s_domashnei_pticei_i_pochkami"
"68741"	"product_id=5184"	"dr.clauders_s_dichu"
"68738"	"product_id=5181"	"dr.clauders_krevetki_i_treska"
"68728"	"product_id=5171"	"dr.clauders_gus_s_pechenu"
"68727"	"product_id=5170"	"dr.clauders_3_vida_pticy"
"68726"	"product_id=5169"	"dr.clauders__losos_i_tunec"
"68725"	"product_id=5168"	"dr.alders_premium_cat_pashtet"
"68723"	"product_id=5166"	"dr.alders_my_lady_classic_nezhnye_kusochki_v_souse"
"68717"	"product_id=5160"	"dr._alders_my_lady_adult__s_govyadinoi_12sht._po_400g"
"68389"	"product_id=4836"	"bozita_sensible_diet_stomach_dlya_koshek_s_chuvstv._pishevareniem"
"68069"	"product_id=4532"	"dr.clauders_dlya_koshek_pechen_indeiki"
"68068"	"product_id=4531"	"dr.clauders_dlya_koshek_myasnoe_assorti_s_ovoshami"
"68067"	"product_id=4530"	"dr.clauders_dlya_koshek_myasnoe_assorti"
"68066"	"product_id=4529"	"dr.clauders_dlya_koshek_losos"
"68065"	"product_id=4528"	"dr.clauders_dlya_koshek_kurica"
"68064"	"product_id=4527"	"dr.clauders_dlya_koshek_krolik"
"68062"	"product_id=4525"	"dr.clauders_dlya_koshek_govyadina"
"66591"	"product_id=3067"	"dr.alders_dento_snax_lakomstva_kostochki"
"64018"	"product_id=539"	"bozita_robur_breeder__puppy_xl_dlya_shenkov_krup.porod"
"63879"	"product_id=401"	"royal_canin_club_h.e_dlya_aktivnyh_sobak"
"63878"	"product_id=400"	"royal_canin_club_c.c_dlya_sobak"
"63651"	"product_id=178"	"dr._alders_specialnaya_smes_n3"
"63650"	"product_id=177"	"dr._alders_s-4_myasnoe_menu_s_risom_hlopya_dlya_sobak"
"408717"	"manufacturer_id=22"	"dr._alders"
"408711"	"manufacturer_id=13"	"dr.clauders"
"408500"	"manufacreview_id=22"	"dr._alders_reviews"
"408494"	"manufacreview_id=13"	"dr.clauders_reviews"
*/

		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');
		$data['og_url'] = (isset($this->request->server['HTTPS']) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		$data['og_image'] = $this->document->getOgImage();

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		if (isset($this->session->data['compare'])) {
            $data['count_compare'] = count($this->session->data['compare']);
        } else {
            $data['count_compare'] = 0;
        }
		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_page'] = $this->language->get('text_page');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['login'] = $this->url->link('account/login', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {

					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

                    $children_data_level3 = array();

                    $level3 = $this->model_catalog_category->getCategories($child['category_id']);
                    foreach ($level3 as $child3) {
                        $children_data_level3[] = array(
                        'name'  => $child3['name'],
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. '_' . $child3['category_id'])
					);
                    }


                    $children_data[] = array(
                        'children' => $children_data_level3,
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

                $class_c=' dogs';
                if ($category['name']=='Собаки') $class_c=' dogs';
                if ($category['name']=='Кошки') $class_c=' cats';
                if ($category['name']=='Грызуны') $class_c=' rats';
                if ($category['name']=='Птицы') $class_c=' birds';
                if ($category['name']=='Рептилии') $class_c=' reptiles';
                if ($category['name']=='Рыбы') $class_c=' fish';
                if ($category['name']=='Лошади') $class_c=' horses';

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
                    'class'    => $class_c,
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
        //var_dump($data['cart']);
		$data['actions'] = $this->load->controller('common/news');
		$data['quicksignup'] = $this->load->controller('common/quicksignup');
		$data['discountinfo'] = $this->load->controller('common/discountinfo');
		$data['location'] = $this->load->controller('common/location/getCitiesByIP');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
			    $class = '-' . $this->request->get['product_id'].' product-page';
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'].' category-page';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'].' manufacturer-page';
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}
}
