<?php
class ControllerCommonLocation extends Controller {
	public function index() {

	}

    public function getCitiesByIP()
    {
		//echo $this->session->data['geo_city'];

        if (!isset($this->session->data['geo_city']) || empty($this->session->data['geo_city'])) {
            $this->session->data['geo_city']='';
            $geo = array();
            $is_bot = preg_match(
                "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
                $_SERVER['HTTP_USER_AGENT']
            );
            $geo = !$is_bot ? json_decode(file_get_contents('http://ru.sxgeo.city/json/'.$_SERVER["REMOTE_ADDR"]), true) : '';
            // var_dump($geo);

                $this->session->data['user_location'] = isset($geo['city']['name_ru'])? $geo['city']['name_ru'] : 'Москва' ;
                $this->session->data['user_location_region'] = isset($geo['region']['name_ru'])? $geo['region']['name_ru'] : 'Московская область';
                $this->session->data['user_location_country'] = isset($geo['country']['name_ru'])? $geo['country']['name_ru'] : 'РФ';

            return  $this->session->data['geo_city'] = isset($geo['city']['name_ru'])? $geo['city']['name_ru'] : 'Москва' ;


        } else {
            return  $this->session->data['geo_city'];

        }
    }
    public function setlocation()
    {
        $user_location='';
        $user_location_id='';
        $user_location_region='';
        $user_location_country='';

        if (isset($this->request->post['user_location'])){
            $this->session->data['user_location'] = $this->session->data['geo_city'] =  $user_location=$this->request->post['user_location'];
        }
        if (isset($this->request->post['user_location_id'])){
            $user_location_id=$this->request->post['user_location_id'];
        }
        if (isset($this->request->post['user_location_region'])){
            $this->session->data['user_location_region'] =  $user_location_region =$this->request->post['user_location_region'];
        }
        if (isset($this->request->post['user_location_country'])){
            $this->session->data['user_location_country'] =  $user_location_country=$this->request->post['user_location_country'];
        }

        $is_moscow_region = ($user_location == 'Москва' || $user_location_region == 'Московская область') ? true : false;

        $json['is_moscow_region'] = $is_moscow_region;
        $json['city']  = $this->session->data['geo_city'];
        $json['region']  = $this->session->data['user_location_region'];
        $json['country']  = $this->session->data['user_location_country'];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


	public function info() {
		$this->response->setOutput($this->index());
	}

    public  function getCitiesfirstAction()
    {
        $this->load->model('localisation/location');
        $q='';
        if (isset($this->request->post['name'])){
            $q=$this->request->post['name'];
        }

        if($q != '') {

            $result = $this->model_localisation_location->getCitiesfirstAction($q);

        } else {
            $result = $this->model_localisation_location->getCitiesfirstActionEmpty($q);
            }
        $json  = $result;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


    function getCities()
    {
        $this->load->model('localisation/location');
        $region_id='';
        $country_id='';
        if (isset($this->request->post['region_id'])){
            $region_id=$this->request->post['region_id'];
        }
        if (isset($this->request->post['country_id'])){
            $country_id=$this->request->post['country_id'];
        }

        if($region_id != 0) {
            $result = $this->model_localisation_location->getCitiesAction($country_id,$region_id);
        } else{
            $result = $this->model_localisation_location->getCitiesActionEmpty($country_id);
        }
        $json  = $result;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    function getregions()
    {
        $countryId =0;
        if (isset($this->request->post['country_id'])){
            $countryId=$this->request->post['country_id'];
        }
        $this->load->model('localisation/location');
        $regions = $this->model_localisation_location->getRegions($countryId);
        $json  = $regions;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }
    function getCountries()
    {
        $this->load->model('localisation/location');
        $countries = $this->model_localisation_location->getCountries();
        $json  = $countries;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


}
