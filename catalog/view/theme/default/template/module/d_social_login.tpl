<?php if (!$islogged) { ?>
<div id="d_social_login">
  <?php if($error){ ?>
  <div class="alert alert-warning"><?php echo $error; ?></div>
  <?php } ?>
  <label class="text-label dsl-label dsl-label-<?php echo $size; ?>"><?php echo $button_sign_in; ?></label>
  <?php foreach($providers as $key => $provider){ ?><?php if ($provider['enabled']) { ?><a id="dsl_<?php echo $provider['id']; ?>_button" class="dsl-button dsl-button-<?php echo $size; ?>" href="index.php?route=module/d_social_login/login&provider=<?php echo $key; ?>"><span class="l-side"><span class="dsl-icon <?php echo $provider['icon']; ?>"></span></span><span class="r-side"><?php echo $provider['heading']; ?></span></a><?php }  ?><?php } ?>
</div>
<script>
$( document ).ready(function() {
  $('.dsl-button').on('click', function(){
    $('.dsl-button').find('.l-side').spin(false);
    $(this).find('.l-side').spin('<?php echo $size; ?>', '#fff');
    
    $('.dsl-button').find('.dsl-icon').removeClass('dsl-hide-icon');
    $(this).find('.dsl-icon').addClass('dsl-hide-icon');
  })
})
</script>
<?php } ?>