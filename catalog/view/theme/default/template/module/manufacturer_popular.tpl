<div class="brands-wrapper">
    <div class="center-wrapper">
        <section class="brands-block">
            <h2><?php echo $heading_title; ?></h2>
            <div class="brands-wrapper  brands-slider">
    <?php if ($manufacturers) { ?>
        <div class="manufacturer_slider">
        <?php foreach ($manufacturers as $manufacturer) { ?>
              <?php foreach ($manufacturers as $manufacturer) { ?>
                   <div class="one-manufacturer-slider"><a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['image']; ?>" alt="<?php echo $manufacturer['image']; ?>"></a></div>
              <?php } ?>
        <?php } ?>
        </div>
    <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
    <?php } ?>
    </div>
    </section>
</div>
</div>

<script>
    $('.manufacturer_slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        autoplay: false,
        slidesToShow: 6,
        slidesToScroll: 3,
        prevArrow:'<button type="button" class="slick-prev" >‹</button>',
        nextArrow:'<button type="button"  class="slick-next" >›</button>',

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2,
                    infinite: true,
                }
            },
            {
                breakpoint: 1001,
                settings: {
                    arrows: false,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });



</script>