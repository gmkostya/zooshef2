<div class="separator-line"></div>
<section class="reviews-block">
	<div class="center-wrapper">
		<h2><?php echo $heading_title; ?></h2>
		<div class="reviews-holder clearfix">
			<?php foreach ($article as $articles) { ?>
			<div class="review-item">
				<?php if ($articles['name']) { ?>
				<a class="name" href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
				<?php } ?>
				<div class="date">
					<?php if ($articles['date_added']) { ?>
						<?php echo $articles['date_added']; ?>
					<?php } ?>
				</div>
				<?php if ($articles['description']) { ?>
				<p class="review-text"><?php echo $articles['description']; ?></p>
				<?php  }?>
				<a class="default-link" href="<?php echo $articles['href']; ?>">Читать</a>

			</div>
			<?php  }?>
		</div><a class="default-link" href="<?php echo $newslink; ?>">Посмотреть все статьи</a>
	</div>
</section>
<div class="separator-line"></div>
