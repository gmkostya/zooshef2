<section class="top-slider unready main-slider ">
    <div id="slideshow<?php echo $module; ?>" >
        <?php foreach ($banners as $banner) { ?>
            <div class="item">
                <?php if ($banner['link']) { ?>
                    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
                <?php } else { ?>
                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</section>
<script type="text/javascript"><!--
    $('#slideshow<?php echo $module; ?>').slick({
        dots: true,
        infinite: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow:'<button type="button" class="slick-prev" >‹</button>',
        nextArrow:'<button type="button"  class="slick-next" >›</button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    dots: false
                }
            }]
    });


    --></script>
