<?php if (isset($man_categories)) { ?>
    <div class="categories-with-brand">
        <?php $k=0; foreach ($man_categories as $key => $item) { $k++; ?>
            <div class="item clearfix <?php if($k>30) echo "close-item"; ?> ">
                <div class="image">
                    <a href="<?php echo $item['url']; ?>">
                        <img src="<?php echo $item['image']; ?>" alt="">
                    </a>
                </div>
                <div class="name">
                    <a href="<?php echo $item['url']; ?>"> <?php echo $item['man_name']; ?>
                        <small><?php echo $item['name']; ?></small>
                    </a>
                </div>
            </div>
        <?php } ?>
        <?php if($k>30) { ?>
            <button class="default-link js-readon-categories-with-brand categories-with-brand">Показать больше</button>
            <button class="default-link js-readon-categories-with-brand categories-with-brand" style="display: none">Скрыть</button>
        <?php } ?>
    </div>
<?php } ?>

<?php if ($description) { ?>
    <div class="hided-block" style="margin-top: 20px">
        <div class="hided-block-inner">
            <?php echo $description; ?>
        </div>
    </div>
<?php } ?>
