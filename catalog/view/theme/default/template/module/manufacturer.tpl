<section class="brands-search">
    <h2>Все бренды в одном месте!</h2>

    <?php if ($categories) { ?>
        <div class="alphabet clearfix">
            <?php $k=''; foreach ($categories as $category) { $k++; ?>
                <?php if($category['name']!='0-9') { ?>
                &nbsp;&nbsp;&nbsp;<a  class="letters <?php if($k=='2') {  ?>active<?php } ?>" href="#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
            <?php } ?>
            <?php } ?>
            <?php foreach ($categories as $category) {  ?>
                <?php if($category['name']=='0-9') { ?>
                    &nbsp;&nbsp;&nbsp;<a  class="letters " href="#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
                <?php } ?>
            <?php } ?>

        </div>

        <?php /* echo "<pre>"; var_dump($categories);*/ $k=''; foreach ($categories as $category) { $k++; ?>
            <div id="<?php echo $category['name']; ?>" class="brands-list clearfix <?php if($k=='2') {  ?>sel<?php } ?> ">
            <?php if ($category['manufacturer']) {   ?>
                <?php foreach (array_chunk($category['manufacturer'], round(count($category['manufacturer'])/5)) as $manufacturers) { ?>
                    <ul class="brands-list-item">
                        <?php foreach ($manufacturers as $manufacturer) { ?>
                            <li><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            <?php } ?>
            </div>
        <?php } ?>
    <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <div class="buttons clearfix">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
    <?php } ?>


</section>
