<?php if ($article) { ?>
    <div id="products-container">
        <?php foreach ($article as $articles) { ?>
            <div class="related-article clearfix">
                <?php if ($articles['thumb']) { ?>
                    <div class="image fl-left">
                     <a href="<?php echo $articles['href']; ?>"><img  src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" /></a>
                    </div>
                <?php } ?>
                <div class="article-content">
                    <div class="prod-link-container">
                        <a class="name" href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
                    </div>
                    <?php if ($articles['description']) { ?>
                        <p><?php echo $articles['description']; ?></p>
                    <?php } ?>
                <?php if ($articles['date_added']) { ?>
                <div class="author-name">
                     <?php echo $articles['date_added']; ?>
                </div>
                <?php } ?>
            </div>
            </div>
		<?php } ?>
    </div>
    <?php if($pagination) { ?>
        <div class="button-block center-text">
            <button class="btn btn-secondary show-more" onclick="agreeloadproduct()">Показать еще</button>
        </div>
        <div class="center-text">
            <div class="pagenator">
                <?php echo $pagination; ?>
            </div>
        </div>
    <?php } ?>

<?php } ?>
<?php if ($is_category) { ?>
  <?php if (!$ncategories && !$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } else { ?>
  <?php if (!$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } ?>
