<?php echo $header; ?>
<div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><?php } ?>
                <?php } ?>
            </div>
            <h1 ><?php echo $heading_title; ?></h1>
        </div>
  <div class="articles-list-page clearfix">
      <?php echo $column_left; ?>
    <div class="related-articles-wrapper">

        <?php echo $content_top; ?>

      <?php echo $description; ?><?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
</div>
<?php echo $footer; ?> 
