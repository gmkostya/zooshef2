<div class="article-content">
	<div class="author-name">
		<?php echo $date_added; ?>
	</div>

	<?php echo $description; ?>
	<?php if ($article_videos) { ?>
		<div class="content blog-videos">
			<?php foreach ($article_videos as $video) { ?>
				<?php echo ($video['text']) ? '<h2 class="video-heading">'.$video['text'].'</h2>' : ''; ?>
				<div>
					<?php echo $video['code']; ?>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<?php if ($gallery_images) { ?>
		<?php if ($gallery_type == 1) { ?>
			<div class="content blog-gallery">
				<?php foreach ($gallery_images as $gallery) { ?>
					<a class="colorbox" href="<?php echo $gallery['popup']; ?>" title="<?php echo $gallery['text']; ?>">
						<img src="<?php echo $gallery['thumb']; ?>" alt="<?php echo $gallery['text']; ?>" />
					</a>
				<?php } ?>
			</div>
		<?php } else { ?>
<script>
	jQuery(document).ready(function ($) {

            var _SlideshowTransitions = [
                {$Duration: 1200, $During: { $Left: [0.3, 0.7] }, $FlyDirection: 1, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $SlideOut: true, $FlyDirection: 2, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $During: { $Left: [0.3, 0.7] }, $FlyDirection: 2, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $SlideOut: true, $FlyDirection: 1, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $During: { $Top: [0.3, 0.7] }, $FlyDirection: 4, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $SlideOut: true, $FlyDirection: 8, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $During: { $Top: [0.3, 0.7] }, $FlyDirection: 8, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $SlideOut: true, $FlyDirection: 4, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Cols: 2, $During: { $Left: [0.3, 0.7] }, $FlyDirection: 1, $ChessMode: { $Column: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Cols: 2, $SlideOut: true, $FlyDirection: 1, $ChessMode: { $Column: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Rows: 2, $During: { $Top: [0.3, 0.7] }, $FlyDirection: 4, $ChessMode: { $Row: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Rows: 2, $SlideOut: true, $FlyDirection: 4, $ChessMode: { $Row: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Cols: 2, $During: { $Top: [0.3, 0.7] }, $FlyDirection: 4, $ChessMode: { $Column: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Cols: 2, $SlideOut: true, $FlyDirection: 8, $ChessMode: { $Column: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Rows: 2, $During: { $Left: [0.3, 0.7] }, $FlyDirection: 1, $ChessMode: { $Row: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Rows: 2, $SlideOut: true, $FlyDirection: 2, $ChessMode: { $Row: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }

                , { $Duration: 1200, $Cols: 2, $Rows: 2, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $FlyDirection: 5, $ChessMode: { $Column: 3, $Row: 12 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $ScaleVertical: 0.3, $Opacity: 2 }
                , { $Duration: 1200, $Cols: 2, $Rows: 2, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $SlideOut: true, $FlyDirection: 5, $ChessMode: { $Column: 3, $Row: 12 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $ScaleVertical: 0.3, $Opacity: 2 }

                , { $Duration: 1200, $Delay: 20, $Clip: 3, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
                , { $Duration: 1200, $Delay: 20, $Clip: 3, $SlideOut: true, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseOutCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
                , { $Duration: 1200, $Delay: 20, $Clip: 12, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
                , { $Duration: 1200, $Delay: 20, $Clip: 12, $SlideOut: true, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseOutCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
                ];

            var options = {
                $AutoPlay: true,             
                $AutoPlayInterval: 3500,                    
                $PauseOnHover: 1,      
                $DragOrientation: 3,                              
                $ArrowKeyNavigation: true,   			          
                $SlideDuration: 800,                                

                $SlideshowOptions: {                              
                    $Class: $JssorSlideshowRunner$,                
                    $Transitions: _SlideshowTransitions,            
                    $TransitionsOrder: 1,                           
                    $ShowLink: true                                    
                },

                $ArrowNavigatorOptions: { 
                    $Class: $JssorArrowNavigator$,  
                    $ChanceToShow: 1  
                },

                $ThumbnailNavigatorOptions: {                      
                    $Class: $JssorThumbnailNavigator$,             
                    $ChanceToShow: 2,                               
                    $ActionMode: 1,                                
                    $SpacingX: 8,                                   
                    $DisplayPieces: 10,                            
                    $ParkingPosition: 360  
                }
            };

            var jssor_slider1 = new $JssorSlider$("blog-gallery-slider", options);
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$SetScaleWidth(Math.max(Math.min(parentWidth, 1920), 300));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
        });
</script>
			<div id="blog-gallery-slider" style="height: <?php echo $gallery_height; ?>px; width:<?php echo $gallery_width; ?>px;">
				<div u="loading">
					<div class="loadin-in-1"></div>
					<div class="loadin-in-2"></div>
				</div>
				<div u="slides" style="height: <?php echo $gallery_height - 100; ?>px; width:<?php echo $gallery_width; ?>px;">
					<?php foreach ($gallery_images as $gallery) { ?>
						<div>
							<a class="colorbox" href="<?php echo $gallery['popup']; ?>" title="<?php echo $gallery['text']; ?>"><img u="image" src="<?php echo $gallery['popup']; ?>" /></a>
							<img u="thumb" src="<?php echo $gallery['thumb']; ?>" />
						</div>
					<?php } ?>
				</div>
				<span u="arrowleft" class="jssora05l"></span>
				<span u="arrowright" class="jssora05r"></span>
				<div u="thumbnavigator" class="jssort01">
					<div u="slides" style="cursor: move;">
						<div u="prototype" class="p" style="position: absolute; width: 72px; height: 72px; top: 0; left: 0;">
							<div class=w><thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></thumbnailtemplate></div>
							<div class=c>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php } ?>
	<?php if ($ntags && count($ntags) > 1) { ?>
		<div class="article-tags">
			<?php echo $text_tags; ?> 
			<?php foreach($ntags as $ntag) { ?>
				<a class="ntag" href="<?php echo $ntag['href']; ?>"><?php echo $ntag['ntag']; ?></a>
			<?php } ?>
		</div>
	<?php } ?>
</div>
<?php if ($products) { ?>
    <div class="popular-products">
            <div class="zag2">Товары участвующие в акции</div>
            <div class="products-wrapper clearfix">
                <?php $h='0'; foreach ($products as $product) { $h++ ?>
                    <?php if($h=='1') {  ?>
                        <div class="prod-cat-line clearfix" style="float: left;">
                    <?php }?>
                    <div class="product-wrapper" id="product-<?php echo $product['product_id']; ?>">
                        <form id="product-form-2852" name="product-form-<?php echo $product['product_id']; ?>">
                            <div class="prod-link-container">
                                <a class="product-link" data-brand="<?php echo $product['manufacturer']; ?>"  data-id="<?php echo $product['product_id']; ?>" data-name="<?php echo $product['name']; ?>" data-position="<?php echo $product['product_id']; ?>" data-price="<?php echo $product['data_price_min']; ?>" data-sku="<?php echo $product['model']; ?>" data-variant="" href="<?php echo $product['href']; ?>">
                                    <?php if ($product['lable_action']==1 ) { ?>
                                        <span class="stripe action-text">Акция</span>
                                    <?php } ?>
                                    <?php if($product['sticker_status']['kod']!='0') { ?>
                                        <span class="stripe new-text  <?php if ($product['lable_action']==1 ) { ?> stripe-2 <?php }?>"><?php echo $product['sticker_status']['name']; ?></span>
                                    <?php } ?>
                                    <div class="image-container"><img alt="<?php echo $product['name']; ?>" src="<?php echo $product['thumb']; ?>"></div>
                                    <span class="link-name"><?php echo $product['name']; ?></span></a>
                            </div>
                            <?php if($product['options'] AND $product['ean_status']['kod']==0) { ?>
                                <div class="options">
                                    <?php foreach ($product['options'] as $option) { ?>
                                        <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '' )){ ?>
                                            <!--option tab-->
                                            <?php if(count($option['product_option_value'])>0) { ?>
                                                <!--heads-->
                                                <div class="options_weight">
                                                    <?php  $z=''; $k=''; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                        <?php if ($k>4) { ?>
                                                            <div>
                                                                <a class="no_active" href="<?php echo $product['href']; ?>" >еще <?php echo  count($option['product_option_value'])-4; ?></a>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="one-v <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" >
                                                            <?php echo str_replace('Упаковка', '', $option_value['owq_title_short']); ?>
                                                        </div>
                                                    <?php } ?>
                                                </div><!--heads-->
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                        <div class="option-tabs-content">
                                                            <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                    <div class="switch-items">
                                                                        <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                            <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                                <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                    <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="button_curier">
                                                                            <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                                <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                            <?php } else  { ?>
                                                                                <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                            <?php } ?>
                                                                            <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                        </div>

                                                                    </div>
                                                                    <div class="extra-info no_options">
                                                                        <div class="custom-select">
                                                                            <div class="select-mask"><?php echo $option_value['owq_title']; ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div><!--option tab-->
                                            <?php } else { ?>
                                                <div class="switch-items type-2"></div>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '1')){  ?>

                                            <?php if(count($option['product_option_value'])>0) { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                        <div class="option-tabs-content">
                                                            <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']);  foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                    <div class="switch-items type-2">
                                                                        <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                            <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                                <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                    <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="button_curier">
                                                                            <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                                <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                            <?php } else  { ?>
                                                                                <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                            <?php } ?>
                                                                            <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="extra-info">
                                                            <!--heads-->
                                                            <div  class="custom-select">
                                                                <div class="select-mask">
                                                                    <?php $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                        <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?>

                                                                    <?php } ?>
                                                                </div>
                                                                <div class="custom-select-content"></div>
                                                                <select class="custom-select-hidden variant-select cat-ch-variant" >
                                                                    <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                        <option data-stock="19" data-city="<?php echo $option_value['owq_title']; ?>" data-product="<?php echo $product['product_id']; ?>"  data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>"  ><?php echo $option_value['owq_title']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div><!--heads-->
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }  else {?>
                                                <div class="switch-items type-2"></div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div><!--!option-->
                            <?php } else { ?>
                                <div class="switch-items type-2">
                                    <?php  if($product['ean_status']['kod']!='0'){ ?>
                                        <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo  $product['ean_status']['name'];?>     </button>
                                    <?php }?>

                                </div>

                            <?php }?>
                            <!--rating-->
                            <div class="reviews-marks clearfix">
                                <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                                <div class="rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($product['rating'] < $i) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="compare-block compare-block-<?php echo $product['product_id']; ?>">
                                    <?php if ($product['compare_status']){ ?>
                                        <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                                    <?php } else { ?>
                                        <span class="btn-compare"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-check"></i> Сравнить </span>
                                    <?php } ?>
                                </div>
                            </div><!--!rating-->
                        </form>
                    </div>
                    <?php if($h=='5') {   $h=''; ?>
                        </div><!--products-container-->
                    <?php }?>
                <?php } ?>
            </div>
    </div>
    <div class="separator-line"></div>

<?php } ?>
<?php if ($article) { ?>
    <div class="content-block">
        <div class="related-articles-wrapper">
            <div class="zag2">
                Рекомендуемые статьи
            </div>
            <?php foreach ($article as $articles) { ?>
                <div class="related-article clearfix">
                    <div class="image fl-left"><img alt="<?php echo $articles['name']; ?>" src="<?php echo $articles['thumb']; ?>"></div>
                    <div class="article-content">
                        <a class="name" href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
                        <p><?php echo $articles['description']; ?></p>
                        <div class="author-name">
                            <?php echo $articles['date_added']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
<?php } ?>
<?php if ($article2) { ?>
<div class="content-block">
    <div class="related-articles-wrapper">
        <div class="zag2">
            Связанные статьи
        </div>

		<?php foreach ($article2 as $articles) { ?>
            <div class="related-article clearfix">
                <div class="image fl-left"><img alt="<?php echo $articles['name']; ?>" src="<?php echo $articles['thumb']; ?>"></div>
                <div class="article-content">
                    <a class="name" href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
                    <p><?php echo $articles['description']; ?></p>
                    <div class="author-name">
                        <?php echo $articles['date_added']; ?>
                    </div>
                </div>
            </div>
		<?php } ?>

</div>
</div>
<?php } ?>
<?php if ($disqus_status) { ?>
<script type="text/javascript">
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($acom != 0 && !$disqus_status && !$fbcom_status) { ?>
    <h2><a name="comments"></a><?php echo $title_comments; ?> <?php echo $text_coms; ?> "<?php echo $heading_title; ?>"</h2>
	<?php if ($comment) { ?>
    <?php foreach ($comment as $comment) { ?>
    <div class="content blog-content">
		<div  class="comment-header"><span class="comment-icon"></span><b><?php echo $comment['author']; ?></b> <?php echo $text_posted_on; ?> <?php echo $comment['date_added']; ?></div>
		<div class="comment-text"><?php echo $comment['text']; ?>
			<a onclick="addReply('<?php echo $comment['ncomment_id']; ?>', '<?php echo $comment['author']; ?>');"><?php echo $text_reply; ?></a>
		</div>
			<?php foreach ($comment['replies'] as $reply) { ?>
				<div class="content blog-reply">
					<div class="reply-header"><span class="comment-icon"></span><b><?php echo $reply['author']; ?></b> <?php echo $text_posted_on; ?> <?php echo $reply['date_added']; ?></div>
					<div class="comment-text"><?php echo $reply['text']; ?></div>
				</div>
			<?php } ?>
	</div>
    <?php } ?>
	<div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $pag_results; ?></div>
	</div>
    <?php } ?>
    <div class="content" id="comment-form">
    <h2 id="com-title"><span class="blog-write"></span><?php echo $writec; ?></h2>
	<input type="hidden" name="reply_id" value="0" id="reply-id-field"/>
    <div class="comment-form">
	<div class="comment-left">
    <b><?php echo $entry_name; ?></b><br />
    <input class="form-control" type="text" name="name" value="<?php echo $customer_name; ?>" style="" />
    <div style="height: 5px; overflow: hidden">&nbsp;</div>
    <b><?php echo $entry_captcha; ?></b><br />
    <input class="form-control" type="text" name="captcha" style="" value="" />
	<div style="height: 5px; overflow: hidden">&nbsp;</div>
    <img src="index.php?route=tool/captcha" alt="" id="captcha" />
	</div>
	<div class="comment-right">
    <b><?php echo $entry_review; ?></b><br />
    <textarea class="form-control" name="text" cols="40" rows="4"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span>
	</div>
    </div>
    <div class="buttons">
      <div class="right">
		<button type="button" id="button-comment" data-loading-text="<?php echo $text_send; ?>" class="btn btn-primary"><?php echo $text_send; ?></button>
	  </div>
    </div>
  </div>
<?php } elseif ($acom != 0 && $disqus_status) { ?>
<div id="disqus_thread"></div>
<script type="text/javascript"><!--
var disqus_shortname = '<?php echo $disqus_sname; ?>';
var disqus_identifier = '<?php echo $disqus_id; ?>';
var disqus_url = '<?php echo $page_url; ?>';
/* * * DON'T EDIT BELOW THIS LINE * * */
(function() {
	var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
	(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
--></script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<?php } elseif ($acom != 0 && $fbcom_status) { ?>
<h2><a name="comments"></a><fb:comments-count href="<?php echo $page_url; ?>"></fb:comments-count> <?php echo $text_comments_to; ?> "<?php echo $heading_title; ?>"</h2>
<div class="fb-comments" data-width="100%" data-href="<?php echo $page_url; ?>" data-numposts="<?php echo $fbcom_posts; ?>" data-colorscheme="<?php echo $fbcom_theme; ?>"></div>
<script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>
<script type="text/javascript"><!--
$('.colorbox').magnificPopup({ 
  type: 'image',
  gallery:{enabled:true},
  zoom: {
	enabled: true,
	duration: 300,
	opener: function(element) {
		return element.find('img');
	}
  }
});
//--></script>  
<script type="text/javascript"><!--
function addReply(reply_id, author) {
	$('#reply-id-field').attr('value', reply_id);
	$('#com-title').html("<span class=\"blog-write\"></span><?php echo $text_reply_to; ?> " + author + "<span onclick=\"cancelReply();\" title=\"Cancel Reply\" class=\"cancel-reply\"></span>");
	var scroll = $('#comment-form').offset();
	$('html, body').animate({ scrollTop: scroll.top - 80 }, 'slow');
}
function cancelReply(reply_id, author) {
	$('#reply-id-field').attr('value', 0);
	$('#com-title').html("<span class=\"blog-write\"></span><?php echo $writec; ?>");
}
$('#button-comment').bind('click', function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=news/article/writecomment&news_id=<?php echo $news_id; ?>',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()) + '&reply_id=' + encodeURIComponent($('input[name=\'reply_id\']').val()),
		beforeSend: function() {
			$('.alert').remove();
			$('#button-comment').attr('disabled', true);
			$('#com-title').after('<div class="alert alert-danger ad1"><i class="fa fa-exclamation-circle"></i> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-comment').attr('disabled', false);
			$('.ad1').remove();
		},
		success: function(data) {
			if (data.error) {
				$('#com-title').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + data.error + '</div>');
			}
			
			if (data.success) {
				$('#com-title').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + data.success + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
	$(document).ready(function() {
		var articleImageWidth = $('img#image-article').width() + 30;
		var pageWidth = $('.article-content').width() * 0.65;
		if (articleImageWidth >= pageWidth) {
			$('img#image-article').attr("align","center");
			$('img#image-article').parent().addClass('centered-image');
		}
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		$(window).resize(function() {
		var articleImageWidth = $('img#image-article').width() + 30;
		var pageWidth = $('.article-content').width() * 0.65;
		if (articleImageWidth >= pageWidth) {
			$('img#image-article').attr("align","center");
			$('img#image-article').parent().addClass('centered-image');
		}
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		});
	});
//--></script> 
