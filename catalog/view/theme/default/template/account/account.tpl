<?php echo $header; ?>
<div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        </div>

        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>

        <div class="catalog-container clearfix">

            <?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = ''; ?>
            <?php } else { ?>
                <?php $class = ' catalog-full'; ?>
            <?php } ?>

            <div id="content" class="catalog-wrapper <?php echo $class; ?>">

<div class="row">
<div class="col-md-3 col-sm-12">
      <h2><?php echo $text_my_account; ?></h2>
      <ul class="list-unstyled">
        <li><a href="<?php echo $edit; ?>">Изменить основные данные</a></li>
        <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
        <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
        <li><a href="<?php echo $asklist; ?>"><?php echo $text_asklist; ?></a></li>
      </ul>
      <h2><?php echo $text_my_orders; ?></h2>
      <ul class="list-unstyled">
        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <?php if ($reward) { ?>
        <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
        <?php } ?>
      </ul>
      <h2><?php echo $text_my_newsletter; ?></h2>
      <ul class="list-unstyled">
        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
      </ul>
      <?php echo $content_bottom; ?></div>

    <div class="col-md-9 col-sm-12 account-info">
        <h2>Основные данные</h2>
        <div class="row">
            <div class="col-md-4 col-sm-12">Ваше имя</div>
            <div class="col-md-8 col-sm-12"><span><?php echo $lastname; ?> </span></div>
            <div class="col-md-4 col-sm-12">Ваш е-майл</div>
            <div class="col-md-8 col-sm-12"><span><?php echo $email; ?> </span></div>
            <div class="col-md-4 col-sm-12">Ваш телефон</div>
            <div class="col-md-8 col-sm-12"><span><?php echo $telephone; ?> </span></div>
            <div class="col-md-4 col-sm-12">Размер скидки</div>
            <div class="col-md-8 col-sm-12"><span><?php echo $discount_groupe; ?> </span></div>
        </div>
    </div>
</div>
 </div>
            <!--catalog-wrapper-->
            <?php echo $column_right; ?>

            </div><!--catalog-container clearfix-->
    </div>
</div>
<?php echo $footer; ?>
