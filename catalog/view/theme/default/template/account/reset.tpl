<?php echo $header; ?>
  <div class="content-wrapper center-wrapper">
    <div class="text-page">
      <div class="content-block">
        <div class="breadcrumbs">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <?php if($i+1<count($breadcrumbs)) { ?>
              <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
            <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
          <?php } ?>
        </div>
        <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
      </div>
      <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
      <?php } ?>

      <div class="catalog-container clearfix">

        <?php echo $column_left; ?>
        <?php if ($column_left) { ?>
          <?php $class = ''; ?>
        <?php } else { ?>
          <?php $class = ' catalog-full'; ?>
        <?php } ?>

        <div id="content" class="catalog-wrapper <?php echo $class; ?>">
          <p><?php echo $text_email; ?></p>
          <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
              <p><?php echo $text_password; ?></p>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                <div class="col-sm-10">
                  <input type="password" name="password" value="" id="input-password" class="form-control" />
                  <?php if ($error_password) { ?>
                    <div class="text-danger"><?php echo $error_password; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                <div class="col-sm-10">
                  <input type="password" name="confirm" value="" id="input-confirm" class="form-control" />
                  <?php if ($error_confirm) { ?>
                    <div class="text-danger"><?php echo $error_confirm; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="buttons clearfix">
                <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default">Отмена</a></div>
                <div class="pull-right">
                  <input type="submit" value="<?php echo $button_save; ?>" class="btn btn-primary" />
                </div>
              </div>
            </form>
          </div>          <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div><!--catalog-container clearfix-->
    </div>
  </div>
<?php echo $footer; ?>
