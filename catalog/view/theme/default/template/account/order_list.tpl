<?php echo $header; ?>
    <div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        </div>

        <div class="catalog-container clearfix">

            <?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = ''; ?>
            <?php } else { ?>
                <?php $class = ' catalog-full'; ?>
            <?php } ?>

            <div id="content" class="catalog-wrapper <?php echo $class; ?>">
      <?php if ($orders) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right"><?php echo $column_order_id; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-right"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_customer; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="text-right">#<?php echo $order['order_id']; ?></td>
              <td class="text-left"><?php echo $order['status']; ?></td>
              <td class="text-left"><?php echo $order['date_added']; ?></td>
              <td class="text-right"><?php echo $order['products']; ?></td>
              <td class="text-left"><?php echo $order['name']; ?></td>
              <td class="text-right"><?php echo $order['total']; ?></td>
              <td class="text-right"><a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div> <div class="text-left" style="padding-top:15px;">
                    <i>*Если вы уже совершали покупки в нашем магазине и не видите здесь свою историю заказов, то хотим пояснить следующее: к сожалению, при обновлении сайта был потерян архив, где хранилась история заказов. Но волноваться не стоит, вся информация о покупках сохранилась, но выводится в личном кабинете не будет. Ваша скидка так же сохранились и закреплена за вашим аккаунтом. Приносим свои извинения за доставленные неудобства и желаем только приятных покупок.</i></div>
      <?php echo $content_bottom; ?></div>
            <!--catalog-wrapper-->
            <?php echo $column_right; ?></div><!--catalog-container clearfix-->
    </div>
    </div>
<?php echo $footer; ?>
