<?php echo $header; ?>
<div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        </div>

        <div class="catalog-container clearfix">

            <?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = ''; ?>
            <?php } else { ?>
                <?php $class = ' catalog-full'; ?>
            <?php } ?>

            <div id="content" class="catalog-wrapper <?php echo $class; ?>">
      <?php if ($products) { ?>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-center"><?php echo $column_image; ?></td>
            <td class="text-left"><?php echo $column_name; ?></td>
            <td class="text-left"><?php echo $column_model; ?></td>
            <td class="text-right"><?php echo $column_stock; ?></td>
            <td class="text-right"><?php echo $column_price; ?></td>
            <td class="text-right"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { $nalichie='0'; ?>
          <tr>
            <td class="text-center"><?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?></td>
            <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <br/>
              <?php if ($product['option']) { ?>
                <?php foreach ($product['option'] as $option) { ?>
                  <small><?php echo $option['title']; ?></small>
                  <?php if($option['quantity'] > 0) { $nalichie = $option['quantity']; } ?>
                <?php } ?>
              <?php } ?>
            </td>
            <td class="text-left"><?php echo $product['model']; ?></td>
            <td class="text-right"><?php if ($nalichie>0) { ?>Уже в наличии <?php } else { ?>Ожидается <?php } ?></td>
            <td class="text-right"><?php if ($product['price']) { ?>
              <div class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                <?php } ?>
              </div>
              <?php } ?></td>
            <td class="text-right">
              <?php if ($nalichie>0) { ?> <a href="<?php echo $product['href']; ?>" class="btn btn-secondary out-p-button txt-up cat-out-p-button">перейти к товару для заказа</a>
             <br/>
              <?php } ?>
              <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger">Удалить из списка избраного</a></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
            <!--catalog-wrapper-->
            <?php echo $column_right; ?></div><!--catalog-container clearfix-->
    </div>
</div>
<?php echo $footer; ?>
