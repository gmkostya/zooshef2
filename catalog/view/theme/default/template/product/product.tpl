<?php echo $header; ?>
<!--content-wrapper center-wrapper-->
<div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        </div>

        <div  id="product-<?php echo $product_id; ?>" class="product-page-wrapper">
            <div class="main_product_block_sale clearfix">
                <?php echo $content_top; ?>

                <?php if ($thumb || $images) { ?>
                    <div class="main_product_block_sale_img">


                        <?php if ($thumb) { ?>
                            <div class="product-main-image"><a class="click-triger" data-link="image-0" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" data-zoom-image="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
                        <?php } ?>
                        <?php if ($images) { ?>
                            <ul id="lightgallery" style="display: none" >
                                <li  data-src="<?php echo $popup; ?>" >
                                    <a  href=""  id="image-0">
                                        <img   src="<?php echo $popup; ?>">
                                    </a>
                                    <?php  $k=''; foreach ($images as $image) { $k++; ?>
                                <li  data-src="<?php echo $image['popup']; ?>" >
                                    <a href="" id="image-<?php echo $k; ?>">
                                        <img   src="<?php echo $image['popup']; ?>">
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <div class="product-images-gallery">
                                <div class="jcarousel">


                                    <?php $k=0; foreach ($images as $image) {  $k++; ?>
                                            <div>
                                                <a class="thumbnails click-triger" data-link="image-<?php echo $k; ?>"  ><img alt="<?php echo $heading_title; ?>" src="<?php echo $image['thumb']; ?>"></a>
                                            </div>
                                        <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php  if ($userLogged) { ?>
                            <div class="edit_container">
                                <a class="edit" target="_blank" href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product_id; ?>">Редактировать</a>
                            </div>
                        <?php } ?>


                    </div>
                <?php } ?>

                <div class="main_product_block_sale_price">
                    <div class="tpldescr">
                        <?php echo $description_shablon; ?>
                    </div>


                    <?php if (empty($product_status)) { ?>
                    <?php if ($options) { ?>
                    <div class="main_product_block_sale_price_list">
                        <div id="product">
                            <?php if ($options) { ?>
                                <script type="text/javascript"><!--
                                    function update_qty_options() {
                                        $('.owq-option input[type="checkbox"]').each(function() {
                                            $qty = $(this).closest('tr').find('.owq-input');
                                            opt_qty = Number($qty.val());
                                            if (isNaN(opt_qty)) opt_qty = 0;

                                            if ($qty.data('max') && opt_qty > $qty.data('max')) {
                                                $qty.closest('tr').addClass('no-stock');
                                            } else {
                                                $qty.closest('tr').removeClass('no-stock');
                                            }

                                            if ($(this).data('id') && opt_qty > 0) {
                                                $(this).val($(this).data('id') + $(this).data('split') + opt_qty).data('price', $(this).data('fprice') * opt_qty).prop('checked', true);
                                            } else {
                                                $(this).prop('checked', false);
                                            }
                                        });
                                        $('.owq-option select').each(function() {
                                            $qty = $(this).closest('div').find('.owq-input');
                                            opt_qty = Number($qty.val());
                                            if (isNaN(opt_qty)) opt_qty = 0;

                                            $(this).find('option').each(function(){
                                                if ($(this).data('id') && opt_qty > 0) {
                                                    $(this).val($(this).data('id') + '|' + opt_qty).data('price', $(this).data('fprice') * opt_qty);
                                                } else {
                                                    $(this).val('').data('price', 0);
                                                }
                                            });
                                        });
                                    }
                                    $(document).ready(function(){
                                        $('.owq-option .owq-input').on('input',function(){
                                            update_qty_options();
                                            if(typeof recalculateprice == 'function') {
                                                recalculateprice();
                                            }
                                        });
                                        $('.owq-quantity .owq-add').on('click', function() {
                                            $input = $(this).prev();
                                            qty = Number($input.val());
                                            if (isNaN(qty)) qty = 0;
                                            qty++;
                                            if ($input.data('max') && qty > $input.data('max')) qty = $input.data('max');
                                            $input.val(qty).trigger('input');
                                            countManyTotal();

                                        });
                                        $('.owq-quantity .owq-sub').on('click', function() {
                                            $input = $(this).next();
                                            qty = Number($input.val());

                                            if (isNaN(qty)) qty = 0;
                                            qty--;
                                            if (qty < 1) qty = '0';
                                            $input.val(qty).trigger('input');
                                            countManyTotal();
                                        });
                                        update_qty_options();
                                    });
                                    //--></script>


                                <div class="zag2">Варианты товара</div>


                                <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />


                            <?php foreach ($options as $option) { ?>

                            <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
                                <div class="price-col clearfix">
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">


                                            <?php if (0) { ?>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="price_big_table__name">
                                                        <input  type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/" data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['owq_title']; ?> <?php if (!empty($option['owq_has_sku'])) { ?><span class="product-sku active" data-product="99" data-variant="238">арт.&nbsp;<?php echo $option_value['owq_sku']; ?></span><?php } ?>
                                                        <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                    </div>
                                                    <div class="price_big_table__price">
                                                        <div class="price-value fl-left sum active" data-product="<?php echo $product_id; ?>" data-stock="37" >
                                                            <?php if ($option_value['owq_full_price_text']) { ?><span class="price_buy_block">Цена</span> <?php echo $option_value['owq_full_price_text']; ?><?php } ?>
                                                        </div><!--  <div class="stock active"><span class="stock-in" style="display: block;">Есть в наличии</span><span class="stock-out" style="display: none;">Нет в наличии</span></div>   -->
                                                    </div>
                                                    <div class="price_big_table__x">
                                                        <div class="separator fl-left">
                                                            х
                                                        </div>
                                                    </div>
                                                    <div class="price_big_table_buy owq-quantity">
                                                        <button class="btn btn-default bootstrap-touchspin-down tsp-small owq-sub"  type="button">-</button>
                                                        <input type="text" value="<?php echo $minimum; ?>" <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?> class="art-quantity-input owq-input" />
                                                        <button class="btn btn-default bootstrap-touchspin-up tsp-small owq-add"  type="button">+</button>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>






                                            <?php if (1) { ?>
                                                <div id="many-products-form">
                                                <table class="price_big_table">
                                                    <tbody>
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <tr <?php if ($option_value['owq_price_old_value']>0 || !empty($option_value['owq_action'])) { ?> class="discount_price_mode"<?php }?>>
                                                            <td class="td_option_name">
                                                                <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/" data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['owq_title']; ?>
                                                                <?php if (!empty($option['owq_has_sku'])) { ?><br/>арт. <?php echo $option_value['owq_sku']; ?><?php } ?>

                                                            </td>
                                                            <?php if (!empty($option['owq_has_stock']) and (0)) { ?>
                                                                <td class="stock">
                                                                    <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                                </td>
                                                            <?php } ?>
                                                            <td class="td_option_price <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder']==1 ) {  ?> ask-availble<?php } ?>">
                                                                <div class="price_text">Цена </div>
                                                                <div class="price-value  sum ">
                                                                    <?php if ($option_value['owq_price_old_value']>0) { ?>
                                                                    <div class="price-value-old"><?php echo $option_value['owq_price_old']; ?></div>
                                                                        <div class="price-value-new"><?php echo $option_value['owq_full_price_text']; ?></div>
                                                                    <?php } else { ?>
                                                                        <?php if ($option_value['owq_full_price_text']) { ?>
                                                                            <?php echo $option_value['owq_full_price_text']; ?>
                                                                        <?php } ?>
                                                                    <?php } ?>

                                                                </div>
                                                            </td>
                                                            <?php if (!empty($option_value['owq_discounts'])) { ?>
                                                                <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                                                                    <td><?php echo $dvalue; ?></td>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <td class="td_option_quantity">
                                                                <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder']==1 ) {  ?>

                                                                    <button class="btn btn-secondary cart-buy button-order"  onclick="asklist.preoptionadd('<?php echo $product_id; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>

                                                                    <?php } else { ?>
                                                                <div class="owq-quantity">
                                                                    <button class="btn bootstrap-touchspin-down tsp-small owq-sub"  type="button">-</button>
                                                                    <input data-price="<?php echo $option_value['owq_full_price']; ?>" data-discount="<?php echo $option_value['owq_price_bonus']; ?>" type="text" value="1" <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?> class="cart-quantity-input owq-input" />
                                                                    <button class="btn bootstrap-touchspin-up tsp-small owq-add"  type="button">+</button>
                                                                </div>
                                                                <?php } ?>
                                                            </td>

                                                    <td class="td_option_add_button">
                                                        <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder']==1 ) {  ?>



                                                        <?php } else { ?>
                                                        <div class="owq-add-button" id="add-<?php echo $option_value['product_option_value_id']; ?>">
                                                            <button type="button"  class="btn btn-primary fl-right buy-button" onclick="cart.optionaddvieinproduct('<?php echo $product_id; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                        </div>
                                                        <?php } ?>
                                                    </td>


                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <style type="text/css">
                                    input[name="quantity"] {
                                        display: none !important;
                                    }
                                </style>
                                <?php $entry_qty = ''; ?>
                            <?php } ?>






                                <?php if ($option['type'] == 'select') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                <?php } ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                    <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'checkbox') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                    <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'image') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'text') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                </div>
                            <?php } ?>



                                <?php if ($option['type'] == 'input_qty_ns') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                                        <table>
                                            <thead>
                                            <tr>
                                                <td><?php echo $option['name']; ?>:</td>
                                                <?php if (!empty($option['owq_has_stock'])) { ?><td>Наличие:</td><?php } ?>
                                                <td>Цена:</td>
                                                <td class="col-quantity"><?php echo $entry_qty; ?>:</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <tr>
                                                    <td>
                                                        <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="|" data-price="0" data-prefix="<?php echo $option_value['price_prefix']; ?>" data-fprice="<?php echo $option_value['owq_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                    </td>
                                                    <?php if (!empty($option['owq_has_stock'])) { ?>
                                                        <td class="stock">
                                                            <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                        </td>
                                                    <?php } ?>
                                                    <td>
                                                        <?php if ($option_value['price']) { ?>
                                                            <?php echo $option_value['price_prefix'].$option_value['price']; ?>
                                                        <?php } ?>
                                                    </td>
                                                    <td class="col-quantity">
                                                        <div class="owq-quantity"><span class="owq-sub">&lt;</span><input type="text" value="" <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?> class="form-control owq-input" /><span class="owq-add">&gt;</span></div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>

                                <?php if ($option['type'] == 'select_qty') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                                        <table>
                                            <thead>
                                            <tr>
                                                <td><?php echo $option['name']; ?>:</td>
                                                <td class="col-quantity"><?php echo $entry_qty; ?>:</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                        <option value=""><?php echo $text_select; ?></option>
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <option value="<?php echo $option_value['product_option_value_id']; ?>" data-fprice="<?php echo $option_value['owq_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="owq-quantity"><span class="owq-sub">&lt;</span><input type="text" value="" class="form-control owq-input" /><span class="owq-add">&gt;</span></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>



                                <?php if ($option['type'] == 'textarea') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'file') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                    <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'date') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <div class="input-group date">
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <div class="input-group datetime">
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                </div>
                            <?php } ?>
                                <?php if ($option['type'] == 'time') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <div class="input-group time">
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                </div>
                            <?php } ?>
                            <?php } ?>
                            <?php } ?>
                            <?php if ($recurrings) { ?>
                                <hr>
                                <h3><?php echo $text_payment_recurring ?></h3>
                                <div class="form-group required">
                                    <select name="recurring_id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($recurrings as $recurring) { ?>
                                            <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="help-block" id="recurring-description"></div>
                                </div>
                            <?php } ?>


                        </div>
                        <div class="product-reviews clearfix">
                            <a class="review-link js-review-link-scroll fl-right" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><span class="link-wrapper"><?php echo $reviews; ?></span></a>
                            <div class="rating fl-left">
                                <div id="rating-one-product-<?php echo $product_id; ?>" style="white-space: nowrap; cursor: pointer;">
                                    <?php if ($review_status) { ?>
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($rating < $i) { ?>
                                                <span class="fa fa-stack" onclick="reviewstar.addstar('<?php echo $product_id; ?>','<?php echo $i;?>')"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <?php } else { ?>
                                                <span class="fa fa-stack"  onclick="reviewstar.addstar('<?php echo $product_id; ?>','<?php echo $i;?>');"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <span class="rating-count" style="display: none"><?php echo $rating2; ?></span>
                                </div>
                            </div>

                        </div>

                        <div class="compare-block compare-block-<?php echo $product_id; ?>">
                            <?php if ($compare_status){ ?>
                                <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                            <?php } else { ?>
                                <span class="btn-compare"  onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-check"></i> Сравнить </span>
                            <?php } ?>
                        </div>


                    </div>
                        <?php if (0) { ?>
                               <!-- /*общая кнопка добавить в корзину*/-->

                    <?php if ($is_qty_option==1) { ?>
                    <!--price_button-->
                    <div class="main_product_block_sale_price_button">
                        <div class="all_price">
                            <span class="all_price__items">Всего штук: <span id="qty_total">0</span></span><br>
                            <span class="all_price__price">Итого: <span id="price_total">0</span> руб.</span><br>
                            <span class="all_price__bonus">Ваша скидка: <span id="bonus_total"></span> руб.</span>
                        </div>
                        <div class="product-options-wrapper">
                            <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary cart-buy buy-button"><?php echo $button_cart; ?></button>
                            <form id="product-form-<?php echo $product_id; ?>" name="product-form-<?php echo $product_id; ?>">
                                <input class="input-variant-id" name="variant_id" type="hidden" value="238">
                            </form>
                            <div class="product-reviews clearfix">
                                <a class="review-link js-review-link-scroll fl-right" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><span class="link-wrapper"><?php echo $reviews; ?></span></a>
                                <div class="rating fl-left">
                                    <div id="rating-one-product-<?php echo $product_id; ?>" style="white-space: nowrap; cursor: pointer;">
                                        <?php if ($review_status) { ?>
                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                <?php if ($rating < $i) { ?>
                                                    <span class="fa fa-stack" onclick="reviewstar.addstar('<?php echo $product_id; ?>','<?php echo $i;?>')"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                <?php } else { ?>
                                                    <span class="fa fa-stack"  onclick="reviewstar.addstar('<?php echo $product_id; ?>','<?php echo $i;?>');"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <span class="rating-count"><?php echo $rating2; ?></span>
                                    </div>
                                </div>

                            </div>
                            <div class="compare-block compare-block-<?php echo $product_id; ?>">
                                <?php if ($compare_status){ ?>
                                    <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                                <?php } else { ?>
                                    <span class="btn-compare"  onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-check"></i> Сравнить </span>
                                <?php } ?>
                            </div>

                        </div>
                    </div><!--price_button-->
                    <?php }  ?>
                    <?php }  ?>


                    <?php } /*$options*/ ?>
                    <?php } else { ?>
                        <button class="btn btn-secondary out-p-button txt-up cat-out-p-button"><?php echo $product_status_text; ?></button>
                    <?php }?>

                    <div class="clearfix"></div>

                    <?php if ($product_attribute_data) { ?>
                        <div class="characteristics-table">
                            <table class="table table-bordered">
                                    <tbody>
                                    <?php foreach ($product_attribute_data as $key=> $value) { ?>
                                        <tr>
                                            <td><?php echo $key; ?></td>
                                            <td>
                                                <?php $k=''; foreach ($value as $text) { $k++; ?>
                                                <?php echo $text['text']; ?><?php if($k<count($value)) echo ","; ?>
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div><!--main_product_block_sale_price-->
            </div><!--main product block sale-->
        </div>
    </div>
</div><!--content-wrapper center-wrapper-->

    <!--  product-consist-->
    <div class="product-consist">
        <div class="center-wrapper">

            <ul class="nav nav-tabs menu_products_properties">
                <li class="active"><a href="#tab-description" data-toggle="tab" class="js-clicked"><?php echo $tab_description; ?></a></li>
                <?php foreach ($description_places as $key=>$value ) { ?>
                    <li><a href="#<?php echo $key; ?>" data-toggle="tab" class="js-clicked"><?php echo (!empty($value['name']))? $value['name'] : 'Дополнительно'; ?></a></li>
                <?php } ?>
                <li><a href="#tab-delivery" data-toggle="tab" class="js-clicked">Доставка</a></li>
                <?php if ($review_status) { ?>
                    <li><a href="#tab-review" data-toggle="tab" class="js-tab-review"><?php echo $tab_review; ?></a></li>
                <?php } ?>




            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-description">
                    <div class="hided-block-inner">
                        <div class="zag2">Описание</div>
                    <?php echo $description; ?>
                    </div>
                </div>
                <?php foreach ($description_places as $key=>$value ) { ?>
                <div class="tab-pane" id="<?php echo $key; ?>">
                    <div class="zag2"><?php echo $value['name']; ?></div>
                    <div class="hided-block-inner"><?php echo $value['descr']; ?></div>
                </div>
                <?php } ?>

                <div class="tab-pane" id="tab-delivery">
                    <div class="hided-block-inner">
                    <div class="zag2">Информация о доставке в Москве</div>
                        <noindex><?php echo $text_delivery; ?></noindex>
                </div>
                </div>


                <?php if ($review_status) { ?>
                    <div class="tab-pane" id="tab-review">
                        <div class="hided-block-inner">
                        <form class="form-horizontal" id="form-review">
                            <div id="review" class="rating2"></div>
                            <h2><?php echo $text_write; ?></h2>
                            <?php if ($review_guest) { ?>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                        <input type="text" name="name" value="" id="input-name" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-name">Ваш e-mail</label>
                                        <input type="text" name="email" value="" id="input-name" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                        <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                        <div class="help-block"><?php echo $text_note; ?></div>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label"><?php echo $entry_rating; ?></label>
                                        &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                        <input type="radio" name="rating" value="1" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="2" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="3" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="4" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="5" />
                                        &nbsp;<?php echo $entry_good; ?></div>
                                </div>
                                <?php echo $captcha; ?>
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?php echo $text_login; ?>
                            <?php } ?>
                        </form>
                    </div>
                    </div>
                <?php } ?>


            </div>




        </div>
    </div>
    <!--product-consist-->



<!--product-releted-->
<?php if ($products) { ?>
<div class="center-wrapper">
    <section class="watched-products">
        <!--products-wrapper-->
        <div class="zag2">К этому товару подходят</div>
        <div class="products-wrapper clearfix" id="products-container-releted" style="float: left; width: 100%;">
            <?php $h='0'; $hh='0';  $h_end= count($products); foreach ($products as $product) { $h++; $hh++ ?>
                <?php if($h=='1') {  ?>
                    <div class="prod-cat-line clearfix" style="float: left;">
                <?php }?>
                <div class="product-wrapper" id="product-<?php echo $product['product_id']; ?>">
                    <form id="product-form-2852" name="product-form-<?php echo $product['product_id']; ?>">
                        <div class="prod-link-container">
                            <?php  if ($userLogged) { ?>
                                <div class="edit_container">
                                    <a class="edit" target="_blank" href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product['product_id']; ?>">Редактировать</a>
                                </div>
                            <?php } ?>

                            <a class="product-link" data-brand="<?php echo $product['manufacturer']; ?>"  data-id="<?php echo $product['product_id']; ?>" data-name="<?php echo $product['name']; ?>" data-position="<?php echo $product['product_id']; ?>" data-price="<?php echo $product['data_price_min']; ?>" data-sku="<?php echo $product['model']; ?>" data-variant="" href="<?php echo $product['href']; ?>">
                                <?php if ($product['lable_action']==1 ) { ?>
                                    <span class="stripe action-text">Акция</span>
                                <?php } ?>
                                <?php if($product['sticker_status']['kod']!='0') { ?>
                                    <span class="stripe new-text  <?php if ($product['lable_action']==1 ) { ?> stripe-2 <?php }?>"><?php echo $product['sticker_status']['name']; ?></span>
                                <?php } ?>
                                <div class="image-container"><img alt="<?php echo $product['name']; ?>" src="<?php echo $product['thumb']; ?>"></div>
                                <span class="link-name"><?php echo $product['name']; ?></span></a>
                        </div>
                        <?php if($product['options'] AND $product['ean_status']['kod']==0) { ?>
                            <div class="options">
                                <?php foreach ($product['options'] as $option) { ?>
                                    <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '' )){ ?>
                                        <!--option tab-->
                                        <?php if(count($option['product_option_value'])>0) { ?>
                                            <!--heads-->
                                            <div class="options_weight">
                                                <?php  $z=''; $k=''; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>

                                                    <?php if ($k == 4) { ?>
                                                        <div>
                                                            <a class="no_active" href="<?php echo $product['href']; ?>" >еще <?php echo  count($option['product_option_value'])-3; ?></a>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($k < 4) { ?>
                                                        <div class="one-v <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" data-variant="rel-<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" >
                                                            <?php echo str_replace('Упаковка', '', $option_value['owq_title_short']); ?>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div><!--heads-->                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                    <div class="option-tabs-content">
                                                        <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                            <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="rel-<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                <div class="switch-items">
                                                                    <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                        <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                            <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                <?php echo $option_value['price']; ?>
                                                                            <?php } else { ?>
                                                                                <?php echo $option_value['price']; ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="button_curier">
                                                                        <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                            <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                        <?php } else  { ?>
                                                                            <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                        <?php } ?>
                                                                        <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                    </div>

                                                                </div>
                                                                <div class="extra-info no_options">
                                                                    <div class="custom-select">
                                                                        <div class="select-mask"><?php echo $option_value['owq_title']; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div><!--option tab-->
                                        <?php } else { ?>
                                            <div class="switch-items type-2"></div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '1')){  ?>

                                        <?php if(count($option['product_option_value'])>0) { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                    <div class="option-tabs-content">
                                                        <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']);  foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                            <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                <div class="switch-items type-2">
                                                                    <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                        <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                            <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                <?php echo $option_value['price']; ?>
                                                                            <?php } else { ?>
                                                                                <?php echo $option_value['price']; ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="button_curier">
                                                                        <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                            <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                        <?php } else  { ?>
                                                                            <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                        <?php } ?>
                                                                        <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="extra-info">
                                                        <!--heads-->
                                                        <div  class="custom-select">
                                                            <div class="select-mask">
                                                                <?php $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                    <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?>

                                                                <?php } ?>
                                                            </div>
                                                            <div class="custom-select-content"></div>
                                                            <select class="custom-select-hidden variant-select cat-ch-variant" >
                                                                <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                    <option data-stock="19" data-city="<?php echo $option_value['owq_title']; ?>" data-product="<?php echo $product['product_id']; ?>"  data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>"  ><?php echo $option_value['owq_title']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div><!--heads-->
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }  else {?>
                                            <div class="switch-items type-2"></div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div><!--!option-->
                        <?php } else { ?>
                            <div class="switch-items type-2">
                                <?php  if($product['ean_status']['kod']!='0'){ ?>
                                    <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo  $product['ean_status']['name'];?>     </button>
                                <?php }?>

                            </div>

                        <?php }?>
                        <!--rating-->
                        <div class="reviews-marks clearfix">
                            <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                            <div class="rating">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($product['rating'] < $i) { ?>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    <?php } else { ?>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div class="compare-block compare-block-<?php echo $product['product_id']; ?>">
                                <?php if ($product['compare_status']){ ?>
                                    <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                                <?php } else { ?>
                                    <span class="btn-compare"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-check"></i> Сравнить </span>
                                <?php } ?>
                            </div>
                        </div><!--!rating-->
                    </form>
                </div>
                <?php if($h=='5' || $hh==$h_end) {   $h=''; ?>
                    </div><!--products-container-->
                <?php }?>
            <?php } ?>
        </div><!--products-wrapper-->
    </section>
</div>
<div class="separator-line "></div>
<?php } ?>
<!--product-related-->


<!--product-from-category-->
<?php if ($products_category) { ?>
    <div class="center-wrapper">
        <section class="watched-products">
            <!--products-wrapper-->
            <div class="zag2">Похожие товары</div>
            <div class="products-wrapper clearfix" id="products-container-category" style="float: left; width: 100%;">
                <?php $h='0'; $hh='0';  $h_end= count($products_category); foreach ($products_category as $product) { $h++; $hh++ ?>
                    <?php if($h=='1') {  ?>
                        <div class="prod-cat-line clearfix" style="float: left;">
                    <?php }?>
                    <div class="product-wrapper" id="product-<?php echo $product['product_id']; ?>">
                        <form id="product-form-2852" name="product-form-<?php echo $product['product_id']; ?>">
                            <div class="prod-link-container">
                                <?php  if ($userLogged) { ?>
                                    <div class="edit_container">
                                        <a class="edit" target="_blank" href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product['product_id']; ?>">Редактировать</a>
                                    </div>
                                <?php } ?>

                                <a class="product-link" data-brand="<?php echo $product['manufacturer']; ?>"  data-id="<?php echo $product['product_id']; ?>" data-name="<?php echo $product['name']; ?>" data-position="<?php echo $product['product_id']; ?>" data-price="<?php echo $product['data_price_min']; ?>" data-sku="<?php echo $product['model']; ?>" data-variant="" href="<?php echo $product['href']; ?>">
                                    <?php if ($product['lable_action']==1 ) { ?>
                                        <span class="stripe action-text">Акция</span>
                                    <?php } ?>
                                    <?php if($product['sticker_status']['kod']!='0') { ?>
                                        <span class="stripe new-text  <?php if ($product['lable_action']==1 ) { ?> stripe-2 <?php }?>"><?php echo $product['sticker_status']['name']; ?></span>
                                    <?php } ?>
                                    <div class="image-container"><img alt="<?php echo $product['name']; ?>" src="<?php echo $product['thumb']; ?>"></div>
                                    <span class="link-name"><?php echo $product['name']; ?></span></a>
                            </div>
                            <?php if($product['options'] AND $product['ean_status']['kod']==0) { ?>
                                <div class="options">
                                    <?php foreach ($product['options'] as $option) { ?>
                                        <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '' )){ ?>
                                            <!--option tab-->
                                            <?php if(count($option['product_option_value'])>0) { ?>
                                                <!--heads-->
                                                <div class="options_weight">
                                                    <?php  $z=''; $k=''; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>

                                                        <?php if ($k == 4) { ?>
                                                            <div>
                                                                <a class="no_active" href="<?php echo $product['href']; ?>" >еще <?php echo  count($option['product_option_value'])-3; ?></a>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($k < 4) { ?>
                                                            <div class="one-v <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" data-variant="cat-<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" >
                                                                <?php echo str_replace('Упаковка', '', $option_value['owq_title_short']); ?>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div><!--heads-->

                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                        <div class="option-tabs-content">
                                                            <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="cat-<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                    <div class="switch-items">
                                                                        <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                            <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                                <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                    <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="button_curier">
                                                                            <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                                <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                            <?php } else  { ?>
                                                                                <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                            <?php } ?>
                                                                            <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                        </div>

                                                                    </div>
                                                                    <div class="extra-info no_options">
                                                                        <div class="custom-select">
                                                                            <div class="select-mask"><?php echo $option_value['owq_title']; ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div><!--option tab-->
                                            <?php } else { ?>
                                                <div class="switch-items type-2"></div>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '1')){  ?>

                                            <?php if(count($option['product_option_value'])>0) { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                        <div class="option-tabs-content">
                                                            <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']);  foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                    <div class="switch-items type-2">
                                                                        <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                            <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                                <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                    <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo $option_value['price']; ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="button_curier">
                                                                            <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                                <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                            <?php } else  { ?>
                                                                                <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                            <?php } ?>
                                                                            <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="extra-info">
                                                            <!--heads-->
                                                            <div  class="custom-select">
                                                                <div class="select-mask">
                                                                    <?php $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                        <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?>

                                                                    <?php } ?>
                                                                </div>
                                                                <div class="custom-select-content"></div>
                                                                <select class="custom-select-hidden variant-select cat-ch-variant" >
                                                                    <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                        <option data-stock="19" data-city="<?php echo $option_value['owq_title']; ?>" data-product="<?php echo $product['product_id']; ?>"  data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>"  ><?php echo $option_value['owq_title']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div><!--heads-->
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }  else {?>
                                                <div class="switch-items type-2"></div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div><!--!option-->
                            <?php } else { ?>
                                <div class="switch-items type-2">
                                    <?php  if($product['ean_status']['kod']!='0'){ ?>
                                        <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo  $product['ean_status']['name'];?>     </button>
                                    <?php }?>

                                </div>

                            <?php }?>
                            <!--rating-->
                            <div class="reviews-marks clearfix">
                                <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                                <div class="rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($product['rating'] < $i) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="compare-block compare-block-<?php echo $product['product_id']; ?>">
                                    <?php if ($product['compare_status']){ ?>
                                        <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                                    <?php } else { ?>
                                        <span class="btn-compare"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-check"></i> Сравнить </span>
                                    <?php } ?>
                                </div>
                            </div><!--!rating-->
                        </form>
                    </div>
                    <?php if($h=='5' || $hh==$h_end) {   $h=''; ?>
                        </div><!--products-container-->
                    <?php }?>
                <?php } ?>
            </div><!--products-wrapper-->
        </section>
    </div>
    <div class="separator-line"></div>
<?php } ?>
<!--product-from-category-->

<div class="clearfix"></div>
      <?php if ($tags and 0) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?>

<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#recurring-description').html('');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-cart').button('loading');
            },
            complete: function() {
                $('#button-cart').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';

                    htm += '</div></div>';
                    $('body').append(htm);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total'] );
                    }, 100);
                    if (json['countproducts']>0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');

                    }
                }
                },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        if ($('#review').length != 0) {
            $('html, body').animate({ scrollTop: $('#review').offset().top - 150 }, 300);
        }
        $('#review').fadeOut('slow');
        $('#review').load(this.href);
        $('#review').fadeIn('slow');

    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function() {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function() {
                $('#button-review').button('loading');
            },
            complete: function() {
                $('#button-review').button('reset');
            },
            success: function(json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });


   $('.jcarousel').slick({
       dots: true,
       infinite: true,
       speed: 300,
       autoplay: false,
       autoplaySpeed: 2000,
       adaptiveHeight: true,
       prevArrow:'<a class="jcarousel-control-prev" href="#"><span></span></a>',
       nextArrow:'<a class="jcarousel-control-next" href="#"><span></span></a>',
       slidesToShow: 5,
       slidesToScroll: 2,
       responsive: [
           {
               breakpoint: 1024,
               settings: {
                   slidesToShow: 3,
                   slidesToScroll: 3,
                   infinite: true,
                   dots: true
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2
               }
           }

       ]
    });


    //--></script>

<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<script src="catalog/view/javascript/jquery/jquery.mousewheel.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#lightgallery').lightGallery({
            loop: true,
            fourceAutoply: true,
            autoplay: false,
            thumbnail: true,
            hash: false,
            speed: 400,
            scale: 1,
            keypress: true,
            counter: false,
            download:false,
        });

                 $('.click-triger').on('click', function(e) {
                console.log($(this).data("link"));
                $(this).data("link") ;
                $( '#' +$(this).data("link") ).trigger( "click" );
            })
    });
    </script>
<script>
    window.dataLayer = window.dataLayer || [];

    dataLayer.push({
        "ecommerce": {
            "detail": {
                "products": [
                    {
                        "id": "<?php echo $model; ?>",
                        "name" : "<?php echo $heading_title; ?>",
                        "price":"<?php echo $shablon_price; ?>",
    "brand":"<?php echo $manufacturer; ?>",
    "quantity":"1",
    "category":"<?php echo $category_to_metrika; ?>",
     },
    ]
    }
    }
    });
</script>

<link href="catalog/view/theme/default/stylesheet/lg_fonts.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/lightgallery.css" rel="stylesheet">
 <?php echo $footer; ?>
