        <div  id="product-<?php echo $product_id; ?>" class="product-page-wrapper product-page-short">
            <div class="main_product_block_sale clearfix">
                <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>

                <?php if ($thumb || $images) { ?>
                    <div class="main_product_block_sale_img">
                        <?php if ($thumb) { ?>
                            <div class="product-main-image"><a class="click-triger" data-link="image-0" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" data-zoom-image="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="main_product_block_sale_price">
                    <?php if (empty($product_status)) { ?>
                        <?php if ($options) { ?>
                            <div class="main_product_block_sale_price_list">
                                <div id="product">
                                    <?php if ($options) { ?>
                                        <script type="text/javascript"><!--
                                            function update_qty_options() {
                                                $('.owq-option input[type="checkbox"]').each(function() {
                                                    $qty = $(this).closest('tr').find('.owq-input');
                                                    opt_qty = Number($qty.val());
                                                    if (isNaN(opt_qty)) opt_qty = 0;

                                                    if ($qty.data('max') && opt_qty > $qty.data('max')) {
                                                        $qty.closest('tr').addClass('no-stock');
                                                    } else {
                                                        $qty.closest('tr').removeClass('no-stock');
                                                    }

                                                    if ($(this).data('id') && opt_qty > 0) {
                                                        $(this).val($(this).data('id') + $(this).data('split') + opt_qty).data('price', $(this).data('fprice') * opt_qty).prop('checked', true);
                                                    } else {
                                                        $(this).prop('checked', false);
                                                    }
                                                });
                                                $('.owq-option select').each(function() {
                                                    $qty = $(this).closest('div').find('.owq-input');
                                                    opt_qty = Number($qty.val());
                                                    if (isNaN(opt_qty)) opt_qty = 0;

                                                    $(this).find('option').each(function(){
                                                        if ($(this).data('id') && opt_qty > 0) {
                                                            $(this).val($(this).data('id') + '|' + opt_qty).data('price', $(this).data('fprice') * opt_qty);
                                                        } else {
                                                            $(this).val('').data('price', 0);
                                                        }
                                                    });
                                                });
                                            }
                                            $(document).ready(function(){
                                                $('.owq-option .owq-input').on('input',function(){
                                                    update_qty_options();
                                                    if(typeof recalculateprice == 'function') {
                                                        recalculateprice();
                                                    }
                                                });
                                                $('.owq-quantity .owq-add').on('click', function() {
                                                    $input = $(this).prev();
                                                    $variantId = $(this).data('variant');
                                                    $productId = $(this).data('product');
                                                    $optionId = $(this).data('option');

                                                    qty = Number($input.val());
                                                    if (isNaN(qty)) qty = 0;
                                                    qty++;
                                                    if ($input.data('max') && qty > $input.data('max')) qty = $input.data('max');
                                                    $input.val(qty).trigger('input');
                                                    $('#add-'+ $variantId).html('<button type="button"  class="btn btn-primary fl-right buy-button" onclick="cart.optionaddview('+$productId+', '+qty+', '+$optionId+', '+$variantId+');">Купить</button>');
                                                    countManyTotal();

                                                });
                                                $('.owq-quantity .owq-sub').on('click', function() {
                                                    $input = $(this).next();
                                                    $variantId = $(this).data('variant');
                                                    $productId = $(this).data('product');
                                                    $optionId = $(this).data('option');

                                                    qty = Number($input.val());

                                                    if (isNaN(qty)) qty = 0;
                                                    qty--;
                                                    if (qty < 1) qty = '0';
                                                    $input.val(qty).trigger('input');
                                                    $('#add-'+ $variantId).html('<button type="button"  class="btn btn-primary fl-right buy-button" onclick="cart.optionaddview('+$productId+', '+qty+', '+$optionId+', '+$variantId+');">Купить</button>');

                                                    countManyTotal();
                                                });
                                                update_qty_options();
                                            });
                                            //--></script>


                                        <div class="zag2">Варианты товара</div>


                                        <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />


                                    <?php foreach ($options as $option) { ?>

                                    <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
                                        <div class="price-col clearfix">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">

                                                    <?php if (1) { ?>
                                                        <div id="many-products-form">
                                                            <table class="price_big_table">
                                                                <tbody>
                                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                    <tr <?php if ($option_value['owq_price_old_value']>0 || !empty($option_value['owq_action'])) { ?> class="discount_price_mode"<?php }?>>
                                                                        <td class="td_option_name">
                                                                            <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/" data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                            <?php echo $option_value['owq_title']; ?>
                                                                            <?php if (!empty($option['owq_has_sku'])) { ?><br/>арт. <?php echo $option_value['owq_sku']; ?><?php } ?>

                                                                        </td>
                                                                        <?php if (!empty($option['owq_has_stock']) and (0)) { ?>
                                                                            <td class="stock">
                                                                                <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                                            </td>
                                                                        <?php } ?>
                                                                        <td class="td_option_price <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder']==1 ) {  ?> ask-availble<?php } ?>">
                                                                            <div class="price_text">Цена </div>
                                                                            <div class="price-value  sum ">
                                                                                <?php if ($option_value['owq_price_old_value']>0) { ?>
                                                                                    <div class="price-value-old"><?php echo $option_value['owq_price_old']; ?></div>
                                                                                    <div class="price-value-new"><?php echo $option_value['owq_full_price_text']; ?></div>
                                                                                <?php } else { ?>
                                                                                    <?php if ($option_value['owq_full_price_text']) { ?>
                                                                                        <?php echo $option_value['owq_full_price_text']; ?>
                                                                                    <?php } ?>
                                                                                <?php } ?>

                                                                            </div>
                                                                        </td>
                                                                        <?php if (!empty($option_value['owq_discounts'])) { ?>
                                                                            <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                                                                                <td><?php echo $dvalue; ?></td>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <td class="td_option_quantity">
                                                                            <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder']==1 ) {  ?>

                                                                                <span class="no-avaible">Нет в наличии</span>

                                                                            <?php } else { ?>
                                                                                <div class="owq-quantity">
                                                                                    <button class="btn bootstrap-touchspin-down tsp-small owq-sub" data-product="<?php echo $product_id; ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" data-option="<?php echo $option['product_option_id']; ?>"  type="button">-</button>
                                                                                    <input data-price="<?php echo $option_value['owq_full_price']; ?>" data-discount="<?php echo $option_value['owq_price_bonus']; ?>" type="text" value="1" <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?> class="cart-quantity-input owq-input" />
                                                                                    <button class="btn bootstrap-touchspin-up tsp-small owq-add" data-product="<?php echo $product_id; ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" data-option="<?php echo $option['product_option_id']; ?>"  type="button">+</button>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td class="td_option_add_button">
                                    <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder']==1 ) {  ?>

                                        <button class="btn btn-secondary cart-buy button-order"  onclick="asklist.preoptionadd('<?php echo $product_id; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>

                                    <?php } else { ?>
                                        <div class="owq-add-button" id="add-<?php echo $option_value['product_option_value_id']; ?>">
                                            <button type="button"  class="btn btn-primary fl-right buy-button" onclick="cart.optionaddview('<?php echo $product_id; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                        </div>
                                    <?php } ?>
                                                                        </td>

                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <style type="text/css">
                                            input[name="quantity"] {
                                                display: none !important;
                                            }
                                        </style>
                                        <?php $entry_qty = ''; ?>
                                    <?php } ?>






                                        <?php if ($option['type'] == 'select') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'radio') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'image') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'text') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        </div>
                                    <?php } ?>



                                        <?php if ($option['type'] == 'input_qty_ns') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <td><?php echo $option['name']; ?>:</td>
                                                        <?php if (!empty($option['owq_has_stock'])) { ?><td>Наличие:</td><?php } ?>
                                                        <td>Цена:</td>
                                                        <td class="col-quantity"><?php echo $entry_qty; ?>:</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <tr>
                                                            <td>
                                                                <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="|" data-price="0" data-prefix="<?php echo $option_value['price_prefix']; ?>" data-fprice="<?php echo $option_value['owq_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['name']; ?>
                                                            </td>
                                                            <?php if (!empty($option['owq_has_stock'])) { ?>
                                                                <td class="stock">
                                                                    <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                                </td>
                                                            <?php } ?>
                                                            <td>
                                                                <?php if ($option_value['price']) { ?>
                                                                    <?php echo $option_value['price_prefix'].$option_value['price']; ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="col-quantity">
                                                                <div class="owq-quantity"><span class="owq-sub">&lt;</span><input type="text" value="" <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?> class="form-control owq-input" /><span class="owq-add">&gt;</span></div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>

                                        <?php if ($option['type'] == 'select_qty') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <td><?php echo $option['name']; ?>:</td>
                                                        <td class="col-quantity"><?php echo $entry_qty; ?>:</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                                <option value=""><?php echo $text_select; ?></option>
                                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                    <option value="<?php echo $option_value['product_option_value_id']; ?>" data-fprice="<?php echo $option_value['owq_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                                        <?php if ($option_value['price']) { ?>
                                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                        <?php } ?>
                                                                    </option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <div class="owq-quantity"><span class="owq-sub">&lt;</span><input type="text" value="" class="form-control owq-input" /><span class="owq-add">&gt;</span></div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>



                                        <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'file') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'date') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group date">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group datetime">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                        <?php if ($option['type'] == 'time') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group time">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php } ?>

                                </div>

                            </div>



                        <?php } /*$options*/ ?>
                    <?php } else { ?>
                        <button class="btn btn-secondary out-p-button txt-up cat-out-p-button"><?php echo $product_status_text; ?></button>
                    <?php }?>

                    <div class="clearfix"></div>

                </div>




<div class="popup-view-dawn-content">
                <?php if ($product_attribute_data) { ?>
                    <div class="characteristics-table">
                        <table class="table table-bordered">
                            <tbody>
                            <?php foreach ($product_attribute_data as $key=> $value) { ?>
                                <tr>
                                    <td><?php echo $key; ?></td>
                                    <td>
                                        <?php $k=''; foreach ($value as $text) { $k++; ?>
                                            <?php echo $text['text']; ?><?php if($k<count($value)) echo ","; ?>
                                        <?php } ?>

                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>

                <?php if ($is_qty_option==1) { ?>
                    <!--price_button-->
                    <div class="main_product_block_sale_price_button"  style="display: none">
                        <div class="all_price">
                            <span class="all_price__items">Всего штук: <span id="qty_total">0</span></span><br>
                            <span class="all_price__price">Итого: <span id="price_total">0</span> руб.</span><br>
                            <span class="all_price__bonus">Ваша скидка: <span id="bonus_total"></span> руб.</span>
                        </div>
                    </div><!--price_button-->
                <?php }  ?>
</div>


                <!--main_product_block_sale_price-->
            </div><!--main product block sale-->
        </div>


