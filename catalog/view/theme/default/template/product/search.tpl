<?php echo $header; ?>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                        <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
                    <?php } ?>
                </div>
                <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
            </div>
      <div class="catalog-container clearfix">
          <div class="catalog-wrapper ">
       <?php echo $content_top; ?>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <div class="row">
        <div class="col-sm-4">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-3">
          <select name="category_id" class="form-control">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-3">
          <label class="checkbox-inline">
            <?php if ($sub_category) { ?>
            <input type="checkbox" name="sub_category" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="sub_category" value="1" />
            <?php } ?>
            <?php echo $text_sub_category; ?></label>
        </div>
      </div>
      <p>
        <label class="checkbox-inline">
          <?php if ($description) { ?>
          <input type="checkbox" name="description" value="1" id="description" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="description" value="1" id="description" />
          <?php } ?>
          <?php echo $entry_description; ?></label>
      </p>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
      <h2><?php echo $text_search; ?></h2>
      <?php if ($products) { ?>
          <div class="sort-filters">
              <div class="sort-filters-left">
                  <div class="sort-filters-name">
                      <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                  </div>
                  <div class="sort-filters-select">
                      <select id="input-sort" class="form-control" onchange="location = this.value;">
                          <?php foreach ($sorts as $sorts) { ?>
                              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                  <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                              <?php } ?>
                          <?php } ?>
                      </select>
                  </div>
              </div>
              <div class="sort-filters-right">
                  <div class="sort-filters-name">
                      <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                  </div>
                  <div class="sort-filters-select">
                      <select id="input-limit" class="form-control" onchange="location = this.value;">
                          <?php foreach ($limits as $limits) { ?>
                              <?php if ($limits['value'] == $limit) { ?>
                                  <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                              <?php } ?>
                          <?php } ?>
                      </select>
                  </div>
              </div>
          </div>
      <br />
      <div class="clearfix"></div>
      <div id="prod-container">
              <!--products-wrapper-->
              <div class="products-wrapper clearfix" id="products-container" style="float: left; width: 100%;">
                  <?php $h='0'; $hh='0';  $h_end= count($products); foreach ($products as $product) { $h++; $hh++ ?>
                      <?php if($h=='1') {  ?>
                          <div class="prod-cat-line clearfix" style="float: left;">
                      <?php }?>
                      <div class="product-wrapper" id="product-<?php echo $product['product_id']; ?>">
                          <form id="product-form-2852" name="product-form-<?php echo $product['product_id']; ?>">
                              <div class="prod-link-container">
                                  <?php  if ($userLogged) { ?>
                                      <div class="edit_container">
                                          <a class="edit" target="_blank" href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product['product_id']; ?>">Редактировать</a>
                                      </div>
                                  <?php } ?>

                                  <a class="product-link" data-brand="<?php echo $product['manufacturer']; ?>"  data-id="<?php echo $product['product_id']; ?>" data-name="<?php echo $product['name']; ?>" data-position="<?php echo $product['product_id']; ?>" data-price="<?php echo $product['data_price_min']; ?>" data-sku="<?php echo $product['model']; ?>" data-variant="" href="<?php echo $product['href']; ?>">
                                      <?php if ($product['lable_action']==1 ) { ?>
                                          <span class="stripe action-text">Акция</span>
                                      <?php } ?>
                                      <?php if($product['sticker_status']['kod']!='0') { ?>
                                          <span class="stripe new-text  <?php if ($product['lable_action']==1 ) { ?> stripe-2 <?php }?>"><?php echo $product['sticker_status']['name']; ?></span>
                                      <?php } ?>
                                      <div class="image-container"><img alt="<?php echo $product['name']; ?>" src="<?php echo $product['thumb']; ?>"></div>
                                      <span class="link-name"><?php echo $product['name']; ?></span></a>
                              </div>
                              <?php if($product['options'] AND $product['ean_status']['kod']==0) { ?>
                                  <div class="options">
                                      <?php foreach ($product['options'] as $option) { ?>
                                          <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '' )){ ?>
                                              <!--option tab-->
                                              <?php if(count($option['product_option_value'])>0) { ?>
                                                  <!--heads-->
                                                  <div class="options_weight">
                                                      <?php  $z=''; $k=''; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>

                                                          <?php if ($k == 4) { ?>
                                                              <div>
                                                                  <a class="no_active" href="<?php echo $product['href']; ?>" >еще <?php echo  count($option['product_option_value'])-3; ?></a>
                                                              </div>
                                                          <?php } ?>
                                                          <?php if ($k < 4) { ?>
                                                              <div class="one-v <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" >
                                                                  <?php echo str_replace('Упаковка', '', $option_value['owq_title_short']); ?>
                                                              </div>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </div><!--heads-->

                                                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                      <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                          <div class="option-tabs-content">
                                                              <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                  <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                      <div class="switch-items">
                                                                          <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                              <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                                  <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                      <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                      <?php echo $option_value['price']; ?>
                                                                                  <?php } else { ?>
                                                                                      <?php echo $option_value['price']; ?>
                                                                                  <?php } ?>
                                                                              </div>
                                                                          </div>
                                                                          <div class="button_curier">
                                                                              <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                                  <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                              <?php } else  { ?>
                                                                                  <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                              <?php } ?>
                                                                              <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                          </div>

                                                                      </div>
                                                                      <div class="extra-info no_options">
                                                                          <div class="custom-select">
                                                                              <div class="select-mask"><?php echo $option_value['owq_title']; ?></div>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              <?php } ?>
                                                          </div>
                                                      </div>
                                                  </div><!--option tab-->
                                              <?php } else { ?>
                                                  <div class="switch-items type-2"></div>
                                              <?php } ?>
                                          <?php } ?>
                                          <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '1')){  ?>

                                              <?php if(count($option['product_option_value'])>0) { ?>
                                                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                      <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option-cart">
                                                          <div class="option-tabs-content">
                                                              <?php  $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']);  foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                  <div class="tab-content2 <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> open  active<?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> open  active<?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                      <div class="switch-items type-2">
                                                                          <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                              <div class="price  <?php if ($option_value['owq_price_old_value']!='0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?><span class="price_text">Цена</span><?php } ?>
                                                                                  <?php if ($option_value['owq_price_old_value']!='0') { ?>
                                                                                      <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                      <?php echo $option_value['price']; ?>
                                                                                  <?php } else { ?>
                                                                                      <?php echo $option_value['price']; ?>
                                                                                  <?php } ?>
                                                                              </div>
                                                                          </div>
                                                                          <div class="button_curier">
                                                                              <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                                  <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                              <?php } else  { ?>
                                                                                  <button class="btn btn-secondary fl-right button-order"  onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">Сообщить о наличии</button>
                                                                              <?php } ?>
                                                                              <button class="btn btn-wishlist"  onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"  type="button">В избранное</button>
                                                                          </div>

                                                                      </div>
                                                                  </div>
                                                              <?php } ?>
                                                          </div>
                                                          <div class="extra-info">
                                                              <!--heads-->
                                                              <div  class="custom-select">
                                                                  <div class="select-mask">
                                                                      <?php $z=''; $k=0; if(count($option['product_option_value'])>3) $qty_option =3; else $qty_option=count($option['product_option_value']); foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                          <?php if ($z!='1' AND $option_value['owq_quantity'] > 0 ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?> <?php if ($z!='1' AND $k == $qty_option ) { $z='1'; ?> <?php  echo $option_value['owq_title']; ?><?php } ?>

                                                                      <?php } ?>
                                                                  </div>
                                                                  <div class="custom-select-content"></div>
                                                                  <select class="custom-select-hidden variant-select cat-ch-variant" >
                                                                      <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                          <option data-stock="19" data-city="<?php echo $option_value['owq_title']; ?>" data-product="<?php echo $product['product_id']; ?>"  data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>"  ><?php echo $option_value['owq_title']; ?></option>
                                                                      <?php } ?>
                                                                  </select>
                                                              </div><!--heads-->
                                                          </div>
                                                      </div>
                                                  </div>
                                              <?php }  else {?>
                                                  <div class="switch-items type-2"></div>
                                              <?php } ?>
                                          <?php } ?>
                                      <?php } ?>
                                  </div><!--!option-->
                              <?php } else { ?>
                                  <div class="switch-items type-2">
                                      <?php  if($product['ean_status']['kod']!='0'){ ?>
                                          <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo  $product['ean_status']['name'];?>     </button>
                                      <?php }?>

                                  </div>

                              <?php }?>
                              <!--rating-->
                              <div class="reviews-marks clearfix">
                                  <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                                  <div class="rating">
                                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                                          <?php if ($product['rating'] < $i) { ?>
                                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                          <?php } else { ?>
                                              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                          <?php } ?>
                                      <?php } ?>
                                  </div>
                                  <div class="compare-block compare-block-<?php echo $product['product_id']; ?>">
                                      <?php if ($product['compare_status']){ ?>
                                          <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                                      <?php } else { ?>
                                          <span class="btn-compare"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-check"></i> Сравнить </span>
                                      <?php } ?>
                                  </div>
                              </div><!--!rating-->
                          </form>
                      </div>
                      <?php if($h=='5' || $hh==$h_end) {   $h=''; ?>
                          </div><!--products-container-->
                      <?php }?>
                  <?php } ?>
              </div><!--products-wrapper-->
          <?php if($pagination) { ?>
              <div >
                  <div class="button-block center-text">
                      <button class="btn btn-secondary show-more" onclick="agreeloadproduct()">Показать еще</button>
                  </div>
                  <div class="center-text">
                      <div class="pagenator">
                          <?php echo $pagination; ?>
                      </div>
                  </div>
              </div>
          <?php } ?>

      </div><!--prod-container-->
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div><!--catalog-container clearfix-->
        </div>
</div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('.catalog-container input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('.catalog-container  select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('.catalog-container  input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('.catalog-container  input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('.catalog-container  input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>
