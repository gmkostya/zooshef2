<?php echo $header; ?>
    <div class="content-wrapper center-wrapper ">
        <div class="wrong-page">
            <div class="content-block clearfix">
                <div class="links-block fl-right">
                    <div class="text-block">
                        Посмотрите наши популярные категории:
                    </div>
                    <ul class="links-list">
                        <li class="">
                            <a href="/tovary_dlya_sobak/">Собаки</a>
                        </li>
                        <li class="">
                            <a href="/tovary_dlya_koshek/">Кошки</a>
                        </li>
                        <li class="">
                            <a href="/tovari_dlja_grizunov/">Грызуны</a>
                        </li>
                        <li class="">
                            <a href="/tovari_dlja_ptic/">Птицы</a>
                        </li>
                        <li class="">
                            <a href="/reptilii/">Рептилии</a>
                        </li>
                        <li class="">
                            <a href="/tovari_dlja_rib/">Рыбы</a>
                        </li>
                        <li class="">
                            <a href="/loshadi/">Лошади</a>
                        </li>
                    </ul>
                </div>
                <div class="info-block">
                    <img alt="" src="/image/404-error.png">
                    <div class="text-block">
                        Извините, такой страницы не существует
                    </div>
                    <div class="message-block">
                        Если вы уверены, что эта страница должна быть, пожалуйста, <a class="bordered-link" href="/contact">сообщите об ошибке</a>
                    </div>
                </div>
            </div>

        </div>
        <?php echo $content_bottom; ?>

    </div>
<?php echo $footer; ?>