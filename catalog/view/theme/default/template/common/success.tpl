<?php echo $header; ?>
<?php
// Информация о транзакции
//$trans = array(‘id‘=>$order_id,‘revenue‘=>$total);
 
// Копируем в переменную items наши покупки, чтоб код был максимально похож на исходный.
$items = $products1;
 
// Переводим данные из php в JS для товаров
function getItemJs(&$order_id, &$item) {
return <<<HTML
ga('ecommerce:addItem', {
'id': '{$order_id}',
'name': '{$item["name"]}',
'sku': '{$item["model"]}',
'price': '{$item["price"]}',
'quantity': '{$item["quantity"]}'
});
HTML;
}

?>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                        <?php } else { ?><?php } ?>
                    <?php } ?>
                </div>
                <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>

      <?php echo $text_message; ?>
      <?php
        foreach ($products1 as $product) {
            $productEcomm = array();
            $productEcomm['id'] = $product["product_id"];
            $productEcomm['name'] = $product["name"];
            $productEcomm['price'] = (float) $product["price"];
            $productEcomm['quantity'] = (int) $product["quantity"];
            $productsEcomm[] = $productEcomm;
            echo "<br/>Наименование товара: ".$product["name"];
            echo " по цене ".$product["price"]." руб.";
            echo ", количество: ".$product["quantity"];
            echo ", на сумму: ".$product["total"]." руб. ";
        }
        $eccom['ecommerce'] = array("purchase" => array(
                               "actionField" => array(
                                   "id" => $order_id,
                                   ),
                               
                               "products" => $productsEcomm,
                               ),
                           );
        $json_eccom = json_encode($eccom);
      ?>
      <?php echo "<br/><br/>Общая сумма Вашего заказа составляет: ".$total." руб."; ?>
      <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
<script>
ga('require', 'ecommerce');
ga('ecommerce:addTransaction', {
'id': '<?php echo $order_id; ?>',
'revenue': '<?php echo $total; ?>'
});
<?php
foreach ($items as &$item) {
echo getItemJs($order_id, $item);
}
?>
 
ga('ecommerce:send');
</script>
<script>
	$(window).load(function() {
	    dataLayer.push(<?php echo  $json_eccom; ?>);
	})
</script>
<?php echo $footer; ?>