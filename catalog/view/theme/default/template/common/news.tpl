<div class="menu-drop">
    <div class="menu-products-wrapper clearfix">
        <?php foreach ($article as $articles) { ?>
        <div class="menu-product">
            <div class="prod-link-container">
                <div class="name">
                    <?php echo $articles['name']; ?>
                </div>
            </div>
            <div class="image"><img alt="" src="<?php echo $articles['thumb']; ?>"></div>
            <div class="description">
                <?php echo $articles['description']; ?>
            </div><a class="btn btn-primary" href="<?php echo $articles['href']; ?>">Купить сейчас</a>
        </div>
        <?php } ?>
    </div>
</div>
