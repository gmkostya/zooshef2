<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $og_url; ?>" />
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>" />
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>" />
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>" />
    <link rel="icon" href="/image/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon"/>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/slick.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main2.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/mobile.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main_new.css" rel="stylesheet">
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/popup_purchase/jquery.magnific-popup.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/popup_purchase/magnific-popup.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/ulogin.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="https://ulogin.ru/css/providers.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/slick.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/ajax_product_loader.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <script src="https://ulogin.ru/js/ulogin.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/ulogin.js" type="text/javascript"></script>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
	<!-- 
    -->
    <script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-70325931-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter33677824 = new Ya.Metrika({
                        id:33677824,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true,
                        ecommerce:"dataLayer"
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/33677824" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body class="<?php echo $class; ?>">

<div class="footer-down">
    <div class="header-wrapper">
        <header class="main-header center-wrapper clearfix">
            <div class="header-elements-block clearfix">
                <a class="logo" href="/">ЗООшеф</a>
                <div class="search_mobile_button">
                    <i aria-hidden="true" class="fa fa-search"></i>
                </div>

                    <?php echo $cart; ?>

                <div class="info-line fl-left">
                    <?php if (1) { ?>
                    <div class="custom-select fl-left">
                        <div class="select-mask" id="selected-city">
                           <?php echo $location; ?>
                        </div>
                        <div class="custom-select-content"></div>
                        <select class="custom-select-hidden" id="user_location_select" name="">
                            <option data-city="Москва" value="Москва">
                                Москва
                            </option>
                            <option data-city="Выбрать регион" value="0">
                                Выбрать регион
                            </option>
                        </select>
                    </div>
                    <?php }?>
                    <div class="contact-data fl-left">
                        <span class="top-contact-phone" id="contact-all-phone">8 800 550 85 86</span><span class="top-contact-phone" id="contact-moscow-phone">8 (495) 544 85 85</span>
                        <div class="tooltip-pop">
                            <div class="tip-body">
                                <div class="heading">
                                    График работы Call-центра
                                </div>
                                <div class="line">
                                    <span class="days">Пн—Пт:</span> с 9:00 до 18:00
                                </div>
                                <div class="line">
                                    <span class="days">Сб—Вс:</span> с 10:00 до 18:00
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contact-data fl-left">
                        Skype: Zooshef.ru
                    </div>
                    <div class="clearfix"></div>
                    <a class="default-link fl-left" href="/delivery.htm">Доставка и оплата</a>
                    <span class="default-link func-link fl-left">Перезвоните мне
                    <div class="tooltip-pop call-us">
                        <span class="default-link func-link fl-left"></span>
                        <div class="tip-body">
                            <span class="default-link func-link fl-left"></span>
                            <div class="close-btn">
                                <span class="default-link func-link fl-left"></span>
                            </div>
                            <div class="zag3">
                                <span class="default-link func-link fl-left">Заказать обратный звонок</span>
                            </div>
                            <div id="form-callback-block">
                                    <form id="form-callback" method="post" name="form-callback">
                                        <label class="text-label" for="">Телефон</label>
                                        <div class="placeholder-container">
                                            <input autocomplete="off" class="contact-field phone-number" data-lh_name="1688760555_field_0" id="phone-number" name="phone" placeholder="+7 ( ) - -" type="text"><label class="placeholder-text" for="phone-number"></label>
                                        </div><label class="text-label" for="">Имя</label>
                                        <div class="placeholder-container">
                                            <input class="contact-field" data-lh_name="1688760555_field_1" id="user-name" name="name" placeholder="Сидоров Михаил" type="text">
                                        </div>
                                        <div class="sub-text">
                                            <em>Время работы операторов с 9:00 до 18:00, кроме субботы и воскресенья.</em>
                                        </div><button class="btn btn-secondary phone" data-lh_name="1688760555_field_2" type="submit">позвоните мне</button><input data-lh_name="1688760555_field_3" name="action" type="hidden" value="callback">
                                    </form>
                            </div>
                            <div id="form-callback-block-sended" style="display: none;">
                                <div class="zag4" style="margin-top: 50px;">
                                    Спасибо!
                                </div>
                                <p>Мы свяжемся с вами в ближайшее время!</p>
                            </div>
                        </div>
                    </div>
                </span>
                    <a class="default-link fl-left" href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
                </div>
                <ul class="header-func-links fl-left <?php if ($logged) { ?> unactived<?php } ?>">
                    <?php if ($logged) { ?>
                        <li><a href="<?php echo $account; ?>" class="account-link">Кабинет</a><a href="<?php echo $logout; ?>">Выход</a></li>
                    <?php } else { ?>
                    <li>
                        <div class="default-link func-link data-enter">
                            Вход в личный кабинет
                            <div class="tooltip-pop log-in">
                                <?php echo $quicksignup; ?>

                            </div>
                        </div>
                    </li>
                    <?php } ?>

                    <li>
                        <?php echo $discountinfo; ?>
                    </li>
                </ul>
                <a class="fl-left" href="/comparison" id="comparison-link" title="Сравнить"><i class="fa fa-2x fa-balance-scale"></i><span class="num"><?php echo $count_compare; ?></span></a>
            </div>
            <div class="search-container">

                <?php echo $search; ?>


            </div>
            <section class="main-menu">
                <div class="burgers"></div>
                <ul class="site-menu clearfix">
                    <?php if ($categories) { ?>
                        <?php foreach ($categories as $category) { ?>
                            <li class="menu-holder left-side <?php echo $category['class']; ?>">
                                <a class="menu-text" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                <?php if ($category['children']) { ?>
                                    <div class="menu-drop">
                                        <div class="menu-products-wrapper clearfix">
                                            <div class="menu-product">
                                                <ul class="links-list">

                                                    <?php foreach ($category['children'] as $child) { ?>
                                                        <li>
                                                            <a <?php if ($child['children']) { ?>class="more_items" <?php }?> href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                                             <?php if ($child['children']) { ?>
                                                                <div class="menu_lvl_3_bg">
                                                                    <div class="menu_lvl_3">
                                                                        <?php foreach ($child['children'] as $child3) { ?>
                                                                            <a href="<?php echo $child3['href'];?>"><?php echo $child3['name'];?></a>
                                                                        <?php } ?>

                                                                    </div>
                                                                </div>
                                                                <select class="menu_mobile_select" name="hero">
                                                                    <option  value="<?php echo $child['href'];?>">
                                                                        <?php echo $child['name']; ?>
                                                                    </option>
                                                                    <option  value="<?php echo $child['href'];?>">
                                                                        <?php echo $child['name']; ?>
                                                                    </option>
                                                                    <?php foreach ($child['children'] as $child3) { ?>
                                                                    <option value="<?php echo $child3['href'];?>">
                                                                        <?php echo $child3['name'];?>
                                                                    </option>
                                                                     <?php } ?>
                                                                </select>
                                                            <?php } ?>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>

                    <li class="menu-holder right-side actions">
                        <a class="menu-text" href="/actions">Акции</a>
                        <?php echo $actions; ?>

                    </li>
                </ul>
            </section><a href="/delivery.htm">
                <div class="delivery_block_mobile"></div></a>
            <div class="call_to_me_mobile default-link func-link"></div>
            <div class="bonus_mobile default-link func-link data-money"></div>
            <a href="/contact.htm">
                <div class="contacts_mobile"></div>
            </a>
            <div class="login_user_mobile"><div><a href="/my-account/" class="account-link">Кабинет</a><a href="/logout">Выход</a></div></div>
        </header>
    </div>
