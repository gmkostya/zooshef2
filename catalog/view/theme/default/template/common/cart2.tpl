<div class="cart-block  <?php if ($products || $vouchers) { echo " full-cart "; }?> fl-right" id="cart">
<div class="cart-info" id="cart-total-curier">
    <?php echo $text_items; ?>
</div>
<div class="cart-drop">
    <div class="cart-drop-wrapper" id="cart-drop-wrapper">
        <?php if ($products || $vouchers) { ?>
        <div class="cart-content">
        <div class="cart-items-wrapper clearfix" id="top-cart-items">
            <div class="clearfix">
                <form class="basket-form" method="post" action="#">
                <?php if ($products || $vouchers) { ?>
                <?php foreach ($products as $product) { ?>
                <div class="cart-item header-cart-item"  data-id="<?php echo $product['cart_id']; ?>" data-name="<?php echo $product['name']; ?>"  data-quantity="1" data-variant="" id="top-cart-item-<?php echo $product['cart_id']; ?>">
                    <div class="image fl-left"><img alt="" src="<?php echo $product['thumb']; ?>"></div>
                    <div class="cart-item-info">
                        <?php if ($product['option']) { ?>
                        <?php foreach ($product['option'] as $option) { ?>
                        <div class="name">
                                    <br />
                                    <?php echo $option['sku']; ?> <?php echo $option['title']; ?></small>
                            <?php //echo $product['name']; ?>
                        </div>
                        <div class="art">
                            <?php if ($product['recurring']) { ?>
                                <br />
                                <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                            <?php } ?>
                        </div>
                        <div class="price-count clearfix">
                            <div class="price-value fl-right">
                                на <span class="price-text"><span class="price-value"><?php echo $product['total']; ?>
                            </div>
                            <div class="items-count fl-left" data-price="<?php echo $product['total']; ?>" data-trigger="spinner" data-variant="<?php echo $product['cart_id']; ?>"><span class="inc btn bootstrap-touchspin-down" onclick="EditMinQuant(<?php echo $product['cart_id']; ?>);">-</span>
                                <span class="qty_input">
                                  <input class="counter meas counter-<?php echo $product['cart_id']; ?> item-<?php echo $product['cart_id']; ?>" name="<?php echo $product['cart_id']; ?>" type="text" value="<?php echo $product['quantity']; ?>" onchange="cart.update2();"> шт.
                                </span>
                                <span class="decr  bootstrap-touchspin-up" onclick="EditMaxQuant(<?php echo $product['cart_id']; ?>);">+</span>
                            </div>

                        </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="name">
                                <?php echo $product['name']; ?>
                                <?php //echo $product['name']; ?>
                            </div>
                            <div class="art">
                                <?php if ($product['recurring']) { ?>
                                    <br />
                                    - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                                <?php } ?>
                            </div>
                            <div class="price-count clearfix">
                                <div class="price-value fl-right">
                                    на <span class="price-text"><span class="price-value"><?php echo $product['total']; ?>
                                </div>
                                <div class="items-count fl-left" data-price="<?php echo $product['total']; ?>" data-trigger="spinner" data-variant="<?php echo $product['cart_id']; ?>">       <input class="counter" name="count" type="text" value="<?php echo $product['quantity']; ?>">
                                </div>
                               <?php if (0) { ?> <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
<?php } ?>

                            </div>

                        <?php } ?>

                        <button style="display: none" type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                    <?php } ?>
                    <?php foreach ($vouchers as $voucher) { ?>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-left"><?php echo $voucher['description']; ?></td>
                            <td class="text-right">x&nbsp;1</td>
                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                            <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </form>
            </div>
        </div>
        <div class="button-block">
            <div id="discount-block-container">
                <?php foreach ($totals as $total) { ?>
                    <?php echo $total['title']; ?>:
                <strong><?php echo $total['text']; ?></strong>
                    <br/>
                <?php } ?>
            </div>
            <div class="cart-bt-block">
            <a class="btn btn-primary" href="<?php echo $checkout; ?>">Оформить заказ</a>
            <span class="view-in-popap fl-left">
            <span class="devider fl-left">или</span>
            <a class="bordered-link " href="#" onclick="$('.return-pop-wrapper').remove(); $('body').css('overflow', 'auto'); return false;"> продолжить покупки</a></span>
            </div>
        </div>
        </div>
        <?php } else { ?>
            <div class="cart-content">
            <div id="top-cart-items" class="cart-items-wrapper clearfix">
                Ваша корзина пуста
            </div>
            </div>

        <?php } ?>

        <div id="cart-drop-fade"></div>

    </div>
</div>

<?php if (0) { ?>

<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-inverse btn-block btn-lg dropdown-toggle"><i class="fa fa-shopping-cart"></i> <span id="cart-total"><?php echo $text_items; ?></span></button>
  <ul class="dropdown-menu pull-right">
    <?php if ($products || $vouchers) { ?>
    <li>
      <table class="table table-striped">
        <?php foreach ($products as $product) { ?>
        <tr>
          <td class="text-center"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
            <?php } ?></td>
          <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            - <small><?php echo $option['name']; ?> <?php echo $option['title']; ?></small>
            <?php } ?>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
            <?php } ?></td>
          <td class="text-right">x <?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
          <td class="text-center"><button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
          <td class="text-center"></td>
          <td class="text-left"><?php echo $voucher['description']; ?></td>
          <td class="text-right">x&nbsp;1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
      </table>
    </li>
    <li>
      <div>
        <table class="table table-bordered">
          <?php foreach ($totals as $total) { ?>
          <tr>
            <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
            <td class="text-right"><?php echo $total['text']; ?></td>
          </tr>
          <?php } ?>
        </table>
        <p class="text-right"><a href="<?php echo $cart; ?>"><strong><i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?></strong></a>&nbsp;&nbsp;&nbsp;<a href="<?php echo $checkout; ?>"><strong><i class="fa fa-share"></i> <?php echo $text_checkout; ?></strong></a></p>
      </div>
    </li>
    <?php } else { ?>
    <li>
      <p class="text-center"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul>
</div>
<?php } ?>
</div>
