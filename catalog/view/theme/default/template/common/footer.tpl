</div> <!--footer-down-->
<footer class="main-footer">
    <div class="center-wrapper clearfix">
        <div class="copyright-block fl-right">
             <div class="phone">
                Телефон офиса:<br>
                +7 (495) 544-85-85
            </div>
            <div class="socials">
                <a class="vk" href="http://vk.com/zooshef" rel="nofollow" target="_blank" title="Вконтакте"><i></i></a><a class="fb" href="https://www.facebook.com/zooshef?ref=hl" rel="nofollow" target="_blank" title="Facebook"><i></i></a><a class="ok" href="http://odnoklassniki.ru/group/52383908430010" rel="nofollow" target="_blank" title="Одноклассники"><i></i></a><a class="ig" href="http://instagram.com/zooshef" rel="nofollow" target="_blank" title="Instagramm"><i></i></a><a class="gp" href="https://plus.google.com/u/0/b/114067795207431978215/114067795207431978215/about" rel="nofollow" target="_blank" title="Google Plus"><i></i></a><a class="tw" href="https://twitter.com/zooshef" rel="nofollow" target="_blank" title="Twitter"><i></i></a>
            </div>Copyright zooshef.ru 2008—2017
        </div>
        <div class="text-block fl-left">
            <ul>
                <li>
                    <a class="default-link" href="/delivery.htm">Доставка</a>
                </li>
                <li>
                    <a class="default-link" href="/how_to_pay.htm">Оплата</a>
                </li>
                <li>
                    <a class="default-link" href="/sistema_skidok.htm">Система скидок</a>
                </li>
                <li>
                    <a class="default-link" href="/contact.htm">Контакты</a>
                </li>
                <li>
                    <a class="default-link" href="/o_nas.htm">О нас</a>
                </li>
                <li>
                    <a class="default-link" href="/FAQ.htm">FAQ</a>
                </li>
            </ul>
        </div>
        <div class="text-block fl-left">
            <ul>
                <li>
                    <a class="default-link" href="/tovary_dlya_sobak/">Для собак</a>
                </li>
                <li>
                    <a class="default-link" href="/tovary_dlya_koshek/">Для кошек</a>
                </li>
                <li>
                    <a class="default-link" href="/tovari_dlja_grizunov/">Для грызунов</a>
                </li>
                <li>
                    <a class="default-link" href="/tovari_dlja_ptic/">Для птиц</a>
                </li>
                <li>
                    <a class="default-link" href="/reptilii/">Для рептилий</a>
                </li>
                <li>
                    <a class="default-link" href="/tovari_dlja_rib/">Для рыб</a>
                </li>
                <li>
                    <a class="default-link" href="/loshadi/">Для лошадей</a>
                </li>
            </ul>
        </div>
        <div class="services-block fl-right">
            <div>
                Удобные способы оплаты
            </div><img alt="" src="/assets/images/payment.png"></div>
        <div class="services-block fl-right">
            <div>
                Доставка по всей России
            </div><img alt="" src="/assets/images/delivery.png"></div>
        <div class="services-block fl-right garanty">
            <div>
                Гарантия безопасности
            </div><img alt="" class="mt20" src="/assets/images/footer-3.png"></div>
    </div><span id="totop"><span></span></span>
</footer>
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
<script src="catalog/view/javascript/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/south-street/jquery-ui.css" />
<script src="catalog/view/javascript/inc/infieldLabel.js" type="text/javascript"></script>
<script src="catalog/view/javascript/inc/plugins.js" type="text/javascript"></script>
<script src="catalog/view/javascript/inc/jquery_004.js" type="text/javascript"></script>
<script src="catalog/view/javascript/inc/jquery.nicescroll.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/inc/copy.js" type="text/javascript"></script>
<script src="catalog/view/javascript/inc/regions.js" type="text/javascript"></script>
<link href="catalog/view/theme/default/stylesheet/d_social_login/styles.css" rel="stylesheet">
<div id="location_dialog">
  <div class="top-bar">
    <div class="countries"></div><span class="close" onclick="$( '#location_dialog' ).dialog('close');">x</span>
  </div>
  <div class="middle-bar">
    <div class="my-city-bar">
      <div class="name" id="selected-city3">
        <?php echo $user_location?>
      </div>
      <div class="descr">
        <span id="selected-country1"><?php if (isset($user_location_country)) { echo $user_location_country; }?></span>, <span id="selected-region1"><?php if (isset($user_location_region)) { echo $user_location_region;}  ?></span>, <span id="selected-city5"><?php echo $user_location?></span>
      </div>
    </div>
    <div class="search-box">
      <input name="city" onkeyup="getCitiesForFirst(this);" placeholder="Выберите свой город или регион" type="text" value="">
      <div class="hint">
        Например: <?php echo $user_location?>
      </div>
    </div>
    <div class="regions-row clearfix">
      <div class="regions-col-2">
        <div class="cities">
          <div class="ttl">
            Населенный пункт
          </div>
          <div class="inner"></div>
        </div>
      </div>
        <div class="regions-col-1">
            <div class="regions">
                <div class="ttl">
                    Регион
                </div>
                <div class="inner"></div>
            </div>
        </div>

    </div>
  </div>
  <div class="footer-bar"></div>
  <form action="/setlocation" id="reg_location_form" method="post" name="reg_location_form">
    <input id="reg_location_input" name="user_location" type="hidden" value="other"><input id="reg_location_input_id" name="user_location_id" type="hidden" value="0"><input id="reg_location_input_region" name="user_location_region" type="hidden" value=""><input id="reg_location_input_country" name="user_location_country" type="hidden" value=""><input id="reg_location_input_area" name="user_location_area" type="hidden" value="">
  </form>
</div>


</body></html>
