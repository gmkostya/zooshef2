<a class="default-link func-link data-money" href="#">Накопительная система скидок
    <div class="tooltip-pop discount-pop">
        <div class="tip-body">
            <div class="close-btn"></div>
            <div class="zag3">
                Накопительная система скидок
            </div>
            <p class="text-block">При общей сумме заказов более 25 000 руб. получаете скиду 2% Больше покупаете — больше экномите!</p>
            <div class="sustem_bonus">

                <?php foreach ($customer_groups as $customer_group) {?>
                    <div class="<?php echo $customer_group['key']; ?>">
                         <div class="elem">
                            <strong><?php echo $customer_group['name']; ?></strong>
                        </div>
                        <div class="elem <?php echo $customer_group['key']; ?>">
                            <?php echo $customer_group['description']; ?>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <p class="text-block">При единовременном заказе на сумму от 20 000 руб. Вы также получаете единовременную скидку 2% с покупки. Единовременная скидка не суммируется с накопительной.</p>
           <?php if ($manufacturers) {?>
            <div class="heading">
                <strong>Скидки не действут на продукцию</strong>
            </div>
            <p class="text-block">
                <?php $k=1; foreach ($manufacturers as $manufacturer) { $k++; ?>
                    <?php echo $manufacturer['name']; if (count($manufacturers)> $k) echo ", ";  ?>
                <?php } ?>
            </p>
           <?php } ?>
        </div>
    </div></a>
