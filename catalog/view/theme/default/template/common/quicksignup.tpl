<div class="tip-body"  id="modal-quicksignup" >
    <div  id="quick-login">
        <div class="zag3">Вход в личный кабинет</div>
        <div class="modal-header">
            <?php  echo $social2; ?>
        </div>
            <label class="text-label" for="input-email">Электронная почта</label>
            <div class="placeholder-container">
                <input type="text" name="email" value=""  id="input-email" class="contact-field" placeholder="familia.imia@gmail.com" />
            </div>
        <label class="text-label" for="input-password">Пароль</label>
            <div class="placeholder-container">
                <input type="password" name="password" value="" id="input-password" class="contact-field"  placeholder="********" type="password"/>
            </div>
        <div class="buttons-block clearfix">
            <button class="btn btn-secondary txt-up fl-left loginaccount" id="button-login">Войти</button><a class="bordered-link default-link fl-left" href="<?php echo $register; ?>">Регистрация</a>
        </div>
        <div class="buttons-block clearfix">
            <a class="bordered-link default-link fl-left" href="<?php echo $forgottenlink; ?>">Забыли пароль?</a>
        </div>
    </div>
</div>
<style>
.quick_signup{
	cursor:pointer;
}
#modal-quicksignup .form-control{
	height:auto;
}</style>

<script type="text/javascript"><!--
$('#quick-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-login .loginaccount').trigger('click');
	}
});
$('#quick-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-login .loginaccount').button('loading');
			$('#modal-quicksignup .alert-danger').remove();
		},
		complete: function() {
			$('#quick-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-quicksignup .form-group').removeClass('has-error');
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			
			if (json['error']) {
				$('#modal-quicksignup .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
				$('#quick-login #input-email').parent().addClass('has-error');
				$('#quick-login #input-password').parent().addClass('has-error');
				$('#quick-login #input-email').focus();
			}
			if(json['success']){
			    if(document.location.pathname == '/logout/'){
                    window.location.href="index.php?route=account/account";
                } else {
                    loacation();
                }

				$('#modal-quicksignup').modal('hide');
			}
			
		}
	});
});
//--></script>
<script type="text/javascript"><!--
function loacation() {
	location.reload();
}
//--></script>
