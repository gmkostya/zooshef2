<div class="simplecheckout-block cart-formrow" id="simplecheckout_comment">
    <?php if ($display_header) { ?>
      <div class="name"><?php echo $label ?></div>
    <?php } ?>
    <div class="value">
      <textarea class="form-control comment_textarea" name="comment" id="comment" placeholder="<?php echo $placeholder ?>" data-reload-payment-form="true"><?php echo $comment ?></textarea>
    </div>
</div>