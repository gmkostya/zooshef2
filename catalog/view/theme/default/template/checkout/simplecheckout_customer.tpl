<div class="simplecheckout-block" id="simplecheckout_customer" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
<?php if(!$logged) { ?>
    <div class="text-top">
        Чтобы не заполнять все поля, войдите через соц. сети или введите <a href="javascript:void(0)" class="js-openLoginBox" data-onclick="openLoginBox">логин и пароль</a>
    </div>
    <?php  echo $social2; ?>
    <?php } ?>
  <?php if ($display_header || $display_login) { ?>
<?php } ?>
  <div class="simplecheckout-block-content">
    <?php if ($display_registered) { ?>
      <div class="success"><?php echo $text_account_created ?></div>
    <?php } ?>
    <?php if ($display_you_will_registered) { ?>
      <div class="you-will-be-registered"><?php echo $text_you_will_be_registered ?></div>
    <?php } ?>
    <?php foreach ($rows as $row) { ?>
      <?php echo $row ?>
    <?php } ?>
  </div>
</div>
<div class="cart-toogle-bt-curier"><a class="bordered-link" id="checkouhide" onclick="checkouthide();" >Не хочу заполнять формы, просто позвоните мне</a><a class="bordered-link" id="checkoutshow" onclick="checkoutshow();" >Хочу указать адрес доставки и выбрать способ оплаты</a></div>