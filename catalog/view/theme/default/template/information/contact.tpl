<?php echo $header; ?>

<div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        </div>

<div class="contact-us-wrapper clearfix">

    <div class="contact-right fl-right">
        <div id="question-block">
            <div class="ask-question-text">
                <div class="heading">
                    Задать вопрос
                </div>
                <p>Если у вас остались вопросы — напишите,<br>
                    наш консультант свяжется с вами в течение рабочего дня.</p>
            </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-block">
                <div class="form-row">
                    <div class="full-input">
                        <div class="label">
                            Ваше сообщение
                        </div>
                         <textarea cols="30" name="enquiry" rows="10" id="input-enquiry" ><?php echo $enquiry; ?></textarea>
                        <?php if ($error_enquiry) { ?>
                            <div class="text-danger"><?php echo $error_enquiry; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="full-input">
                        <div class="label">
                            Имя
                        </div>
                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name"  />
                        <?php if ($error_name) { ?>
                            <div class="text-danger"><?php echo $error_name; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="full-input">
                        <div class="label">
                            Электронная почта для ответа
                        </div>
                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                        <?php if ($error_email) { ?>
                            <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>

                    </div>
                </div>
                <div class="button-block">
                    <button class="btn btn-secondary send-message txt-up" type="submit">Задать вопрос</button>
                </div>
            </div>

        </form>
        </div>
    </div>

  <div class="contact-info">
    <?php echo $content_top; ?>
    <?php echo $description; ?>
    <?php echo $content_bottom; ?>
   </div>
</div>
    </div>
</div>
<?php echo $footer; ?>
