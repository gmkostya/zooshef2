<?php echo $header; ?>

<div class="content-wrapper center-wrapper">
  <div class="text-page">
         <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        <?php echo $content_top; ?>
          <?php echo $description; ?><?php echo $content_bottom; ?>
        <?php echo $column_right; ?>
    </div>
</div>
</div>
<?php echo $footer; ?>