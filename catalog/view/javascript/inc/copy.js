jQuery(function(){
    jQuery.fn.scrollToTop=function(){
        jQuery(this).hide().removeAttr("href");
        if(jQuery(window).scrollTop()!="0"){
            jQuery(this).fadeIn("slow")
        }
        var scrollDiv=jQuery(this);
        jQuery(window).scroll(function(){
            if(jQuery(window).scrollTop()=="0"){
                jQuery(scrollDiv).fadeOut("slow")
            }else{
                jQuery(scrollDiv).fadeIn("slow")
            }
        });
        jQuery(this).click(function(){
            jQuery("html, body").animate({scrollTop:0},"slow")
        })
    }
});

$(document).ready(function(){

    $("#totop").scrollToTop();

    $('.alphabet a').click(function(e)
    {
        e.preventDefault();

        $('.alphabet a').removeClass('active');
        $(this).addClass('active');

        $('.brands-list').removeClass('sel');
        console.log($(this).attr('href'));

        $($(this).attr('href')).addClass('sel');
    })


	$('.discount-pop').clone().appendTo('.bonus_mobile');

	if($(window).width()<= 530)
		{$('.bonus_mobile .tooltip-pop.discount-pop').css('width', ($(window).width()-20));}
	$(window).resize(function(){
	if($(window).width()<= 530)
		{$('.bonus_mobile .tooltip-pop.discount-pop').css('width', ($(window).width()-20));}
	else
		{$('.bonus_mobile .tooltip-pop.discount-pop').removeAttr('style');}
	
	});	
    if ($(window).width() > 1000){

        $(document).on('mouseenter', '.menu-holder',function(){
            $('.footer-down>div:not(:first-child)').addClass('unactivate');
        });
        $(document).on('mouseleave', '.menu-holder',function(){
            $('.footer-down>div').removeClass('unactivate');
        });

        $(document).on('mouseenter', '.links-list>li',function(){
            $(this).removeClass('unactive').addClass('active');
            $('.content-wrapper.center-wrapper').addClass('unactivate');

        });

        $(document).on('mouseleave', '.links-list>li',function(){
            $(this).parent().find('li').removeClass('unactive').removeClass('active');
            $('.content-wrapper.center-wrapper').removeClass('unactivate');
        });
    }
    else
    {
        //$(document).on('click','.bonus_mobile', function() {
        	//$(this).find('.discount-pop').toggle();
        //});

        $(document).on('change','.menu_mobile_select', function() {
            console.log($(this).val());
            location.href=$(this).val();

                });


        $(document).on('click','.more_items', function(e) {
            e.preventDefault();
            e.stopPropagation();

           if ($(this).hasClass('open')) {
              location.href=$(this).attr('href');
           }
            $('.more_items').removeClass('open');
            $(this).addClass('open');
            $('.menu_lvl_3_bg').hide();
            $(this).siblings('.menu_lvl_3_bg').toggle();

        });

        $(document).on('click','.login_user_mobile', function() {
                if($('.site-menu').is(':visible')){$('.site-menu').fadeOut('700');}
                $(this).toggleClass('active');
                if($(this).hasClass('active')){$('.footer-down>div:not(:first-child)').addClass('opacity_fade');}
                else{$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');}

        });
    }
        if ($('.main-header').width() < 970) {
        $('.tooltip-pop.call-us').appendTo('.call_to_me_mobile');
    }
    $(window).resize(function(){
        if ($('.main-header').width() < 970) {
            if (!$('.call_to_me_mobile').is('.tooltip-pop.call-us')){   $('.tooltip-pop.call-us').appendTo('.call_to_me_mobile');   }
        }
        if ($('.main-header').width() >= 970) {
            if (!$('.info-line.fl-left>span.default-link.func-link.fl-left').is('.tooltip-pop.call-us')){   $('.tooltip-pop.call-us').appendTo('.info-line.fl-left>span.default-link.func-link.fl-left');}
        }

    });
    
});

$(document).on('click','.default-link', function() {
if ($(window).width() < 1001){ $('.site-menu').fadeOut('700');}
if(!$(this).hasClass('active')){$('.footer-down>div:not(:first-child)').addClass('opacity_fade');}
else{$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');}
});
$(document).on('click','.default-link .close-btn', function() {
	$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');
});
$(document).on('click','body', function() {
	if(!$('.default-link .func-link').hasClass('active')){
	$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');}
});
/*
$(window).load(function(){var active_label;
    $('.product-wrapper').each(function(a){
        $(this).find('.switch-items>div').each(function(b){
            if ($(this).hasClass('active')) { active_label = 1+b; }
        });
        $(this).find('.options_weight>div').removeClass('active');
        $(this).find('.options_weight>div:nth-child('+active_label+')').addClass('active');
    });
});
*/


$(document).on('touchstart','.search_mobile_button', function() {
 $('.search-container').toggle(700);
});
$(window).resize(function(){
    if($('.header-wrapper').width()>484){
        $('.search-container').removeAttr('style');
    }
});

function countManyTotal()
{
    var total = 0;
    var numprod = 0;
    var numdisc = 0;
    $('#many-products-form .cart-quantity-input').each(function()
    {
        var num = parseInt($(this).val());
        var price = $(this).data('price');
        var discount = $(this).data('discount');

        total += num*price;
        numprod += num;
        numdisc += discount*num;
        console.log(discount);
    })
    console.log(total );

    $('.all_price__items span').text(numprod);
    $('.all_price__price span').text(total);
    $('.all_price__bonus span').text(Math.ceil(numdisc));
}






function show_basket_window(data)
{
    var htm = '<div class="return-pop-wrapper">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>Корзина</h2>\
                <div class="cart-items-wrapper clearfix">';
    for(var k in data.variants)
    {
        htm += '<div class="cart-item">\
                        <div class="image fl-left">\
                            <img src="/files/products/'+ data.variants[k].small_image +'" alt="">\
                        </div>\
                        <div class="cart-item-info">\
                            <div class="name">'+ data.variants[k].name +'</div>';
        /*if(data.variants[k].special)
         {
         htm +=          '<div class="bonus">Акция! '+ data.variants[k].special_text +'</div>';
         }*/
        htm +=              '<div class="price-count clearfix" data-variant="'+ data.variants[k].variant_id +'">\
                                <span class="price-wrap">'+ Math.ceil(data.variants[k].remission_price*data.variants[k].amount) +' руб.</span>\
                                &nbsp;&nbsp; <button class="btn btn-default bootstrap-touchspin-down" data-variant="'+ data.variants[k].variant_id +'">-</button>\
                                <span class="meas">'+ data.variants[k].amount +' шт.</span>\
                                <button class="btn btn-default bootstrap-touchspin-up" data-variant="'+ data.variants[k].variant_id +'">+</button>\
                            </div>\
                        </div>\
                    </div>';
    }
    htm += '</div>\
                <div class="more-text">\
                    Всего: <strong id="popup-total">'+ Math.ceil(data.total_products_price) +' руб.</strong>\
                </div>\
                <div class="buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="/cart">\
                        Оформить заказ\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'.return-pop-wrapper\').remove(); $(\'body\').css(\'overflow\', \'auto\'); return false;">\
                        сохранить товары и продолжить покупки\
                    </a>\
                </div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
    update_basket_top(data);

    $('.return-pop-wrapper .bootstrap-touchspin-down').click(function()
    {
        var variant_id = $(this).data('variant');
        var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
        elm.trigger("touchspin.downonce");
        if(elm.val() == 0)
        {
            $(this).closest('.cart-item').remove();
        }
        else
        {
            var el = $(this);

            var price = ($('#cart-drop-wrapper div[data-variant='+variant_id+']').data('price')*elm.val())+' руб.';
            el.parent().find('.price-wrap').text(price);
            el.parent().find('.meas').text(elm.val()+' шт.');
        }
    })

    $('.return-pop-wrapper .bootstrap-touchspin-up').click(function()
    {
        var variant_id = $(this).data('variant');
        var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
        elm.trigger("touchspin.uponce");
    })

    $('body').css('overflow', 'hidden');
}

