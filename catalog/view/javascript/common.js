function checkouthide() {

	$('#simplecheckout_shipping_address, #simplecheckout_payment').hide();
	$('#checkouhide').hide();
	$('#checkoutshow').show();
	$('#cod').attr('checked','checked');
    $( "input[name='payment_method_current']" ).val( "cod" );
    $( "input[name='payment_method_checked']" ).val( "cod" );

}

function fastview(product_id) {
	console.log(product_id);
	$.magnificPopup.open({
		tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
		items: {
			src: 'index.php?route=product/product/view&product_id='+product_id,
			type: 'ajax'
		}
	});
}
function checkoutshow() {
	$('#simplecheckout_shipping_address, #simplecheckout_payment').show();
	$('#checkouhide').show();
	$('#checkoutshow').hide();

}
function burgersMenu() {
    if ($(window).width() < 1281) {
        //$('.menu-holder .menu-text').each(function () {
        //$(this).replaceWith('<span class="menu-text">' + $(this).text() + '</span>');
        //});

        $('.site-menu .menu-holder').on('click', function()		{
            //$('.main-menu').removeClass('isActive');
            //$('.main-menu').addClass('isActive');

        });
        $('.menu_mobile_select').click(function (event) {
            event.preventDefault();
            event.stopPropagation();
        });

        $(document).on('click', '.main-menu .menu-holder', function(e)
        {

            if($(this).hasClass('isActive')){
                $(this).removeClass('isActive');
                lhref = $(this).find('a.menu-text').attr('href');
                window.location = lhref;            }
            else {
                $('.menu-holder').removeClass('isActive').find('.menu-drop a').css("pointer-events", "none");
                $('.menu-holder .menu-drop a').css("pointer-events", "none");
                $(this).addClass('isActive');
            }
            function func(that) {
                that.find('.menu-drop a').css("pointer-events","all");
                console.log('in');
            }

            setTimeout(func($(this)), 2000);
            $('html, body').animate({ scrollTop: $(this).offset().top }, 500);
        });

		$(document).on('click', '.main-menu .menu-holder.isActive', function(e) {

		});

		} else{
        if ($(window).width() > 1000) {
            $('.site-menu .menu-holder').on('mouseenter', function()
            {
                //$('.main-menu').addClass('shadowed');
                //$('.slider-wrapper').css('opacity', '0.2');
            });

            $('.site-menu .menu-holder').on('mouseleave', function()
            {
                //$('.main-menu').removeClass('shadowed');
                //$('.slider-wrapper').css('opacity', '1');
            });
        }

    }
}
function wrap_ks_filters() {
    var form_filter = $('#ocfilter');
    if (form_filter.length > 0) {
        var i = 0;
       /* form_filter.find('.zag4').each(function() {
            var _this = $(this);
            if (_this.text() != 'Подобрать' &&
                _this.text() != 'Упаковка, кг' &&
                _this.text() != 'Цена, руб') {
                $(this).wrap('<div class="ks_filters_wrap"></div>');
            }
            i++;
        });*/
        /*form_filter.find('.ks_filters_wrap').each(function() {
            var _this = $(this);
            _this.append(_this.next());
        });*/

        $('.ks_filters_wrap .zag4').on('click', function() {
            var _this = $(this).parent();

                _this.find('.zag4').find('.options-list').toggleClass('expanded').slideToggle();
                _this.find('.options-list').slideToggle();
                _this.toggleClass('expanded')

        });
    }
}
function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}
function EditMinQuant(product_id) {
    var qty = $('.item-' + product_id).val();
    if ((parseFloat(qty) != parseInt(qty)) || isNaN(qty)) {
        qty = 0;
    }else{
        qty=Number(qty)-1;
        if (qty<0) qty='0';
    }
    $('.item-' + product_id).val(qty);
    cart.update2();
}

function EditMaxQuant(product_id) {
    var qty = $('.item-' + product_id).val();
    if ((parseFloat(qty) != parseInt(qty)) || isNaN(qty)) {
        qty = 0;
    }else{
        qty=Number(qty)+1;
    }
    $('.item-' + product_id).val(qty);
    cart.update2();

}

var nice = false;
$(document).ready(function() {
    if ($(window).width() > 1139) {
          nice = $("#cart-drop-wrapper").niceScroll({horizrailenabled: false, cursorwidth:"8px"});
    }
});

$(document).ready(function() {

     $('[data-toggle="tooltip"]').tooltip();


    if ($(window).width() < 1000) {

    	$('.tab-content .tab-pane').removeClass('active');

        $('.menu_products_properties').on('click', 'a.js-clicked', function () {
            $('.newElement').remove()
            idtab = $(this).attr('href');
            $(idtab).addClass('tab-hidden');
            scrollTab = $(this).offset().top;
            $(document).scrollTop(scrollTab);
            $(this).after($(idtab).clone().addClass("newElement"));
        });
    }
    $('.js-review-link-scroll').on('click',  function () {
        scrollTab = $('.js-tab-review').offset().top;
        $(document).scrollTop(scrollTab);
    });

    wrap_ks_filters();
	/**/
    burgersMenu();

    $('.burgers').on('click', function()
    {
        $('.main-menu .site-menu').toggle();
    });

    if ($(window).width() < 1000) {
       /* $(document).click(function (event) {
            if ($(event.target).closest(".site-menu, .burgers").length)
                return;
            $(".site-menu").hide("slow");
            event.stopPropagation();
        });
*/
        $(document).mouseup(function (e) {
            var container = $(".site-menu, .burgers");
            if (container.has(e.target).length === 0) {
                $(".site-menu").hide();
                //alert ('sss');
            }
        });

    }


/* categories-with-brand*/
    $('.js-readon-categories-with-brand').on('click', function(e) {
        $('.close-item').toggle();
        $('.js-readon-categories-with-brand').toggle();
    });

/*add with old*/
    $('#form-callback').submit(function()
    {
        var data_ask = jQuery(this).serialize();
        var data_arr = jQuery(this).serializeArray();
        var err='';
        $.each(data_arr,function(){

            if(this.name=='phone' && this.value=='') { err +='<em style="color: red;">Укажите телефон <br/></em>'; }
            if(this.name=='name' && this.value=='') err +='<em style="color: red;">Укажите имя</em>';
        });

        if (!err) {

            $.post(
                "index.php?route=module/sendmail/callback",
                data_ask,
                function (data) {
                    $('#form-callback-block').hide();
                   $('#form-callback-block-sended').show();
                }
            );
        } else {
            $('.sub-text').html(err);
        }

        return false;
    })

	var is_moscow_region='0';
	//**/
    if(is_moscow_region)
    {
        $('#contact-all-phone').hide();
        $('#contact-moscow-phone').fadeIn(700);
    }
    else
    {
        $('#contact-all-phone').fadeIn(700);
        $('#contact-moscow-phone').hide();
    }
   // $('.header_curier').affix({offset: {top: 0} });

    /***/



    // Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
	$('#search').find('button').on('click', function() {
		url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('header input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header #search').find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body',trigger: 'hover'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});
});

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#cart').addClass('full-cart');


                   // $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total'] );
                    }, 100);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart  #top-cart-items > .clearfix').load('index.php?route=common/cart/info #top-cart-items .cart-item');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},

    'optionadd': function(product_id, quantity, option_id, option_value_id) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option['+ option_id +'][]='+ option_value_id+'/'+'1',
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }


                if (json['success']) {
					$('#cart').addClass('full-cart');
					var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';

					htm += '</div></div>';
					$('body').append(htm);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total'] );
                    }, 100);
					if (json['countproducts']>0) {
						$('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();

					}
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'optionaddview': function(product_id, quantity, option_id, option_value_id) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option['+ option_id +'][]='+ option_value_id+'/'+ quantity,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }


                if (json['success']) {
					$('#cart').addClass('full-cart');
					$('.alert').remove();
					$('.product-page-short .main_product_block_sale h1').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success']);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total'] );
                    }, 100);
                    setTimeout(function () {
                        $('.alert').remove();
                        $.magnificPopup.close();
                    }, 5000);
					if (json['countproducts']>0) {
						$('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();

					}
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'optionaddvieinproduct': function(product_id, quantity, option_id, option_value_id) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option['+ option_id +'][]='+ option_value_id+'/'+ quantity,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }


                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';

                    htm += '</div></div>';
                    $('body').append(htm);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total'] );
                    }, 100);
                    if (json['countproducts']>0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');

                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },

    'update2': function(key, quantity) {

        var form_data=$('.basket-form').serializeArray();
        console.log(form_data);
        $.ajax({
            url: 'index.php?route=checkout/cart/edit2',
            type: 'post',

            data: {data: form_data},
            dataType: 'json',
            beforeSend: function() {
                $('#cart #cart-total').button('loading');
                $('#basket-form').mask('loading...');


            },
            complete: function() {
                $('#cart #cart-total').button('reset');
            },
            success: function(json) {
            	console.log(json);

                if (json['success']) {
                    //$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';

                    htm += '</div></div>';

                    //$('body').append(htm);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);

                   console.log(json['total']);

                    if (json['countproducts']>0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {

                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();


                    }

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },


	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
                if (json['success']) {
                   // $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';

                    htm += '</div></div>';

                    $('body').append(htm);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total'] );
                    }, 100);

                    if (json['countproducts']>0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
						$('.cart-drop  .button-block').remove();

                    }

                }
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#wishlist-total span').html(json['total']);
				$('#wishlist-total').attr('title', json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
    'optionadd': function(product_id, qty, option_id, option_value_id) {
	    var name=$('#report-form #us-request-name').val();
	    var email=$('#report-form #us-request-email').val();

        $.ajax({
            url: 'index.php?route=account/wishlist/add2',
            type: 'post',
            data: 'product_id=' + product_id + '&option['+ option_id +']='+ option_value_id + '&email='+ email + '&name='+ name,
            dataType: 'json',
            beforeSend: function() {
                $('#report-form .button-report-wrapper').css('opacity','.5');
            },
            complete: function() {
            },
            success: function(json) {


				show_report_window_wish();
				$('.return-pop-content').html(json['success']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'preoptionadd': function(product_id, qty, option_id, option_value_id) {
		show_report_window_wish(product_id, qty, option_id, option_value_id);
    },
    'remove': function() {
	}
}
var asklist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/asklist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#asklist-total span').html(json['total']);
				$('#asklist-total').attr('title', json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
    'optionadd': function(product_id, qty, option_id, option_value_id) {
	    var name=$('#report-form #us-request-name').val();
	    var email=$('#report-form #us-request-email').val();

        $.ajax({
            url: 'index.php?route=account/asklist/add2',
            type: 'post',
            data: 'product_id=' + product_id + '&option['+ option_id +']='+ option_value_id + '&email='+ email + '&name='+ name,
            dataType: 'json',
            beforeSend: function() {
                $('#report-form .button-report-wrapper').css('opacity','.5');
            },
            complete: function() {
            },
            success: function(json) {

                $('.return-pop-content').html(json['success']);
                console.log(json['success']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'preoptionadd': function(product_id, qty, option_id, option_value_id) {
        show_report_window(product_id, qty, option_id, option_value_id);
    },
    'remove': function() {
	}
}

function show_report_window(product_id, qty, option_id, option_value_id)
{
    $.magnificPopup.close();    //console.log(product_id);
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>Сообщите, когда появится в продаже</h2>\
                <div class="form-report-wrapper clearfix">';
    htm += '<form id="report-form">\
                    <div class="form-group">\
                        <label>Имя <span class="req">*</span></label>\
                        <input id="us-request-name" type="text" name="name" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>E-mail <span class="req">*</span></label>\
                        <input id="us-request-email" type="text" name="email" value="" class="form-control" required="required" />\
                    </div>\
                    <input type="hidden" id="us-request-product_id" value="'+product_id+'" />\
                    <input type="hidden" id="us-request-option_id" value="'+option_id+'" />\
                    <input type="hidden" id="us-request-option_value_id" value="'+option_value_id+'" />\
                </form>';
    htm += '</div>\
                <div class="button-report-wrapper buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="#" onclick="asklist.optionadd('+product_id+',1,'+option_id+','+option_value_id+'); return false;">\
                        Сообщить\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'#report-form-block\').remove(); return false;">\
                        отмена\
                    </a>\
                </div>\
            </div>\
        </div>';
   // console.log(htm);
    $(htm).appendTo('body');
}
function show_report_window_wish(product_id, qty, option_id, option_value_id)
{
   // console.log(product_id);
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>Добавление товара в избранное</h2>\
                <div class="form-report-wrapper clearfix">';
    htm += '<form id="report-form">\
                    <div class="form-group">\
                        <label>Имя <span class="req">*</span></label>\
                        <input id="us-request-name" type="text" name="name" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>E-mail <span class="req">*</span></label>\
                        <input id="us-request-email" type="text" name="email" value="" class="form-control" required="required" />\
                    </div>\
                    <input type="hidden" id="us-request-product_id" value="'+product_id+'" />\
                    <input type="hidden" id="us-request-option_id" value="'+option_id+'" />\
                    <input type="hidden" id="us-request-option_value_id" value="'+option_value_id+'" />\
                </form>';
    htm += '</div>\
                <div class="button-report-wrapper buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="#" onclick="wishlist.optionadd('+product_id+',1,'+option_id+','+option_value_id+'); return false;">\
                        Сообщить\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'#report-form-block\').remove(); return false;">\
                        отмена\
                    </a>\
                </div>\
            </div>\
        </div>';
   // console.log(htm);
    $(htm).appendTo('body');
}
function show_report_window_info(succsess)
{
   // console.log(product_id);
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>'+ succsess +'</h2>\
                <div class="form-report-wrapper clearfix">';
    htm += '</div>\
            </div>\
        </div>';
   // console.log(htm);
    $(htm).appendTo('body');
}
/*
var reviewstar = {
    'addstar': function(product_id, rating) {
        $.ajax({
            url: 'index.php?route=product/product/writeStar',
            type: 'post',
            data: 'product_id=' + product_id + '&rating' + rating + ',
            dataType: 'json',
            beforeSend: function () {
                $('#report-form .button-report-wrapper').css('opacity', '.5');
            },
            complete: function () {
            },
            success: function (json) {
                $('.return-pop-content').html(json['success']);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}*/

var reviewstar = {
    'addstar': function(product_id, rating) {
        $.ajax({
            url: 'index.php?route=product/product/writeStar',
            type: 'post',
            data: 'product_id=' + product_id + '&rating=' + rating,
            dataType: 'json',
            beforeSend: function () {
                $('#report-form .button-report-wrapper').css('opacity', '.5');
            },
            complete: function () {
            },
            success: function (json) {
                show_report_window_info();
                $('.return-pop-content').html('<h2>'+ json['success']+'</h2>');
                $('.rating-count').html(json['total']);

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}
var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {

					$('#comparison-link .num').html(json['total']);

					$('.compare-block-' + product_id).html('<span class="btn-compare added"><i class="fa fa-check"></i> Сравнить</span>');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});



// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}
})(window.jQuery);


