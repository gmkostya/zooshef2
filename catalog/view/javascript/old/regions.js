function showRegionsWindow()
{
    $( "#location_dialog" ).dialog('open');
    /*$('<div id="regions_window"></div>').appendTo('body').css('position', 'fixed').css('left', '100px').css('top', '20px').css('width', $(window).width()-200+'px').css('height', $(window).height()-40+'px')
    $.get("region_ajax.php", {  },
        function(data){
            $('#regions_window').html(data);
            $('#regions_window .left').height($('#regions_window').height()-50).css('overflow', 'auto')
            $('#regions_window .right').height($('#regions_window').height()-50).css('overflow', 'auto')
            getRegionsCountries();
            
        });*/
}

function getRegionsCountries()
{
    $.getJSON('/ajax/getcountries', function(data) {
        var items = [];
        $.each(data, function(key, val) {
            items.push('<li data-id="' + val.id + '" data-name="' + val.title + '"><span>' + val.title + '</span></li>');
        });
        $('#location_dialog .top-bar .countries').html($('<ul/>', {
            'class': 'countries_list',
            html: items.join('')
        }));
        $('.countries_list li').eq(0).click();
    });
}

function getRegionsRegions(country_id)
{
    $.getJSON('/ajax/getregions/'+country_id, function(data) {
        var items = [];
        items.push('<li data-id="0" data-country="' + country_id + '"><span>Крупные населенные пункты</span></li>');
        $.each(data, function(key, val) {
            items.push('<li data-id="' + val.id + '" data-country="' + country_id + '"><span>' + val.title + '</span></li>');
        });
        $('#location_dialog .regions .inner').html($('<ul/>', {
            'class': 'regions_list',
            html: items.join('')
        }));
        $('.regions_list li').eq(0).click();
    });
}

function getRegionsCities(region_id, country_id)
{
    $.getJSON('/ajax/getcities/'+country_id+'/'+region_id, function(data) {
        var half = Math.ceil(data.length/2);
        
        var let;
        var add = '<span></span>';
        var htm = '<ul class="cities_list">';
        $.each(data, function(key, val) {
            if(key == half)
                htm += '</ul><ul class="cities_list">'
            add = '<span class="letter"></span>';
            if(let != val.letter)
            {
                add = '<span class="letter">'+val.letter+'</span>';
                let = val.letter;
            }
            htm += '<li data-id="' + val.id + '" data-region="' + val.region_name + '" data-country="' + val.country_name + '" data-val="' + val.title + '">' + add + ' <span class="val">' + val.title + '</span></li>';
        });
        htm += '<ul>';
        $('#location_dialog .cities .inner').html(htm);
    });
}

function getCitiesForFirst(el)
{
    $('#location_dialog .regions li').removeClass('active');
    var name = $(el).val();
    var country = $('#location_dialog .countries_list li.active').attr('data-id');
    
    $.getJSON('/ajax/getcitiesfirst/'+country+'/'+name, function(data) {
    console.log(data)
        var half = Math.ceil(data.length/2);
        var let;
        var add = '<span></span>';
        var htm = '<ul class="cities_list">';
        $.each(data, function(key, val) {
            if(key == half)
                htm += '</ul><ul class="cities_list">'
            add = '<span class="letter"></span>';
            if(let != val.letter)
            {
                add = '<span class="letter">'+val.letter+'</span>';
                let = val.letter;
            }
            htm += '<li data-id="' + val.id + '" data-region="' + val.region_name + '" data-country="' + val.country_name + '" data-val="' + val.title + '">' + add + ' <span class="val">' + val.title + '</span></li>';
        });
        htm += '<ul>';
        $('#location_dialog .cities .inner').html(htm);
    });
}

function closeRegionsWindow()
{
    $('#regions_window').remove();
}

function toggleChangePasswordWindow()
{
    if($('#change-password-window').css('display') == 'none')
    {
        $('#change-password-window').show();
    }
    else
    {
        $('#change-password-window').hide();
    }
}

function changePasswordAjax()
{
    $.ajax({
        type: "POST",
        url: "/account/changepass",
        data: $('#change_password_form').serialize(),
        success: function(msg){
            $('#change-password-window .msg').html(msg);
        }
    });
}

function setCartPayments()
{
    var datacity = '';
    $.ajax({
        type: "POST",
        url: "/paymentajax",
        data: 'city='+datacity,
        success: function(msg){
            $('#cart-payments').html(msg);
        }
    });
}


$(document).ready(function()
{    
    $( "#location_dialog" ).dialog({
        dialogClass: "regions_dialog dlgfixed",
        draggable: false,
        resizable: false,
        autoOpen: false,
        modal: true,
        width: 800,
        position: "center",
        open: function( event, ui ) {
            $('.regions_dialog').center();
            getRegionsCountries();
        }
    });
    
    $( "#cart_renew_dialog" ).dialog({
        dialogClass: "dlgfixed",
        draggable: false,
        resizable: false,
        autoOpen: false,
        modal: true,
        width: 400,
        position: "center",
        open: function( event, ui ) {
            //$('#cart_renew_dialog').center();
        }
    });
    
    if(is_moscow_region)
    {
        $('#contact-all-phone').hide();
        $('#contact-moscow-phone').fadeIn(700);
    }
    else
    {
        $('#contact-all-phone').fadeIn(700);
        $('#contact-moscow-phone').hide();
    }
    
    $('body').on('click', '.countries_list li', function()
    {
        $('#location_dialog .countries_list li').removeClass('active');
        $(this).addClass('active');
        var cid = $(this).attr('data-id');
        getRegionsRegions(cid);
    })
    
    $('body').on('click', '.regions_list li', function()
    {
        $('#location_dialog .regions li').removeClass('active');
        $(this).addClass('active');
        var rid = $(this).attr('data-id');
        var cid = $(this).attr('data-country');
        getRegionsCities(rid, cid);
    })
    
    $('body').on('click', '.cities_list li', function()
    {
        $('#location_dialog .cities .cities_list li').removeClass('active');
        $(this).addClass('active');
        var city = $(this).attr('data-val');
        var cityid = $(this).attr('data-id');
        var region = $(this).attr('data-region');
        var country = $(this).attr('data-country');
        $('#reg_location_input').val(city);
        $('#reg_location_input_id').val(cityid);
        if(region != 'undefined')
            $('#reg_location_input_region').val(region);
        $('#reg_location_input_country').val(country);
        
        var data = $('#reg_location_form').serialize();
        
        
        $.ajax({
            type: "POST",
            url: "/setlocation",
            data: data,
            dataType: 'json',
            success: function(msg){
                $('#selected-city').text(msg.city);
                $('#selected-city2').text(msg.city);
                $('#selected-city3').text(msg.city);
                $('#selected-city5').text(msg.city);
                $('.city-text').text(msg.city);
                
                $('#selected-country1').text(msg.country);
                $('#selected-region1').text(msg.region);
                $('#location_dialog').dialog('close');
                
                $('.delivery-text').removeClass('active');
                if(msg.city == 'Москва')
                {
                    $('#delivery-text-moscow').addClass('active');
                }
                else
                {
                    $('#delivery-text-other').addClass('active');
                }
                
                if(msg.is_moscow_region)
                {
                    $('#contact-all-phone').hide();
                    $('#contact-moscow-phone').show();
                }
                else
                {
                    $('#contact-all-phone').show();
                    $('#contact-moscow-phone').hide();
                }
                        
                        
                if(typeof(basket_page) != "undefined")
                {
                    setCartPayments();
                }
            }
        });
        
        //$('#reg_location_form').submit();
    })
    
    $('body').on('change', '#user_location_select', function()
    {
        if($(this).val() == 0)
        {
            showRegionsWindow();
        }
        else if($(this).val() == 'Москва')
        {
            $('#selected-city').text('Москва');
            $('#selected-city2').text('Москва');
            $('#selected-city3').text('Москва');
            $('#selected-city5').text('Москва');
            $('.city-text').text('Москва');
            
            $('#selected-country1').text('Россия');
            $('#selected-region1').text('Московская область');
            
            $('#reg_location_input_id').val('1');
            $('#reg_location_input').val('Москва');
            $('#reg_location_input_region').val('Московская область');
            $('#reg_location_input_country').val('Россия');
            $('.delivery-text').removeClass('active');
            $('#delivery-text-moscow').addClass('active');
            $('#contact-all-phone').hide();
            $('#contact-moscow-phone').show();
            
            
            
            var data = $('#reg_location_form').serialize();
            $.ajax({
                type: "POST",
                url: "/setlocation",
                data: data,
                dataType: 'json',
                success: function(msg){
                    if(typeof(basket_page) != "undefined")
                    {
                        console.log('ffdf');
                        $.ajax({
                            type: "POST",
                            url: "/paymentajax",
                            data: 'city='+'Москва',
                            success: function(msg){
                                $('#cart-payments').html(msg);
                            }
                        });
                    }
                }
            })
        }
    })
})