(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * @preserve jquery-param (c) 2015 KNOWLEDGECODE | MIT
 */
/*global define */
(function (global) {
    'use strict';

    var param = function (a) {
        var add = function (s, k, v) {
            v = typeof v === 'function' ? v() : v === null ? '' : v === undefined ? '' : v;
            s[s.length] = encodeURIComponent(k) + '=' + encodeURIComponent(v);
        }, buildParams = function (prefix, obj, s) {
            var i, len, key;

            if (Object.prototype.toString.call(obj) === '[object Array]') {
                for (i = 0, len = obj.length; i < len; i++) {
                    buildParams(prefix + '[' + (typeof obj[i] === 'object' ? i : '') + ']', obj[i], s);
                }
            } else if (obj && obj.toString() === '[object Object]') {
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        if (prefix) {
                            buildParams(prefix + '[' + key + ']', obj[key], s, add);
                        } else {
                            buildParams(key, obj[key], s, add);
                        }
                    }
                }
            } else if (prefix) {
                add(s, prefix, obj);
            } else {
                for (key in obj) {
                    add(s, key, obj[key]);
                }
            }
            return s;
        };
        return buildParams('', a, []).join('&').replace(/%20/g, '+');
    };

    if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = param;
    } else if (typeof define === 'function' && define.amd) {
        define([], function () {
            return param;
        });
    } else {
        global.param = param;
    }

}(this));

},{}],2:[function(require,module,exports){
/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				return (document.cookie = [
					key, '=', value,
					attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
					attributes.path ? '; path=' + attributes.path : '',
					attributes.domain ? '; domain=' + attributes.domain : '',
					attributes.secure ? '; secure' : ''
				].join(''));
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));

},{}],3:[function(require,module,exports){
/*!
 * mustache.js - Logic-less {{mustache}} templates with JavaScript
 * http://github.com/janl/mustache.js
 */

/*global define: false Mustache: true*/

(function defineMustache (global, factory) {
  if (typeof exports === 'object' && exports && typeof exports.nodeName !== 'string') {
    factory(exports); // CommonJS
  } else if (typeof define === 'function' && define.amd) {
    define(['exports'], factory); // AMD
  } else {
    global.Mustache = {};
    factory(global.Mustache); // script, wsh, asp
  }
}(this, function mustacheFactory (mustache) {

  var objectToString = Object.prototype.toString;
  var isArray = Array.isArray || function isArrayPolyfill (object) {
    return objectToString.call(object) === '[object Array]';
  };

  function isFunction (object) {
    return typeof object === 'function';
  }

  /**
   * More correct typeof string handling array
   * which normally returns typeof 'object'
   */
  function typeStr (obj) {
    return isArray(obj) ? 'array' : typeof obj;
  }

  function escapeRegExp (string) {
    return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
  }

  /**
   * Null safe way of checking whether or not an object,
   * including its prototype, has a given property
   */
  function hasProperty (obj, propName) {
    return obj != null && typeof obj === 'object' && (propName in obj);
  }

  // Workaround for https://issues.apache.org/jira/browse/COUCHDB-577
  // See https://github.com/janl/mustache.js/issues/189
  var regExpTest = RegExp.prototype.test;
  function testRegExp (re, string) {
    return regExpTest.call(re, string);
  }

  var nonSpaceRe = /\S/;
  function isWhitespace (string) {
    return !testRegExp(nonSpaceRe, string);
  }

  var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };

  function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function fromEntityMap (s) {
      return entityMap[s];
    });
  }

  var whiteRe = /\s*/;
  var spaceRe = /\s+/;
  var equalsRe = /\s*=/;
  var curlyRe = /\s*\}/;
  var tagRe = /#|\^|\/|>|\{|&|=|!/;

  /**
   * Breaks up the given `template` string into a tree of tokens. If the `tags`
   * argument is given here it must be an array with two string values: the
   * opening and closing tags used in the template (e.g. [ "<%", "%>" ]). Of
   * course, the default is to use mustaches (i.e. mustache.tags).
   *
   * A token is an array with at least 4 elements. The first element is the
   * mustache symbol that was used inside the tag, e.g. "#" or "&". If the tag
   * did not contain a symbol (i.e. {{myValue}}) this element is "name". For
   * all text that appears outside a symbol this element is "text".
   *
   * The second element of a token is its "value". For mustache tags this is
   * whatever else was inside the tag besides the opening symbol. For text tokens
   * this is the text itself.
   *
   * The third and fourth elements of the token are the start and end indices,
   * respectively, of the token in the original template.
   *
   * Tokens that are the root node of a subtree contain two more elements: 1) an
   * array of tokens in the subtree and 2) the index in the original template at
   * which the closing tag for that section begins.
   */
  function parseTemplate (template, tags) {
    if (!template)
      return [];

    var sections = [];     // Stack to hold section tokens
    var tokens = [];       // Buffer to hold the tokens
    var spaces = [];       // Indices of whitespace tokens on the current line
    var hasTag = false;    // Is there a {{tag}} on the current line?
    var nonSpace = false;  // Is there a non-space char on the current line?

    // Strips all whitespace tokens array for the current line
    // if there was a {{#tag}} on it and otherwise only space.
    function stripSpace () {
      if (hasTag && !nonSpace) {
        while (spaces.length)
          delete tokens[spaces.pop()];
      } else {
        spaces = [];
      }

      hasTag = false;
      nonSpace = false;
    }

    var openingTagRe, closingTagRe, closingCurlyRe;
    function compileTags (tagsToCompile) {
      if (typeof tagsToCompile === 'string')
        tagsToCompile = tagsToCompile.split(spaceRe, 2);

      if (!isArray(tagsToCompile) || tagsToCompile.length !== 2)
        throw new Error('Invalid tags: ' + tagsToCompile);

      openingTagRe = new RegExp(escapeRegExp(tagsToCompile[0]) + '\\s*');
      closingTagRe = new RegExp('\\s*' + escapeRegExp(tagsToCompile[1]));
      closingCurlyRe = new RegExp('\\s*' + escapeRegExp('}' + tagsToCompile[1]));
    }

    compileTags(tags || mustache.tags);

    var scanner = new Scanner(template);

    var start, type, value, chr, token, openSection;
    while (!scanner.eos()) {
      start = scanner.pos;

      // Match any text between tags.
      value = scanner.scanUntil(openingTagRe);

      if (value) {
        for (var i = 0, valueLength = value.length; i < valueLength; ++i) {
          chr = value.charAt(i);

          if (isWhitespace(chr)) {
            spaces.push(tokens.length);
          } else {
            nonSpace = true;
          }

          tokens.push([ 'text', chr, start, start + 1 ]);
          start += 1;

          // Check for whitespace on the current line.
          if (chr === '\n')
            stripSpace();
        }
      }

      // Match the opening tag.
      if (!scanner.scan(openingTagRe))
        break;

      hasTag = true;

      // Get the tag type.
      type = scanner.scan(tagRe) || 'name';
      scanner.scan(whiteRe);

      // Get the tag value.
      if (type === '=') {
        value = scanner.scanUntil(equalsRe);
        scanner.scan(equalsRe);
        scanner.scanUntil(closingTagRe);
      } else if (type === '{') {
        value = scanner.scanUntil(closingCurlyRe);
        scanner.scan(curlyRe);
        scanner.scanUntil(closingTagRe);
        type = '&';
      } else {
        value = scanner.scanUntil(closingTagRe);
      }

      // Match the closing tag.
      if (!scanner.scan(closingTagRe))
        throw new Error('Unclosed tag at ' + scanner.pos);

      token = [ type, value, start, scanner.pos ];
      tokens.push(token);

      if (type === '#' || type === '^') {
        sections.push(token);
      } else if (type === '/') {
        // Check section nesting.
        openSection = sections.pop();

        if (!openSection)
          throw new Error('Unopened section "' + value + '" at ' + start);

        if (openSection[1] !== value)
          throw new Error('Unclosed section "' + openSection[1] + '" at ' + start);
      } else if (type === 'name' || type === '{' || type === '&') {
        nonSpace = true;
      } else if (type === '=') {
        // Set the tags for the next time around.
        compileTags(value);
      }
    }

    // Make sure there are no open sections when we're done.
    openSection = sections.pop();

    if (openSection)
      throw new Error('Unclosed section "' + openSection[1] + '" at ' + scanner.pos);

    return nestTokens(squashTokens(tokens));
  }

  /**
   * Combines the values of consecutive text tokens in the given `tokens` array
   * to a single token.
   */
  function squashTokens (tokens) {
    var squashedTokens = [];

    var token, lastToken;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      token = tokens[i];

      if (token) {
        if (token[0] === 'text' && lastToken && lastToken[0] === 'text') {
          lastToken[1] += token[1];
          lastToken[3] = token[3];
        } else {
          squashedTokens.push(token);
          lastToken = token;
        }
      }
    }

    return squashedTokens;
  }

  /**
   * Forms the given array of `tokens` into a nested tree structure where
   * tokens that represent a section have two additional items: 1) an array of
   * all tokens that appear in that section and 2) the index in the original
   * template that represents the end of that section.
   */
  function nestTokens (tokens) {
    var nestedTokens = [];
    var collector = nestedTokens;
    var sections = [];

    var token, section;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      token = tokens[i];

      switch (token[0]) {
        case '#':
        case '^':
          collector.push(token);
          sections.push(token);
          collector = token[4] = [];
          break;
        case '/':
          section = sections.pop();
          section[5] = token[2];
          collector = sections.length > 0 ? sections[sections.length - 1][4] : nestedTokens;
          break;
        default:
          collector.push(token);
      }
    }

    return nestedTokens;
  }

  /**
   * A simple string scanner that is used by the template parser to find
   * tokens in template strings.
   */
  function Scanner (string) {
    this.string = string;
    this.tail = string;
    this.pos = 0;
  }

  /**
   * Returns `true` if the tail is empty (end of string).
   */
  Scanner.prototype.eos = function eos () {
    return this.tail === '';
  };

  /**
   * Tries to match the given regular expression at the current position.
   * Returns the matched text if it can match, the empty string otherwise.
   */
  Scanner.prototype.scan = function scan (re) {
    var match = this.tail.match(re);

    if (!match || match.index !== 0)
      return '';

    var string = match[0];

    this.tail = this.tail.substring(string.length);
    this.pos += string.length;

    return string;
  };

  /**
   * Skips all text until the given regular expression can be matched. Returns
   * the skipped string, which is the entire tail if no match can be made.
   */
  Scanner.prototype.scanUntil = function scanUntil (re) {
    var index = this.tail.search(re), match;

    switch (index) {
      case -1:
        match = this.tail;
        this.tail = '';
        break;
      case 0:
        match = '';
        break;
      default:
        match = this.tail.substring(0, index);
        this.tail = this.tail.substring(index);
    }

    this.pos += match.length;

    return match;
  };

  /**
   * Represents a rendering context by wrapping a view object and
   * maintaining a reference to the parent context.
   */
  function Context (view, parentContext) {
    this.view = view;
    this.cache = { '.': this.view };
    this.parent = parentContext;
  }

  /**
   * Creates a new context using the given view with this context
   * as the parent.
   */
  Context.prototype.push = function push (view) {
    return new Context(view, this);
  };

  /**
   * Returns the value of the given name in this context, traversing
   * up the context hierarchy if the value is absent in this context's view.
   */
  Context.prototype.lookup = function lookup (name) {
    var cache = this.cache;

    var value;
    if (cache.hasOwnProperty(name)) {
      value = cache[name];
    } else {
      var context = this, names, index, lookupHit = false;

      while (context) {
        if (name.indexOf('.') > 0) {
          value = context.view;
          names = name.split('.');
          index = 0;

          /**
           * Using the dot notion path in `name`, we descend through the
           * nested objects.
           *
           * To be certain that the lookup has been successful, we have to
           * check if the last object in the path actually has the property
           * we are looking for. We store the result in `lookupHit`.
           *
           * This is specially necessary for when the value has been set to
           * `undefined` and we want to avoid looking up parent contexts.
           **/
          while (value != null && index < names.length) {
            if (index === names.length - 1)
              lookupHit = hasProperty(value, names[index]);

            value = value[names[index++]];
          }
        } else {
          value = context.view[name];
          lookupHit = hasProperty(context.view, name);
        }

        if (lookupHit)
          break;

        context = context.parent;
      }

      cache[name] = value;
    }

    if (isFunction(value))
      value = value.call(this.view);

    return value;
  };

  /**
   * A Writer knows how to take a stream of tokens and render them to a
   * string, given a context. It also maintains a cache of templates to
   * avoid the need to parse the same template twice.
   */
  function Writer () {
    this.cache = {};
  }

  /**
   * Clears all cached templates in this writer.
   */
  Writer.prototype.clearCache = function clearCache () {
    this.cache = {};
  };

  /**
   * Parses and caches the given `template` and returns the array of tokens
   * that is generated from the parse.
   */
  Writer.prototype.parse = function parse (template, tags) {
    var cache = this.cache;
    var tokens = cache[template];

    if (tokens == null)
      tokens = cache[template] = parseTemplate(template, tags);

    return tokens;
  };

  /**
   * High-level method that is used to render the given `template` with
   * the given `view`.
   *
   * The optional `partials` argument may be an object that contains the
   * names and templates of partials that are used in the template. It may
   * also be a function that is used to load partial templates on the fly
   * that takes a single argument: the name of the partial.
   */
  Writer.prototype.render = function render (template, view, partials) {
    var tokens = this.parse(template);
    var context = (view instanceof Context) ? view : new Context(view);
    return this.renderTokens(tokens, context, partials, template);
  };

  /**
   * Low-level method that renders the given array of `tokens` using
   * the given `context` and `partials`.
   *
   * Note: The `originalTemplate` is only ever used to extract the portion
   * of the original template that was contained in a higher-order section.
   * If the template doesn't use higher-order sections, this argument may
   * be omitted.
   */
  Writer.prototype.renderTokens = function renderTokens (tokens, context, partials, originalTemplate) {
    var buffer = '';

    var token, symbol, value;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      value = undefined;
      token = tokens[i];
      symbol = token[0];

      if (symbol === '#') value = this.renderSection(token, context, partials, originalTemplate);
      else if (symbol === '^') value = this.renderInverted(token, context, partials, originalTemplate);
      else if (symbol === '>') value = this.renderPartial(token, context, partials, originalTemplate);
      else if (symbol === '&') value = this.unescapedValue(token, context);
      else if (symbol === 'name') value = this.escapedValue(token, context);
      else if (symbol === 'text') value = this.rawValue(token);

      if (value !== undefined)
        buffer += value;
    }

    return buffer;
  };

  Writer.prototype.renderSection = function renderSection (token, context, partials, originalTemplate) {
    var self = this;
    var buffer = '';
    var value = context.lookup(token[1]);

    // This function is used to render an arbitrary template
    // in the current context by higher-order sections.
    function subRender (template) {
      return self.render(template, context, partials);
    }

    if (!value) return;

    if (isArray(value)) {
      for (var j = 0, valueLength = value.length; j < valueLength; ++j) {
        buffer += this.renderTokens(token[4], context.push(value[j]), partials, originalTemplate);
      }
    } else if (typeof value === 'object' || typeof value === 'string' || typeof value === 'number') {
      buffer += this.renderTokens(token[4], context.push(value), partials, originalTemplate);
    } else if (isFunction(value)) {
      if (typeof originalTemplate !== 'string')
        throw new Error('Cannot use higher-order sections without the original template');

      // Extract the portion of the original template that the section contains.
      value = value.call(context.view, originalTemplate.slice(token[3], token[5]), subRender);

      if (value != null)
        buffer += value;
    } else {
      buffer += this.renderTokens(token[4], context, partials, originalTemplate);
    }
    return buffer;
  };

  Writer.prototype.renderInverted = function renderInverted (token, context, partials, originalTemplate) {
    var value = context.lookup(token[1]);

    // Use JavaScript's definition of falsy. Include empty arrays.
    // See https://github.com/janl/mustache.js/issues/186
    if (!value || (isArray(value) && value.length === 0))
      return this.renderTokens(token[4], context, partials, originalTemplate);
  };

  Writer.prototype.renderPartial = function renderPartial (token, context, partials) {
    if (!partials) return;

    var value = isFunction(partials) ? partials(token[1]) : partials[token[1]];
    if (value != null)
      return this.renderTokens(this.parse(value), context, partials, value);
  };

  Writer.prototype.unescapedValue = function unescapedValue (token, context) {
    var value = context.lookup(token[1]);
    if (value != null)
      return value;
  };

  Writer.prototype.escapedValue = function escapedValue (token, context) {
    var value = context.lookup(token[1]);
    if (value != null)
      return mustache.escape(value);
  };

  Writer.prototype.rawValue = function rawValue (token) {
    return token[1];
  };

  mustache.name = 'mustache.js';
  mustache.version = '2.3.0';
  mustache.tags = [ '{{', '}}' ];

  // All high-level mustache.* functions use this writer.
  var defaultWriter = new Writer();

  /**
   * Clears all cached templates in the default writer.
   */
  mustache.clearCache = function clearCache () {
    return defaultWriter.clearCache();
  };

  /**
   * Parses and caches the given template in the default writer and returns the
   * array of tokens it contains. Doing this ahead of time avoids the need to
   * parse templates on the fly as they are rendered.
   */
  mustache.parse = function parse (template, tags) {
    return defaultWriter.parse(template, tags);
  };

  /**
   * Renders the `template` with the given `view` and `partials` using the
   * default writer.
   */
  mustache.render = function render (template, view, partials) {
    if (typeof template !== 'string') {
      throw new TypeError('Invalid template! Template should be a "string" ' +
                          'but "' + typeStr(template) + '" was given as the first ' +
                          'argument for mustache#render(template, view, partials)');
    }

    return defaultWriter.render(template, view, partials);
  };

  // This is here for backwards compatibility with 0.4.x.,
  /*eslint-disable */ // eslint wants camel cased function name
  mustache.to_html = function to_html (template, view, partials, send) {
    /*eslint-enable*/

    var result = mustache.render(template, view, partials);

    if (isFunction(send)) {
      send(result);
    } else {
      return result;
    }
  };

  // Export the escaping function so that the user may override it.
  // See https://github.com/janl/mustache.js/issues/244
  mustache.escape = escapeHtml;

  // Export these mainly for testing, but also for advanced usage.
  mustache.Scanner = Scanner;
  mustache.Context = Context;
  mustache.Writer = Writer;

  return mustache;
}));

},{}],4:[function(require,module,exports){
(function (process){
/*
 * PinkySwear.js 2.2.2 - Minimalistic implementation of the Promises/A+ spec
 * 
 * Public Domain. Use, modify and distribute it any way you like. No attribution required.
 *
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 *
 * PinkySwear is a very small implementation of the Promises/A+ specification. After compilation with the
 * Google Closure Compiler and gzipping it weighs less than 500 bytes. It is based on the implementation for 
 * Minified.js and should be perfect for embedding. 
 *
 *
 * PinkySwear has just three functions.
 *
 * To create a new promise in pending state, call pinkySwear():
 *         var promise = pinkySwear();
 *
 * The returned object has a Promises/A+ compatible then() implementation:
 *          promise.then(function(value) { alert("Success!"); }, function(value) { alert("Failure!"); });
 *
 *
 * The promise returned by pinkySwear() is a function. To fulfill the promise, call the function with true as first argument and
 * an optional array of values to pass to the then() handler. By putting more than one value in the array, you can pass more than one
 * value to the then() handlers. Here an example to fulfill a promsise, this time with only one argument: 
 *         promise(true, [42]);
 *
 * When the promise has been rejected, call it with false. Again, there may be more than one argument for the then() handler:
 *         promise(true, [6, 6, 6]);
 *         
 * You can obtain the promise's current state by calling the function without arguments. It will be true if fulfilled,
 * false if rejected, and otherwise undefined.
 * 		   var state = promise(); 
 * 
 * https://github.com/timjansen/PinkySwear.js
 */
(function(target) {
	var undef;

	function isFunction(f) {
		return typeof f == 'function';
	}
	function isObject(f) {
		return typeof f == 'object';
	}
	function defer(callback) {
		if (typeof setImmediate != 'undefined')
			setImmediate(callback);
		else if (typeof process != 'undefined' && process['nextTick'])
			process['nextTick'](callback);
		else
			setTimeout(callback, 0);
	}

	target[0][target[1]] = function pinkySwear(extend) {
		var state;           // undefined/null = pending, true = fulfilled, false = rejected
		var values = [];     // an array of values as arguments for the then() handlers
		var deferred = [];   // functions to call when set() is invoked

		var set = function(newState, newValues) {
			if (state == null && newState != null) {
				state = newState;
				values = newValues;
				if (deferred.length)
					defer(function() {
						for (var i = 0; i < deferred.length; i++)
							deferred[i]();
					});
			}
			return state;
		};

		set['then'] = function (onFulfilled, onRejected) {
			var promise2 = pinkySwear(extend);
			var callCallbacks = function() {
	    		try {
	    			var f = (state ? onFulfilled : onRejected);
	    			if (isFunction(f)) {
		   				function resolve(x) {
						    var then, cbCalled = 0;
		   					try {
				   				if (x && (isObject(x) || isFunction(x)) && isFunction(then = x['then'])) {
										if (x === promise2)
											throw new TypeError();
										then['call'](x,
											function() { if (!cbCalled++) resolve.apply(undef,arguments); } ,
											function(value){ if (!cbCalled++) promise2(false,[value]);});
				   				}
				   				else
				   					promise2(true, arguments);
		   					}
		   					catch(e) {
		   						if (!cbCalled++)
		   							promise2(false, [e]);
		   					}
		   				}
		   				resolve(f.apply(undef, values || []));
		   			}
		   			else
		   				promise2(state, values);
				}
				catch (e) {
					promise2(false, [e]);
				}
			};
			if (state != null)
				defer(callCallbacks);
			else
				deferred.push(callCallbacks);
			return promise2;
		};
        if(extend){
            set = extend(set);
        }
		return set;
	};
})(typeof module == 'undefined' ? [window, 'pinkySwear'] : [module, 'exports']);


}).call(this,require('_process'))
},{"_process":5}],5:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],6:[function(require,module,exports){
/*! qwest 4.4.5 (https://github.com/pyrsmk/qwest) */

module.exports = function() {

	var global = typeof window != 'undefined' ? window : self,
		pinkyswear = require('pinkyswear'),
		jparam = require('jquery-param'),
		defaultOptions = {},
		// Default response type for XDR in auto mode
		defaultXdrResponseType = 'json',
		// Default data type
		defaultDataType = 'post',
		// Variables for limit mechanism
		limit = null,
		requests = 0,
		request_stack = [],
		// Get XMLHttpRequest object
		getXHR = global.XMLHttpRequest? function(){
			return new global.XMLHttpRequest();
		}: function(){
			return new ActiveXObject('Microsoft.XMLHTTP');
		},
		// Guess XHR version
		xhr2 = (getXHR().responseType===''),

	// Core function
	qwest = function(method, url, data, options, before) {
		// Format
		method = method.toUpperCase();
		data = data || null;
		options = options || {};
		for(var name in defaultOptions) {
			if(!(name in options)) {
				if(typeof defaultOptions[name] == 'object' && typeof options[name] == 'object') {
					for(var name2 in defaultOptions[name]) {
						options[name][name2] = defaultOptions[name][name2];
					}
				}
				else {
					options[name] = defaultOptions[name];
				}
			}
		}

		// Define variables
		var nativeResponseParsing = false,
			crossOrigin,
			xhr,
			xdr = false,
			timeout,
			aborted = false,
			attempts = 0,
			headers = {},
			mimeTypes = {
				text: '*/*',
				xml: 'text/xml',
				json: 'application/json',
				post: 'application/x-www-form-urlencoded',
				document: 'text/html'
			},
			accept = {
				text: '*/*',
				xml: 'application/xml; q=1.0, text/xml; q=0.8, */*; q=0.1',
				json: 'application/json; q=1.0, text/*; q=0.8, */*; q=0.1'
			},
			i, j,
			response,
			sending = false,

		// Create the promise
		promise = pinkyswear(function(pinky) {
			pinky.abort = function() {
				if(!aborted) {
					if(xhr && xhr.readyState != 4) { // https://stackoverflow.com/questions/7287706/ie-9-javascript-error-c00c023f
						xhr.abort();
					}
					if(sending) {
						--requests;
						sending = false;
					}
					aborted = true;
				}
			};
			pinky.send = function() {
				// Prevent further send() calls
				if(sending) {
					return;
				}
				// Reached request limit, get out!
				if(requests == limit) {
					request_stack.push(pinky);
					return;
				}
				// Verify if the request has not been previously aborted
				if(aborted) {
					if(request_stack.length) {
						request_stack.shift().send();
					}
					return;
				}
				// The sending is running
				++requests;
				sending = true;
				// Get XHR object
				xhr = getXHR();
				if(crossOrigin) {
					if(!('withCredentials' in xhr) && global.XDomainRequest) {
						xhr = new XDomainRequest(); // CORS with IE8/9
						xdr = true;
						if(method != 'GET' && method != 'POST') {
							method = 'POST';
						}
					}
				}
				// Open connection
				if(xdr) {
					xhr.open(method, url);
				}
				else {
					xhr.open(method, url, options.async, options.user, options.password);
					if(xhr2 && options.async) {
						xhr.withCredentials = options.withCredentials;
					}
				}
				// Set headers
				if(!xdr) {
					for(var i in headers) {
						if(headers[i]) {
							xhr.setRequestHeader(i, headers[i]);
						}
					}
				}
				// Verify if the response type is supported by the current browser
				if(xhr2 && options.responseType != 'auto') {
					try {
						xhr.responseType = options.responseType;
						nativeResponseParsing = (xhr.responseType == options.responseType);
					}
					catch(e) {}
				}
				// Plug response handler
				if(xhr2 || xdr) {
					xhr.onload = handleResponse;
					xhr.onerror = handleError;
					// http://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
					if(xdr) {
						xhr.onprogress = function() {};
					}
				}
				else {
					xhr.onreadystatechange = function() {
						if(xhr.readyState == 4) {
							handleResponse();
						}
					};
				}
				// Plug timeout
				if(options.async) {
					if('timeout' in xhr) {
						xhr.timeout = options.timeout;
						xhr.ontimeout = handleTimeout;
					}
					else {
						timeout = setTimeout(handleTimeout, options.timeout);
					}
				}
				// http://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
				else if(xdr) {
					xhr.ontimeout = function() {};
				}
				// Override mime type to ensure the response is well parsed
				if(options.responseType != 'auto' && 'overrideMimeType' in xhr) {
					xhr.overrideMimeType(mimeTypes[options.responseType]);
				}
				// Run 'before' callback
				if(before) {
					before(xhr);
				}
				// Send request
				if(xdr) {
					// https://developer.mozilla.org/en-US/docs/Web/API/XDomainRequest
					setTimeout(function() {
						xhr.send(method != 'GET'? data : null);
					}, 0);
				}
				else {
					xhr.send(method != 'GET' ? data : null);
				}
			};
			return pinky;
		}),

		// Handle the response
		handleResponse = function() {
			var i, responseType;
			// Stop sending state
			sending = false;
			clearTimeout(timeout);
			// Launch next stacked request
			if(request_stack.length) {
				request_stack.shift().send();
			}
			// Verify if the request has not been previously aborted
			if(aborted) {
				return;
			}
			// Decrease the number of requests
			--requests;
			// Handle response
			try{
				// Process response
				if(nativeResponseParsing) {
					if('response' in xhr && xhr.response === null) {
						throw 'The request response is empty';
					}
					response = xhr.response;
				}
				else {
					// Guess response type
					responseType = options.responseType;
					if(responseType == 'auto') {
						if(xdr) {
							responseType = defaultXdrResponseType;
						}
						else {
							var ct = xhr.getResponseHeader('Content-Type') || '';
							if(ct.indexOf(mimeTypes.json)>-1) {
								responseType = 'json';
							}
							else if(ct.indexOf(mimeTypes.xml) > -1) {
								responseType = 'xml';
							}
							else {
								responseType = 'text';
							}
						}
					}
					// Handle response type
					switch(responseType) {
						case 'json':
							if(xhr.responseText.length) {
								try {
									if('JSON' in global) {
										response = JSON.parse(xhr.responseText);
									}
									else {
										response = new Function('return (' + xhr.responseText + ')')();
									}
								}
								catch(e) {
									throw "Error while parsing JSON body : "+e;
								}
							}
							break;
						case 'xml':
							// Based on jQuery's parseXML() function
							try {
								// Standard
								if(global.DOMParser) {
									response = (new DOMParser()).parseFromString(xhr.responseText,'text/xml');
								}
								// IE<9
								else {
									response = new ActiveXObject('Microsoft.XMLDOM');
									response.async = 'false';
									response.loadXML(xhr.responseText);
								}
							}
							catch(e) {
								response = undefined;
							}
							if(!response || !response.documentElement || response.getElementsByTagName('parsererror').length) {
								throw 'Invalid XML';
							}
							break;
						default:
							response = xhr.responseText;
					}
				}
				// Late status code verification to allow passing data when, per example, a 409 is returned
				// --- https://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
				if('status' in xhr && !/^2|1223/.test(xhr.status)) {
					throw xhr.status + ' (' + xhr.statusText + ')';
				}
				// Fulfilled
				promise(true, [xhr, response]);
			}
			catch(e) {
				// Rejected
				promise(false, [e, xhr, response]);
			}
		},

		// Handle errors
		handleError = function(message) {
			if(!aborted) {
				message = typeof message == 'string' ? message : 'Connection aborted';
				promise.abort();
				promise(false, [new Error(message), xhr, null]);
			}
		},
			
		// Handle timeouts
		handleTimeout = function() {
			if(!aborted) {
				if(!options.attempts || ++attempts != options.attempts) {
					xhr.abort();
					sending = false;
					promise.send();
				}
				else {
					handleError('Timeout (' + url + ')');
				}
			}
		};

		// Normalize options
		options.async = 'async' in options ? !!options.async : true;
		options.cache = 'cache' in options ? !!options.cache : false;
		options.dataType = 'dataType' in options ? options.dataType.toLowerCase() : defaultDataType;
		options.responseType = 'responseType' in options ? options.responseType.toLowerCase() : 'auto';
		options.user = options.user || '';
		options.password = options.password || '';
		options.withCredentials = !!options.withCredentials;
		options.timeout = 'timeout' in options ? parseInt(options.timeout, 10) : 30000;
		options.attempts = 'attempts' in options ? parseInt(options.attempts, 10) : 1;

		// Guess if we're dealing with a cross-origin request
		i = url.match(/\/\/(.+?)\//);
		crossOrigin = i && (i[1] ? i[1] != location.host : false);

		// Prepare data
		if('ArrayBuffer' in global && data instanceof ArrayBuffer) {
			options.dataType = 'arraybuffer';
		}
		else if('Blob' in global && data instanceof Blob) {
			options.dataType = 'blob';
		}
		else if('Document' in global && data instanceof Document) {
			options.dataType = 'document';
		}
		else if('FormData' in global && data instanceof FormData) {
			options.dataType = 'formdata';
		}
		if(data !== null) {
			switch(options.dataType) {
				case 'json':
					data = JSON.stringify(data);
					break;
				case 'post':
					data = jparam(data);
			}
		}

		// Prepare headers
		if(options.headers) {
			var format = function(match,p1,p2) {
				return p1 + p2.toUpperCase();
			};
			for(i in options.headers) {
				headers[i.replace(/(^|-)([^-])/g,format)] = options.headers[i];
			}
		}
		if(!('Content-Type' in headers) && method!='GET') {
			if(options.dataType in mimeTypes) {
				if(mimeTypes[options.dataType]) {
					headers['Content-Type'] = mimeTypes[options.dataType];
				}
			}
		}
		if(!headers.Accept) {
			headers.Accept = (options.responseType in accept) ? accept[options.responseType] : '*/*';
		}
		if(!crossOrigin && !('X-Requested-With' in headers)) { // (that header breaks in legacy browsers with CORS)
			headers['X-Requested-With'] = 'XMLHttpRequest';
		}
		if(!options.cache && !('Cache-Control' in headers)) {
			headers['Cache-Control'] = 'no-cache';
		}

		// Prepare URL
		if(method == 'GET' && data && typeof data == 'string') {
			url += (/\?/.test(url)?'&':'?') + data;
		}

		// Start the request
		if(options.async) {
			promise.send();
		}

		// Return promise
		return promise;

	};
	
	// Define external qwest object
	var getNewPromise = function(q) {
			// Prepare
			var promises = [],
				loading = 0,
				values = [];
			// Create a new promise to handle all requests
			return pinkyswear(function(pinky) {
				// Basic request method
				var method_index = -1,
					createMethod = function(method) {
						return function(url, data, options, before) {
							var index = ++method_index;
							++loading;
							promises.push(qwest(method, pinky.base + url, data, options, before).then(function(xhr, response) {
								values[index] = arguments;
								if(!--loading) {
									pinky(true, values.length == 1 ? values[0] : [values]);
								}
							}, function() {
								pinky(false, arguments);
							}));
							return pinky;
						};
					};
				// Define external API
				pinky.get = createMethod('GET');
				pinky.post = createMethod('POST');
				pinky.put = createMethod('PUT');
				pinky['delete'] = createMethod('DELETE');
				pinky['catch'] = function(f) {
					return pinky.then(null, f);
				};
				pinky.complete = function(f) {
					var func = function() {
						f(); // otherwise arguments will be passed to the callback
					};
					return pinky.then(func, func);
				};
				pinky.map = function(type, url, data, options, before) {
					return createMethod(type.toUpperCase()).call(this, url, data, options, before);
				};
				// Populate methods from external object
				for(var prop in q) {
					if(!(prop in pinky)) {
						pinky[prop] = q[prop];
					}
				}
				// Set last methods
				pinky.send = function() {
					for(var i=0, j=promises.length; i<j; ++i) {
						promises[i].send();
					}
					return pinky;
				};
				pinky.abort = function() {
					for(var i=0, j=promises.length; i<j; ++i) {
						promises[i].abort();
					}
					return pinky;
				};
				return pinky;
			});
		},
		q = {
			base: '',
			get: function() {
				return getNewPromise(q).get.apply(this, arguments);
			},
			post: function() {
				return getNewPromise(q).post.apply(this, arguments);
			},
			put: function() {
				return getNewPromise(q).put.apply(this, arguments);
			},
			'delete': function() {
				return getNewPromise(q)['delete'].apply(this, arguments);
			},
			map: function() {
				return getNewPromise(q).map.apply(this, arguments);
			},
			xhr2: xhr2,
			limit: function(by) {
				limit = by;
				return q;
			},
			setDefaultOptions: function(options) {
				defaultOptions = options;
				return q;
			},
			setDefaultXdrResponseType: function(type) {
				defaultXdrResponseType = type.toLowerCase();
				return q;
			},
			setDefaultDataType: function(type) {
				defaultDataType = type.toLowerCase();
				return q;
			},
			getOpenRequests: function() {
				return requests;
			}
		};
	
	return q;

}();

},{"jquery-param":1,"pinkyswear":4}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsCookie = require('js-cookie');

var _jsCookie2 = _interopRequireDefault(_jsCookie);

var _Logger = require('./Logger');

var _Logger2 = _interopRequireDefault(_Logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var widgetCookieName = 'lh_widget_%n_pages_counter';
var widgetSystemCookieName = 'lh_widget_system_pages_counter';

var isMobile = function isMobile() {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
    //ololo
};

var ConditionsValidator = function () {
    /**
     * Constructor
     * @param {Object} conditions_and
     * @param {Object} conditions_or
     * @param {String} widgetId
     */
    function ConditionsValidator(conditions_and, conditions_or, widgetId) {
        var _this = this;

        _classCallCheck(this, ConditionsValidator);

        /**
         * Make validate function
         * @returns {boolean}
         */
        this.validate = function () {
            // Get results of conditions validation
            conditions_and = conditions_and || [];
            conditions_or = conditions_or || [[]];
            var allCheck = conditions_and.map(compile, _this);
            var oneOfCheck = conditions_or.map(function (or_array) {
                return or_array.map(compile, _this);
            });
            // Do we need to show widget
            var isAll = allCheck.length === allCheck.filter(function (item) {
                return item === true;
            }).length;

            var isOneOf = false;
            if (oneOfCheck.length) {
                isOneOf = oneOfCheck[0].filter(function (item) {
                    return item === true;
                }).length;
            }

            function compile(options) {
                return this.compileValidator(options.type, options.name, options.op, options.value, widgetId);
            }

            return Boolean(isAll || isOneOf);
        };
    }

    _createClass(ConditionsValidator, [{
        key: 'compileValidator',
        value: function compileValidator(type, name, operation, conditionValue, widgetId) {
            var value = void 0;

            switch (type) {
                case 'cookie':
                    value = _jsCookie2.default.get(name) || '';
                    break;
                case 'fixed_vars':
                    value = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId)[name];

                    if (name === 'widget_shown') {
                        value = window.lhWidgetSystem.getWidgetVariablesByWidgetId(conditionValue)[name];
                    }
                    if (name === 'pages_counter') {
                        value = parseInt(_jsCookie2.default.get(widgetSystemCookieName));
                    }
                    if (name === 'referrer') {
                        value = document.referrer;
                    }
                    if (name === 'device') {
                        if (isMobile()) {
                            value = "mobile";
                        } else {
                            value = "desktop";
                        }
                    };
                    /*
                    FOR EVERY WIDGET. DO NOT REMOVE
                    if (name === 'pages_counter') {
                        const currentWidgetCookieName = widgetCookieName.replace('%n', widgetId);
                        value = parseInt(Cookie.get(currentWidgetCookieName));
                    }*/
                    break;
                case 'ls_vars':
                    value = window.localStorage.getItem(name);
                    break;
                case 'global_vars':
                    value = window[name];
                    break;
            }

            var isValid = void 0;
            // Simple logic
            switch (operation) {
                case 'is':
                    isValid = conditionValue === value;
                    break;
                case 'not':
                    isValid = conditionValue !== value;
                    break;
                case 'more':
                    isValid = value > conditionValue;
                    break;
                case 'less':
                    isValid = value < conditionValue;
                    break;
                case 'contain':
                    isValid = value.indexOf(conditionValue, 0) !== -1;
                    break;
                case 'not_contain':
                    isValid = value.indexOf(conditionValue, 0) === -1;
                    break;
            }

            // Widget shown logic
            if (name === 'widget_shown') {
                switch (operation) {
                    case 'is':
                        isValid = value;
                        break;
                    case 'not':
                        isValid = !value;
                        break;
                }
            }

            _Logger2.default.log('Condition', ['NAME: ' + name, 'CONDITION VALUE: ' + conditionValue + ', VALUE TYPE ' + (typeof conditionValue === 'undefined' ? 'undefined' : _typeof(conditionValue)), 'REAL VALUE: ' + value + ', VALUE TYPE ' + (typeof value === 'undefined' ? 'undefined' : _typeof(value)), 'OPERATION: ' + operation, 'IS VALID: ' + isValid]);

            return isValid;
        }
    }, {
        key: 'isWidgetShown',
        value: function isWidgetShown(widgetId) {}
    }]);

    return ConditionsValidator;
}();

exports.default = ConditionsValidator;

},{"./Logger":9,"js-cookie":2}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Observable = require('./Observable');

var _Observable2 = _interopRequireDefault(_Observable);

var _Observer = require('./Observer');

var _Observer2 = _interopRequireDefault(_Observer);

var _Logger = require('./Logger');

var _Logger2 = _interopRequireDefault(_Logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var notifiersOfWidgets = {};

exports.default = function () {
    var eventsTriggers = {
        timer: timer,
        idle_timer: idleTimer,
        mouse_out_top: mouseOutTop,
        mouse_out: mouseOut
    };

    // API function, which add listeners to widget
    function bind(widgetId, eventType, eventValue, widgetShowMethod, validator) {
        // Get trigger from list base on incoming parameters
        var initTrigger = eventsTriggers[eventType];

        // If no notifiers for current widgetId - make new Observable instance
        if (!notifiersOfWidgets[widgetId]) {
            notifiersOfWidgets[widgetId] = new _Observable2.default(widgetId);
        }

        // Save notifier in variable
        var notifier = notifiersOfWidgets[widgetId];
        // Run trigger
        initTrigger(eventValue, widgetShowMethod, notifier, validator, widgetId);

        return notifier;
    }

    function timer(secondsValue, widgetShowMethod, notifier, validator, widgetId) {
        var seconds = parseInt(secondsValue);
        seconds = seconds !== seconds ? null : seconds !== 0 ? seconds : 0;

        var widgetVariables = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId);

        widgetVariables['timer'] = 0;

        var timeout = setInterval(function () {
            if (seconds === null || widgetVariables['timer'] < seconds) {
                widgetVariables['timer'] += 1;
            } else {
                callShowMethod();
            }

            //console.log(widgetVariables['timer']);
        }, 1000);

        function callShowMethod() {
            _Logger2.default.log('DONE SIMPLE TIMER');

            if (validator.validate()) {
                widgetShowMethod();
                notifier.notifyObservers();

                widgetVariables['widget_shown'] = true;
            }

            unbind();
        }

        function unbind() {
            clearInterval(timeout);
        }

        notifier.addObserver(new _Observer2.default(unbind));
    }
    function idleTimer(secondsValue, widgetShowMethod, notifier, validator, widgetId) {
        var seconds = parseInt(secondsValue);
        seconds = seconds !== seconds ? null : seconds !== 0 ? seconds : 0;

        var timeout = void 0;
        var widgetVariables = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId);

        function resetTimer() {
            clearTimeout(timeout);
            widgetVariables['idle_timer'] = 0;

            timeout = setInterval(function () {
                if (seconds === null || widgetVariables['idle_timer'] < seconds) {
                    widgetVariables['idle_timer'] += 1;
                } else {
                    callShowMethod();
                }
            }, 1000);
        }

        document.addEventListener('mousemove', resetTimer);
        document.addEventListener('keypress', resetTimer);

        function callShowMethod() {
            _Logger2.default.log('DONE IDLE TIMER');
            if (validator.validate()) {
                widgetShowMethod();
                notifier.notifyObservers();

                widgetVariables['widget_shown'] = true;
            }

            unbind();
        }

        function unbind() {
            clearInterval(timeout);
            document.removeEventListener('mousemove', resetTimer);
            document.removeEventListener('onkeypress', resetTimer);
        }

        notifier.addObserver(new _Observer2.default(unbind));
    }
    function mouseOutTop(eventValue, widgetShowMethod, notifier, validator, widgetId) {
        if (eventValue !== 1) {
            return false;
        }

        var widgetVariables = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId);
        var xPosition = null;
        var yPosition = null;

        document.addEventListener('mouseout', isMouseOutTop);

        function isMouseOutTop(e) {
            var isOutOfPage = isMouseOutOfPage(e);
            // Get current mouse position coordinates
            var x = e.clientX;
            var y = e.clientY;
            // If previous positions is not set,
            // and mouse is not out of page - set current mouse position
            if (xPosition === null && yPosition === null || !isOutOfPage) {
                xPosition = x;
                yPosition = y;
            }
            // Is mouse out of window
            if (isOutOfPage) {
                // Check:
                // 1. If new mouse y position is less then previous
                // 2. If new mouse y position is less then fix pixel size from top
                // 3. If new mouse x position is more then 0
                if (y < yPosition && y <= 50 && x >= 100) {
                    // Mouse gone out top of window
                    callShowMethod();
                }
            }
        }

        function callShowMethod() {
            _Logger2.default.log('DONE MOUSE OUT TOP');
            if (validator.validate()) {
                var result = widgetShowMethod();

                if (result !== -1) {
                    notifier.notifyObservers();
                    widgetVariables['widget_shown'] = true;
                    unbind();
                }
            }
        }

        function unbind() {
            document.removeEventListener('mouseout', isMouseOutTop);
        }

        notifier.addObserver(new _Observer2.default(unbind));
    }
    function mouseOut(eventValue, widgetShowMethod, notifier, validator, widgetId) {
        if (eventValue !== 1) {
            return false;
        }

        var widgetVariables = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId);

        document.addEventListener('mouseout', isMouseOut);

        function isMouseOut(e) {
            if (isMouseOutOfPage(e)) {
                callShowMethod();
            }
        }

        function callShowMethod() {
            _Logger2.default.log('DONE MOUSE OUT OF PAGE');
            if (validator.validate()) {
                var result = widgetShowMethod();

                if (result !== -1) {
                    notifier.notifyObservers();
                    widgetVariables['widget_shown'] = true;
                    unbind();
                }
            }
        }

        function unbind() {
            document.removeEventListener('mouseout', isMouseOut);
        }

        notifier.addObserver(new _Observer2.default(unbind));
    }

    /*
    * Helpers
    * */
    function isMouseOutOfPage(e) {
        return Boolean(e.toElement === null && e.relatedTarget === null);
    }

    return {
        listen: bind
    };
}();

},{"./Logger":9,"./Observable":10,"./Observer":11}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var Logger = function () {

    var titleConfig = ['font-size: 16px;', 'font-weight: 900;', 'background: #FDFEFF;', 'color: #051A38;'];

    var stringConfig = ['font-size: 12px;', 'background: #FDFEFF;', 'color: #001820;'];

    return {
        log: function log(title) {
            var strings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            console.log('%c' + title, titleConfig.join(' '));
            strings.forEach(function (string) {
                console.log('%c' + string, stringConfig.join(' '));
            });
            console.log('\r\n');
        }
    };
}();

exports.default = Logger;

},{}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Observer = require('./Observer');

var _Observer2 = _interopRequireDefault(_Observer);

var _Logger = require('./Logger');

var _Logger2 = _interopRequireDefault(_Logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Observable = function () {
    var Observable = function () {
        function Observable(widgetId) {
            _classCallCheck(this, Observable);

            this.observersList = [];
        }

        _createClass(Observable, [{
            key: 'addObserver',
            value: function addObserver(observer) {
                if (!observer instanceof _Observer2.default) {
                    throw new Error('Try to add not Observer object');
                }
                this.observersList.push(observer);

                // Log information
                /*let widgetId = this.getWidgetId();
                let widgetName = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId).widgetName;
                Logger.log(
                    'Add observer to widget',
                    [
                        `ID: ${widgetId}`,
                        `NAME : ${widgetName}`
                    ]
                );*/
            }
        }, {
            key: 'notifyObservers',
            value: function notifyObservers() {
                this.observersList.forEach(function (observer) {
                    observer.handle();
                });

                /*// Log information
                let widgetId = this.getWidgetId();
                let widgetName = window.lhWidgetSystem.getWidgetVariablesByWidgetId(widgetId).widgetName;
                 Logger.log(
                    'Notify from widget',
                    [
                        `ID: ${widgetId}`,
                        `NAME : ${widgetName}`
                    ]
                );*/
            }
        }]);

        return Observable;
    }();

    return Observable;
}();

exports.default = Observable;

},{"./Logger":9,"./Observer":11}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Observer = function Observer(handleFunction) {
    _classCallCheck(this, Observer);

    this.handle = handleFunction;
};

exports.default = Observer;

/*
* В общем, Юра (: резюмирую ситуацию: если тебе не сильно дофига сложно,
* реализуй произвольное количество блоков или... если геморрой - то никто не расстроется,
* сошлись на том, что это не сильно нужно на данном этапе,
* но на всякий случай можно сделать на будущее, с другой стороны
* Дима не хочет усложнять логику и интерфейс админки, так что как-то так (:
*
* */

},{}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _qwest = require('qwest');

var _qwest2 = _interopRequireDefault(_qwest);

var _mustache = require('mustache');

var _mustache2 = _interopRequireDefault(_mustache);

var _Events = require('./Events');

var _Events2 = _interopRequireDefault(_Events);

var _ConditionsValidator = require('./ConditionsValidator');

var _ConditionsValidator2 = _interopRequireDefault(_ConditionsValidator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//import Cookie from 'js-cookie';

var widgetsTypesPath = 'https://media.leadhit.io/widgets_v2';
var widgetCookieName = 'lh_widget_%n_pages_counter';
var widgetCookieDaysExpire = 1;

var requiredTriggerTypes = ['timer', 'idle_timer'];

function Widget(id, name, type, widgetOptions) {

    var showMethod = null;
    var setTriggersMethod = null;

    var validator = new _ConditionsValidator2.default(widgetOptions.conditions_and, widgetOptions.conditions_or, id);
    setTriggersMethod = setTriggers(id, widgetOptions, validator);

    var html = void 0;
    var htmlVariables = preCompileVariables(widgetOptions.html_vars);
    var css = void 0;
    var cssVariables = preCompileVariables(widgetOptions.css_vars);
    var js = void 0;
    var jsDefaultVariables = [{
        name: 'widgetId',
        value: id
    }];
    var jsVariables = preCompileVariables(jsDefaultVariables.concat(widgetOptions.js_vars));

    var getTemplate = _qwest2.default.get(widgetsTypesPath + '/' + type + '/widget.html');
    var getStyle = _qwest2.default.get(widgetsTypesPath + '/' + type + '/widget.css');
    var getScript = _qwest2.default.get(widgetsTypesPath + '/' + type + '/widget.js');

    getTemplate.then(function (xhr, response) {
        // Save html compiled with variables
        html = _mustache2.default.render(response, htmlVariables);
        // Make widget html container
        var widgetContainer = document.createElement('div');
        // Add id
        widgetContainer.id = 'lhWidgetItem-' + id;
        // Add class name
        widgetContainer.className = 'lhWidgetItem lhWidgetItem-' + type;
        // Append html into the end of widgetContainer element
        widgetContainer.insertAdjacentHTML('beforeEnd', html);
        // Append widgetContainer element to document.body
        document.body.appendChild(widgetContainer);
    });
    getStyle.then(function (xhr, response) {
        // Save css compiled with variables
        css = _mustache2.default.render(response, cssVariables);

        // Make style tag
        var style = document.createElement('style');
        // Insert compiled style into style tag
        style.textContent = css;
        // Append style into the end of document.head
        document.head.appendChild(style);
    });
    getScript.then(function (xhr, response) {
        // Save js compiled with variables
        js = _mustache2.default.render(response, jsVariables);
        // Make script tag
        var script = document.createElement('script');
        // Insert into script tag

        script.textContent = js;
        // Append script into the end of document.body
        document.body.appendChild(script);
    });

    /*const pagesCounterCookieName = widgetCookieName.replace('%n', id);
     let pagesCounterCookieValue = Cookie.get(pagesCounterCookieName);
    if (!pagesCounterCookieValue) {
        Cookie.set(pagesCounterCookieName, 0, widgetCookieDaysExpire);
    } else {
        const newPagesCounterCookieValue = parseInt(pagesCounterCookieValue) + 1;
        Cookie.set(pagesCounterCookieName, newPagesCounterCookieValue);
    }*/

    function setTriggers(widgetId, options, validator) {
        return function () {
            var requiredTypes = requiredTriggerTypes.slice();

            for (var triggerType in options.triggers) {
                var value = options.triggers[triggerType];

                var requiredTriggerTypePosition = requiredTypes.indexOf(triggerType, 0);
                if (requiredTriggerTypePosition !== -1) {
                    requiredTypes.splice(requiredTriggerTypePosition, 1);
                }

                _Events2.default.listen(widgetId, triggerType, value, showMethod, validator);
            }

            requiredTypes.forEach(function (type) {
                _Events2.default.listen(widgetId, type, null, null, validator);
            });
        };
    }

    function preCompileVariables(variablesObject) {
        var variables = {};

        variablesObject.forEach(function (variable) {
            variables[variable.name] = variable.value;
        });

        return variables;
    }

    return {
        getWidgetId: function getWidgetId() {
            return id;
        },
        addShowInterface: function addShowInterface(widgetObject) {
            if (typeof widgetObject.show === 'function' && !showMethod) {
                showMethod = widgetObject.show.bind(widgetObject);

                setTriggersMethod();
            }
        }
    };
}

exports.default = Widget;

},{"./ConditionsValidator":7,"./Events":8,"mustache":3,"qwest":6}],13:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Init empty widgets list
var list = [];

var WidgetList = function () {
    function WidgetList() {
        _classCallCheck(this, WidgetList);
    }

    _createClass(WidgetList, [{
        key: "put",

        /**
         * Add Object {widgetId : widgetObject} to list
         * @param {String} id - widget id
         * @param {Widget} widget - Widget class instance
         */
        value: function put(id, widget) {
            var newWidget = {};
            newWidget[id] = widget;
            list.push(newWidget);
        }

        /**
         * Returns widget, or null
         * @param {String} id - widget id
         * @returns {Widget} or null, if no widget by id
         */

    }, {
        key: "getById",
        value: function getById(id) {
            var widgetsById = list.filter(function (widget) {
                var widgetId = Object.keys(widget)[0];
                return widgetId === id;
            });
            if (widgetsById.length) {
                return widgetsById[0][id];
            }

            return null;
        }
    }]);

    return WidgetList;
}();

exports.default = WidgetList;

},{}],14:[function(require,module,exports){
'use strict';

var _jsCookie = require('js-cookie');

var _jsCookie2 = _interopRequireDefault(_jsCookie);

var _Widget = require('./Widget');

var _Widget2 = _interopRequireDefault(_Widget);

var _WidgetList = require('./WidgetList');

var _WidgetList2 = _interopRequireDefault(_WidgetList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {

    var widgetSystemCookieName = 'lh_widget_system_pages_counter';

    // Fake config
    var fakeConfig = {
        "widgets": {
            "69588": {
                "show_params": {
                    "html_vars": [{
                        "name": "title",
                        "value": "Ololo",
                        "title": "Title description"
                    }, { "name": "subtitle", "value": "Trololo", "title": "Subtitle description" }],
                    "css_vars": [{ "name": "title", "value": "Default Title", "title": "Title description" }, {
                        "name": "subtitle",
                        "value": "Default Sub-title",
                        "title": "Subtitle description"
                    }],
                    "triggers": { "timer": 0, "mouse_out": 1 },
                    "custom_js": "",
                    "conditions_and": [{ "type": "cookie", "name": "aaa", "op": "is", "value": "333" }],
                    "conditions_or": [[{ "type": "fixed_vars", "name": "timer", "op": "more", "value": 6 }]],
                    "show_delay": 0
                }, "type": "Смартоффер", "name": "Hello, world!"
            },
            "5f8a6": {
                "show_params": {
                    "html_vars": [{
                        "name": "title",
                        "value": "Ololo",
                        "title": "Title description"
                    }, { "name": "subtitle", "value": "Trololo", "title": "Subtitle description" }],
                    "css_vars": [{
                        "name": "title",
                        "value": "Default Title",
                        "title": "Title description"
                    }, { "name": "subtitle", "value": "Default Sub-title", "title": "Subtitle description" }],
                    "triggers": { "mouse_out_top": 1 },
                    "custom_js": "",
                    "conditions_and": [{ "type": "fixed_vars", "value": "69588", "op": "not", "name": "widget_shown" }],
                    "conditions_or": [[{
                        "type": "fixed_vars",
                        "name": "url",
                        "value": "localhost",
                        "op": "contain"
                    }, { "type": "fixed_vars", "name": "url", "value": "netbed.ru", "op": "contain" }]],
                    "show_delay": 0
                }, "type": "wish_list", "name": "Welcome to the machine"
            }
        }, "testcases": [{ "69588": 1 }, { "5f8a6": 1 }]
    };

    // Get widgets config
    //let widgetsConfig = window.lh_widgets_conf.widgets;
    var widgetsConfig = Boolean(~window.location.href.indexOf('localhost', 0)) ? fakeConfig.widgets : window.top.lh_widgets_conf.widgets;
    var widgetsCases = Boolean(~window.location.href.indexOf('localhost', 0)) ? fakeConfig.testcases : window.top.lh_widgets_conf.testcases;
    // Init widgets list
    var widgetsList = new _WidgetList2.default();
    // Init empty widgets variables object
    var widgetsVariables = {};

    /**
     * Init widgets
     */

    var widgetsToShow = getWidgetIdsToShow();

    // For each widget widget name
    console.log(widgetsToShow);
    widgetsToShow.forEach(function (widgetId) {
        // Get widget config by name
        var widgetData = widgetsConfig[widgetId];
        // Init widget
        var widget = new _Widget2.default(widgetId, widgetData.name, widgetData.type, widgetData.show_params);
        // Set widget name
        // Set widget visible state to boolean "false" as default
        widgetsVariables[widgetId] = {
            widgetName: widgetData.name,
            widget_shown: false,
            url: window.location.href
        };
        // Add widget in widgets list
        widgetsList.put(widgetId, widget);
    });

    var pagesCounterCookieValue = _jsCookie2.default.get(widgetSystemCookieName);
    if (!pagesCounterCookieValue) {
        _jsCookie2.default.set(widgetSystemCookieName, 0, { expires: 10 });
    } else {
        var newPagesCounterCookieValue = parseInt(pagesCounterCookieValue) + 1;
        _jsCookie2.default.set(widgetSystemCookieName, newPagesCounterCookieValue);
    }

    window.lhWidgetSystem = {
        register: registerWidget,
        getWidgetVariablesByWidgetId: getWidgetVariablesByWidgetId
    };

    /**
     * Add show interface to widget
     * @param {Object} widgetInterface - javascript object width show field
     * @param {String} widgetId
     */
    function registerWidget(widgetInterface, widgetId) {
        var widget = widgetsList.getById(widgetId);

        if (widget) {
            widget.addShowInterface(widgetInterface);
        } else {
            throw new Error('Failed to register widget named "' + widgetId + '"');
        }
    }

    /**
     * Return object with widget states options
     * @param {String} widgetId
     * @returns {Object}
     */
    function getWidgetVariablesByWidgetId(widgetId) {
        return widgetsVariables[widgetId];
    }

    function getWidgetIdsToShow() {
        var tmp_uid;
        if (_jsCookie2.default.get('_lhtm_u')) {
            tmp_uid = _jsCookie2.default.get('_lhtm_u');
        } else {
            tmp_uid = 100;
        }
        var uid = tmp_uid;
        //const uid = generateUID(); // Fake uid
        var intId = parseInt('0x' + uid, 16) - 1;
        var widgetIds = [];

        //FAKE CASES
        /*let widgetsCases = [
            {
                /!*ajsndjknds: 1,
                 ajksndkajn: 2,
                 asasjdnkj: 3,
                 siu981h2: 4,
                 ajsnd1io: 5,
                 amsodas7: 6,
                 i23mdas: 7,
                 alskndn1: 8,
                 masklmd8: 9,*!/
                69588: 1,
                //'5f8a6': 2
            }
        ];*/

        widgetsCases.forEach(function (dictionary) {
            widgetIds.push(throwDice(intId, dictionary));
        });

        return widgetIds;
    }

    function throwDice(id, dictionary) {
        var caseKeys = Object.keys(dictionary);
        var sum = 0;
        caseKeys.forEach(function (caseKey) {
            sum += dictionary[caseKey];
        });
        var bound = 0;

        for (var key in dictionary) {
            bound += dictionary[key];
            if (id % sum < bound) {
                return key;
            }
        }
    }

    function generateUID() {
        return "xxxxxxxxxxxxxxxxxxxxxxxx".replace(/x/g, function () {
            var rand = void 0;
            try {
                rand = crypto.getRandomValues(new Uint8Array(1))[0] % 16;
            } catch (e) {
                rand = Math.floor(Math.random() * 16);
            }
            return rand.toString(16);
        });
    }
})();

},{"./Widget":12,"./WidgetList":13,"js-cookie":2}]},{},[14]);
