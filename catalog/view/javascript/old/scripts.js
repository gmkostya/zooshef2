function addProdQ(prod_id)
{
    var num = parseInt($('#variant_quantity-'+prod_id).val());
    var maxstock = $('#product-'+prod_id+' .price-value.active').data('stock');
    if(num < maxstock)
    {
        $('#variant_quantity-'+prod_id).val((num+1)+' шт.');
        $('#buy-product-'+prod_id).data('quantity', num+1);
    }
}

function decProdQ(prod_id)
{
    var num = parseInt($('#variant_quantity-'+prod_id).val());
    if(num > 1)
    {
        $('#variant_quantity-'+prod_id).val((num-1)+' шт.');
        $('#buy-product-'+prod_id).data('quantity', num-1);
    }
}

function addManyProdQ(prod_id)
{
    var num = parseInt($('#variant_quantity-'+prod_id).val());
    var maxstock = $('.price-value[data-variant='+prod_id+']').data('stock');
    
    
    
    if(num < maxstock)
    {
        $('#variant_quantity-'+prod_id).val((num+1)+' шт.');
        $('#buy-product-'+prod_id).data('quantity', num+1);
        countManyTotal();
    }
}

function decManyProdQ(prod_id)
{
    var num = parseInt($('#variant_quantity-'+prod_id).val());
    if(num > 0)
    {
        $('#variant_quantity-'+prod_id).val((num-1)+' шт.');
        $('#buy-product-'+prod_id).data('quantity', num-1);
        countManyTotal();
    }
}

function countManyTotal()
{
    var total = 0;
    var numprod = 0;
    var numdisc = 0;
    $('#many-products-form .cart-quantity-input').each(function()
    {
        var num = parseInt($(this).val());
        var price = $(this).data('price');
        var discount = $(this).data('discount');
        
        total += num*price;
        numprod += num;
        numdisc += discount*num;
    })
    console.log(numdisc);
    $('.all_price__items span').text(numprod);
    $('.all_price__price span').text(total);
    $('.all_price__bonus span').text(Math.ceil(numdisc));
}
 

function make_variants()
{
    $('.weight-control a').click(function(e)
    {
        e.preventDefault();
        var variant = $(this).data('variant');
        $('.weight-control a').removeClass('active');
        $(this).addClass('active');
        $('#selected_variant').val(variant);
    });
    
    $('#variant_quantity').change(function()
    {
        var num = parseInt($(this).val().replace(/\D+/g,""));
        $('#how_variant').val(num);
    });
    
    $('#buy_main').click(function()
    {
        var variant_id = $('#selected_variant').val();
        var how = $('#how_variant').val();
        $.post(
            "/ajax/buy",
            {
            variant_id: variant_id,
            amount: how,
            action: 'buy'
            },
            function(data)
            {
                show_basket_window(data);
            }
        );
    })
}

function buy_goods(variant_id, amount)
{
    $.post(
        "/ajax/buy",
        {
        variant_id: variant_id,
        amount: amount,
        action: 'buy'
        },
        function(data)
        {
            //show_basket_window(data);
            if(data != false)
            {
                SetAllBasketContent(data, true);
            }
        },
        'json'
    );
}


function show_basket_window(data)
{
    var htm = '<div class="return-pop-wrapper">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>Корзина</h2>\
                <div class="cart-items-wrapper clearfix">';
        for(var k in data.variants)
        {
        htm += '<div class="cart-item">\
                        <div class="image fl-left">\
                            <img src="/files/products/'+ data.variants[k].small_image +'" alt="">\
                        </div>\
                        <div class="cart-item-info">\
                            <div class="name">'+ data.variants[k].name +'</div>';
        /*if(data.variants[k].special)
        {
            htm +=          '<div class="bonus">Акция! '+ data.variants[k].special_text +'</div>';
        }*/
        htm +=              '<div class="price-count clearfix" data-variant="'+ data.variants[k].variant_id +'">\
                                <span class="price-wrap">'+ Math.ceil(data.variants[k].remission_price*data.variants[k].amount) +' руб.</span>\
                                &nbsp;&nbsp; <button class="btn btn-default bootstrap-touchspin-down" data-variant="'+ data.variants[k].variant_id +'">-</button>\
                                <span class="meas">'+ data.variants[k].amount +' шт.</span>\
                                <button class="btn btn-default bootstrap-touchspin-up" data-variant="'+ data.variants[k].variant_id +'">+</button>\
                            </div>\
                        </div>\
                    </div>';
        }
        htm += '</div>\
                <div class="more-text">\
                    Всего: <strong id="popup-total">'+ Math.ceil(data.total_products_price) +' руб.</strong>\
                </div>\
                <div class="buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="/cart">\
                        Оформить заказ\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'.return-pop-wrapper\').remove(); $(\'body\').css(\'overflow\', \'auto\'); return false;">\
                        сохранить товары и продолжить покупки\
                    </a>\
                </div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
    update_basket_top(data);
    
    $('.return-pop-wrapper .bootstrap-touchspin-down').click(function()
    {
        var variant_id = $(this).data('variant');
        var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
        elm.trigger("touchspin.downonce");
        if(elm.val() == 0)
        {
            $(this).closest('.cart-item').remove();
        }
        else
        {
            var el = $(this);
            
            var price = ($('#cart-drop-wrapper div[data-variant='+variant_id+']').data('price')*elm.val())+' руб.';
            el.parent().find('.price-wrap').text(price);
            
            el.parent().find('.meas').text(elm.val()+' шт.');   
        }     
    })
    
    $('.return-pop-wrapper .bootstrap-touchspin-up').click(function()
    {
        var variant_id = $(this).data('variant');
        var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
        elm.trigger("touchspin.uponce");
    })
    
    $('body').css('overflow', 'hidden');
}




function show_report_window(variant_id)
{
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>Сообщите, когда появится в продаже</h2>\
                <div class="form-report-wrapper clearfix">';
        htm += '<form id="report-form">\
                    <div class="form-group">\
                        <label>Имя <span class="req">*</span></label>\
                        <input id="us-request-name" type="text" name="name" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>E-mail <span class="req">*</span></label>\
                        <input id="us-request-email" type="text" name="email" value="" class="form-control" required="required" />\
                    </div>\
                    <input type="hidden" id="us-request-variant" value="'+variant_id+'" />\
                </form>';
        htm += '</div>\
                <div class="button-report-wrapper buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="#" onclick="sendUsRequest(); return false;">\
                        Сообщить\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'#report-form-block\').remove(); return false;">\
                        отмена\
                    </a>\
                </div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
}



function show_password_window()
{
    var htm = '<div class="return-pop-wrapper" id="report-password-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2 style="margin-bottom: 20px;">Смена пароля</h2>\
                <div class="form-password-wrapper clearfix">';
        htm += '<form id="password-form">\
                    <div class="form-group">\
                        <label>Старый пароль: <span class="req">*</span></label>\
                        <input id="us-request-op" type="password" name="old" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>Новый пароль: <span class="req">*</span></label>\
                        <input id="us-request-np" type="password" name="pass" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>Повторите новый пароль: <span class="req">*</span></label>\
                        <input id="us-request-np2" type="password" name="pass2" value="" class="form-control" required="required" />\
                    </div>\
                </form>';
        htm += '</div>\
                <div class="button-report-wrapper buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="#" onclick="sendChangePasswordRequest(); return false;">\
                        Сменить\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'#report-password-block\').remove(); return false;">\
                        отмена\
                    </a>\
                </div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
}

function sendChangePasswordRequest()
{
    $op = $("#us-request-op").val();
    $np = $("#us-request-np").val();
    $np2 = $("#us-request-np2").val();
    
    $.post( "/ajax/changepassword",
        {
            'old': $op,
            'new': $np,
            'new2': $np2
        },
        function( data ) {
            if(data == 'success') {
                $('.form-password-wrapper').html('Пароль успешно изменен!');
            } else if(data == 'error1') {
                $('.form-password-wrapper').html('Неправильный старый пароль!');
            } else if(data == 'error2') {
                $('.form-password-wrapper').html('Слишком короткий пароль! Длина должна быть не менее 6 символов.');
            } else if(data == 'error3') {
                $('.form-password-wrapper').html('Пароли не совпадают!');
            } else {
                $('.form-password-wrapper').html('Ошбика сохранения');
            }
            $('.button-report-wrapper').html('<a class="btn btn-primary fl-left" href="#" onclick="$(\'#report-password-block\').remove(); return false;">ОК</a>')
        },
        'html'
    );
}



function sendUsRequest(){
    var variant_id = $("#us-request-variant").val();
    var user_name = $("#us-request-name").val();
    var user_email = $("#us-request-email").val();
    if (user_name == ''){
        alert('Введите имя!');
    }
    else if (user_email == ''){
        alert('Введите email!');
    } else {
        $.post( "/ajax/updatestockrequest",
            {
                'variant_id': variant_id,
                'user_name': user_name,
                'user_email': user_email
            },
            function( data ) {
                if(data == 'success') {
                    $('.form-report-wrapper').html('Товар добавлен в Лист ожидания.<br> Интернет магазин "Зоошеф" постарается максимально быстро найти необходимые вам товары и сообщить вам об этом на E-mail.');
                } else if(data == 'info') {
                    $('.form-report-wrapper').html('Вы уже делали этот запрос');
                } else {
                    $('.form-report-wrapper').html('Ошбика сохранения');
                }
                $('.button-report-wrapper').html('<a class="btn btn-primary fl-left" href="#" onclick="$(\'#report-form-block\').remove(); return false;">ОК</a>')
            },
            'html'
        );

    }
}

function showPopup(text)
{
    var htm = '<div class="return-pop-wrapper">\
            <div class="return-pop-bg"></div>\
                <div class="return-pop-content">\
                    <h2>'+text+'</h2>\
            </div>\
        </div>';
    $(htm).appendTo('body');
}


function SetAllBasketContent(data, is_buy)
{
    if(typeof(is_buy) == 'undefined')
        is_buy = false;
    
    var htm_top = '';
    var htm_popup = '';
    var htm_page = '';
    var htm_popup_content = '';
    
    
    
    if(typeof(data.variants) != 'undefined')
    {
        //DEFINE VARIABLES
        var ord_shippingprice = data.shipping_price;
        var ord_discountsum = data.discount_sum;
        var ord_totaldiscount = data.total_remission_price;
        var ord_discountproc = data.discount;
        
        $('.main-header .cart-block').addClass('full-cart');
        $('#total_ammount').text(data.total_ammount);
        $('#total_products_price').text(data.total_products_price);
            
        //HTML TOP BASKET
        htm_top = '<div class="cart-items-wrapper clearfix" id="top-cart-items">';
        htm_top += '<div class="clearfix">';
        
        
        //HTML POPUP BASKET
        htm_popup = '<div class="return-pop-wrapper">\
            <div class="return-pop-bg"></div>\
                <div class="return-pop-content">\
                    <h2>Корзина</h2>\
                    <div class="cart-items-wrapper clearfix" id="popup-cart-items">';
                    
                    
        
        //HTML PAGE BASKET
        htm_page = '<h4>Заказ № 68374 — ожидает оплаты</h4>\
            <div class="cart-items-wrapper clearfix">';
            
        var i = 0;
        for(var ks in data.keysort)
        {
            var k = data.keysort[ks];
            
            //DEFINE VARIABLES
            var prod_image = '/files/products/' + data.variants[k].small_image;
            var prod_name = data.variants[k].name;
            var prod_sku = data.variants[k].sku;
            var prod_priceone = data.variants[k].price;
            var prod_priceall = (data.variants[k].price*data.variants[k].amount);
            var prod_variantid = data.variants[k].variant_id;
            var prod_amount = data.variants[k].amount;
            var prod_url = '/products/'+data.variants[k].url;
            
            
            //HTML TOP BASKET
            if(i >= 3)
            {
                i = 0;
                htm_top += '</div><div class="clearfix">'
            }
            
            htm_top += '<div class="cart-item" data-name="'+data.variants[k].model+'"\
                            data-id="'+data.variants[k].variant_id+'"\
                            data-brand="'+data.variants[k].brand+'"\
                            data-category="'+data.variants[k].cat_names+'"\
                            data-variant="'+data.variants[k].name+'"\
                            data-quantity="'+prod_amount+'"\
                            data-price="'+prod_priceone+'"\
                            >\
                        <div class="image fl-left">\
                            <img src="'+ prod_image +'" alt=""/>\
                        </div>\
                        <div class="cart-item-info">\
                            <div class="name">'+ prod_name +'</div>\
                            <div class="art">\
                                Артикул '+ prod_sku +'\
                            </div>\
                            <div class="price-count clearfix">\
                                <div class="price-value fl-right">\
                                    на <span class="price-text"><span class="price-value">'+ prod_priceall +'</span> руб.</span>\
                                </div>\
                                <div data-trigger="spinner" class="items-count fl-left" data-variant="'+ prod_variantid +'" data-price="'+ prod_priceone +'">\
                                    <input class="counter" name="count" type="text" value="'+ prod_amount +'" />\
                                </div>\
                            </div>\
                        </div>\
                    </div>';
            
            
            
            
            //HTML POPUP BASKET
            htm_popup_content += '<div class="cart-item">\
                        <div class="image fl-left">\
                            <img src="'+ prod_image +'" alt="">\
                        </div>\
                        <div class="cart-item-info">\
                            <div class="name">'+ prod_name +'</div>';
            /*if(data.variants[k].special)
            {
                htm_popup +=      '<div class="bonus">Акция! '+ data.variants[k].special_text +'</div>';
            }*/
            htm_popup_content +=    '<div class="price-count clearfix" data-variant="'+ prod_variantid +'">\
                                <span class="price-wrap">'+ prod_priceall +' руб.</span>\
                                &nbsp;&nbsp; <button class="btn btn-default bootstrap-touchspin-down" data-variant="'+ prod_variantid +'">-</button>\
                                <span class="meas">'+ prod_amount +' шт.</span>\
                                <button class="btn btn-default bootstrap-touchspin-up" data-variant="'+ prod_variantid +'">+</button>\
                            </div>\
                        </div>\
                    </div>';
            
            
            //HTML PAGE BASKET
            htm_page += '<div class="cart-item page-cart-item" id="page-cart-item-'+ prod_variantid +'" data-variant="'+ prod_variantid +'">\
                            <div class="image fl-left">\
                                <a href="'+ prod_url +'"><img src="'+ prod_image +'" alt=""/></a>\
                            </div>\
                            <div class="cart-item-info">\
                                <div class="name"><a href="'+ prod_url +'">'+ prod_name +'</a></div>\
                                <div class="art">\
                                    Артикул '+ prod_sku +'\
                                </div>\
                                <div class="price-count clearfix">\
                                    <div class="price-value fl-right">\
                                        на <span class="price-text"><span class="price-value">'+ prod_priceall +'</span> руб.</span>\
                                    </div>\
                                    <div data-trigger="spinner" class="items-count fl-left" data-variant="'+ prod_variantid +'" data-price="'+ prod_priceone +'">\
                                        <input class="counter" name="count" type="text" value="'+ prod_amount +'" />\
                                    </div>\
                                </div>\
                            </div>\
                        </div>';
            
            i = i+1;
        }
        
        //HTML TOP BASKET
        htm_top += '</div>';
        htm_top += '</div><div class="button-block">\
            <div id="discount-block-container">Ваша скидка: <strong>'+ord_discountsum+'</strong> руб.</div>\
            <a class="btn btn-primary" href="/cart">Оформить заказ</a></div><div id="cart-drop-fade">\
            </div>';
        
        
        
        //HTML POPUP BASKET
        htm_popup += '</div>\
                <div class="more-text">\
                    Всего: <strong><span id="popup-total">'+ Math.ceil(data.total_products_price) +'</span> руб.</strong>\
                    <div id="popup-discount-container"><br /><br />Ваша скидка: <strong><span id="popup-discount">'+ Math.ceil(data.discount_sum) +'</span> руб.</strong></div>\
                </div>\
                <div class="buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="/cart">\
                        Оформить заказ\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'.return-pop-wrapper\').remove(); $(\'body\').css(\'overflow\', \'auto\'); return false;">\
                        сохранить товары и продолжить покупки\
                    </a>\
                </div>\
            </div>\
        </div>';
        
    
        //HTML PAGE BASKET
        htm_page += '</div>\
            <div class="delivery">\
                Доставка: <strong><span id="delivery-price">'+ord_shippingprice+'</span> руб.</strong>\
            </div>\
            <div class="discount">\
                <strong>Ваша скидка: '+ord_discountproc+'%, <span id="discount-price">'+ord_discountsum+'</span> руб.</strong>\
            </div>\
            <div class="total-sum">\
                Итого со скидкой <span id="cart-all-sum">'+ord_totaldiscount+'</span> руб.\
            </div>\
            <div id="cart-page-drop-fade"></div>';
    
        
        $('#cart-drop-wrapper').html(htm_top);
        if(is_buy && $('.return-pop-wrapper').length == 0)
        {
            $(htm_popup).appendTo('body');
        }
        
        $('#popup-cart-items').html(htm_popup_content);
        $('#popup-total').text(data.total_products_price);
        $('#popup-discount').text(data.discount_sum);
        
        $('#cart-block-product').html(htm_page);
        make_top_spinner();
        make_spinner();
        
        if(ord_discountsum == 0)
        {
            $('#discount-block-container').hide();
            $('#popup-discount-container').hide();
        }
    }
    else
    {
        $('.main-header .cart-block').removeClass('full-cart');
        $('#cart-drop-wrapper').html('<div class="empty-text">Ваша корзина пуста.</div>')
        $('#total_ammount').text(0);
        $('#total_products_price').text(0);
        
        if(typeof(basket_page != 'undefined'))
        {
            window.location.reload();
        }
    }
}

function update_basket_top(data)
{
    if(typeof(data.variants) != 'undefined' && data.variants.length > 0)
    {
        $('.main-header .cart-block').addClass('full-cart');
    }
    else
    {
        $('.main-header .cart-block').removeClass('full-cart');
        $('#cart-drop-wrapper').html('<div class="empty-text">Ваша корзина пуста.</div>')
        $('#total_ammount').text(0);
        $('#total_products_price').text(0);
    }
    
    
    
    if(typeof(data.variants) != 'undefined' && data.variants.length > 0)
    {
    $('#total_ammount').text(data.total_amount);
    $('#total_products_price').text(data.total_discount);
    var i = 0;
    
    var htm = '<div class="cart-items-wrapper clearfix" id="top-cart-items">';
    
    htm += '<div class="clearfix">';
    for(var k in data.variants)
    {
        if(i >= 3)
        {
            i = 0;
            htm += '</div><div class="clearfix">'
        }
        htm += '<div class="cart-item">\
                    <div class="image fl-left">\
                        <img src="/files/products/'+ data.variants[k].small_image +'" alt=""/>\
                    </div>\
                    <div class="cart-item-info">\
                        <div class="name">'+ data.variants[k].name +'</div>\
                        <div class="art">\
                            Артикул '+ data.variants[k].sku +'\
                        </div>\
                        <div class="price-count clearfix">\
                            <div class="price-value fl-right">\
                                на <span class="price-text"><span class="price-value">'+ (data.variants[k].discount_price*data.variants[k].amount) +'</span> руб.</span>\
                            </div>\
                            <div data-trigger="spinner" class="items-count fl-left" data-variant="'+ data.variants[k].variant_id +'" data-price="'+ data.variants[k].price +'">\
                                <input class="counter" name="count" type="text" value="'+ data.variants[k].amount +'" />\
                            </div>\
                        </div>\
                    </div>\
                </div>';
        i = i+1;
    }
    htm += '</div>';
    htm += '</div><div class="button-block"><a class="btn btn-primary" href="/cart">Оформить заказ</a></div><div id="cart-drop-fade"></div>';
    $('#cart-drop-wrapper').html(htm);
    
    make_top_spinner();
    }
}

var priceSpinnerTop;
var priceSpinner;

function make_spinner()
{
        priceSpinner = $('.items-count .counter').TouchSpin({
            min: 0,
            max: 99,
            step: 1,
            initval: 1,
            postfix: 'шт.'
        });
        priceSpinner.change(function(){
            if(!$(this).parent().parent().data('disable'))
            {
                var par = $(this).parent().parent();
                var cloest = $(this).closest('.cart-item');
                var thisValue = $(this).val();
                var variant_id = par.data('variant');
                var price = par.data('price');
                var el = $(this).closest('.cart-item');
                var inputel = $(this);
                
                var maskElem = '<span class="counter-value"></span>';
    
                if(!$(this).parent().find('.counter-value').length){
                    $(this).before(maskElem);
                }
    
                /*if(thisValue > 9){
                    $(this).addClass('hidden');
                    $(this).parent().find('.counter-value').html(thisValue);
                };*/
                
                $('#cart-drop-fade').show();
                $('#cart-page-drop-fade').show();
                
                $.get(
                    "/ajax/adddec/"+variant_id+'/'+thisValue,
                    {},
                    function(data)
                    {
                        SetAllBasketContent(data);
                    },
                    'json'
                );
            }
            else
            {
                var par = $(this).parent().parent()
                var price = par.data('price');
                var thisValue = $(this).val();
                par.parent().find('span.price-value').text(price*thisValue);
                par.parent().find('.buy-button2').data('amount', thisValue);
            }
        });     
        
    priceSpinner.on('touchspin.on.stopdownspin', function()
    {
        var variant_id = $(this).closest('[data-variant]').data('variant');
        
        var bt = $('#cart-drop-wrapper .cart-item[data-variant='+variant_id+']')
        
        dataLayer.push({
        	"event": "removeFromCart",
        	"ecommerce": {
        		"remove": {
        			"products": [{
        				"name": bt.data('name'),
        				"id": bt.data('sku'),
        				"price": bt.data('price'),
        				"brand": bt.data('brand'),
        				"category": bt.data('category'),
        				"variant": bt.data('variant'),
        				"quantity": bt.data('quantity')
        			}]
        		}
        	}
        });
        console.log('rm')
        console.log(dataLayer)
    })
    
    priceSpinner.on('touchspin.on.stopupspin', function()
    {
        var variant_id = $(this).closest('[data-variant]').data('variant');
        
        var bt = $('#cart-drop-wrapper .cart-item[data-variant='+variant_id+']')
        
        dataLayer.push({
        	"event": "addToCart",
        	"ecommerce": {
        		"add": {
        			"products": [{
        				"name": bt.data('name'),
        				"id": bt.data('sku'),
        				"price": bt.data('price'),
        				"brand": bt.data('brand'),
        				"category": bt.data('category'),
        				"variant": bt.data('variant'),
        				"quantity": bt.data('quantity')
        			}]
        		}
        	}
        });
        
        dataLayer.push({
        	'event': 'addToCart',
        	'google_tag_params': {
        		'ecomm_prodid': [bt.data('sku')],
        		'ecomm_pagetype': 'cart',
        		'ecomm_totalvalue': bt.data('price')
        	}
        });

    })
}

function make_top_spinner()
{
    priceSpinnerTop = $('#top-cart-items .items-count .counter').TouchSpin({
            min: 0,
            max: 99,
            step: 1,
            initval: 1,
            postfix: 'шт.'
        });
        
    priceSpinnerTop.change(function(ee){
        if(!$(this).parent().parent().data('disable'))
        {
            var par = $(this).parent().parent();
            var thisValue = $(this).val();
            var variant_id = par.data('variant');
            var price = par.data('price');
            
            var el = $(this).closest('.cart-item');
            var inputel = $(this);
            
            var maskElem = '<span class="counter-value"></span>';

            if(!$(this).parent().find('.counter-value').length){
                $(this).before(maskElem);
            }

            if(thisValue > 9){
                $(this).addClass('hidden');
                $(this).parent().find('.counter-value').html(thisValue);
            };
            
            $('#cart-drop-fade').show();
            
            $.get(
                "/ajax/adddec/"+variant_id+'/'+thisValue,
                {},
                function(data)
                {  
                    //var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
                    //elm.val(data.added).trigger("touchspin.change");
                    //console.log(elm)
                    /*console.log(data)
                    console.log('data')
                    if(data.added == 0)
                    {
                        el.remove();
                        
                        if($('.cart-drop .cart-item').length == 0)
                        {
                            $('.main-header .cart-block').removeClass('full-cart');
                            $('.return-pop-wrapper').remove();
                            $('body').css('overflow', 'auto');
                            $('#cart-drop-wrapper').html('<div class="empty-text">Ваша корзина пуста.</div>')
                        }
                    }
                    else
                    {
                        inputel.val(data.added);                         
                        par.parent().find('span.price-value').text(data.variants.variants.remission_price*data.added);
                    }
                    
                    $('#total_products_price').text(Math.ceil(data.variants.total_products_price));
                    $('#total_ammount').text(data.variants.total_ammount);
                    $('#cart-drop-fade').hide();
                    update_basket_top(data.variants);
                    
                    $('#popup-total').text(Math.ceil(data.variants.total_products_price)+' руб.');
                    
                    $('.return-pop-wrapper .price-count[data-variant='+variant_id+']').find('.price-wrap').text(price*data.added);
                    
                    $('.return-pop-wrapper .price-count[data-variant='+variant_id+']').find('.meas').text(data.added+' шт.');
                    */
                    SetAllBasketContent(data);
                },
                'json'
            );
        }
        else
        {
            var par = $(this).parent().parent()
            var price = par.data('price');
            var thisValue = $(this).val();
            par.parent().find('span.price-value').text(price*thisValue);
            par.parent().find('.buy-button2').data('amount', thisValue);
        }
    });
    
    priceSpinnerTop.on('touchspin.on.stopdownspin', function()
    {
        var variant_id = $(this).closest('[data-variant]').data('variant');
        
        var bt = $('#cart-drop-wrapper .cart-item[data-variant='+variant_id+']')
        
        dataLayer.push({
        	"event": "removeFromCart",
        	"ecommerce": {
        		"remove": {
        			"products": [{
        				"name": bt.data('name'),
        				"id": bt.data('sku'),
        				"price": bt.data('price'),
        				"brand": bt.data('brand'),
        				"category": bt.data('category'),
        				"variant": bt.data('variant'),
        				"quantity": bt.data('quantity')
        			}]
        		}
        	}
        });
    })
    
    priceSpinnerTop.on('touchspin.on.stopupspin', function()
    {
        var variant_id = $(this).closest('[data-variant]').data('variant');
        
        var bt = $('#cart-drop-wrapper .cart-item[data-variant='+variant_id+']')
        
        dataLayer.push({
        	"event": "addToCart",
        	"ecommerce": {
        		"add": {
        			"products": [{
        				"name": bt.data('name'),
        				"id": bt.data('sku'),
        				"price": bt.data('price'),
        				"brand": bt.data('brand'),
        				"category": bt.data('category'),
        				"variant": bt.data('variant'),
        				"quantity": bt.data('quantity')
        			}]
        		}
        	}
        });
    })
}



function insert_basket_window(data)
{
    var htm = '<div class="clearfix">';
    var i = 0;
    for(var k in data.variants)
    {
        if(i >= 3)
        {
            i = 0;
            htm += '</div><div class="clearfix">'
        }
        htm += '<div class="cart-item">\
                    <div class="image fl-left">\
                        <img src="/files/products/'+ data.variants[k].small_image +'" alt=""/>\
                    </div>\
                    <div class="cart-item-info">\
                        <div class="name">'+ data.variants[k].name +'</div>\
                        <div class="art">\
                            Артикул '+ data.variants[k].sku +'\
                        </div>\
                        <div class="price-count clearfix">\
                            <div class="price-value fl-right">\
                                на <span class="price-text"><span class="price-value">'+ data.variants[k].price +'</span> руб.</span>\
                            </div>\
                            <div data-trigger="spinner" class="items-count fl-left" data-variant="'+ data.variants[k].variant_id +'">\
                                <input class="counter" name="count" type="text" value="'+ data.variants[k].amount +'" />\
                            </div>\
                        </div>\
                    </div>\
                </div>';
        i = i+1;
    }
    htm += '</div>'
    $('.cart-items-wrapper').html(htm);
}

function repeat_order(id)
{
    var order_id = id;
    
    $.post(
        "/ajax/buy",
        {
        order_id: order_id,
        action: 'buy'
        },
        function(data)
        {
            SetAllBasketContent(data, true);
        },
        'json'
    );
}



jQuery(function(){
  jQuery.fn.scrollToTop=function(){
    jQuery(this).hide().removeAttr("href");
    if(jQuery(window).scrollTop()!="0"){
        jQuery(this).fadeIn("slow")
  }
  var scrollDiv=jQuery(this);
  jQuery(window).scroll(function(){
    if(jQuery(window).scrollTop()=="0"){
    jQuery(scrollDiv).fadeOut("slow")
    }else{
    jQuery(scrollDiv).fadeIn("slow")
  }
  });
    jQuery(this).click(function(){
      jQuery("html, body").animate({scrollTop:0},"slow")
    })
  }
});


function submitFilter()
{
    jQuery('#input_page').val(1);
    
    var url = $('#current_category_url').val();
    var par = [];
    
    url += '/s-';
    if($('#form_filter .filter-param').length > 0)
    {
        $('#form_filter .filter-param').each(function()
        {
            
            par = [];
            
            $('.'+$(this).data('for')+':checked').each(function()
            {
                par.push($(this).val());
            })
            
            if(par.length > 0)
            {
                url += $(this).val()+'-is-';
                url += par.join('-and-')+'/';
            }
        })
    }
    
    if($('#submit-weight-input').val() == 1)
        url += 'weight_from_'+$('#filter-weight-min').val()+'_to_'+$('#filter-weight-max').val()+'/';
        
    if($('#submit-price-input').val() == 1)
        url += 'price_from_'+$('#price-min').val()+'_to_'+$('#price-max').val()+'/';
    
    var getparams = '?';
    
    if($('#filter_novelty').length > 0 && $('#filter_novelty').val() == 1)
    {
        getparams += 'novelty=1&';
    }
    getparams += 'page='+$('#input_page').val();
    
    
    url += getparams;
    
    document.location.href = url;
    jQuery('#ajax_load_articles').show();
}


function burgersMenu() {
	if ($(window).width() < 1281) {
		//$('.menu-holder .menu-text').each(function () {
        //$(this).replaceWith('<span class="menu-text">' + $(this).text() + '</span>');
		//});

		$('.site-menu .menu-holder').on('click', function()		{
			//$('.main-menu').removeClass('isActive');
			//$('.main-menu').addClass('isActive');

		});
			$('.menu_mobile_select').click(function(event){
				event.preventDefault();
				event.stopPropagation();
			});

		$(document).on('click', '.main-menu .menu-holder', function(e)
        {

          if($(this).hasClass('isActive')){
		        $(this).removeClass('isActive');
		  }
		  else {
                    $('.menu-holder').removeClass('isActive').find('.menu-drop a').css("pointer-events", "none");
                    $('.menu-holder .menu-drop a').css("pointer-events", "none");
                    $(this).addClass('isActive');
          }
			function func(that) {
			  that.find('.menu-drop a').css("pointer-events","all");
			  console.log('in');
			}

			setTimeout(func($(this)), 2000);
			$('html, body').animate({ scrollTop: $(this).offset().top }, 500);
		});
		
	} else{
        if ($(window).width() > 1000) {
            $('.site-menu .menu-holder').on('mouseenter', function()
            {
                //$('.main-menu').addClass('shadowed');
                //$('.slider-wrapper').css('opacity', '0.2');
            });

            $('.site-menu .menu-holder').on('mouseleave', function()
            {
                //$('.main-menu').removeClass('shadowed');
                //$('.slider-wrapper').css('opacity', '1');
            });
        }

	}
}

$( window ).resize(function() {
  //burgersMenu();
  resize_ks_filters();
});

function resize_ks_filters() {
    var intViewportWidth = window.innerWidth;
    var form_filter = $('#form_filter');
    if (intViewportWidth <= 1000) {
        form_filter.addClass('expandable');
    } else {
        form_filter.removeClass('expandable');
    }
}

function wrap_ks_filters() {
    var form_filter = $('#form_filter');
    if (form_filter.length > 0) {
        var i = 0;
        form_filter.find('.zag4').each(function() {
            var _this = $(this);
            if (_this.text() != 'Подобрать' &&
                _this.text() != 'Упаковка, кг' &&
                _this.text() != 'Цена, руб') {
                $(this).wrap('<div class="ks_filters_wrap"></div>');
            }
            i++;
        });
        form_filter.find('.ks_filters_wrap').each(function() {
            var _this = $(this);
            _this.append(_this.next());
        });

        $('.ks_filters_wrap .zag4').on('click', function() {
            var _this = $(this).parent();
            if (form_filter.hasClass('expandable')) {
                _this.find('.zag4').next().toggleClass('expanded').slideToggle();
                _this.toggleClass('expanded')
            }
        });
    }
}

jQuery(document).ready(function()
{
    wrap_ks_filters();

	burgersMenu();
    
    $('.burgers').on('click', function()
	{
		$('.main-menu .site-menu').toggle();
	});
    
    
    
    var nice = false;
    $(document).ready(function() {  
        nice = $("#cart-drop-wrapper").niceScroll({horizrailenabled: false, cursorwidth:"8px"});
    });
    
    
    $('#form_filter_articles input').on('change', function(){
        $('#form_filter_articles').submit();
    });
    
    jQuery('#ajax_load_articles').click(function()
    {
        jQuery('#input_page').val(((jQuery('#input_page').val()*1)+1));
        var data = jQuery('#form_filter_articles').serialize();
        $.ajax({
            url: '/articles?'+data+'&ajax=true',
            type: "GET",
            success: function(data){
                if(data.length > 0)
                {
                    var htm = '';
                    for(var k in data)
                    {
                        htm += '<div class="related-article clearfix">\
                            <div class="image fl-left"><img src="'+data[k].image+'" alt=""/></div>\
                            <div class="article-content">\
                                <div class="prod-link-container">\
                                <a class="name" href="/articles/'+data[k].url+'">'+data[k].name+'</a>\
                                </div>\
                                <p>'+data[k].annotation+'</p>\
                                <div class="author-name">\
                                    '+data[k].date_add+', '+data[k].author+'\
                                </div>\
                            </div>\
                        </div>'
                    }
                    jQuery('#articles_container').append(htm);
                }
                else
                {
                    jQuery('#ajax_load_articles').hide();
                }
            },
            dataType: "json"
        });
    });
    
    jQuery('#ajax_load_actions').click(function()
    {
        jQuery('#input_page').val(((jQuery('#input_page').val()*1)+1));
        var data = jQuery('#form_actions_filter').serialize();
        $.ajax({
            url: '/actions',
            data: data+'&ajax=true',
            success: function(data){
                if(data.length > 0)
                {
                    var htm = '';
                    for(var k in data)
                    {
                        htm += '<div class="product-action-item">\
                                    <div class="name">'+data[k].name+'</div>\
                                    <div class="image-block">\
                                        <img src="/files/actions/'+data[k].image+'" alt=""/>\
                                    </div>\
                                    <div class="description">'+data[k].annotation+'</div>\
                                    <a class="btn btn-primary" href="/actions/'+data[k].url+'">\
                                        Купить сейчас\
                                    </a>\
                                </div>';
                    }
                    jQuery('#actions_container').append(htm);
                }
                else
                {
                    jQuery('#ajax_load_actions').hide();
                }
            },
            dataType: "json"
        });
    });
    
    $('#form_actions_filter input').on('change', function(){
        $('#form_actions_filter').submit();
    });
    
    
    jQuery('#ajax_load_products').click(function()
    {   var page_number = location.href.split('=');
        if(!page_number[1]){jQuery('#input_page').val(((jQuery('#input_page').val()*1)+1));}
        else{
            jQuery('#input_page').val(((page_number[1])*1)+1);
            new_page= (page_number[1]*1)+1;
            console.log(page_number[0],page_number[1],new_page);
            history.replaceState('', '', ''+page_number[0]+'='+new_page+'');
    }

        var dataform = jQuery('#form_filter').serialize();
        $('#prod-preloader').show();
        
        $.ajax({
            url: window.location.href.split('?')[0],
            data: dataform+'&ajax=true',
            success: function(data){
                jQuery('#products-container').append(data);
                    make_select();
                    $('.pagenator .page[data-pagenumber='+jQuery('#input_page').val()+']').addClass('active');
                    
                if( jQuery('#input_page').val() == jQuery('.pagenator>a.page:last-child').attr('data-pagenumber')){jQuery('#ajax_load_products').hide();}
                $('#prod-preloader').hide();
                return;
                    
                    
                var ct = 0;
                var cls;
                var slc;
                var cprod = 0;
                if(data.length > 0)
                {
                    var ids = [];
                    var htm = '<div class="prod-cat-line clearfix" style="float: left;">';
                    for(var k in data)
                    {
                        if(cprod == products_columns)
                        {
                            cprod = 0;
                            htm += '</div><div class="prod-cat-line clearfix" style="float: left;">';
                        }
                        htm += '<div class="product-wrapper" id="product-'+data[k].product_id+'">\
                                <form id="product-form-'+data[k].product_id+'">\
                                <a class="product-link" href="'+data[k].url+'">';
                        if(data[k].special != 0)
                        {
                            htm += '<span class="stripe action-text">акция</span>';
                        }
                        else if(data[k].novelty != 0)
                        {
                            htm += '<span class="stripe new-text">новинка</span>';
                        }
                        htm += '<div class="image-container">\
                                        <img src="/files/products/'+data[k].small_image+'" alt=""/>\
                                    </div>\
                                    <span class="link-name">'+data[k].model+'</span>\
                                </a>';
                        if(data[k].weight_select != 0) {
                        htm += '<div class="options_weight">';
                            for(var kv in data[k].variants)
                            {
                                cls = '';
                                if(ct == 0)
                                {
                                    cls = ' active ';
                                }
                                if(ct >= 3)
                                {
                                    ct = 0;
                                    break;
                                }
                                htm += '<div data-variant="'+data[k].variants[kv].variant_id+'" data-product="'+data[k].product_id+'" class="one-v '+cls+'">'+(Math.round(data[k].variants[kv].weight * 100) / 100)+' кг</div>';
                                ct++;
                            }
                            ct = 0;
                            htm += '<div>';
                            cls = '';
                            if(data[k].variants.length-3 <= 0)
                            {
                                cls = ' style="display: none;" class="no_active"';
                            }
                            htm += '<a href="'+data[k].url+'" '+cls+'>еще '+(data[k].variants.length-3)+'</a>';
                            htm += '</div>';
                        htm += '</div>';
                        }
                        htm += '<div class="switch-items">\
                                    <button data-product="'+data[k].product_id+'" class="btn btn-primary fl-right buy-button" type="button">Купить</button>';
                            for(var kv in data[k].variants) {
                                cls = '';
                                if(ct == 0)
                                {
                                    cls = ' active';
                                }
                               htm += '<div data-variant="'+data[k].variants[kv].variant_id+'" data-product="'+data[k].product_id+'" class="sum'+cls+'">'+(Math.round(data[k].variants[kv].price * 100) / 100)+' руб.</div>';
                               ct++;
                            }
                            ct = 0;
                        htm += '</div>\
                                <div class="extra-info">';
                        if(data[k].variant_select != 0) {
                            var vname = (typeof(data[k].variants[0]) != 'undefined') ? data[k].variants[kv].name : '';
                             htm += '<div class="custom-select">\
                                        <div class="select-mask">'+vname+'</div>\
                                        <div class="custom-select-content"></div>\
                                        <select class="custom-select-hidden variant-select cat-ch-variant" name="" id="">';
                                for(var kv in data[k].variants) {
                                    htm += '<option data-variant="'+data[k].variants[kv].variant_id+'" data-product="'+data[k].product_id+'" data-city="'+data[k].variants[kv].name+'" value="'+data[k].variants[kv].variant_id+'">'+data[k].variants[kv].name+'</option>';
                                }
                                 htm += '</select>\
                                    </div>';
                        }
                        if(data[k].color_select != 0)
                        {
                             htm += '<div class="color-picker-wrapper clearfix">\
                                        <div class="no-bowl fl-right"></div>';
                                for(var kv in data[k].variants) {
                                    slc = '';
                                    cls = '';
                                    if(ct == 0)
                                    {
                                        cls = ' active';
                                        slc = ' selected';
                                    }
                                    htm += '<div data-variant="'+data[k].variants[kv].variant_id+'" data-product="'+data[k].product_id+'" class="color-wrapper cat-ch-color '+cls+'" style="background: '+data[k].variants[kv].color+';">\
                                                <input '+slc+' type="radio" name="color" />\
                                            </div>';
                                }
                             htm += '</div>';
                        }
                            htm += '<div class="reviews-marks clearfix">\
                                        <a href="'+data[k].url+'" class="review-link fl-right">\
                                            Отзывы '+data[k].rate_count+'\
                                        </a>\
                                        <div class="rating">\
                                            <div id="rating-product-'+data[k].product_id+'"></div>\
                                        </div>\
                                    </div>\
                                    <div class="consist-text">\
                                        Производитель: <strong class="country">'+data[k].brand_name+'</strong>\
                                    </div>\
                                </div>';
                                if(typeof(data[k].variants[0]) != 'undefined')
                                {
                                    htm += '<input type="hidden" name="variant_id" class="input-variant-id" value="'+ data[k].variants[0].variant_id +'" />';
                                }
                                
                            htm += '</form>\
                                <script>\
                                    jQuery(\'#rating-product-'+data[k].product_id+'\').jRate({\
                                        count: 5,\
                                        rating: '+data[k].rate+',\
                                        precision: 1,\
                                        startColor: \'#F88B1A\',\
                                		endColor: \'#F88B1A\',\
                                        readOnly: true,\
                                        width: 17,\
                                        height: 17\
                                    });\
                                </script>\
                            </div>';
                            cprod++;
                    }
                    htm += '</div>';
                    jQuery('#products-container').append(htm);
                    make_select();
                    $('.pagenator .page[data-pagenumber='+jQuery('#input_page').val()+']').addClass('active');
                }
                else
                {
                    jQuery('#ajax_load_products').hide();
                }
                if( jQuery('#input_page').val() == jQuery('.pagenator>a.page:last-child').attr('data-pagenumber')){jQuery('#ajax_load_products').hide();}
                $('#prod-preloader').hide();
            }
        });
    })
        
    $('#form_filter input').on('change', function(){
        submitFilter();
    });
    
    $('#submit-weight').click(function()
    {
        $('#submit-weight-input').val(1);
        submitFilter();
    })
    
    $('#submit-price').click(function()
    {
        $('#submit-price-input').val(1);
        submitFilter();
    })
    
    
    
    $(document).on('click', '.cat-ch-weight', function(){
        var prod_id = $(this).data('product');
        var variant_id = $(this).data('variant');
        var stock = $(this).data('stock');
        var prod_sel = $(this).closest('#product-'+prod_id);
        //prod_sel.find('.cat-ch-color[data-variant='+variant_id+']').click();
        
        prod_sel.find('.input-variant-id').val(variant_id);
        
        if(!$(this).hasClass('active'))
        {
            var holder = $(this).parent();
            var activeTab = $(this).index() - 1;

            holder.find('.cat-ch-weight').removeClass('active');
            holder.next().find('.sum').removeClass('active');
            $(this).addClass('active');
            
            var sumsel = holder.next().find('.sum[data-variant='+variant_id+']')
            sumsel.addClass('active');
            
            if(stock == 0)
            {
                prod_sel.find('.buy-button').hide();
                prod_sel.find('.button-order').show();
            }
            else
            {
                prod_sel.find('.buy-button').show();
                prod_sel.find('.button-order').hide();
            }
        }
    })
    
    $(document).on('click', '.cat-ch-color', function(){
        var prod_id = $(this).data('product');
        var variant_id = $(this).data('variant');
        var stock = $(this).data('stock');
        var prod_sel = $(this).closest('#product-'+prod_id);
        
        prod_sel.find('.variant-select').val(variant_id);
        prod_sel.find('.cat-ch-weight[data-variant='+variant_id+']').click();
        
        prod_sel.find('.input-variant-id').val(variant_id);
        prod_sel.find('.price-value').removeClass('active');
        prod_sel.find('.price-value[data-variant='+variant_id+']').addClass('active');
        prod_sel.find('.product-sku').removeClass('active');
        prod_sel.find('.product-sku[data-variant='+variant_id+']').addClass('active');
        
        if(stock == 0)
        {
            prod_sel.find('.buy-button').hide();
            prod_sel.find('.button-order').show();
        }
        else
        {
            prod_sel.find('.buy-button').show();
            prod_sel.find('.button-order').hide();
        }
    })
    
    $(document).on('click', '.prod-ch-color', function()
    {
        var prod_id = $(this).data('product');
        var variant_id = $(this).data('variant');
        var stock = $(this).data('stock');
        var prod_sel = $(this).closest('#product-'+prod_id);
        
        prod_sel.find('.input-variant-id').val(variant_id);
        prod_sel.find('.price-value').removeClass('active');
        prod_sel.find('.price-value[data-variant='+variant_id+']').addClass('active');
        prod_sel.find('.product-sku').removeClass('active');
        prod_sel.find('.product-sku[data-variant='+variant_id+']').addClass('active');
        if(stock == 0)
        {
            prod_sel.find('.buy-button').hide();
            prod_sel.find('.button-order').show();
        }
        else
        {
            prod_sel.find('.buy-button').show();
            prod_sel.find('.button-order').hide();
        }
        
    })
    
    $(document).on('click', '.prod-ch-weight', function(e)
    {
        e.preventDefault();
        
        $('.prod-ch-weight').removeClass('active');
        $(this).addClass('active');
        
        var prod_id = $(this).data('product');
        var variant_id = $(this).data('variant');
        var prod_sel = $('#product-'+prod_id);
        
        prod_sel.find('.input-variant-id').val(variant_id);
        prod_sel.find('.price-value').removeClass('active');
        prod_sel.find('.price-value[data-variant='+variant_id+']').addClass('active');
        prod_sel.find('.product-sku').removeClass('active');
        prod_sel.find('.product-sku[data-variant='+variant_id+']').addClass('active');
    })
    
    $(document).on('mouseup touchend', '.prod-ch-variant', function(e)
    {
        var prod_id = $(this).data('product');
        var variant_id = $(this).data('variant');
        var stock = $(this).data('stock');
        var prod_sel = $(this).closest('#product-'+prod_id);
        
        $('#variant_quantity-'+prod_id).val('1 шт.');
        
        prod_sel.find('.input-variant-id').val(variant_id);
        prod_sel.find('.price-value').removeClass('active');
        prod_sel.find('.price-value[data-variant='+variant_id+']').addClass('active');
        prod_sel.find('.product-sku').removeClass('active');
        prod_sel.find('.product-sku[data-variant='+variant_id+']').addClass('active');
        
        if(stock > 0)
        {
            prod_sel.find('.stock-in').show();
            prod_sel.find('.stock-out').hide();
            prod_sel.find('.buy-button').show();
            prod_sel.find('.button-order').hide();
        }
        else
        {
            prod_sel.find('.stock-in').hide();
            prod_sel.find('.stock-out').show();
            prod_sel.find('.buy-button').hide();
            prod_sel.find('.button-order').show();
        }
    })
    
	$(document).on('click', '.prod-ch-variant', function(e)
    {
		 e.preventDefault();
		$('.prod-ch-variant').removeClass('active');
		$('.prod-ch-variant').removeClass('isCheck');
		$(this).addClass('isCheck');
	});
    
    $(document).on('click', '.buy-button', function(){
        if($('#many-products-form').length > 0)
        {
            var is_added = false;
            
            $('#many-products-form .cart-quantity-input').each(function()
            {
                var num = parseInt($(this).val());
                if(num > 0)
                {
                    is_added = true;
                }
            })
            
            if(!is_added)
            {
                return false;
            }
            
            var dataform = $('#many-products-form').serialize();
            $.post(
                "/ajax/buy",
                dataform,
                function(data)
                {
                    //show_basket_window(data);
                    if(data != false)
                    {
                        console.log(data)
                        SetAllBasketContent(data, true);
                    }
                },
                'json'
            );
        }
        else
        {
            var prod_id = $(this).data('product');
            var variant_id = $('#product-'+prod_id).find('.input-variant-id').val();
            var amount = ($('#variant_quantity-'+prod_id).length > 0) ? parseInt($('#variant_quantity-'+prod_id).val()) : 1;
            
            var bt = $(this);
            
            dataLayer.push({
            	"event": "addToCart",
            	"ecommerce": {
            		"add": {
            			"products": [{
            				"name": bt.data('name'),
            				"id": bt.data('sku'),
            				"price": bt.data('price'),
            				"brand": bt.data('brand'),
            				"category": bt.data('category'),
            				"variant": bt.data('variant'),
            				"quantity": bt.data('quantity')
            			}]
            		}
            	}
            });
            
            buy_goods(variant_id, amount);
        }
    })
    
    $(document).on('click', '.buy-button2', function(e){
        e.preventDefault();
        var variant_id = $(this).data('variant');
        var amount = $(this).data('amount');

        
        buy_goods(variant_id, amount);
    })
    
    
    $(document).on('click', '.button-order', function(){
        var prod_id = $(this).data('product');
        var variant_id = $('#product-'+prod_id).find('.input-variant-id').val();
        
        show_report_window(variant_id);
    });
    
    $('#toggleAddBasketInfo').click(function(e)
    {
        e.preventDefault();
        var sel = $(this).attr('data-sel');
        if(sel == 'hide')
        {
            $(this).html('Не хочу заполнять формы, просто позвоните мне');
            $(this).attr('data-sel','show');
            $('#login-table-hide').show();
        }
        else
        {
            $(this).html('Хочу указать адрес доставки и выбрать способ оплаты');
            $(this).attr('data-sel','hide');
            $('#login-table-hide').hide();
        }
    })
    
    $("#jRate").jRate({
        count: 5,
        rating: 5,
        precision: 1,
        onChange: function(rating) {
            $('#rating_value').val(rating);
        },
        startColor: '#F88B1A',
		endColor: '#F88B1A'
    });
    
    jQuery('#product-anchor-links a').click(function(e) {
        e.preventDefault();
        if(jQuery(jQuery(this).attr('href')))
        {
            var destination = (jQuery(jQuery(this).attr('href')).offset().top) - 120;
    		jQuery("html,body").animate({scrollTop: destination}, 1000);
        }
    });
    
    $("#totop").scrollToTop();
    
    $('.product-main-image').click(function()
    {
        $('.product-images-gallery .images-list li a').eq(0).click();
    })
    
    
    var elj = jQuery('.product-images-gallery .jcarousel')
    elj.jcarousel({ });
    elj.parent().find('.jcarousel-control-prev').click(function(e) {
        e.preventDefault();
        elj.jcarousel('scroll', '-=1');
    });
    
    
    elj.parent().find('.jcarousel-control-next').click(function(e) {
        e.preventDefault();
        elj.jcarousel('scroll', '+=1');
    });
    
    jQuery('.alphabet a').click(function(e)
    {
        e.preventDefault();
        
        jQuery('.alphabet a').removeClass('active');
        jQuery(this).addClass('active');
        
        jQuery('.brands-list').removeClass('sel');
        jQuery(jQuery(this).attr('href')).addClass('sel');
    })
    
    $('.adisable').click(function(e)
    {
        e.preventDefault();
    })
    
    
    jQuery('#button-login').click(function(e)
    {
        e.preventDefault();
        var data_login = jQuery('#form-login').serialize();
        $.post(
            "/login",
            data_login,
            function(data)
            {
                if(data.error == 0)
                {
                    window.location.reload();
                }
                else
                {
                    alert(data.message)
                }
            },
            'json'
        );
    })
    
    jQuery('#form-question').submit(function()
    {
        var data_ask = jQuery(this).serialize();
        $.post(
            "/account/ask",
            data_ask,
            function(data)
            {
                $('#question-block').hide();
                $('#question-block-sended').show();
            }
        );
        return false;
    })
    
    
    jQuery('#form-callback').submit(function()
    {
        var data_ask = jQuery(this).serialize();
        $.post(
            "/ajax/add_callback",
            data_ask,
            function(data)
            {
                $('#form-callback-block').hide();
                $('#form-callback-block-sended').show();
            }
        );
        return false;
    })
    
    if ($(window).width() > 1000) {
        $('.site-menu .menu-holder').on('mouseenter', function () {
            $('.main-menu').addClass('shadowed');
        });

        $('.site-menu .menu-holder').on('mouseleave', function () {
            $('.main-menu').removeClass('shadowed');
        });
    }
    
    
    
    $('body').on('click', '.out-p-button', function(e)
    {
        e.preventDefault();
    })
    
    
        
    $('body').on('click', '.return-pop-wrapper .bootstrap-touchspin-down', function()
    {
        var variant_id = $(this).data('variant');
        
        var bt = $('#cart-drop-wrapper .cart-item[data-id='+variant_id+']')
        
        dataLayer.push({
        	"event": "removeFromCart",
        	"ecommerce": {
        		"remove": {
        			"products": [{
        				"name": bt.data('name'),
        				"id": bt.data('sku'),
        				"price": bt.data('price'),
        				"brand": bt.data('brand'),
        				"category": bt.data('category'),
        				"variant": bt.data('variant'),
        				"quantity": 1
        			}]
        		}
        	}
        });
        
        var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
        elm.trigger("touchspin.downonce");
        if(elm.val() == 0)
        {
            $(this).closest('.cart-item').remove();
        }
        else
        {
            var el = $(this);
            
            var price = ($('#cart-drop-wrapper div[data-variant='+variant_id+']').data('price')*elm.val())+' руб.';
            el.parent().find('.price-wrap').text(price);
            
            el.parent().find('.meas').text(elm.val()+' шт.');   
        }     
        
    })
    
    
    
    $('body').on('click', '.return-pop-wrapper .bootstrap-touchspin-up', function()
    {
        var variant_id = $(this).data('variant');
        
        var bt = $('#cart-drop-wrapper .cart-item[data-id='+variant_id+']')
        
        
        dataLayer.push({
        	"event": "addToCart",
        	"ecommerce": {
        		"add": {
        			"products": [{
        				"name": bt.data('name'),
        				"id": bt.data('sku'),
        				"price": bt.data('price'),
        				"brand": bt.data('brand'),
        				"category": bt.data('category'),
        				"variant": bt.data('variant'),
        				"quantity": bt.data('quantity')
        			}]
        		}
        	}
        });
        
        var elm = $('#cart-drop-wrapper div[data-variant='+variant_id+']').find('input.counter');
        elm.trigger("touchspin.uponce");
    })
  
    $('.hided-block').each(function()
    {
        if($(this).height() >= 380)
        {
            $(this).children('.hided-block-inner').css('max-height', '320px');
            jQuery('<a class="show-full hided" href="#">Раскрыть описание полностью</a>').appendTo($(this)).click(function(e)
            {
                e.preventDefault();
                if(jQuery(this).hasClass('hided'))
                {
                    jQuery(this).parent().children('.hided-block-inner').css('max-height', 'none');
                    jQuery(this).removeClass('hided');
                    jQuery(this).html('Скрыть описание');
                }
                else
                {
                    jQuery(this).parent().children('.hided-block-inner').css('max-height', '320px');
                    jQuery(this).addClass('hided');
                    jQuery(this).html('Раскрыть описание полностью');
                }
            });
        }
    })
    
    $('.product-link').click(function()
    {
        var prod = $(this);
        dataLayer.push({
        	"event": "productClick",
        	"ecommerce": {
        		"click": {
        			"actionField": {
        				"list": "Search"
        			},
        			"products": [{
        				"name": prod.data('name'),
        				"id": prod.data('sku'),
        				"price": prod.data('price'),
        				"brand": prod.data('brand'),
        				"category": prod.data('category'),
        				"variant": prod.data('variant'),
        				"position": prod.data('position')
        			}]
        		}
        	}
        });

    })
    
    $('body').on('click', '.link-to-action', function()
    {
        var action = $(this);
        dataLayer.push({
        	"event": "promotionClick",
        	"ecommerce": {
        		"promoClick": {
        			"promotions": [{
        				"id": action.data('sku'),
        				"name": action.data('name'),
        				"creative": action.data('banner'),
        				"position": action.data('position')
        			}]
        		}
        	}
        });
    })
    
    $('body').on('click', '.product-wrapper .options_weight > .one-v', function()
    {
        var variant_id = $(this).data('variant');
        var prod_sel = $(this).closest('.product-wrapper');
        prod_sel.find('.custom-select').click();
        prod_sel.find('.custom-select-item[data-value='+variant_id+']').click();
        prod_sel.find('.variant-select').val(variant_id).change();
        
        $(this).parent().find('.one-v').removeClass('active');
        $(this).addClass('active');
    });
    
});

jQuery(window).load(function() {
  jQuery('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 210,
    itemMargin: 0,
    controlNav: false
  });
  
});


$(document).ready(function(){
	$('.discount-pop').clone().appendTo('.bonus_mobile');

	if($(window).width()<= 530)
		{$('.bonus_mobile .tooltip-pop.discount-pop').css('width', ($(window).width()-20));}
	$(window).resize(function(){
	if($(window).width()<= 530)
		{$('.bonus_mobile .tooltip-pop.discount-pop').css('width', ($(window).width()-20));}
	else
		{$('.bonus_mobile .tooltip-pop.discount-pop').removeAttr('style');}
	
	});	
    if ($(window).width() > 1000){

        $(document).on('mouseenter', '.menu-holder',function(){
            $('.footer-down>div:not(:first-child)').addClass('unactivate');
        });
        $(document).on('mouseleave', '.menu-holder',function(){
            $('.footer-down>div').removeClass('unactivate');
        });

        $(document).on('mouseenter', '.links-list>li',function(){
            $(this).removeClass('unactive').addClass('active');
            $('.content-wrapper.center-wrapper').addClass('unactivate');

        });

        $(document).on('mouseleave', '.links-list>li',function(){
            $(this).parent().find('li').removeClass('unactive').removeClass('active');
            $('.content-wrapper.center-wrapper').removeClass('unactivate');
        });
    }
    else
    {
        //$(document).on('click','.bonus_mobile', function() {
        	//$(this).find('.discount-pop').toggle();
        //});

        $(document).on('change','.menu_mobile_select', function() {
            location.href=$(this).val();
                });
        $(document).on('click','.login_user_mobile', function() {
                if($('.site-menu').is(':visible')){$('.site-menu').fadeOut('700');}
                $(this).toggleClass('active');
                if($(this).hasClass('active')){$('.footer-down>div:not(:first-child)').addClass('opacity_fade');}
                else{$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');}

        });
    }
        if ($('.main-header').width() < 970) {
        $('.tooltip-pop.call-us').appendTo('.call_to_me_mobile');
    }
    $(window).resize(function(){
        if ($('.main-header').width() < 970) {
            if (!$('.call_to_me_mobile').is('.tooltip-pop.call-us')){   $('.tooltip-pop.call-us').appendTo('.call_to_me_mobile');   }
        }
        if ($('.main-header').width() >= 970) {
            if (!$('.info-line.fl-left>span.default-link.func-link.fl-left').is('.tooltip-pop.call-us')){   $('.tooltip-pop.call-us').appendTo('.info-line.fl-left>span.default-link.func-link.fl-left');}
        }

    });
    
});

$(document).on('click','.default-link', function() {
if ($(window).width() < 1001){ $('.site-menu').fadeOut('700');}
if(!$(this).hasClass('active')){$('.footer-down>div:not(:first-child)').addClass('opacity_fade');}
else{$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');}
});
$(document).on('click','.default-link .close-btn', function() {
	$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');
});
$(document).on('click','body', function() {
	if(!$('.default-link .func-link').hasClass('active')){
	$('.footer-down>div:not(:first-child)').removeClass('opacity_fade');}
});

$(window).load(function(){var active_label;
    $('.product-wrapper').each(function(a){
        $(this).find('.switch-items>div').each(function(b){
            if ($(this).hasClass('active')) { active_label = 1+b; }
        });
        $(this).find('.options_weight>div').removeClass('active');
        $(this).find('.options_weight>div:nth-child('+active_label+')').addClass('active');
    });
});

$('.cat-ch-variant').change(function(){
   var nuber_active = $(this).find('option:selected').index();
   $(this).parents('form').find('.options_weight>div').removeClass('active');
   $(this).parents('form').find('.options_weight>div:nth-child('+ (nuber_active+1) +')').addClass('active');
});

$(document).on('touchstart','.search_mobile_button', function() {
 $('.search-container').toggle(700);
});
$(window).resize(function(){
    if($('.header-wrapper').width()>484){
        $('.search-container').removeAttr('style');
    }
});

