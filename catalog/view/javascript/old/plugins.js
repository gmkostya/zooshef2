function make_select()
{
    $('.custom-select').on('click', function(){
        var thisElem = $(this);

        if(!thisElem.hasClass('active')){
            var $activeSelect = thisElem.find('select');
            var $modalContent = thisElem.find('.custom-select-content');
            var itemContent, itemMarkUp, activeOption;

            $modalContent.empty();

            $activeSelect.find('option').each(function() {
                itemContent = $(this).data('city');
                activeOption = '';
                itemMarkUp = itemContent;

                /*var rmultiDash = /([a-z])([A-Z])/g;
                var className;
                for(var i in itemContent) {
                    className = i.replace( rmultiDash, "$1-$2" ).toLowerCase();
                    itemMarkUp = itemMarkUp + '<span class='+ className +'>' + itemContent[i] + '</span> '
                }*/

                if($(this).attr('selected') == 'selected') {
                    activeOption = ' custom-select-active'
                }

                thisElem.addClass('active');
                $modalContent.append('<div class="custom-select-item'+ activeOption +'" data-value="' + $(this).val() + '" data-stock="' + $(this).data('stock') + '">' + itemMarkUp + '</div>');
            });

            $modalContent.show();
        } else{
            thisElem.removeClass('active');
            thisElem.find('.custom-select-content').hide();
        }
    });
    
    $(document).on('click', '.custom-select-item', function(){
        var $activeOptionVal = $(this).attr('data-value');
        var $owner = $(this).closest('.custom-select').find('select');
        var $mask = $(this).closest('.custom-select').find('.select-mask');
        var $content = $(this).html();

        $mask.html($content);

        $('.custom-select-content').hide();
        $owner.val($activeOptionVal).trigger("change");
        
        var optsel = $owner.find('option:selected');
        
        var prod_id = optsel.data('product');
        var variant_id = optsel.data('variant');
        var prod_sel = $(this).closest('#product-'+prod_id);
        var stock = $(this).data('stock');
        
        prod_sel.find('.input-variant-id').val(variant_id);
        prod_sel.find('.price-value').removeClass('active');
        prod_sel.find('.price-value[data-variant='+variant_id+']').addClass('active');
        prod_sel.find('.product-sku').removeClass('active');
        prod_sel.find('.product-sku[data-variant='+variant_id+']').addClass('active');
        
        if(stock == 0)
        {
            prod_sel.find('.buy-button').hide();
            prod_sel.find('.button-order').show();
        }
        else
        {
            prod_sel.find('.buy-button').show();
            prod_sel.find('.button-order').hide();
        }
    });
}



$(document).ready(function(){

    //placeholder init
    if($('.placeholder-container').length){
        $('.placeholder-text').inFieldLabels();
    }

    //custom select
    
    //custom select
    
    make_select();


    $(document).on('mouseleave', '.product-wrapper', function(){
        var thisElem = $(this).find('.custom-select');
        thisElem.removeClass('active');
        thisElem.find('.custom-select-content').hide();
    });

    $(document).on('mouseenter', '.full-cart', function(){
        $('.custom-select').removeClass('active');
        $('.custom-select-content').hide();
    });

    //choose color
    $(document).on('click', '.color-wrapper', function(){
        var thisElem = $(this);
        if(!thisElem.hasClass('active')){
            thisElem.parent().find('.color-wrapper').removeClass('active');
            thisElem.addClass('active').find('input').prop('checked', true);
        }
    });

    //phone input placeholder
    if($('.phone-number').length){
        $('.phone-number').mask("+7 (000) 000 00 00", {placeholder: "+7 (    ) - -"});
        
        $('.phone-number').on('input', function()
        {
            if($(this).val() == 8)
            {
                $(this).val('+7');
            }
        })
    }
    
    
    /*if($('.date-number').length){
        $('.date-number').mask("00/00/0000", {placeholder: "дд/мм/гггг"});
    }*/
    
    if($('.cart-quantity-input').length){
        //$('.cart-quantity-input').mask("0 шт.", {placeholder: " "});
    }


    //call us pop initialize
    $(document).on('click', '.func-link', function(e){
        e.stopPropagation();
        e.preventDefault();
        $('.custom-select').removeClass('active');
        $('.custom-select-content').hide();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else{
            $('.func-link.active').removeClass('active');
            $(this).addClass('active');
        }

    });
    $(document).on('click', '.tooltip-pop *', function(e){
        e.stopPropagation();
    });
    $(document).on('click', '.tip-body .close-btn', function(e){
        e.preventDefault();
        $('.func-link.active').removeClass('active');
    });
    $(document).on('click', function(){
        $('.func-link').removeClass('active');
    });

    //cart item incrementer
    if($('.items-count').length){
        make_spinner();
    }

    //slider init
    if($('.main-slider').length){
        $('.main-slider').bxSlider({
            controls: true,
            nextText: '›',
            prevText: '‹',
            auto: true
        })
        $(window).load(function(){
            $('.top-slider').removeClass('unready');
        });
    }

    //tabs on product plate
    /*$(document).on('click', '.weight-item', function(){
        if(!$(this).hasClass('active')){
            var holder = $(this).parent();
            var activeTab = $(this).index() - 1;

            holder.find('.weight-item').removeClass('active');
            holder.next().find('.sum').removeClass('active');
            $(this).addClass('active');
            holder.next().find('.sum').eq(activeTab).addClass('active');
        }
    })*/

    //icheck init
    /*if($('input[type="checkbox"]').length){
        $('input').iCheck({
            checkboxClass: 'custom-check'
        });
        $('input').on('ifChecked', function(){
            $(this).parents('label').addClass('active');
        });
        $('input').on('ifUnchecked', function(){
            $(this).parents('label').removeClass('active');
        });
    }

    if($('.custom-radio').length){
        $('.custom-radio').iCheck({
            radioClass: 'custom-radio'
        });
        $('input').on('ifChecked', '.custom-radio', function(){
            $('.custom-radio').parents('label').removeClass('active');
            $(this).parents('label').addClass('active');
        });
    }*/

    //clear-inputs
    $('.clear-input').on('click', function(){
        $(this).parent().find('input, textarea').val('');
    });

    //accordion
    $('.accordion-open').on('click', function(){
        if(!$(this).hasClass('active')){
            $('.accordion-open').removeClass('active');
            $('.accordion-content').hide();

            $(this).addClass('active').next('.accordion-content').show();
        }
    });


    //close-pop
    $('.return-pop-content, .return-pop-content *').on('click', function(e){
        e.stopPropagation();
    });
    $('body').on('click', '.return-pop-bg', function(){
        $('.return-pop-wrapper').remove();
        $('body').css('overflow', 'auto');
    })

    //show product-description
    $('.show-full').on('click', function(){
        $(this).hide().prev().show();
    });

    /*if($('.product-main-image').length){
        $("#img_01").elevateZoom({
            gallery:'gallery_01',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true
        });
    }*/

    //fixed header left offset
    $(window).scroll( function(){
        if($(window).scrollTop() > 0){
            $('.header-wrapper').addClass('sticky');
        }
        else{
            $('.header-wrapper').removeClass('sticky');
        }

        if($(window).width() < 1230){
            var correctingMargin = $(window).scrollLeft();
            $('.header-elements-block').css({
                'margin-left': - correctingMargin,
                'left': '15px'
            });
        }
        else{
        }
    }).trigger('scroll');

    $(window).resize(function(){
        if($(window).width() < 1230){
            var correctingMargin = $('.main-header').offset().left;
            $('.header-elements-block').css({
                'margin-left': - correctingMargin,
                'left': '15px'
            });
       }
        else{
            $('.header-elements-block').attr('style', ' ');
        }
    }).trigger('resize');
});
