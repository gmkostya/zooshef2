!function(e,r){"object"==typeof exports&&"object"==typeof module?module.exports=r():"function"==typeof define&&define.amd?define([],r):"object"==typeof exports?exports.vanillaTextMask=r():e.vanillaTextMask=r()}(this,function(){return function(e){function r(n){if(t[n])return t[n].exports;var o=t[n]={exports:{},id:n,loaded:!1};return e[n].call(o.exports,o,o.exports,r),o.loaded=!0,o.exports}var t={};return r.m=e,r.c=t,r.p="",r(0)}([function(e,r,t){"use strict";function n(e){return e&&e.__esModule?e:{default:e}}function o(e){var r=e.inputElement,t=(0,u.default)(e),n=function(e){var r=e.target.value;return t.update(r)};return r.addEventListener("input",n),t.update(r.value),{textMaskInputElement:t,destroy:function(){r.removeEventListener("input",n)}}}Object.defineProperty(r,"__esModule",{value:!0}),r.conformToMask=void 0,r.maskInput=o;var a=t(2);Object.defineProperty(r,"conformToMask",{enumerable:!0,get:function(){return n(a).default}});var i=t(5),u=n(i);r.default=o},function(e,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.placeholderChar="_"},function(e,r,t){"use strict";function n(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:i,r=arguments.length>1&&void 0!==arguments[1]?arguments[1]:i,t=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{},n=t.guide,u=void 0===n||n,l=t.previousConformedValue,s=void 0===l?i:l,f=t.placeholderChar,d=void 0===f?a.placeholderChar:f,c=t.placeholder,p=void 0===c?(0,o.convertMaskToPlaceholder)(r,d):c,v=t.currentCaretPosition,h=t.keepCharPositions,m=u===!1&&void 0!==s,g=e.length,y=s.length,C=p.length,b=r.length,k=g-y,x=k>0,j=v+(x?-k:0),O=j+Math.abs(k);if(h===!0&&!x){for(var P=i,V=j;V<O;V++)p[V]===d&&(P+=d);e=e.slice(0,j)+P+e.slice(j,g)}for(var w=e.split(i).map(function(e,r){return{char:e,isNew:r>=j&&r<O}}),M=g-1;M>=0;M--){var T=w[M].char;if(T!==d){var _=M>=j&&y===b;T===p[_?M-k:M]&&w.splice(M,1)}}var R=i,S=!1;e:for(var N=0;N<C;N++){var E=p[N];if(E===d){if(w.length>0)for(;w.length>0;){var I=w.shift(),L=I.char,J=I.isNew;if(L===d&&m!==!0){R+=d;continue e}if(r[N].test(L)){if(h===!0&&J!==!1&&s!==i&&u!==!1&&x){for(var A=w.length,W=null,q=0;q<A;q++){var z=w[q];if(z.char!==d&&z.isNew===!1)break;if(z.char===d){W=q;break}}null!==W?(R+=L,w.splice(W,1)):N--}else R+=L;continue e}S=!0}m===!1&&(R+=p.substr(N,C));break}R+=E}if(m&&x===!1){for(var B=null,D=0;D<R.length;D++)p[D]===d&&(B=D);R=null!==B?R.substr(0,B+1):i}return{conformedValue:R,meta:{someCharsRejected:S}}}Object.defineProperty(r,"__esModule",{value:!0}),r.default=n;var o=t(3),a=t(1),i=""},function(e,r,t){"use strict";function n(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:l,r=arguments.length>1&&void 0!==arguments[1]?arguments[1]:u.placeholderChar;if(e.indexOf(r)!==-1)throw new Error("Placeholder character must not be used as part of the mask. Please specify a character that is not present in your mask as your placeholder character.\n\n"+("The placeholder character that was received is: "+JSON.stringify(r)+"\n\n")+("The mask that was received is: "+JSON.stringify(e)));return e.map(function(e){return e instanceof RegExp?r:e}).join("")}function o(e){return"string"==typeof e||e instanceof String}function a(e){return"number"==typeof e&&void 0===e.length&&!isNaN(e)}function i(e){for(var r=[],t=void 0;t=e.indexOf(s),t!==-1;)r.push(t),e.splice(t,1);return{maskWithoutCaretTraps:e,indexes:r}}Object.defineProperty(r,"__esModule",{value:!0}),r.convertMaskToPlaceholder=n,r.isString=o,r.isNumber=a,r.processCaretTraps=i;var u=t(1),l=[],s="[]"},function(e,r){"use strict";function t(e){var r=e.previousConformedValue,t=void 0===r?o:r,a=e.currentCaretPosition,i=void 0===a?0:a,u=e.conformedValue,l=e.rawValue,s=e.placeholderChar,f=e.placeholder,d=e.indexesOfPipedChars,c=void 0===d?n:d,p=e.caretTrapIndexes,v=void 0===p?n:p;if(0===i)return 0;var h=l.length,m=t.length,g=f.length,y=u.length,C=h-m,b=C>0,k=0===m,x=C>1&&!b&&!k;if(x)return i;var j=b&&(t===u||u===f),O=0;if(j?O=i-C:!function(){for(var e=u.toLowerCase(),r=l.toLowerCase(),t=r.substr(0,i).split(o),n=t.filter(function(r){return e.indexOf(r)!==-1}),a=n[n.length-1],d=c.map(function(r){return e[r]}),p=d.filter(function(e){return e===a}).length,v=n.filter(function(e){return e===a}).length,h=f.substr(0,f.indexOf(s)).split(o).filter(function(e,r){return e===a&&l[r]!==e}).length,m=h+v+p,g=0,C=0;C<y;C++){var b=e[C];if(O=C+1,b===a&&g++,g>=m)break}}(),b){for(var P=O,V=O;V<=g;V++)if(f[V]===s&&(P=V),f[V]===s||v.indexOf(V)!==-1||V===g)return P}else for(var w=O;w>=0;w--)if(f[w-1]===s||v.indexOf(w)!==-1||0===w)return w}Object.defineProperty(r,"__esModule",{value:!0}),r.default=t;var n=[],o=""},function(e,r,t){"use strict";function n(e){return e&&e.__esModule?e:{default:e}}function o(e){var r=e.inputElement,t=e.mask,n=e.guide,o=e.pipe,s=e.placeholderChar,d=void 0===s?v.placeholderChar:s,g=e.onAccept,C=e.onReject,b=e.keepCharPositions,k=void 0!==b&&b;("undefined"==typeof t?"undefined":l(t))===y&&void 0!==t.pipe&&void 0!==t.mask&&(o=t.pipe,t=t.mask);var x={previousConformedValue:m,previousOnRejectRawValue:m},j=void 0,O=void 0;return t instanceof Array&&(j=(0,p.convertMaskToPlaceholder)(t,d)),{state:x,update:function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:r.value;if(e!==x.previousConformedValue){var s=i(e),v=r.selectionStart,y=x.previousConformedValue,b=void 0;if(("undefined"==typeof t?"undefined":l(t))===h){O=t(s,{currentCaretPosition:v,previousConformedValue:y,placeholderChar:d});var P=(0,p.processCaretTraps)(O),V=P.maskWithoutCaretTraps,w=P.indexes;O=V,b=w,j=(0,p.convertMaskToPlaceholder)(O,d)}else O=t;var M={previousConformedValue:y,guide:n,placeholderChar:d,pipe:o,placeholder:j,currentCaretPosition:v,keepCharPositions:k},T=(0,c.default)(s,O,M),_=T.conformedValue,R=T.meta.someCharsRejected,S=("undefined"==typeof o?"undefined":l(o))===h,N={};S&&(N=o(_,u({rawValue:s},M)),N===!1?N={value:y,rejected:!0}:(0,p.isString)(N)&&(N={value:N}));var E=S?N.value:_,I=(0,f.default)({previousConformedValue:y,conformedValue:E,placeholder:j,rawValue:s,currentCaretPosition:v,placeholderChar:d,indexesOfPipedChars:N.indexesOfPipedChars,caretTrapIndexes:b}),L=E===j&&0===I,J=L?m:E;if(x.previousConformedValue=J,r.value!==J){r.value=J,a(r,I),("undefined"==typeof g?"undefined":l(g))===h&&J!==y&&J!==j&&(x.previousOnRejectRawValue=null,g());var A=s.length<y.length;("undefined"==typeof C?"undefined":l(C))===h&&(R||N.rejected)&&A===!1&&x.previousOnRejectRawValue!==e&&(x.previousOnRejectRawValue=e,C({conformedValue:E,pipeRejection:N.rejected,maskRejection:R}))}}}}}function a(e,r){document.activeElement===e&&e.setSelectionRange(r,r,g)}function i(e){if((0,p.isString)(e))return e;if((0,p.isNumber)(e))return String(e);if(void 0===e||null===e)return m;throw new Error("The 'value' provided to Text Mask needs to be a string or a number. The value received was:\n\n "+JSON.stringify(e))}Object.defineProperty(r,"__esModule",{value:!0});var u=Object.assign||function(e){for(var r=1;r<arguments.length;r++){var t=arguments[r];for(var n in t)Object.prototype.hasOwnProperty.call(t,n)&&(e[n]=t[n])}return e},l="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};r.default=o;var s=t(4),f=n(s),d=t(2),c=n(d),p=t(3),v=t(1),h="function",m="",g="none",y="object"}])});

/*********************************************************************************/
/*                                  raven                                        */
/*********************************************************************************/

/*! Raven.js 1.1.20 (568d778) | github.com/getsentry/raven-js */
!function(a,b){"use strict";function c(a,b){var c,d;b=b||{},a="raven"+a.substr(0,1).toUpperCase()+a.substr(1),document.createEvent?(c=document.createEvent("HTMLEvents"),c.initEvent(a,!0,!0)):(c=document.createEventObject(),c.eventType=a);for(d in b)l(b,d)&&(c[d]=b[d]);if(document.createEvent)document.dispatchEvent(c);else try{document.fireEvent("on"+c.eventType.toLowerCase(),c)}catch(e){}}function d(a){this.name="RavenConfigError",this.message=a}function e(a){var b=Z.exec(a),c={},e=7;try{for(;e--;)c[Y[e]]=b[e]||""}catch(f){throw new d("Invalid DSN: "+a)}if(c.pass)throw new d("Do not specify your private key in the DSN!");return c}function f(a){return void 0===a}function g(a){return"function"==typeof a}function h(a){return"[object String]"===S.toString.call(a)}function i(a){return"object"==typeof a&&null!==a}function j(a){for(var b in a)return!1;return!0}function k(a){return i(a)&&"[object Error]"===S.toString.call(a)||a instanceof Error}function l(a,b){return S.hasOwnProperty.call(a,b)}function m(a,b){var c,d;if(f(a.length))for(c in a)l(a,c)&&b.call(null,c,a[c]);else if(d=a.length)for(c=0;d>c;c++)b.call(null,c,a[c])}function n(){N="?sentry_version=4&sentry_client=raven-js/"+X.VERSION+"&sentry_key="+L}function o(a,b){var d=[];a.stack&&a.stack.length&&m(a.stack,function(a,b){var c=p(b);c&&d.push(c)}),c("handle",{stackInfo:a,options:b}),r(a.name,a.message,a.url,a.lineno,d,b)}function p(a){if(a.url){var b,c={filename:a.url,lineno:a.line,colno:a.column,"function":a.func||"?"},d=q(a);if(d){var e=["pre_context","context_line","post_context"];for(b=3;b--;)c[e[b]]=d[b]}return c.in_app=!(!Q.includePaths.test(c.filename)||/(Raven|TraceKit)\./.test(c["function"])||/raven\.(min\.)?js$/.test(c.filename)),c}}function q(a){if(a.context&&Q.fetchContext){for(var b=a.context,c=~~(b.length/2),d=b.length,e=!1;d--;)if(b[d].length>300){e=!0;break}if(e){if(f(a.column))return;return[[],b[c].substr(a.column,50),[]]}return[b.slice(0,c),b[c],b.slice(c+1)]}}function r(a,b,c,d,e,f){var g,h;Q.ignoreErrors.test(b)||(b+="",b=t(b,Q.maxMessageLength),h=a+": "+b,h=t(h,Q.maxMessageLength),e&&e.length?(c=e[0].filename||c,e.reverse(),g={frames:e}):c&&(g={frames:[{filename:c,lineno:d,in_app:!0}]}),Q.ignoreUrls&&Q.ignoreUrls.test(c)||(!Q.whitelistUrls||Q.whitelistUrls.test(c))&&w(s({exception:{type:a,value:b},stacktrace:g,culprit:c,message:h},f)))}function s(a,b){return b?(m(b,function(b,c){a[b]=c}),a):a}function t(a,b){return a.length<=b?a:a.substr(0,b)+"â€¦"}function u(){return+new Date}function v(){if(document.location&&document.location.href){var a={headers:{"User-Agent":navigator.userAgent}};return a.url=document.location.href,document.referrer&&(a.headers.Referer=document.referrer),a}}function w(a){var b={project:M,logger:Q.logger,platform:"javascript"},c=v();c&&(b.request=c),a=s(b,a),a.tags=s(s({},Q.tags),a.tags),a.extra=s(s({},Q.extra),a.extra),a.extra=s({"session:duration":u()-V},a.extra),j(a.tags)&&delete a.tags,K&&(a.user=K),Q.release&&(a.release=Q.release),g(Q.dataCallback)&&(a=Q.dataCallback(a)||a),a&&!j(a)&&(!g(Q.shouldSendCallback)||Q.shouldSendCallback(a))&&(I=a.event_id||(a.event_id=B()),x(a))}function x(a){var b,d;C("debug","Raven about to send:",a),z()&&(b=y(),d=J+N+"&sentry_data="+encodeURIComponent(JSON.stringify(a)),(Q.crossOrigin||""===Q.crossOrigin)&&(b.crossOrigin=Q.crossOrigin),b.onload=function(){c("success",{data:a,src:d})},b.onerror=b.onabort=function(){c("failure",{data:a,src:d})},b.src=d)}function y(){return document.createElement("img")}function z(){return P?J?!0:($||C("error","Error: Raven has not been configured."),$=!0,!1):!1}function A(a){for(var b,c=[],d=0,e=a.length;e>d;d++)b=a[d],h(b)?c.push(b.replace(/([.*+?^=!:${}()|\[\]\/\\])/g,"\\$1")):b&&b.source&&c.push(b.source);return new RegExp(c.join("|"),"i")}function B(){var b=a.crypto||a.msCrypto;if(!f(b)&&b.getRandomValues){var c=new Uint16Array(8);b.getRandomValues(c),c[3]=4095&c[3]|16384,c[4]=16383&c[4]|32768;var d=function(a){for(var b=a.toString(16);b.length<4;)b="0"+b;return b};return d(c[0])+d(c[1])+d(c[2])+d(c[3])+d(c[4])+d(c[5])+d(c[6])+d(c[7])}return"xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx".replace(/[xy]/g,function(a){var b=16*Math.random()|0,c="x"==a?b:3&b|8;return c.toString(16)})}function C(a){U[a]&&X.debug&&U[a].apply(T,F.call(arguments,1))}function D(){var b=a.RavenConfig;b&&X.config(b.dsn,b.config).install()}var E={remoteFetching:!1,collectWindowErrors:!0,linesOfContext:7,debug:!1},F=[].slice,G="?";E.wrap=function(a){function b(){try{return a.apply(this,arguments)}catch(b){throw E.report(b),b}}return b},E.report=function(){function c(a){h(),o.push(a)}function d(a){for(var b=o.length-1;b>=0;--b)o[b]===a&&o.splice(b,1)}function e(){i(),o=[]}function f(a,b){var c=null;if(!b||E.collectWindowErrors){for(var d in o)if(l(o,d))try{o[d].apply(null,[a].concat(F.call(arguments,2)))}catch(e){c=e}if(c)throw c}}function g(a,b,c,d,e){var g=null;if(r)E.computeStackTrace.augmentStackTraceWithInitialElement(r,b,c,a),j();else if(e)g=E.computeStackTrace(e),f(g,!0);else{var h={url:b,line:c,column:d};h.func=E.computeStackTrace.guessFunctionName(h.url,h.line),h.context=E.computeStackTrace.gatherContext(h.url,h.line),g={message:a,url:document.location.href,stack:[h]},f(g,!0)}return m?m.apply(this,arguments):!1}function h(){n||(m=a.onerror,a.onerror=g,n=!0)}function i(){n&&(a.onerror=m,n=!1,m=b)}function j(){var a=r,b=p;p=null,r=null,q=null,f.apply(null,[a,!1].concat(b))}function k(b,c){var d=F.call(arguments,1);if(r){if(q===b)return;j()}var e=E.computeStackTrace(b);if(r=e,q=b,p=d,a.setTimeout(function(){q===b&&j()},e.incomplete?2e3:0),c!==!1)throw b}var m,n,o=[],p=null,q=null,r=null;return k.subscribe=c,k.unsubscribe=d,k.uninstall=e,k}(),E.computeStackTrace=function(){function b(b){if(!E.remoteFetching)return"";try{var c=function(){try{return new a.XMLHttpRequest}catch(b){return new a.ActiveXObject("Microsoft.XMLHTTP")}},d=c();return d.open("GET",b,!1),d.send(""),d.responseText}catch(e){return""}}function c(a){if(!h(a))return[];if(!l(t,a)){var c="",d="";try{d=document.domain}catch(e){}-1!==a.indexOf(d)&&(c=b(a)),t[a]=c?c.split("\n"):[]}return t[a]}function d(a,b){var d,e=/function ([^(]*)\(([^)]*)\)/,g=/['"]?([0-9A-Za-z$_]+)['"]?\s*[:=]\s*(function|eval|new Function)/,h="",i=10,j=c(a);if(!j.length)return G;for(var k=0;i>k;++k)if(h=j[b-k]+h,!f(h)){if(d=g.exec(h))return d[1];if(d=e.exec(h))return d[1]}return G}function e(a,b){var d=c(a);if(!d.length)return null;var e=[],g=Math.floor(E.linesOfContext/2),h=g+E.linesOfContext%2,i=Math.max(0,b-g-1),j=Math.min(d.length,b+h-1);b-=1;for(var k=i;j>k;++k)f(d[k])||e.push(d[k]);return e.length>0?e:null}function g(a){return a.replace(/[\-\[\]{}()*+?.,\\\^$|#]/g,"\\$&")}function i(a){return g(a).replace("<","(?:<|&lt;)").replace(">","(?:>|&gt;)").replace("&","(?:&|&amp;)").replace('"','(?:"|&quot;)').replace(/\s+/g,"\\s+")}function j(a,b){for(var d,e,f=0,g=b.length;g>f;++f)if((d=c(b[f])).length&&(d=d.join("\n"),e=a.exec(d)))return{url:b[f],line:d.substring(0,e.index).split("\n").length,column:e.index-d.lastIndexOf("\n",e.index)-1};return null}function k(a,b,d){var e,f=c(b),h=new RegExp("\\b"+g(a)+"\\b");return d-=1,f&&f.length>d&&(e=h.exec(f[d]))?e.index:null}function m(b){for(var c,d,e,f,h=[a.location.href],k=document.getElementsByTagName("script"),l=""+b,m=/^function(?:\s+([\w$]+))?\s*\(([\w\s,]*)\)\s*\{\s*(\S[\s\S]*\S)\s*\}\s*$/,n=/^function on([\w$]+)\s*\(event\)\s*\{\s*(\S[\s\S]*\S)\s*\}\s*$/,o=0;o<k.length;++o){var p=k[o];p.src&&h.push(p.src)}if(e=m.exec(l)){var q=e[1]?"\\s+"+e[1]:"",r=e[2].split(",").join("\\s*,\\s*");c=g(e[3]).replace(/;$/,";?"),d=new RegExp("function"+q+"\\s*\\(\\s*"+r+"\\s*\\)\\s*{\\s*"+c+"\\s*}")}else d=new RegExp(g(l).replace(/\s+/g,"\\s+"));if(f=j(d,h))return f;if(e=n.exec(l)){var s=e[1];if(c=i(e[2]),d=new RegExp("on"+s+"=[\\'\"]\\s*"+c+"\\s*[\\'\"]","i"),f=j(d,h[0]))return f;if(d=new RegExp(c),f=j(d,h))return f}return null}function n(a){if(!f(a.stack)&&a.stack){for(var b,c,g=/^\s*at (.*?) ?\(?((?:(?:file|https?|chrome-extension):.*?)|<anonymous>):(\d+)(?::(\d+))?\)?\s*$/i,h=/^\s*(.*?)(?:\((.*?)\))?@((?:file|https?|chrome).*?):(\d+)(?::(\d+))?\s*$/i,i=/^\s*at (?:((?:\[object object\])?.+) )?\(?((?:ms-appx|http|https):.*?):(\d+)(?::(\d+))?\)?\s*$/i,j=a.stack.split("\n"),l=[],m=/^(.*) is undefined$/.exec(a.message),n=0,o=j.length;o>n;++n){if(b=h.exec(j[n]))c={url:b[3],func:b[1]||G,args:b[2]?b[2].split(","):"",line:+b[4],column:b[5]?+b[5]:null};else if(b=g.exec(j[n]))c={url:b[2],func:b[1]||G,line:+b[3],column:b[4]?+b[4]:null};else{if(!(b=i.exec(j[n])))continue;c={url:b[2],func:b[1]||G,line:+b[3],column:b[4]?+b[4]:null}}!c.func&&c.line&&(c.func=d(c.url,c.line)),c.line&&(c.context=e(c.url,c.line)),l.push(c)}return l.length?(l[0].line&&!l[0].column&&m?l[0].column=k(m[1],l[0].url,l[0].line):l[0].column||f(a.columnNumber)||(l[0].column=a.columnNumber+1),{name:a.name,message:a.message,url:document.location.href,stack:l}):null}}function o(a){var b=a.stacktrace;if(!f(a.stacktrace)&&a.stacktrace){for(var c,g=/ line (\d+), column (\d+) in (?:<anonymous function: ([^>]+)>|([^\)]+))\((.*)\) in (.*):\s*$/i,h=b.split("\n"),i=[],j=0,k=h.length;k>j;j+=2)if(c=g.exec(h[j])){var l={line:+c[1],column:+c[2],func:c[3]||c[4],args:c[5]?c[5].split(","):[],url:c[6]};if(!l.func&&l.line&&(l.func=d(l.url,l.line)),l.line)try{l.context=e(l.url,l.line)}catch(m){}l.context||(l.context=[h[j+1]]),i.push(l)}return i.length?{name:a.name,message:a.message,url:document.location.href,stack:i}:null}}function p(b){var f=b.message.split("\n");if(f.length<4)return null;var g,h,k,m,n=/^\s*Line (\d+) of linked script ((?:file|https?)\S+)(?:: in function (\S+))?\s*$/i,o=/^\s*Line (\d+) of inline#(\d+) script in ((?:file|https?)\S+)(?:: in function (\S+))?\s*$/i,p=/^\s*Line (\d+) of function script\s*$/i,q=[],r=document.getElementsByTagName("script"),s=[];for(h in r)l(r,h)&&!r[h].src&&s.push(r[h]);for(h=2,k=f.length;k>h;h+=2){var t=null;if(g=n.exec(f[h]))t={url:g[2],func:g[3],line:+g[1]};else if(g=o.exec(f[h])){t={url:g[3],func:g[4]};var u=+g[1],v=s[g[2]-1];if(v&&(m=c(t.url))){m=m.join("\n");var w=m.indexOf(v.innerText);w>=0&&(t.line=u+m.substring(0,w).split("\n").length)}}else if(g=p.exec(f[h])){var x=a.location.href.replace(/#.*$/,""),y=g[1],z=new RegExp(i(f[h+1]));m=j(z,[x]),t={url:x,line:m?m.line:y,func:""}}if(t){t.func||(t.func=d(t.url,t.line));var A=e(t.url,t.line),B=A?A[Math.floor(A.length/2)]:null;A&&B.replace(/^\s*/,"")===f[h+1].replace(/^\s*/,"")?t.context=A:t.context=[f[h+1]],q.push(t)}}return q.length?{name:b.name,message:f[0],url:document.location.href,stack:q}:null}function q(a,b,c,f){var g={url:b,line:c};if(g.url&&g.line){a.incomplete=!1,g.func||(g.func=d(g.url,g.line)),g.context||(g.context=e(g.url,g.line));var h=/ '([^']+)' /.exec(f);if(h&&(g.column=k(h[1],g.url,g.line)),a.stack.length>0&&a.stack[0].url===g.url){if(a.stack[0].line===g.line)return!1;if(!a.stack[0].line&&a.stack[0].func===g.func)return a.stack[0].line=g.line,a.stack[0].context=g.context,!1}return a.stack.unshift(g),a.partial=!0,!0}return a.incomplete=!0,!1}function r(a,b){for(var c,e,f,g=/function\s+([_$a-zA-Z\xA0-\uFFFF][_$a-zA-Z0-9\xA0-\uFFFF]*)?\s*\(/i,h=[],i={},j=!1,l=r.caller;l&&!j;l=l.caller)if(l!==s&&l!==E.report){if(e={url:null,func:G,line:null,column:null},l.name?e.func=l.name:(c=g.exec(l.toString()))&&(e.func=c[1]),"undefined"==typeof e.func)try{e.func=c.input.substring(0,c.input.indexOf("{"))}catch(n){}if(f=m(l)){e.url=f.url,e.line=f.line,e.func===G&&(e.func=d(e.url,e.line));var o=/ '([^']+)' /.exec(a.message||a.description);o&&(e.column=k(o[1],f.url,f.line))}i[""+l]?j=!0:i[""+l]=!0,h.push(e)}b&&h.splice(0,b);var p={name:a.name,message:a.message,url:document.location.href,stack:h};return q(p,a.sourceURL||a.fileName,a.line||a.lineNumber,a.message||a.description),p}function s(a,b){var c=null;b=null==b?0:+b;try{if(c=o(a))return c}catch(d){if(E.debug)throw d}try{if(c=n(a))return c}catch(d){if(E.debug)throw d}try{if(c=p(a))return c}catch(d){if(E.debug)throw d}try{if(c=r(a,b+1))return c}catch(d){if(E.debug)throw d}return{name:a.name,message:a.message,url:document.location.href}}var t={};return s.augmentStackTraceWithInitialElement=q,s.computeStackTraceFromStackProp=n,s.guessFunctionName=d,s.gatherContext=e,s}();var H,I,J,K,L,M,N,O=a.Raven,P=!("object"!=typeof JSON||!JSON.stringify),Q={logger:"javascript",ignoreErrors:[],ignoreUrls:[],whitelistUrls:[],includePaths:[],crossOrigin:"anonymous",collectWindowErrors:!0,tags:{},maxMessageLength:100,extra:{}},R=!1,S=Object.prototype,T=a.console||{},U={},V=u();for(var W in T)U[W]=T[W];var X={VERSION:"1.1.20",debug:!0,noConflict:function(){return a.Raven=O,X},config:function(a,b){if(J)return C("error","Error: Raven has already been configured"),X;if(!a)return X;var c=e(a),d=c.path.lastIndexOf("/"),f=c.path.substr(1,d);return b&&m(b,function(a,b){Q[a]=b}),Q.ignoreErrors.push(/^Script error\.?$/),Q.ignoreErrors.push(/^Javascript error: Script error\.? on line 0$/),Q.ignoreErrors=A(Q.ignoreErrors),Q.ignoreUrls=Q.ignoreUrls.length?A(Q.ignoreUrls):!1,Q.whitelistUrls=Q.whitelistUrls.length?A(Q.whitelistUrls):!1,Q.includePaths=A(Q.includePaths),L=c.user,M=c.path.substr(d+1),J="//"+c.host+(c.port?":"+c.port:"")+"/"+f+"api/"+M+"/store/",c.protocol&&(J=c.protocol+":"+J),Q.fetchContext&&(E.remoteFetching=!0),Q.linesOfContext&&(E.linesOfContext=Q.linesOfContext),E.collectWindowErrors=!!Q.collectWindowErrors,n(),X},install:function(){return z()&&!R&&(E.report.subscribe(o),R=!0),X},context:function(a,c,d){return g(a)&&(d=c||[],c=a,a=b),X.wrap(a,c).apply(this,d)},wrap:function(a,c){function d(){for(var b=[],d=arguments.length,e=!a||a&&a.deep!==!1;d--;)b[d]=e?X.wrap(a,arguments[d]):arguments[d];try{return c.apply(this,b)}catch(f){throw X.captureException(f,a),f}}if(f(c)&&!g(a))return a;if(g(a)&&(c=a,a=b),!g(c))return c;if(c.__raven__)return c;for(var e in c)l(c,e)&&(d[e]=c[e]);return d.__raven__=!0,d.__inner__=c,d},uninstall:function(){return E.report.uninstall(),R=!1,X},captureException:function(a,b){if(!k(a))return X.captureMessage(a,b);H=a;try{var c=E.computeStackTrace(a);o(c,b)}catch(d){if(a!==d)throw d}return X},captureMessage:function(a,b){return Q.ignoreErrors.test&&Q.ignoreErrors.test(a)?void 0:(w(s({message:a+""},b)),X)},setUserContext:function(a){return K=a,X},setExtraContext:function(a){return Q.extra=a||{},X},setTagsContext:function(a){return Q.tags=a||{},X},setReleaseContext:function(a){return Q.release=a,X},setDataCallback:function(a){return Q.dataCallback=a,X},setShouldSendCallback:function(a){return Q.shouldSendCallback=a,X},lastException:function(){return H},lastEventId:function(){return I},isSetup:function(){return z()}};X.setUser=X.setUserContext;var Y="source protocol user pass host port path".split(" "),Z=/^(?:(\w+):)?\/\/(?:(\w+)(:\w+)?@)?([\w\.-]+)(?::(\d+))?(\/.*)/;d.prototype=new Error,d.prototype.constructor=d;var $;D(),"function"==typeof define&&define.amd?(a.Raven=X,define("raven",[],function(){return X})):"object"==typeof module?module.exports=X:"object"==typeof exports?exports=X:a.Raven=X}("undefined"!=typeof window?window:this);

/*
 * Swiper 2.7.6
 * Mobile touch slider and framework with hardware accelerated transitions
 *
 * http://www.idangero.us/sliders/swiper/
 *
 * Copyright 2010-2015, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 *
 * Licensed under GPL & MIT
 *
 * Released on: February 11, 2015
 */
var Swiper=function(a,b){"use strict";function c(a,b){return document.querySelectorAll?(b||document).querySelectorAll(a):jQuery(a,b)}function d(a){return"[object Array]"===Object.prototype.toString.apply(a)?!0:!1}function e(){var a=G-J;return b.freeMode&&(a=G-J),b.slidesPerView>D.slides.length&&!b.centeredSlides&&(a=0),0>a&&(a=0),a}function f(){function a(a){var c,d,e=function(){"undefined"!=typeof D&&null!==D&&(void 0!==D.imagesLoaded&&D.imagesLoaded++,D.imagesLoaded===D.imagesToLoad.length&&(D.reInit(),b.onImagesReady&&D.fireCallback(b.onImagesReady,D)))};a.complete?e():(d=a.currentSrc||a.getAttribute("src"),d?(c=new Image,c.onload=e,c.onerror=e,c.src=d):e())}var d=D.h.addEventListener,e="wrapper"===b.eventTarget?D.wrapper:D.container;if(D.browser.ie10||D.browser.ie11?(d(e,D.touchEvents.touchStart,p),d(document,D.touchEvents.touchMove,q),d(document,D.touchEvents.touchEnd,r)):(D.support.touch&&(d(e,"touchstart",p),d(e,"touchmove",q),d(e,"touchend",r)),b.simulateTouch&&(d(e,"mousedown",p),d(document,"mousemove",q),d(document,"mouseup",r))),b.autoResize&&d(window,"resize",D.resizeFix),g(),D._wheelEvent=!1,b.mousewheelControl){if(void 0!==document.onmousewheel&&(D._wheelEvent="mousewheel"),!D._wheelEvent)try{new WheelEvent("wheel"),D._wheelEvent="wheel"}catch(f){}D._wheelEvent||(D._wheelEvent="DOMMouseScroll"),D._wheelEvent&&d(D.container,D._wheelEvent,j)}if(b.keyboardControl&&d(document,"keydown",i),b.updateOnImagesReady){D.imagesToLoad=c("img",D.container);for(var h=0;h<D.imagesToLoad.length;h++)a(D.imagesToLoad[h])}}function g(){var a,d=D.h.addEventListener;if(b.preventLinks){var e=c("a",D.container);for(a=0;a<e.length;a++)d(e[a],"click",n)}if(b.releaseFormElements){var f=c("input, textarea, select",D.container);for(a=0;a<f.length;a++)d(f[a],D.touchEvents.touchStart,o,!0),D.support.touch&&b.simulateTouch&&d(f[a],"mousedown",o,!0)}if(b.onSlideClick)for(a=0;a<D.slides.length;a++)d(D.slides[a],"click",k);if(b.onSlideTouch)for(a=0;a<D.slides.length;a++)d(D.slides[a],D.touchEvents.touchStart,l)}function h(){var a,d=D.h.removeEventListener;if(b.onSlideClick)for(a=0;a<D.slides.length;a++)d(D.slides[a],"click",k);if(b.onSlideTouch)for(a=0;a<D.slides.length;a++)d(D.slides[a],D.touchEvents.touchStart,l);if(b.releaseFormElements){var e=c("input, textarea, select",D.container);for(a=0;a<e.length;a++)d(e[a],D.touchEvents.touchStart,o,!0),D.support.touch&&b.simulateTouch&&d(e[a],"mousedown",o,!0)}if(b.preventLinks){var f=c("a",D.container);for(a=0;a<f.length;a++)d(f[a],"click",n)}}function i(a){var b=a.keyCode||a.charCode;if(!(a.shiftKey||a.altKey||a.ctrlKey||a.metaKey)){if(37===b||39===b||38===b||40===b){for(var c=!1,d=D.h.getOffset(D.container),e=D.h.windowScroll().left,f=D.h.windowScroll().top,g=D.h.windowWidth(),h=D.h.windowHeight(),i=[[d.left,d.top],[d.left+D.width,d.top],[d.left,d.top+D.height],[d.left+D.width,d.top+D.height]],j=0;j<i.length;j++){var k=i[j];k[0]>=e&&k[0]<=e+g&&k[1]>=f&&k[1]<=f+h&&(c=!0)}if(!c)return}N?((37===b||39===b)&&(a.preventDefault?a.preventDefault():a.returnValue=!1),39===b&&D.swipeNext(),37===b&&D.swipePrev()):((38===b||40===b)&&(a.preventDefault?a.preventDefault():a.returnValue=!1),40===b&&D.swipeNext(),38===b&&D.swipePrev())}}function j(a){var c=D._wheelEvent,d=0;if(a.detail)d=-a.detail;else if("mousewheel"===c)if(b.mousewheelControlForceToAxis)if(N){if(!(Math.abs(a.wheelDeltaX)>Math.abs(a.wheelDeltaY)))return;d=a.wheelDeltaX}else{if(!(Math.abs(a.wheelDeltaY)>Math.abs(a.wheelDeltaX)))return;d=a.wheelDeltaY}else d=a.wheelDelta;else if("DOMMouseScroll"===c)d=-a.detail;else if("wheel"===c)if(b.mousewheelControlForceToAxis)if(N){if(!(Math.abs(a.deltaX)>Math.abs(a.deltaY)))return;d=-a.deltaX}else{if(!(Math.abs(a.deltaY)>Math.abs(a.deltaX)))return;d=-a.deltaY}else d=Math.abs(a.deltaX)>Math.abs(a.deltaY)?-a.deltaX:-a.deltaY;if(b.freeMode){var f=D.getWrapperTranslate()+d;if(f>0&&(f=0),f<-e()&&(f=-e()),D.setWrapperTransition(0),D.setWrapperTranslate(f),D.updateActiveSlide(f),0===f||f===-e())return}else(new Date).getTime()-V>60&&(0>d?D.swipeNext():D.swipePrev()),V=(new Date).getTime();return b.autoplay&&D.stopAutoplay(!0),a.preventDefault?a.preventDefault():a.returnValue=!1,!1}function k(a){D.allowSlideClick&&(m(a),D.fireCallback(b.onSlideClick,D,a))}function l(a){m(a),D.fireCallback(b.onSlideTouch,D,a)}function m(a){if(a.currentTarget)D.clickedSlide=a.currentTarget;else{var c=a.srcElement;do{if(c.className.indexOf(b.slideClass)>-1)break;c=c.parentNode}while(c);D.clickedSlide=c}D.clickedSlideIndex=D.slides.indexOf(D.clickedSlide),D.clickedSlideLoopIndex=D.clickedSlideIndex-(D.loopedSlides||0)}function n(a){return D.allowLinks?void 0:(a.preventDefault?a.preventDefault():a.returnValue=!1,b.preventLinksPropagation&&"stopPropagation"in a&&a.stopPropagation(),!1)}function o(a){return a.stopPropagation?a.stopPropagation():a.returnValue=!1,!1}function p(a){if(b.preventLinks&&(D.allowLinks=!0),D.isTouched||b.onlyExternal)return!1;var c=a.target||a.srcElement;document.activeElement&&document.activeElement!==document.body&&document.activeElement!==c&&document.activeElement.blur();var d="input select textarea".split(" ");if(b.noSwiping&&c&&t(c))return!1;if(_=!1,D.isTouched=!0,$="touchstart"===a.type,!$&&"which"in a&&3===a.which)return D.isTouched=!1,!1;if(!$||1===a.targetTouches.length){D.callPlugins("onTouchStartBegin"),!$&&!D.isAndroid&&d.indexOf(c.tagName.toLowerCase())<0&&(a.preventDefault?a.preventDefault():a.returnValue=!1);var e=$?a.targetTouches[0].pageX:a.pageX||a.clientX,f=$?a.targetTouches[0].pageY:a.pageY||a.clientY;D.touches.startX=D.touches.currentX=e,D.touches.startY=D.touches.currentY=f,D.touches.start=D.touches.current=N?e:f,D.setWrapperTransition(0),D.positions.start=D.positions.current=D.getWrapperTranslate(),D.setWrapperTranslate(D.positions.start),D.times.start=(new Date).getTime(),I=void 0,b.moveStartThreshold>0&&(X=!1),b.onTouchStart&&D.fireCallback(b.onTouchStart,D,a),D.callPlugins("onTouchStartEnd")}}function q(a){if(D.isTouched&&!b.onlyExternal&&(!$||"mousemove"!==a.type)){var c=$?a.targetTouches[0].pageX:a.pageX||a.clientX,d=$?a.targetTouches[0].pageY:a.pageY||a.clientY;if("undefined"==typeof I&&N&&(I=!!(I||Math.abs(d-D.touches.startY)>Math.abs(c-D.touches.startX))),"undefined"!=typeof I||N||(I=!!(I||Math.abs(d-D.touches.startY)<Math.abs(c-D.touches.startX))),I)return void(D.isTouched=!1);if(N){if(!b.swipeToNext&&c<D.touches.startX||!b.swipeToPrev&&c>D.touches.startX)return}else if(!b.swipeToNext&&d<D.touches.startY||!b.swipeToPrev&&d>D.touches.startY)return;if(a.assignedToSwiper)return void(D.isTouched=!1);if(a.assignedToSwiper=!0,b.preventLinks&&(D.allowLinks=!1),b.onSlideClick&&(D.allowSlideClick=!1),b.autoplay&&D.stopAutoplay(!0),!$||1===a.touches.length){if(D.isMoved||(D.callPlugins("onTouchMoveStart"),b.loop&&(D.fixLoop(),D.positions.start=D.getWrapperTranslate()),b.onTouchMoveStart&&D.fireCallback(b.onTouchMoveStart,D)),D.isMoved=!0,a.preventDefault?a.preventDefault():a.returnValue=!1,D.touches.current=N?c:d,D.positions.current=(D.touches.current-D.touches.start)*b.touchRatio+D.positions.start,D.positions.current>0&&b.onResistanceBefore&&D.fireCallback(b.onResistanceBefore,D,D.positions.current),D.positions.current<-e()&&b.onResistanceAfter&&D.fireCallback(b.onResistanceAfter,D,Math.abs(D.positions.current+e())),b.resistance&&"100%"!==b.resistance){var f;if(D.positions.current>0&&(f=1-D.positions.current/J/2,D.positions.current=.5>f?J/2:D.positions.current*f),D.positions.current<-e()){var g=(D.touches.current-D.touches.start)*b.touchRatio+(e()+D.positions.start);f=(J+g)/J;var h=D.positions.current-g*(1-f)/2,i=-e()-J/2;D.positions.current=i>h||0>=f?i:h}}if(b.resistance&&"100%"===b.resistance&&(D.positions.current>0&&(!b.freeMode||b.freeModeFluid)&&(D.positions.current=0),D.positions.current<-e()&&(!b.freeMode||b.freeModeFluid)&&(D.positions.current=-e())),!b.followFinger)return;if(b.moveStartThreshold)if(Math.abs(D.touches.current-D.touches.start)>b.moveStartThreshold||X){if(!X)return X=!0,void(D.touches.start=D.touches.current);D.setWrapperTranslate(D.positions.current)}else D.positions.current=D.positions.start;else D.setWrapperTranslate(D.positions.current);return(b.freeMode||b.watchActiveIndex)&&D.updateActiveSlide(D.positions.current),b.grabCursor&&(D.container.style.cursor="move",D.container.style.cursor="grabbing",D.container.style.cursor="-moz-grabbin",D.container.style.cursor="-webkit-grabbing"),Y||(Y=D.touches.current),Z||(Z=(new Date).getTime()),D.velocity=(D.touches.current-Y)/((new Date).getTime()-Z)/2,Math.abs(D.touches.current-Y)<2&&(D.velocity=0),Y=D.touches.current,Z=(new Date).getTime(),D.callPlugins("onTouchMoveEnd"),b.onTouchMove&&D.fireCallback(b.onTouchMove,D,a),!1}}}function r(a){if(I&&D.swipeReset(),!b.onlyExternal&&D.isTouched){D.isTouched=!1,b.grabCursor&&(D.container.style.cursor="move",D.container.style.cursor="grab",D.container.style.cursor="-moz-grab",D.container.style.cursor="-webkit-grab"),D.positions.current||0===D.positions.current||(D.positions.current=D.positions.start),b.followFinger&&D.setWrapperTranslate(D.positions.current),D.times.end=(new Date).getTime(),D.touches.diff=D.touches.current-D.touches.start,D.touches.abs=Math.abs(D.touches.diff),D.positions.diff=D.positions.current-D.positions.start,D.positions.abs=Math.abs(D.positions.diff);var c=D.positions.diff,d=D.positions.abs,f=D.times.end-D.times.start;5>d&&300>f&&D.allowLinks===!1&&(b.freeMode||0===d||D.swipeReset(),b.preventLinks&&(D.allowLinks=!0),b.onSlideClick&&(D.allowSlideClick=!0)),setTimeout(function(){"undefined"!=typeof D&&null!==D&&(b.preventLinks&&(D.allowLinks=!0),b.onSlideClick&&(D.allowSlideClick=!0))},100);var g=e();if(!D.isMoved&&b.freeMode)return D.isMoved=!1,b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),void D.callPlugins("onTouchEnd");if(!D.isMoved||D.positions.current>0||D.positions.current<-g)return D.swipeReset(),b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),void D.callPlugins("onTouchEnd");if(D.isMoved=!1,b.freeMode){if(b.freeModeFluid){var h,i=1e3*b.momentumRatio,j=D.velocity*i,k=D.positions.current+j,l=!1,m=20*Math.abs(D.velocity)*b.momentumBounceRatio;-g>k&&(b.momentumBounce&&D.support.transitions?(-m>k+g&&(k=-g-m),h=-g,l=!0,_=!0):k=-g),k>0&&(b.momentumBounce&&D.support.transitions?(k>m&&(k=m),h=0,l=!0,_=!0):k=0),0!==D.velocity&&(i=Math.abs((k-D.positions.current)/D.velocity)),D.setWrapperTranslate(k),D.setWrapperTransition(i),b.momentumBounce&&l&&D.wrapperTransitionEnd(function(){_&&(b.onMomentumBounce&&D.fireCallback(b.onMomentumBounce,D),D.callPlugins("onMomentumBounce"),D.setWrapperTranslate(h),D.setWrapperTransition(300))}),D.updateActiveSlide(k)}return(!b.freeModeFluid||f>=300)&&D.updateActiveSlide(D.positions.current),b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),void D.callPlugins("onTouchEnd")}H=0>c?"toNext":"toPrev","toNext"===H&&300>=f&&(30>d||!b.shortSwipes?D.swipeReset():D.swipeNext(!0,!0)),"toPrev"===H&&300>=f&&(30>d||!b.shortSwipes?D.swipeReset():D.swipePrev(!0,!0));var n=0;if("auto"===b.slidesPerView){for(var o,p=Math.abs(D.getWrapperTranslate()),q=0,r=0;r<D.slides.length;r++)if(o=N?D.slides[r].getWidth(!0,b.roundLengths):D.slides[r].getHeight(!0,b.roundLengths),q+=o,q>p){n=o;break}n>J&&(n=J)}else n=F*b.slidesPerView;"toNext"===H&&f>300&&(d>=n*b.longSwipesRatio?D.swipeNext(!0,!0):D.swipeReset()),"toPrev"===H&&f>300&&(d>=n*b.longSwipesRatio?D.swipePrev(!0,!0):D.swipeReset()),b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),D.callPlugins("onTouchEnd")}}function s(a,b){return a&&a.getAttribute("class")&&a.getAttribute("class").indexOf(b)>-1}function t(a){var c=!1;do s(a,b.noSwipingClass)&&(c=!0),a=a.parentElement;while(!c&&a.parentElement&&!s(a,b.wrapperClass));return!c&&s(a,b.wrapperClass)&&s(a,b.noSwipingClass)&&(c=!0),c}function u(a,b){var c,d=document.createElement("div");return d.innerHTML=b,c=d.firstChild,c.className+=" "+a,c.outerHTML}function v(a,c,d){function e(){var f=+new Date,l=f-g;h+=i*l/(1e3/60),k="toNext"===j?h>a:a>h,k?(D.setWrapperTranslate(Math.ceil(h)),D._DOMAnimating=!0,window.setTimeout(function(){e()},1e3/60)):(b.onSlideChangeEnd&&("to"===c?d.runCallbacks===!0&&D.fireCallback(b.onSlideChangeEnd,D,j):D.fireCallback(b.onSlideChangeEnd,D,j)),D.setWrapperTranslate(a),D._DOMAnimating=!1)}var f="to"===c&&d.speed>=0?d.speed:b.speed,g=+new Date;if(D.support.transitions||!b.DOMAnimation)D.setWrapperTranslate(a),D.setWrapperTransition(f);else{var h=D.getWrapperTranslate(),i=Math.ceil((a-h)/f*(1e3/60)),j=h>a?"toNext":"toPrev",k="toNext"===j?h>a:a>h;if(D._DOMAnimating)return;e()}D.updateActiveSlide(a),b.onSlideNext&&"next"===c&&d.runCallbacks===!0&&D.fireCallback(b.onSlideNext,D,a),b.onSlidePrev&&"prev"===c&&d.runCallbacks===!0&&D.fireCallback(b.onSlidePrev,D,a),b.onSlideReset&&"reset"===c&&d.runCallbacks===!0&&D.fireCallback(b.onSlideReset,D,a),"next"!==c&&"prev"!==c&&"to"!==c||d.runCallbacks!==!0||w(c)}function w(a){if(D.callPlugins("onSlideChangeStart"),b.onSlideChangeStart)if(b.queueStartCallbacks&&D.support.transitions){if(D._queueStartCallbacks)return;D._queueStartCallbacks=!0,D.fireCallback(b.onSlideChangeStart,D,a),D.wrapperTransitionEnd(function(){D._queueStartCallbacks=!1})}else D.fireCallback(b.onSlideChangeStart,D,a);if(b.onSlideChangeEnd)if(D.support.transitions)if(b.queueEndCallbacks){if(D._queueEndCallbacks)return;D._queueEndCallbacks=!0,D.wrapperTransitionEnd(function(c){D.fireCallback(b.onSlideChangeEnd,c,a)})}else D.wrapperTransitionEnd(function(c){D.fireCallback(b.onSlideChangeEnd,c,a)});else b.DOMAnimation||setTimeout(function(){D.fireCallback(b.onSlideChangeEnd,D,a)},10)}function x(){var a=D.paginationButtons;if(a)for(var b=0;b<a.length;b++)D.h.removeEventListener(a[b],"click",z)}function y(){var a=D.paginationButtons;if(a)for(var b=0;b<a.length;b++)D.h.addEventListener(a[b],"click",z)}function z(a){for(var c,d=a.target||a.srcElement,e=D.paginationButtons,f=0;f<e.length;f++)d===e[f]&&(c=f);b.autoplay&&D.stopAutoplay(!0),D.swipeTo(c)}function A(){ab=setTimeout(function(){b.loop?(D.fixLoop(),D.swipeNext(!0,!0)):D.swipeNext(!0,!0)||(b.autoplayStopOnLast?(clearTimeout(ab),ab=void 0):D.swipeTo(0)),D.wrapperTransitionEnd(function(){"undefined"!=typeof ab&&A()})},b.autoplay)}function B(){D.calcSlides(),b.loader.slides.length>0&&0===D.slides.length&&D.loadSlides(),b.loop&&D.createLoop(),D.init(),f(),b.pagination&&D.createPagination(!0),b.loop||b.initialSlide>0?D.swipeTo(b.initialSlide,0,!1):D.updateActiveSlide(0),b.autoplay&&D.startAutoplay(),D.centerIndex=D.activeIndex,b.onSwiperCreated&&D.fireCallback(b.onSwiperCreated,D),D.callPlugins("onSwiperCreated")}if(!document.body.outerHTML&&document.body.__defineGetter__&&HTMLElement){var C=HTMLElement.prototype;C.__defineGetter__&&C.__defineGetter__("outerHTML",function(){return(new XMLSerializer).serializeToString(this)})}if(window.getComputedStyle||(window.getComputedStyle=function(a){return this.el=a,this.getPropertyValue=function(b){var c=/(\-([a-z]){1})/g;return"float"===b&&(b="styleFloat"),c.test(b)&&(b=b.replace(c,function(){return arguments[2].toUpperCase()})),a.currentStyle[b]?a.currentStyle[b]:null},this}),Array.prototype.indexOf||(Array.prototype.indexOf=function(a,b){for(var c=b||0,d=this.length;d>c;c++)if(this[c]===a)return c;return-1}),(document.querySelectorAll||window.jQuery)&&"undefined"!=typeof a&&(a.nodeType||0!==c(a).length)){var D=this;D.touches={start:0,startX:0,startY:0,current:0,currentX:0,currentY:0,diff:0,abs:0},D.positions={start:0,abs:0,diff:0,current:0},D.times={start:0,end:0},D.id=(new Date).getTime(),D.container=a.nodeType?a:c(a)[0],D.isTouched=!1,D.isMoved=!1,D.activeIndex=0,D.centerIndex=0,D.activeLoaderIndex=0,D.activeLoopIndex=0,D.previousIndex=null,D.velocity=0,D.snapGrid=[],D.slidesGrid=[],D.imagesToLoad=[],D.imagesLoaded=0,D.wrapperLeft=0,D.wrapperRight=0,D.wrapperTop=0,D.wrapperBottom=0,D.isAndroid=navigator.userAgent.toLowerCase().indexOf("android")>=0;var E,F,G,H,I,J,K={eventTarget:"wrapper",mode:"horizontal",touchRatio:1,speed:300,freeMode:!1,freeModeFluid:!1,momentumRatio:1,momentumBounce:!0,momentumBounceRatio:1,slidesPerView:1,slidesPerGroup:1,slidesPerViewFit:!0,simulateTouch:!0,followFinger:!0,shortSwipes:!0,longSwipesRatio:.5,moveStartThreshold:!1,onlyExternal:!1,createPagination:!0,pagination:!1,paginationElement:"span",paginationClickable:!1,paginationAsRange:!0,resistance:!0,scrollContainer:!1,preventLinks:!0,preventLinksPropagation:!1,noSwiping:!1,noSwipingClass:"swiper-no-swiping",initialSlide:0,keyboardControl:!1,mousewheelControl:!1,mousewheelControlForceToAxis:!1,useCSS3Transforms:!0,autoplay:!1,autoplayDisableOnInteraction:!0,autoplayStopOnLast:!1,loop:!1,loopAdditionalSlides:0,roundLengths:!1,calculateHeight:!1,cssWidthAndHeight:!1,updateOnImagesReady:!0,releaseFormElements:!0,watchActiveIndex:!1,visibilityFullFit:!1,offsetPxBefore:0,offsetPxAfter:0,offsetSlidesBefore:0,offsetSlidesAfter:0,centeredSlides:!1,queueStartCallbacks:!1,queueEndCallbacks:!1,autoResize:!0,resizeReInit:!1,DOMAnimation:!0,loader:{slides:[],slidesHTMLType:"inner",surroundGroups:1,logic:"reload",loadAllSlides:!1},swipeToPrev:!0,swipeToNext:!0,slideElement:"div",slideClass:"swiper-slide",slideActiveClass:"swiper-slide-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",wrapperClass:"swiper-wrapper",paginationElementClass:"swiper-pagination-switch",paginationActiveClass:"swiper-active-switch",paginationVisibleClass:"swiper-visible-switch"};b=b||{};for(var L in K)if(L in b&&"object"==typeof b[L])for(var M in K[L])M in b[L]||(b[L][M]=K[L][M]);else L in b||(b[L]=K[L]);D.params=b,b.scrollContainer&&(b.freeMode=!0,b.freeModeFluid=!0),b.loop&&(b.resistance="100%");var N="horizontal"===b.mode,O=["mousedown","mousemove","mouseup"];D.browser.ie10&&(O=["MSPointerDown","MSPointerMove","MSPointerUp"]),D.browser.ie11&&(O=["pointerdown","pointermove","pointerup"]),D.touchEvents={touchStart:D.support.touch||!b.simulateTouch?"touchstart":O[0],touchMove:D.support.touch||!b.simulateTouch?"touchmove":O[1],touchEnd:D.support.touch||!b.simulateTouch?"touchend":O[2]};for(var P=D.container.childNodes.length-1;P>=0;P--)if(D.container.childNodes[P].className)for(var Q=D.container.childNodes[P].className.split(/\s+/),R=0;R<Q.length;R++)Q[R]===b.wrapperClass&&(E=D.container.childNodes[P]);D.wrapper=E,D._extendSwiperSlide=function(a){return a.append=function(){return b.loop?a.insertAfter(D.slides.length-D.loopedSlides):(D.wrapper.appendChild(a),D.reInit()),a},a.prepend=function(){return b.loop?(D.wrapper.insertBefore(a,D.slides[D.loopedSlides]),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()):D.wrapper.insertBefore(a,D.wrapper.firstChild),D.reInit(),a},a.insertAfter=function(c){if("undefined"==typeof c)return!1;var d;return b.loop?(d=D.slides[c+1+D.loopedSlides],d?D.wrapper.insertBefore(a,d):D.wrapper.appendChild(a),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()):(d=D.slides[c+1],D.wrapper.insertBefore(a,d)),D.reInit(),a},a.clone=function(){return D._extendSwiperSlide(a.cloneNode(!0))},a.remove=function(){D.wrapper.removeChild(a),D.reInit()},a.html=function(b){return"undefined"==typeof b?a.innerHTML:(a.innerHTML=b,a)},a.index=function(){for(var b,c=D.slides.length-1;c>=0;c--)a===D.slides[c]&&(b=c);return b},a.isActive=function(){return a.index()===D.activeIndex?!0:!1},a.swiperSlideDataStorage||(a.swiperSlideDataStorage={}),a.getData=function(b){return a.swiperSlideDataStorage[b]},a.setData=function(b,c){return a.swiperSlideDataStorage[b]=c,a},a.data=function(b,c){return"undefined"==typeof c?a.getAttribute("data-"+b):(a.setAttribute("data-"+b,c),a)},a.getWidth=function(b,c){return D.h.getWidth(a,b,c)},a.getHeight=function(b,c){return D.h.getHeight(a,b,c)},a.getOffset=function(){return D.h.getOffset(a)},a},D.calcSlides=function(a){var c=D.slides?D.slides.length:!1;D.slides=[],D.displaySlides=[];for(var d=0;d<D.wrapper.childNodes.length;d++)if(D.wrapper.childNodes[d].className)for(var e=D.wrapper.childNodes[d].className,f=e.split(/\s+/),i=0;i<f.length;i++)f[i]===b.slideClass&&D.slides.push(D.wrapper.childNodes[d]);for(d=D.slides.length-1;d>=0;d--)D._extendSwiperSlide(D.slides[d]);c!==!1&&(c!==D.slides.length||a)&&(h(),g(),D.updateActiveSlide(),D.params.pagination&&D.createPagination(),D.callPlugins("numberOfSlidesChanged"))},D.createSlide=function(a,c,d){c=c||D.params.slideClass,d=d||b.slideElement;var e=document.createElement(d);return e.innerHTML=a||"",e.className=c,D._extendSwiperSlide(e)},D.appendSlide=function(a,b,c){return a?a.nodeType?D._extendSwiperSlide(a).append():D.createSlide(a,b,c).append():void 0},D.prependSlide=function(a,b,c){return a?a.nodeType?D._extendSwiperSlide(a).prepend():D.createSlide(a,b,c).prepend():void 0},D.insertSlideAfter=function(a,b,c,d){return"undefined"==typeof a?!1:b.nodeType?D._extendSwiperSlide(b).insertAfter(a):D.createSlide(b,c,d).insertAfter(a)},D.removeSlide=function(a){if(D.slides[a]){if(b.loop){if(!D.slides[a+D.loopedSlides])return!1;D.slides[a+D.loopedSlides].remove(),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()}else D.slides[a].remove();return!0}return!1},D.removeLastSlide=function(){return D.slides.length>0?(b.loop?(D.slides[D.slides.length-1-D.loopedSlides].remove(),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()):D.slides[D.slides.length-1].remove(),!0):!1},D.removeAllSlides=function(){for(var a=D.slides.length,b=D.slides.length-1;b>=0;b--)D.slides[b].remove(),b===a-1&&D.setWrapperTranslate(0)},D.getSlide=function(a){return D.slides[a]},D.getLastSlide=function(){return D.slides[D.slides.length-1]},D.getFirstSlide=function(){return D.slides[0]},D.activeSlide=function(){return D.slides[D.activeIndex]},D.fireCallback=function(){var a=arguments[0];if("[object Array]"===Object.prototype.toString.call(a))for(var c=0;c<a.length;c++)"function"==typeof a[c]&&a[c](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);else"[object String]"===Object.prototype.toString.call(a)?b["on"+a]&&D.fireCallback(b["on"+a],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]):a(arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},D.addCallback=function(a,b){var c,e=this;return e.params["on"+a]?d(this.params["on"+a])?this.params["on"+a].push(b):"function"==typeof this.params["on"+a]?(c=this.params["on"+a],this.params["on"+a]=[],this.params["on"+a].push(c),this.params["on"+a].push(b)):void 0:(this.params["on"+a]=[],this.params["on"+a].push(b))},D.removeCallbacks=function(a){D.params["on"+a]&&(D.params["on"+a]=null)};var S=[];for(var T in D.plugins)if(b[T]){var U=D.plugins[T](D,b[T]);U&&S.push(U)}D.callPlugins=function(a,b){b||(b={});for(var c=0;c<S.length;c++)a in S[c]&&S[c][a](b)},!D.browser.ie10&&!D.browser.ie11||b.onlyExternal||D.wrapper.classList.add("swiper-wp8-"+(N?"horizontal":"vertical")),b.freeMode&&(D.container.className+=" swiper-free-mode"),D.initialized=!1,D.init=function(a,c){var d=D.h.getWidth(D.container,!1,b.roundLengths),e=D.h.getHeight(D.container,!1,b.roundLengths);if(d!==D.width||e!==D.height||a){D.width=d,D.height=e;var f,g,h,i,j,k,l;J=N?d:e;var m=D.wrapper;if(a&&D.calcSlides(c),"auto"===b.slidesPerView){var n=0,o=0;b.slidesOffset>0&&(m.style.paddingLeft="",m.style.paddingRight="",m.style.paddingTop="",m.style.paddingBottom=""),m.style.width="",m.style.height="",b.offsetPxBefore>0&&(N?D.wrapperLeft=b.offsetPxBefore:D.wrapperTop=b.offsetPxBefore),b.offsetPxAfter>0&&(N?D.wrapperRight=b.offsetPxAfter:D.wrapperBottom=b.offsetPxAfter),b.centeredSlides&&(N?(D.wrapperLeft=(J-this.slides[0].getWidth(!0,b.roundLengths))/2,D.wrapperRight=(J-D.slides[D.slides.length-1].getWidth(!0,b.roundLengths))/2):(D.wrapperTop=(J-D.slides[0].getHeight(!0,b.roundLengths))/2,D.wrapperBottom=(J-D.slides[D.slides.length-1].getHeight(!0,b.roundLengths))/2)),N?(D.wrapperLeft>=0&&(m.style.paddingLeft=D.wrapperLeft+"px"),D.wrapperRight>=0&&(m.style.paddingRight=D.wrapperRight+"px")):(D.wrapperTop>=0&&(m.style.paddingTop=D.wrapperTop+"px"),D.wrapperBottom>=0&&(m.style.paddingBottom=D.wrapperBottom+"px")),k=0;var p=0;for(D.snapGrid=[],D.slidesGrid=[],h=0,l=0;l<D.slides.length;l++){f=D.slides[l].getWidth(!0,b.roundLengths),g=D.slides[l].getHeight(!0,b.roundLengths),b.calculateHeight&&(h=Math.max(h,g));var q=N?f:g;if(b.centeredSlides){var r=l===D.slides.length-1?0:D.slides[l+1].getWidth(!0,b.roundLengths),s=l===D.slides.length-1?0:D.slides[l+1].getHeight(!0,b.roundLengths),t=N?r:s;if(q>J){if(b.slidesPerViewFit)D.snapGrid.push(k+D.wrapperLeft),D.snapGrid.push(k+q-J+D.wrapperLeft);else for(var u=0;u<=Math.floor(q/(J+D.wrapperLeft));u++)D.snapGrid.push(0===u?k+D.wrapperLeft:k+D.wrapperLeft+J*u);D.slidesGrid.push(k+D.wrapperLeft)}else D.snapGrid.push(p),D.slidesGrid.push(p);p+=q/2+t/2}else{if(q>J)if(b.slidesPerViewFit)D.snapGrid.push(k),D.snapGrid.push(k+q-J);else if(0!==J)for(var v=0;v<=Math.floor(q/J);v++)D.snapGrid.push(k+J*v);else D.snapGrid.push(k);else D.snapGrid.push(k);D.slidesGrid.push(k)}k+=q,n+=f,o+=g}b.calculateHeight&&(D.height=h),N?(G=n+D.wrapperRight+D.wrapperLeft,b.cssWidthAndHeight&&"height"!==b.cssWidthAndHeight||(m.style.width=n+"px"),b.cssWidthAndHeight&&"width"!==b.cssWidthAndHeight||(m.style.height=D.height+"px")):(b.cssWidthAndHeight&&"height"!==b.cssWidthAndHeight||(m.style.width=D.width+"px"),b.cssWidthAndHeight&&"width"!==b.cssWidthAndHeight||(m.style.height=o+"px"),G=o+D.wrapperTop+D.wrapperBottom)}else if(b.scrollContainer)m.style.width="",m.style.height="",i=D.slides[0].getWidth(!0,b.roundLengths),j=D.slides[0].getHeight(!0,b.roundLengths),G=N?i:j,m.style.width=i+"px",m.style.height=j+"px",F=N?i:j;else{if(b.calculateHeight){for(h=0,j=0,N||(D.container.style.height=""),m.style.height="",l=0;l<D.slides.length;l++)D.slides[l].style.height="",h=Math.max(D.slides[l].getHeight(!0),h),N||(j+=D.slides[l].getHeight(!0));g=h,D.height=g,N?j=g:(J=g,D.container.style.height=J+"px")}else g=N?D.height:D.height/b.slidesPerView,b.roundLengths&&(g=Math.ceil(g)),j=N?D.height:D.slides.length*g;for(f=N?D.width/b.slidesPerView:D.width,b.roundLengths&&(f=Math.ceil(f)),i=N?D.slides.length*f:D.width,F=N?f:g,b.offsetSlidesBefore>0&&(N?D.wrapperLeft=F*b.offsetSlidesBefore:D.wrapperTop=F*b.offsetSlidesBefore),b.offsetSlidesAfter>0&&(N?D.wrapperRight=F*b.offsetSlidesAfter:D.wrapperBottom=F*b.offsetSlidesAfter),b.offsetPxBefore>0&&(N?D.wrapperLeft=b.offsetPxBefore:D.wrapperTop=b.offsetPxBefore),b.offsetPxAfter>0&&(N?D.wrapperRight=b.offsetPxAfter:D.wrapperBottom=b.offsetPxAfter),b.centeredSlides&&(N?(D.wrapperLeft=(J-F)/2,D.wrapperRight=(J-F)/2):(D.wrapperTop=(J-F)/2,D.wrapperBottom=(J-F)/2)),N?(D.wrapperLeft>0&&(m.style.paddingLeft=D.wrapperLeft+"px"),D.wrapperRight>0&&(m.style.paddingRight=D.wrapperRight+"px")):(D.wrapperTop>0&&(m.style.paddingTop=D.wrapperTop+"px"),D.wrapperBottom>0&&(m.style.paddingBottom=D.wrapperBottom+"px")),G=N?i+D.wrapperRight+D.wrapperLeft:j+D.wrapperTop+D.wrapperBottom,parseFloat(i)>0&&(!b.cssWidthAndHeight||"height"===b.cssWidthAndHeight)&&(m.style.width=i+"px"),parseFloat(j)>0&&(!b.cssWidthAndHeight||"width"===b.cssWidthAndHeight)&&(m.style.height=j+"px"),k=0,D.snapGrid=[],D.slidesGrid=[],l=0;l<D.slides.length;l++)D.snapGrid.push(k),D.slidesGrid.push(k),k+=F,parseFloat(f)>0&&(!b.cssWidthAndHeight||"height"===b.cssWidthAndHeight)&&(D.slides[l].style.width=f+"px"),parseFloat(g)>0&&(!b.cssWidthAndHeight||"width"===b.cssWidthAndHeight)&&(D.slides[l].style.height=g+"px")}D.initialized?(D.callPlugins("onInit"),b.onInit&&D.fireCallback(b.onInit,D)):(D.callPlugins("onFirstInit"),b.onFirstInit&&D.fireCallback(b.onFirstInit,D)),D.initialized=!0}},D.reInit=function(a){D.init(!0,a)},D.resizeFix=function(a){D.callPlugins("beforeResizeFix"),D.init(b.resizeReInit||a),b.freeMode?D.getWrapperTranslate()<-e()&&(D.setWrapperTransition(0),D.setWrapperTranslate(-e())):(D.swipeTo(b.loop?D.activeLoopIndex:D.activeIndex,0,!1),b.autoplay&&(D.support.transitions&&"undefined"!=typeof ab?"undefined"!=typeof ab&&(clearTimeout(ab),ab=void 0,D.startAutoplay()):"undefined"!=typeof bb&&(clearInterval(bb),bb=void 0,D.startAutoplay()))),D.callPlugins("afterResizeFix")},D.destroy=function(a){var c=D.h.removeEventListener,d="wrapper"===b.eventTarget?D.wrapper:D.container;if(D.browser.ie10||D.browser.ie11?(c(d,D.touchEvents.touchStart,p),c(document,D.touchEvents.touchMove,q),c(document,D.touchEvents.touchEnd,r)):(D.support.touch&&(c(d,"touchstart",p),c(d,"touchmove",q),c(d,"touchend",r)),b.simulateTouch&&(c(d,"mousedown",p),c(document,"mousemove",q),c(document,"mouseup",r))),b.autoResize&&c(window,"resize",D.resizeFix),h(),b.paginationClickable&&x(),b.mousewheelControl&&D._wheelEvent&&c(D.container,D._wheelEvent,j),b.keyboardControl&&c(document,"keydown",i),b.autoplay&&D.stopAutoplay(),a){D.wrapper.removeAttribute("style");for(var e=0;e<D.slides.length;e++)D.slides[e].removeAttribute("style")}D.callPlugins("onDestroy"),window.jQuery&&window.jQuery(D.container).data("swiper")&&window.jQuery(D.container).removeData("swiper"),window.Zepto&&window.Zepto(D.container).data("swiper")&&window.Zepto(D.container).removeData("swiper"),D=null},D.disableKeyboardControl=function(){b.keyboardControl=!1,D.h.removeEventListener(document,"keydown",i)},D.enableKeyboardControl=function(){b.keyboardControl=!0,D.h.addEventListener(document,"keydown",i)};var V=(new Date).getTime();if(D.disableMousewheelControl=function(){return D._wheelEvent?(b.mousewheelControl=!1,D.h.removeEventListener(D.container,D._wheelEvent,j),!0):!1},D.enableMousewheelControl=function(){return D._wheelEvent?(b.mousewheelControl=!0,D.h.addEventListener(D.container,D._wheelEvent,j),!0):!1},b.grabCursor){var W=D.container.style;W.cursor="move",W.cursor="grab",W.cursor="-moz-grab",W.cursor="-webkit-grab"}D.allowSlideClick=!0,D.allowLinks=!0;var X,Y,Z,$=!1,_=!0;D.swipeNext=function(a,c){"undefined"==typeof a&&(a=!0),!c&&b.loop&&D.fixLoop(),!c&&b.autoplay&&D.stopAutoplay(!0),D.callPlugins("onSwipeNext");var d=D.getWrapperTranslate().toFixed(2),f=d;if("auto"===b.slidesPerView){for(var g=0;g<D.snapGrid.length;g++)if(-d>=D.snapGrid[g].toFixed(2)&&-d<D.snapGrid[g+1].toFixed(2)){f=-D.snapGrid[g+1];break}}else{var h=F*b.slidesPerGroup;f=-(Math.floor(Math.abs(d)/Math.floor(h))*h+h)}return f<-e()&&(f=-e()),f===d?!1:(v(f,"next",{runCallbacks:a}),!0)},D.swipePrev=function(a,c){"undefined"==typeof a&&(a=!0),!c&&b.loop&&D.fixLoop(),!c&&b.autoplay&&D.stopAutoplay(!0),D.callPlugins("onSwipePrev");var d,e=Math.ceil(D.getWrapperTranslate());if("auto"===b.slidesPerView){d=0;for(var f=1;f<D.snapGrid.length;f++){if(-e===D.snapGrid[f]){d=-D.snapGrid[f-1];break}if(-e>D.snapGrid[f]&&-e<D.snapGrid[f+1]){d=-D.snapGrid[f];break}}}else{var g=F*b.slidesPerGroup;d=-(Math.ceil(-e/g)-1)*g}return d>0&&(d=0),d===e?!1:(v(d,"prev",{runCallbacks:a}),!0)},D.swipeReset=function(a){"undefined"==typeof a&&(a=!0),D.callPlugins("onSwipeReset");{var c,d=D.getWrapperTranslate(),f=F*b.slidesPerGroup;-e()}if("auto"===b.slidesPerView){c=0;for(var g=0;g<D.snapGrid.length;g++){if(-d===D.snapGrid[g])return;if(-d>=D.snapGrid[g]&&-d<D.snapGrid[g+1]){c=D.positions.diff>0?-D.snapGrid[g+1]:-D.snapGrid[g];break}}-d>=D.snapGrid[D.snapGrid.length-1]&&(c=-D.snapGrid[D.snapGrid.length-1]),d<=-e()&&(c=-e())}else c=0>d?Math.round(d/f)*f:0,d<=-e()&&(c=-e());return b.scrollContainer&&(c=0>d?d:0),c<-e()&&(c=-e()),b.scrollContainer&&J>F&&(c=0),c===d?!1:(v(c,"reset",{runCallbacks:a}),!0)},D.swipeTo=function(a,c,d){a=parseInt(a,10),D.callPlugins("onSwipeTo",{index:a,speed:c}),b.loop&&(a+=D.loopedSlides);var f=D.getWrapperTranslate();if(!(!isFinite(a)||a>D.slides.length-1||0>a)){var g;return g="auto"===b.slidesPerView?-D.slidesGrid[a]:-a*F,g<-e()&&(g=-e()),g===f?!1:("undefined"==typeof d&&(d=!0),v(g,"to",{index:a,speed:c,runCallbacks:d}),!0)}},D._queueStartCallbacks=!1,D._queueEndCallbacks=!1,D.updateActiveSlide=function(a){if(D.initialized&&0!==D.slides.length){D.previousIndex=D.activeIndex,"undefined"==typeof a&&(a=D.getWrapperTranslate()),a>0&&(a=0);var c;if("auto"===b.slidesPerView){if(D.activeIndex=D.slidesGrid.indexOf(-a),D.activeIndex<0){for(c=0;c<D.slidesGrid.length-1&&!(-a>D.slidesGrid[c]&&-a<D.slidesGrid[c+1]);c++);var d=Math.abs(D.slidesGrid[c]+a),e=Math.abs(D.slidesGrid[c+1]+a);
    D.activeIndex=e>=d?c:c+1}}else D.activeIndex=Math[b.visibilityFullFit?"ceil":"round"](-a/F);if(D.activeIndex===D.slides.length&&(D.activeIndex=D.slides.length-1),D.activeIndex<0&&(D.activeIndex=0),D.slides[D.activeIndex]){if(D.calcVisibleSlides(a),D.support.classList){var f;for(c=0;c<D.slides.length;c++)f=D.slides[c],f.classList.remove(b.slideActiveClass),D.visibleSlides.indexOf(f)>=0?f.classList.add(b.slideVisibleClass):f.classList.remove(b.slideVisibleClass);D.slides[D.activeIndex].classList.add(b.slideActiveClass)}else{var g=new RegExp("\\s*"+b.slideActiveClass),h=new RegExp("\\s*"+b.slideVisibleClass);for(c=0;c<D.slides.length;c++)D.slides[c].className=D.slides[c].className.replace(g,"").replace(h,""),D.visibleSlides.indexOf(D.slides[c])>=0&&(D.slides[c].className+=" "+b.slideVisibleClass);D.slides[D.activeIndex].className+=" "+b.slideActiveClass}if(b.loop){var i=D.loopedSlides;D.activeLoopIndex=D.activeIndex-i,D.activeLoopIndex>=D.slides.length-2*i&&(D.activeLoopIndex=D.slides.length-2*i-D.activeLoopIndex),D.activeLoopIndex<0&&(D.activeLoopIndex=D.slides.length-2*i+D.activeLoopIndex),D.activeLoopIndex<0&&(D.activeLoopIndex=0)}else D.activeLoopIndex=D.activeIndex;b.pagination&&D.updatePagination(a)}}},D.createPagination=function(a){if(b.paginationClickable&&D.paginationButtons&&x(),D.paginationContainer=b.pagination.nodeType?b.pagination:c(b.pagination)[0],b.createPagination){var d="",e=D.slides.length,f=e;b.loop&&(f-=2*D.loopedSlides);for(var g=0;f>g;g++)d+="<"+b.paginationElement+' class="'+b.paginationElementClass+'"></'+b.paginationElement+">";D.paginationContainer.innerHTML=d}D.paginationButtons=c("."+b.paginationElementClass,D.paginationContainer),a||D.updatePagination(),D.callPlugins("onCreatePagination"),b.paginationClickable&&y()},D.updatePagination=function(a){if(b.pagination&&!(D.slides.length<1)){var d=c("."+b.paginationActiveClass,D.paginationContainer);if(d){var e=D.paginationButtons;if(0!==e.length){for(var f=0;f<e.length;f++)e[f].className=b.paginationElementClass;var g=b.loop?D.loopedSlides:0;if(b.paginationAsRange){D.visibleSlides||D.calcVisibleSlides(a);var h,i=[];for(h=0;h<D.visibleSlides.length;h++){var j=D.slides.indexOf(D.visibleSlides[h])-g;b.loop&&0>j&&(j=D.slides.length-2*D.loopedSlides+j),b.loop&&j>=D.slides.length-2*D.loopedSlides&&(j=D.slides.length-2*D.loopedSlides-j,j=Math.abs(j)),i.push(j)}for(h=0;h<i.length;h++)e[i[h]]&&(e[i[h]].className+=" "+b.paginationVisibleClass);b.loop?void 0!==e[D.activeLoopIndex]&&(e[D.activeLoopIndex].className+=" "+b.paginationActiveClass):e[D.activeIndex]&&(e[D.activeIndex].className+=" "+b.paginationActiveClass)}else b.loop?e[D.activeLoopIndex]&&(e[D.activeLoopIndex].className+=" "+b.paginationActiveClass+" "+b.paginationVisibleClass):e[D.activeIndex]&&(e[D.activeIndex].className+=" "+b.paginationActiveClass+" "+b.paginationVisibleClass)}}}},D.calcVisibleSlides=function(a){var c=[],d=0,e=0,f=0;N&&D.wrapperLeft>0&&(a+=D.wrapperLeft),!N&&D.wrapperTop>0&&(a+=D.wrapperTop);for(var g=0;g<D.slides.length;g++){d+=e,e="auto"===b.slidesPerView?N?D.h.getWidth(D.slides[g],!0,b.roundLengths):D.h.getHeight(D.slides[g],!0,b.roundLengths):F,f=d+e;var h=!1;b.visibilityFullFit?(d>=-a&&-a+J>=f&&(h=!0),-a>=d&&f>=-a+J&&(h=!0)):(f>-a&&-a+J>=f&&(h=!0),d>=-a&&-a+J>d&&(h=!0),-a>d&&f>-a+J&&(h=!0)),h&&c.push(D.slides[g])}0===c.length&&(c=[D.slides[D.activeIndex]]),D.visibleSlides=c};var ab,bb;D.startAutoplay=function(){if(D.support.transitions){if("undefined"!=typeof ab)return!1;if(!b.autoplay)return;D.callPlugins("onAutoplayStart"),b.onAutoplayStart&&D.fireCallback(b.onAutoplayStart,D),A()}else{if("undefined"!=typeof bb)return!1;if(!b.autoplay)return;D.callPlugins("onAutoplayStart"),b.onAutoplayStart&&D.fireCallback(b.onAutoplayStart,D),bb=setInterval(function(){b.loop?(D.fixLoop(),D.swipeNext(!0,!0)):D.swipeNext(!0,!0)||(b.autoplayStopOnLast?(clearInterval(bb),bb=void 0):D.swipeTo(0))},b.autoplay)}},D.stopAutoplay=function(a){if(D.support.transitions){if(!ab)return;ab&&clearTimeout(ab),ab=void 0,a&&!b.autoplayDisableOnInteraction&&D.wrapperTransitionEnd(function(){A()}),D.callPlugins("onAutoplayStop"),b.onAutoplayStop&&D.fireCallback(b.onAutoplayStop,D)}else bb&&clearInterval(bb),bb=void 0,D.callPlugins("onAutoplayStop"),b.onAutoplayStop&&D.fireCallback(b.onAutoplayStop,D)},D.loopCreated=!1,D.removeLoopedSlides=function(){if(D.loopCreated)for(var a=0;a<D.slides.length;a++)D.slides[a].getData("looped")===!0&&D.wrapper.removeChild(D.slides[a])},D.createLoop=function(){if(0!==D.slides.length){D.loopedSlides="auto"===b.slidesPerView?b.loopedSlides||1:Math.floor(b.slidesPerView)+b.loopAdditionalSlides,D.loopedSlides>D.slides.length&&(D.loopedSlides=D.slides.length);var a,c="",d="",e="",f=D.slides.length,g=Math.floor(D.loopedSlides/f),h=D.loopedSlides%f;for(a=0;g*f>a;a++){var i=a;if(a>=f){var j=Math.floor(a/f);i=a-f*j}e+=D.slides[i].outerHTML}for(a=0;h>a;a++)d+=u(b.slideDuplicateClass,D.slides[a].outerHTML);for(a=f-h;f>a;a++)c+=u(b.slideDuplicateClass,D.slides[a].outerHTML);var k=c+e+E.innerHTML+e+d;for(E.innerHTML=k,D.loopCreated=!0,D.calcSlides(),a=0;a<D.slides.length;a++)(a<D.loopedSlides||a>=D.slides.length-D.loopedSlides)&&D.slides[a].setData("looped",!0);D.callPlugins("onCreateLoop")}},D.fixLoop=function(){var a;D.activeIndex<D.loopedSlides?(a=D.slides.length-3*D.loopedSlides+D.activeIndex,D.swipeTo(a,0,!1)):("auto"===b.slidesPerView&&D.activeIndex>=2*D.loopedSlides||D.activeIndex>D.slides.length-2*b.slidesPerView)&&(a=-D.slides.length+D.activeIndex+D.loopedSlides,D.swipeTo(a,0,!1))},D.loadSlides=function(){var a="";D.activeLoaderIndex=0;for(var c=b.loader.slides,d=b.loader.loadAllSlides?c.length:b.slidesPerView*(1+b.loader.surroundGroups),e=0;d>e;e++)a+="outer"===b.loader.slidesHTMLType?c[e]:"<"+b.slideElement+' class="'+b.slideClass+'" data-swiperindex="'+e+'">'+c[e]+"</"+b.slideElement+">";D.wrapper.innerHTML=a,D.calcSlides(!0),b.loader.loadAllSlides||D.wrapperTransitionEnd(D.reloadSlides,!0)},D.reloadSlides=function(){var a=b.loader.slides,c=parseInt(D.activeSlide().data("swiperindex"),10);if(!(0>c||c>a.length-1)){D.activeLoaderIndex=c;var d=Math.max(0,c-b.slidesPerView*b.loader.surroundGroups),e=Math.min(c+b.slidesPerView*(1+b.loader.surroundGroups)-1,a.length-1);if(c>0){var f=-F*(c-d);D.setWrapperTranslate(f),D.setWrapperTransition(0)}var g;if("reload"===b.loader.logic){D.wrapper.innerHTML="";var h="";for(g=d;e>=g;g++)h+="outer"===b.loader.slidesHTMLType?a[g]:"<"+b.slideElement+' class="'+b.slideClass+'" data-swiperindex="'+g+'">'+a[g]+"</"+b.slideElement+">";D.wrapper.innerHTML=h}else{var i=1e3,j=0;for(g=0;g<D.slides.length;g++){var k=D.slides[g].data("swiperindex");d>k||k>e?D.wrapper.removeChild(D.slides[g]):(i=Math.min(k,i),j=Math.max(k,j))}for(g=d;e>=g;g++){var l;i>g&&(l=document.createElement(b.slideElement),l.className=b.slideClass,l.setAttribute("data-swiperindex",g),l.innerHTML=a[g],D.wrapper.insertBefore(l,D.wrapper.firstChild)),g>j&&(l=document.createElement(b.slideElement),l.className=b.slideClass,l.setAttribute("data-swiperindex",g),l.innerHTML=a[g],D.wrapper.appendChild(l))}}D.reInit(!0)}},B()}};Swiper.prototype={plugins:{},wrapperTransitionEnd:function(a,b){"use strict";function c(h){if(h.target===f&&(a(e),e.params.queueEndCallbacks&&(e._queueEndCallbacks=!1),!b))for(d=0;d<g.length;d++)e.h.removeEventListener(f,g[d],c)}var d,e=this,f=e.wrapper,g=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"];if(a)for(d=0;d<g.length;d++)e.h.addEventListener(f,g[d],c)},getWrapperTranslate:function(a){"use strict";var b,c,d,e,f=this.wrapper;return"undefined"==typeof a&&(a="horizontal"===this.params.mode?"x":"y"),this.support.transforms&&this.params.useCSS3Transforms?(d=window.getComputedStyle(f,null),window.WebKitCSSMatrix?e=new WebKitCSSMatrix("none"===d.webkitTransform?"":d.webkitTransform):(e=d.MozTransform||d.OTransform||d.MsTransform||d.msTransform||d.transform||d.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),b=e.toString().split(",")),"x"===a&&(c=window.WebKitCSSMatrix?e.m41:parseFloat(16===b.length?b[12]:b[4])),"y"===a&&(c=window.WebKitCSSMatrix?e.m42:parseFloat(16===b.length?b[13]:b[5]))):("x"===a&&(c=parseFloat(f.style.left,10)||0),"y"===a&&(c=parseFloat(f.style.top,10)||0)),c||0},setWrapperTranslate:function(a,b,c){"use strict";var d,e=this.wrapper.style,f={x:0,y:0,z:0};3===arguments.length?(f.x=a,f.y=b,f.z=c):("undefined"==typeof b&&(b="horizontal"===this.params.mode?"x":"y"),f[b]=a),this.support.transforms&&this.params.useCSS3Transforms?(d=this.support.transforms3d?"translate3d("+f.x+"px, "+f.y+"px, "+f.z+"px)":"translate("+f.x+"px, "+f.y+"px)",e.webkitTransform=e.MsTransform=e.msTransform=e.MozTransform=e.OTransform=e.transform=d):(e.left=f.x+"px",e.top=f.y+"px"),this.callPlugins("onSetWrapperTransform",f),this.params.onSetWrapperTransform&&this.fireCallback(this.params.onSetWrapperTransform,this,f)},setWrapperTransition:function(a){"use strict";var b=this.wrapper.style;b.webkitTransitionDuration=b.MsTransitionDuration=b.msTransitionDuration=b.MozTransitionDuration=b.OTransitionDuration=b.transitionDuration=a/1e3+"s",this.callPlugins("onSetWrapperTransition",{duration:a}),this.params.onSetWrapperTransition&&this.fireCallback(this.params.onSetWrapperTransition,this,a)},h:{getWidth:function(a,b,c){"use strict";var d=window.getComputedStyle(a,null).getPropertyValue("width"),e=parseFloat(d);return(isNaN(e)||d.indexOf("%")>0||0>e)&&(e=a.offsetWidth-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-left"))-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-right"))),b&&(e+=parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-left"))+parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-right"))),c?Math.ceil(e):e},getHeight:function(a,b,c){"use strict";if(b)return a.offsetHeight;var d=window.getComputedStyle(a,null).getPropertyValue("height"),e=parseFloat(d);return(isNaN(e)||d.indexOf("%")>0||0>e)&&(e=a.offsetHeight-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-top"))-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-bottom"))),b&&(e+=parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-top"))+parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-bottom"))),c?Math.ceil(e):e},getOffset:function(a){"use strict";var b=a.getBoundingClientRect(),c=document.body,d=a.clientTop||c.clientTop||0,e=a.clientLeft||c.clientLeft||0,f=window.pageYOffset||a.scrollTop,g=window.pageXOffset||a.scrollLeft;return document.documentElement&&!window.pageYOffset&&(f=document.documentElement.scrollTop,g=document.documentElement.scrollLeft),{top:b.top+f-d,left:b.left+g-e}},windowWidth:function(){"use strict";return window.innerWidth?window.innerWidth:document.documentElement&&document.documentElement.clientWidth?document.documentElement.clientWidth:void 0},windowHeight:function(){"use strict";return window.innerHeight?window.innerHeight:document.documentElement&&document.documentElement.clientHeight?document.documentElement.clientHeight:void 0},windowScroll:function(){"use strict";return"undefined"!=typeof pageYOffset?{left:window.pageXOffset,top:window.pageYOffset}:document.documentElement?{left:document.documentElement.scrollLeft,top:document.documentElement.scrollTop}:void 0},addEventListener:function(a,b,c,d){"use strict";"undefined"==typeof d&&(d=!1),a.addEventListener?a.addEventListener(b,c,d):a.attachEvent&&a.attachEvent("on"+b,c)},removeEventListener:function(a,b,c,d){"use strict";"undefined"==typeof d&&(d=!1),a.removeEventListener?a.removeEventListener(b,c,d):a.detachEvent&&a.detachEvent("on"+b,c)}},setTransform:function(a,b){"use strict";var c=a.style;c.webkitTransform=c.MsTransform=c.msTransform=c.MozTransform=c.OTransform=c.transform=b},setTranslate:function(a,b){"use strict";var c=a.style,d={x:b.x||0,y:b.y||0,z:b.z||0},e=this.support.transforms3d?"translate3d("+d.x+"px,"+d.y+"px,"+d.z+"px)":"translate("+d.x+"px,"+d.y+"px)";c.webkitTransform=c.MsTransform=c.msTransform=c.MozTransform=c.OTransform=c.transform=e,this.support.transforms||(c.left=d.x+"px",c.top=d.y+"px")},setTransition:function(a,b){"use strict";var c=a.style;c.webkitTransitionDuration=c.MsTransitionDuration=c.msTransitionDuration=c.MozTransitionDuration=c.OTransitionDuration=c.transitionDuration=b+"ms"},support:{touch:window.Modernizr&&Modernizr.touch===!0||function(){"use strict";return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)}(),transforms3d:window.Modernizr&&Modernizr.csstransforms3d===!0||function(){"use strict";var a=document.createElement("div").style;return"webkitPerspective"in a||"MozPerspective"in a||"OPerspective"in a||"MsPerspective"in a||"perspective"in a}(),transforms:window.Modernizr&&Modernizr.csstransforms===!0||function(){"use strict";var a=document.createElement("div").style;return"transform"in a||"WebkitTransform"in a||"MozTransform"in a||"msTransform"in a||"MsTransform"in a||"OTransform"in a}(),transitions:window.Modernizr&&Modernizr.csstransitions===!0||function(){"use strict";var a=document.createElement("div").style;return"transition"in a||"WebkitTransition"in a||"MozTransition"in a||"msTransition"in a||"MsTransition"in a||"OTransition"in a}(),classList:function(){"use strict";var a=document.createElement("div");return"classList"in a}()},browser:{ie8:function(){"use strict";var a=-1;if("Microsoft Internet Explorer"===navigator.appName){var b=navigator.userAgent,c=new RegExp(/MSIE ([0-9]{1,}[\.0-9]{0,})/);null!==c.exec(b)&&(a=parseFloat(RegExp.$1))}return-1!==a&&9>a}(),ie10:window.navigator.msPointerEnabled,ie11:window.navigator.pointerEnabled}},(window.jQuery||window.Zepto)&&!function(a){"use strict";a.fn.swiper=function(b){var c;return this.each(function(d){var e=a(this),f=new Swiper(e[0],b);d||(c=f),e.data("swiper",f)}),c}}(window.jQuery||window.Zepto),"undefined"!=typeof module?module.exports=Swiper:"function"==typeof define&&define.amd&&define([],function(){"use strict";return Swiper});


(function () {
	// ----------------
	// Global variables
	// ----------------
	var isFirstVisit = null;
	var isNewVisit = null;
	var form_event = null;
	var isSubmitOccured = null;
	var isFirstSubmit = null;
	var proto = 'https:'
	var baseTrackUrl = '//track.leadhit.io/';
	var baseMediakUrl = '//media.leadhit.io/';
	var tickInterval = 60000;
	var contentVersion = Math.floor(Date.now()/100000).toString();
	var debug = localStorage.getItem('leadhit_secret');
	// Variables for new form algoritm
	var defaultDisabledForms = ['#id_popup_leadhit_wrapper_discount_form', '#id_popup_leadhit_blub_wrapper_discount_form', '#deferred-products__form'];
	var disabledForms = [];
    var orderButtons = [];

	//form submits storage
	var __forms = [];

	// if not IE9+, Webkit, Opera
	var old_browser = !document.getElementsByClassName;

	var domain = '';
	var params = {"popup": false};

	window.lh_banner_data = {};
	window.Leadhit = new Object();

	// ----------------
	// Global(?) helpers
	// ----------------

  sendError = function(err, params) {
    console.log("ERROR");
    console.error(err);
    if (location.protocol == 'http:') {
      Raven.config('http://7b976babc798429e96ffd296983aaaeb@sentry.dev.leadhit.ru/5');
      Raven.setUserContext(params);
      Raven.captureException(err);
    }
  }


	// Do we really need this comments?  +=
	function lh_banner_setCookie(name, value, expires, path, domain, secure) {
	path = '/';
		if (!name || !value) return false;
		var str = name + '=' + encodeURIComponent(value);
		if (expires) str += '; expires=' + expires.toGMTString();
		if (path)    str += '; path=' + path;
		if (domain)  str += '; domain=' + domain; else str += '; domain=.' + window.location.hostname.replace('www.', '');
		if (secure)  str += '; secure';
		document.cookie = str;
		return true;
	}
	window.Leadhit.setCookie = lh_banner_setCookie;

	// Get query dictionary will hav every GET parameter with its value for later ease
	var GETqueryDict = {};
	location.search.substr(1).split("&").forEach(function(item) {GETqueryDict[item.split("=")[0]] = item.split("=")[1]})
	window.Leadhit.GETqueryDict = GETqueryDict;

	// This should set disablement cookie if arg value is one of disablers list
	function disableBannerByGET(arg, disablers) {
		if (disablers.indexOf(GETqueryDict[arg]) != -1) {
			var date = new Date();
			var minutes = 3600 * 3600;
			var n = (date.getTime()) / 1000;
			date.setTime(date.getTime() + (minutes * 60 * 1000));
			lh_banner_setCookie('lh_banner_filled', n, date);
		}
	}
	window.Leadhit.disableBannerByGET = disableBannerByGET;


	// Add event listener
	function addEvent(elem, evType, fn) {
                if( Object.prototype.toString.call( elem ) === '[object Array]' ) elems = elem;
                else elems = [elem];
                for (i=0; i<elems.length; i++) {
                        elem = elems[i];
                        if (elem.addEventListener) {
                                elem.addEventListener(evType, fn, false);
                        } else if (elem.attachEvent) {
                                elem.attachEvent('on' + evType, fn);
                        } else {
                                elem['on' + evType] = fn;
                        }
                }
	}

    // Add event listener by selector
	function addEventLive(elementQuerySelector, eventType, cb) {
        document.addEventListener(eventType, function (event) {
        var qs = document.querySelectorAll(elementQuerySelector);
        if (qs) {
            var el = event.target, index = -1;
            while (el && ((index = Array.prototype.indexOf.call(qs, el)) === -1)) {
                el = el.parentElement;
            }

            if (index > -1) {
                cb.call(el, event);
            }
        }
    });
}

	// Truncate url (http://www.example.com -> example.com)
	function getHostname(url) {
		url = url.toString();
		url = url.replace("/www.", "/");
		var m = ((url || '') + '').match(/^https?:\/\/[^/]+/);
		return m ? m[0] : "";
	}

	function loadScript(url, cb) {
		var sc = document.createElement("script");
		sc.type = 'text/javascript';
		sc.src = url;
		sc.charset = 'utf-8';
		var head = document.getElementsByTagName("head")[0];
		sc.onreadystatechange = sc.onload = cb;
		head.appendChild(sc);
	}

    function postRequest(url, cb) {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function(){
            if (xhr.readyState == 4 && xhr.status == 200){
                cb(xhr);
            }
        }
		xhr.open("POST", url, true);
        xhr.setRequestHeader('Content-type', 'text/plain; charset=utf-8');
        // prevent SecurityViolation on some opera versions
        try {
          xhr.send();
        } catch(e) {}
	}


	function safePreventEvent(e) {
		if (e.preventDefault) {
			e.preventDefault();
		} else e.returnValue = false;
	}

	function getURLParameter(name, str) {
		url = str ? str : location.search;
		return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, ''])[1]);
	}

	// Escape params and generate query string
	function makeQuery(params) {
		var query = '';
		for (key in params) {
			query += encodeURIComponent(key) + "=" + encodeURIComponent(params[key]) + '&';
		}
		return query.replace(/&$/, '');
	}

	function createCookie(name, value, minutes, esc) {
		var date = new Date();
		date.setTime(date.getTime() + minutes * 1000 * 60);
		if (esc) value = escape(value);
		document.cookie = name + "=" + value + ";path=/;domain=." + domain + ((minutes === null) ? "" : ";expires=" + date.toGMTString());
	}

	function createComplexCookie(name, values, minutes, esc) {
		var date = new Date();
		date.setTime(date.getTime() + minutes * 1000 * 60);
		var value = '';
		for (i = 0; i < values.length; i++) if (esc) values[i] = escape(values[i]);
		value = values.join('|');
		document.cookie = name + "=" + value + ";path=/;domain=." + domain + ((minutes === null) ? "" : ";expires=" + date.toGMTString());
	}

	function readCookie(name) {
		if (document.cookie.length > 0) {
			offset = document.cookie.indexOf(name + "=");
			if (offset != -1) {
				offset = offset + name.length + 1;
				tail = document.cookie.indexOf(";", offset);
				if (tail == -1) tail = document.cookie.length;
				return unescape(document.cookie.substring(offset, tail));
			}
		}
		return null;
	}

	function getReferrer() {
		sess = readCookie('_lhtm_r');
		if (sess) {
			return sess.split('|')[0];
		}
		else {
			return window.lh_ref;
		}
	}

	function getVisitId() {
		sess = readCookie('_lhtm_r');
		if (sess) {
			return sess.split('|')[1];
		}
		else {
			return window.lh_vid;
		}
	}

	// Return uid in MongoDB ObjectID format
    function genUID() {
      return "xxxxxxxxxxxxxxxxxxxxxxxx".replace(/x/g, function() {
        try {
          var rand = crypto.getRandomValues((new Uint8Array(1)))[0] % 16;
        } catch(e) {
          var rand = Math.floor(Math.random() * 16);
        }
        return rand.toString(16);
      });
    }

	function getUID() {
		return readCookie('_lhtm_u');
	}

  function wrapToForm(el, formAction) {
      var form = document.createElement('form');
      form.action = formAction || '';
      el.parentNode.insertBefore(form, el);
      form.appendChild(el);
      return form
  }


	// ----------------
	// Visit helpers
	// ----------------

	//isFirstVisit = !readCookie('_lhtm_u');
	//isNewVisit = !readCookie('_lhtm_r');

	// ----------------
	// Form helpers
	// ----------------

	function getElemValue(el) {
		switch (el.nodeName.toUpperCase()) {
		case "INPUT":
			switch (el.type.toLowerCase()) {
			case "checkbox":
			case "radio":
				if (el.checked) {
					return el.value;
				}
				break;
			case "text":
			case "email":
			case "hidden":
			case "submit":
			case "button":
			case "tel":
			case "image":
				return el.value;
				break;
			}
			break;
		case "TEXTAREA":
			return el.value;
			break;
		case "SELECT":
			if (el.type.toLowerCase() !== "select-multiple") {
				//return el.value
				if (el.options[el.selectedIndex] == 'undefined') {
					return el.options[el.selectedIndex].text;
				} else { return ""; }
			}
			break;
		}
		return "";
	}

	function getStringByXPath(query) {
		try {
			var el_cart_sum = document.evaluate(window.lh_vars['cart_sum_query'], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
			return el_cart_sum.innerText || el_cart_sum.innerHTML || el_cart_sum.data;
		} catch(e) {return ""}
	}

  function getStringByQS(selector) {
    var el = document.querySelector(selector);
    return (el ? el.innerText || el.innerHTML || el.data || el.value : '').replace(/\u00a0/g, ' ');
  }

	function log(str) {
		console.log((new Date()).toISOString() + ' :: ' + str);
	}

	function nearest(el, query) {
    if (el.tagName.toLowerCase() == 'html' || !el.parentNode) return false;

    // checks current element itself
    var els = el.parentNode.querySelectorAll(query);
    for (i=0; i<els.length; i++) if (els[i] == el)
    {
    return el;
    }

    result = el.querySelector(query);
    return result ? result : nearest(el.parentElement, query);
	}

	// Special function for ajax sendForm request
	function sendWForm(form) {
		if (!form) return false;

		var params = {};
		var sc = document.createElement("script");
		sc.type = 'text/javascript';
		for (i = 0; i < form.elements.length; i++) {
			el = form.elements[i];
			if (el.type != 'password') {
				//if (el.type.toLowerCase != 'submit')
				params['f_' + el.name.replace(/\$/g, '*')] = getElemValue(el);
			}
		}

		params['url'] = location.toString().split('#')[0];
		params['action'] = (form.attributes['action'] || {
			value: ''
		}).value || ('://' + document.location.host + document.location.pathname).replace('://www.', '://');
    params['uid'] = getUID();
		params['vid'] = getVisitId();
		params['ref'] = getReferrer();
		params['clid'] = window.lh_clid;

		try {
			var el_cart_sum = document.evaluate(window.lh_vars['cart_sum_query'], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
			params['f_cart_sum'] = el_cart_sum.innerText || el_cart_sum.innerHTML || el_cart_sum.data;
		}
		catch (e) {
			params['f_cart_sum'] = "";
		}
    // if cart sum not found try to get cart_sum with CSS selector
    try {
			if (!params['f_cart_sum']) params['f_cart_sum'] = getStringByQS(lh_vars['cart_sum_query']);
    } catch(e) {}

    params['f_cart_sum'].replace(/\u00a0/g, ' ');


		params['formid'] = form._lh_formid;

    if (!form._lh_formid) form._lh_formid = genUID();
    saveForm(form._lh_formid, params);
    sendFormAsync(form._lh_formid);


		form_url = proto + baseTrackUrl + "stat/lead_form?" + makeQuery(params);
		var headID = document.getElementsByTagName("head")[0];
		set_lead = function () {};

		if (old_browser) sc.onreadystatechange = set_lead;
		else addEvent(sc, 'load', set_lead);

		// sc.src = form_url;
		// headID.appendChild(sc);
	}

	function returnFalse() {
		return false;
	}

	function returnTrue() {
		return true;
	}

	function sendForm(e) {
		if (typeof(e) == 'boolean'){
			e = null;
			delete e;
		}

		// Exit on duplicate call
		if (isFirstSubmit !== null) return null;
		isFirstSubmit = true;

		// Is it event?
		// If no, then `this` points to form
		if (typeof(e) != 'undefined' && e != null) {

			is_event = true;
			e = e ? e : window.event;
			var form = (e.currentTarget) ? e.currentTarget : e.srcElement;
			form.submit = __forms[form._lh_formid];
			form_event = e;
			form_event.isPrevented = (form_event.defaultPrevented || form_event.returnValue === false || form_event.getPreventDefault && form_event.getPreventDefault()) ? returnTrue() : returnFalse();
		} else {
			this.submit = __forms[this._lh_formid];
			var form = this;
			is_event = false;
		}

		if (typeof(e) != "undefined" && e != null) {
			safePreventEvent(e);
		}

		setTimeout(function () {
			if (!(form_event && form_event.isPrevented) && !isSubmitOccured) {
				document.createElement('form').submit.call(form);
			}
		}, 1500);
		var params = {};
		var sc = document.createElement("script");
		sc.type = 'text/javascript';
		for (i = 0; i < form.elements.length; i++) {
			el = form.elements[i];
			if (el.type != 'password') {
				//if (el.type.toLowerCase != 'submit')
				params['f_' + el.name.replace(/\$/g, '*')] = getElemValue(el);
			}
		}

		params['url'] = location.toString();
		params['action'] = (form.attributes['action'] || {
			value: ''
		}).value || ('//' + document.location.host + document.location.pathname).replace('//www.', '//');
		params['uid'] = getUID();
		params['vid'] = getVisitId();
		params['ref'] = getReferrer();
		params['formid'] = form._lh_formid;
		params['clid'] = window.lh_clid;
    saveForm(form._lh_formid, params);

		try {
			cart_queries = window.lh_vars['cart_sum_query']
			if (typeof(cart_queries) == 'object') {
				for (var i = 0; i < queries.length; i++) {
					 var cart_sum = getStringByXPath(queries[i]);
					 if (cart_sum) params['f_cart_sum'] = cart_sum;
				};
			} else {
				params['f_cart_sum'] = getStringByXPath(cart_queries);
			}
		}
		catch (e) {
			params['f_cart_sum'] = "";
		}
    // if cart sum not found try to get cart_sum with CSS selector
    try {
			if (!params['f_cart_sum']) params['f_cart_sum'] = getStringByQS(lh_vars['cart_sum_query']);
    } catch(e) {}

		form_url = proto + baseTrackUrl + "stat/lead_form?" + makeQuery(params);

		var headID = document.getElementsByTagName("head")[0];
		set_lead = function () {
						// Check for jQuery.live event handler
						/*
						if ($ && $(document) && $(document).data('events') && $(document).data('events').submit) {
								submits = $(document).data('events').submit
								$.each(submits, function() {
										if ($(this.selector)[0] == form) {
												isSubmitOccured = true;
										}
								});
						}
						*/
			// Submit form only if this is not prevented(for ajax forms)
			if (!(form_event && form_event.isPrevented) && !isSubmitOccured) {
				document.createElement('form').submit.call(form);
				isSubmitOccured = true;
			}
		};

		if (old_browser) sc.onreadystatechange = set_lead;
		else addEvent(sc, 'load', set_lead);

		sc.src = form_url;
		headID.appendChild(sc);
	}

  function saveForm(formId, params) {
    if (debug) console.log(formId, params);

    // if (debug)
    localStorage.setItem('lht_form_' + formId, JSON.stringify(params));
  }

  function sendFormAsync(formId) {
    // if (!debug) return true;
    if (debug) console.log('sendFormAsync ' + formId);

    var params = JSON.parse(localStorage.getItem('lht_form_' + formId));
    params['async'] = '1';
    params['formid'] = formId;
		var form_url = proto + baseTrackUrl + "stat/lead_form?" + makeQuery(params);
		var headID = document.getElementsByTagName("head")[0];

		var sc = document.createElement("script");
		sc.type = 'text/javascript';
		sc.src = form_url;
		headID.appendChild(sc);
    deleteFormEntry(formId);

		if (old_browser) sc.onreadystatechange = deleteFormEntry;
    addEvent(sc, 'load', function () { deleteFormEntry(formId); });
  }

  function deleteFormEntry(formId) {
    localStorage.removeItem('lht_form_' + formId);
  }

  function sendQueuedForms() {
    // if (!debug) return true;

    for (var entry in localStorage) {
      if (entry.indexOf('lht_form_') > -1) {
        var formId = entry.substr(9, 24);
        sendFormAsync(formId);
      }
    }
  }

	// Attach handler to webpage forms
	function handleForms() {

		var forms = document.getElementsByTagName("form");
		for (var i = 0; i < forms.length; i++) {
			form = forms[i];
			// Make sure that only one submit on handled form
			for (var j = 0, c = 0; j < form.elements.length; j++) {
				if (form.elements[j].type && form.elements[j].type.toLowerCase() == 'submit') c++;
			}

			if (c < 2 && form.name != "aspnetForm" && !form.lhDisable) {
				addEvent(forms[i], 'submit', sendForm);

				// Save previous submit function
				__forms[i] = forms[i].submit;

				// Set unique id on form
				forms[i]._lh_formid = genUID();

				// Overwrite form submit
				forms[i].submit = sendForm;
				//forms[i].setAttribute('submit', sendForm);

				// Append hidden field with copied submit attributes to prevent losing sent data
				var el_count = form.elements.length;
				for (j=0; j<el_count; j++) {
					var element = form.elements[j];
					if (element.type && element.name && (element.type.toLowerCase() == "submit" || element.type.toLowerCase() == "button")) {
						var input = document.createElement('input');
						input.type = "hidden";
						input.name = element.name;
						input.value = element.value;
						form.appendChild(input);
					}
				}

			}
		}
	}

	// ----------------
	// Main
	// ----------------

	// Parse url and return actual referrer
	function parseRef(referrer) {

		// Check lh_message_id
		lh_message_id = getURLParameter('lh_message_id');
		if (lh_message_id) referrer = 'mail_' + lh_message_id;

		// Check yandex ad param(_openstat)
		yad = getURLParameter('_openstat');
		if (yad) referrer = 'yad';

		// Check adwords param
		adwords = getURLParameter('gclid');
		if (adwords) referrer = 'adwords';

		// Check lh_campaign
		campaign = getURLParameter('lh_campaign');
		if (campaign) referrer = 'lh_campaign_' + campaign;

		return referrer;
	}

	// Set global variables
	function init() {

		sess = readCookie('_lhtm_r');
		if (sess) {
			window.lh_vid = sess.split('|')[1];
			window.lh_ref = sess.split('|')[0];
		}

		isFirstVisit = !readCookie('_lhtm_u');
		isNewVisit = !readCookie('_lhtm_r');
		domain = location.host.replace('www.', '');
		window.lh_sf = sendWForm;

		if (typeof window._lh_params != 'undefined') {
			params = window._lh_params;
			if ('popup' in params && params['popup'])
				params['popup'] = true;
			else
				params['popup'] = false;
		}

		try {
			if (typeof $x == 'undefined') {} // wgxpath.install();
		} catch(e) {sendError(e);}


//				if (XMLHttpRequest.prototype.send.toString().indexOf('native code') > -1) {
//						XMLHttpRequest.prototype.lh_send = XMLHttpRequest.prototype.send
//						XMLHttpRequest.prototype.send = function(content) {
//							this.lh_send(content);
//							var p = 'formData=' + encodeURIComponent(content);
//							var i = document.createElement("img");
//							i.src = 'http://track.dev.leadhit.ru/log?' + p;
//						};
//				}
	if (debug) log('init complete');
	}

	function processVisit() {
    try {
		var params = {};

		// Check referer from same domain
		referrer_host = getHostname(document.referrer);
		local_host = window.location.protocol + '//' + window.location.hostname.replace('www.', '');
		referrer = (referrer_host == local_host) ? 'direct' : document.referrer;
		referrer = referrer ? referrer : 'direct';
		referrer = parseRef(referrer);

		// send tracked email
		var tracked_email = getURLParameter('lh_track_email');
		if (tracked_email) {
			params['f_lh_track_email'] = tracked_email;
			params['url'] = location.toString().split('#')[0];
			params['action'] = 'lh_track_email'
			params['uid'] = getUID();
			params['vid'] = getVisitId();
			params['ref'] = getReferrer();
			var track_email_form = proto + baseTrackUrl+ "stat/lead_form?" + makeQuery(params) + '&clid=' + window.lh_clid;
			var headID = document.getElementsByTagName("head")[0];
			var sc = document.createElement("script");
			sc.type = 'text/javascript';
			sc.src = track_email_form;
			headID.appendChild(sc);
			console.log(track_email_form);
		}

		if (isFirstVisit) {
			// Get lead_id from url if possible(used in the mailing)
			lead_id = getURLParameter('lead_id');
			if (!lead_id || lead_id == 'None') lead_id = genUID();
			createCookie('_lhtm_u', lead_id, 60 * 24 * 3650, false);
			var UID = genUID();
			createComplexCookie('_lhtm_r', [referrer, UID], 60 * 24 * 3650, true);
			window.lh_vid = UID;
			window.lh_ref = referrer;

		} else if (isNewVisit) {
			var UID = genUID();
			createComplexCookie('_lhtm_r', [referrer, UID], 60 * 24 * 3650, true);
			window.lh_vid = UID;
			window.lh_ref = referrer;
		}

		params['ref'] = getReferrer();
		params['vid'] = getVisitId();
		params['uid'] = getUID();
		params['n'] = ( !! isNewVisit ? 1 : 0).toString();
		params['location'] = document.location.toString().split('#')[0];
		params['stat'] = getStatistics()

		var i = document.createElement("img");
		var track_url = proto + baseTrackUrl+ "stat/counter_server?" + makeQuery(params) + '&clid=' + window.lh_clid;

		i.src = track_url;
		if (debug) log('counter_server request start');
    } catch(e) { sendError(e) }
	}

	function initPlugins() {
		if (params["popup"]) initPopup();
	}

	function initPopup() {
		var sc = document.createElement("script");
		sc.type = 'text/javascript';
		sc.src = proto + baseMediakUrl + 'widgets/popup_new/js/popup.js';
		var head = document.getElementsByTagName("head")[0];
		head.appendChild(sc);
	}

	function loadLHVars() {
		// var url = proto + baseTrackUrl + "stat/lh_vars?" + 'clid=' + window.lh_clid + '&ver=' + contentVersion;
		var url = proto + baseTrackUrl + "stat/lh_vars?" + 'clid=' + window.lh_clid;
		loadScript(url, choiceSendForm);
	}

	function startTicker() {
		function sendTick(forced) {
			// Needs to avoid multiple queries from several tabs
			if ((new Date().getTime() - localStorage.getItem('_lh_lastTick') > tickInterval) || forced) {
				localStorage.setItem('_lh_lastTick', new Date().getTime());
				var params = {
					'uid': getUID(),
					'vid': getVisitId()
				};
				var url = proto + baseTrackUrl + "stat/tick?" + makeQuery(params) + '&clid=' + window.lh_clid;

				function SetDiffTime(xhr){
                    lhDiffTime = Date.parse(xhr.getResponseHeader('Date')) - Date.now();
                    localStorage.setItem('lhDiffTime', lhDiffTime);
                };

				postRequest(url, SetDiffTime);
	            if (debug) log('tick sent');
			};
		}
		sendTick(true);
		setInterval(sendTick, tickInterval);
	}

	function addToCart(item_url) {
		try {
			var cart_params = {
				item_id: '0',
				item_url: item_url ? item_url : document.location.toString(),
				event_type: item_url ? 'catalog' : 'item',
				uid: Leadhit.getUID(),
				vid: Leadhit.getVisitId(),
				clid: window.lh_clid
			};
			console.log(cart_params);
			Leadhit.loadScript(proto + '//track.leadhit.ru/stat/add_to_cart?' + Leadhit.makeQuery(cart_params));
		} catch(e) {sendError(e);};
	}

	function saveItemUrl(name, url) {
		var ItemUrl = JSON.parse(localStorage['lhItemUrl']);
		ItemUrl[name] = url;
		localStorage['lhItemUrl'] = JSON.stringify(lhItemUrl);
    }

    function getItemUrl(name) {
		var ItemUrl = JSON.parse(localStorage['lhItemUrl']);
		var url = ItemUrl[name];
		return url
    }

	function addItemToOrder(item_urls) {
	    try {
	        var cart_params = {
	            item_urls: item_urls,
	            uid: Leadhit.getUID(),
				vid: Leadhit.getVisitId(),
				clid: window.lh_clid
	        };
	        console.log(cart_params);
			Leadhit.loadScript(proto + baseTrackUrl + 'stat/add_item_to_order?' + Leadhit.makeQuery(cart_params));
		} catch(e) {sendError(e);};
	}

	function enableStatistics() {
		if (debug) log('lh_vars loaded');

		if (lh_vars['statistics_enabled'].toLowerCase() !== 'true') return false;

		//if we came from ahother page but same domane - finish previous vitit
		referrer_host = getHostname(document.referrer);
		local_host = window.location.protocol + '//' + window.location.hostname.replace('www.', '');
		if (referrer_host == local_host)
			saveEnd(document.referrer);

		//define visit source and save start mark
		if (referrer.substring(0, 5) == 'mail_')
			source = 'FRM';
		else if ((referrer.substring(7,13) == 'yandex')
				 || (referrer.substring(7,13) == 'google'))
			source ='ISR'
		else
			source = 'ANY';
		saveStart(source);

		//save catalog views mark
		// saveCatalogItems();

		//provide visit end mark on return from another page if possible
		if (typeof document['hidden'] !== "undefined")
			document.addEventListener('visibilitychange', handleVisibilityChange, false);
		else
			saveEnd(location.href);

		function handleVisibilityChange() {
		  if (document['hidden']) saveEnd(location.href);
		  else saveStart('RET');
		}

		function saveStart(source) {
			if (localStorage['lhPageviews'])
				var lhPageviews = JSON.parse(localStorage['lhPageviews']);
			else {
				var lhPageviews = {};
			};
			if (!lhPageviews[location.href])
				lhPageviews[location.href] = [];

			var item = lhPageviews[location.href];
			item[item.length] = [source,Date.now()];
			localStorage['lhPageviews'] = JSON.stringify(lhPageviews);
		}

		function saveEnd(page) {
			try {
				var lhPageviews = JSON.parse(localStorage['lhPageviews']);
				var item = lhPageviews[page];
				item[item.length-1][2] = Date.now();
				localStorage['lhPageviews'] = JSON.stringify(lhPageviews);
			} catch(err) {
/*				sendError(err, {
				    ended_page: page,
				    lhPageviews: JSON.stringify(lhPageviews)
				});*/
			}
		}

		function saveCatalogItems() {
			var catalogItems;
			var links = document.querySelectorAll(lh_vars['catalogItemQuery']);
			var views = [];
			for (var i in links) {
				url = links[i].href;
				if (url) views.push(url);
			}
			localStorage['lhCatalogItemViews'] = JSON.stringify(views);
		}
	}

	function getStatistics() {
		/*if (!localStorage['lhPageviews'])
			return ''*/
		var result = [], now = Date.now();
		/*var lhPageviews = JSON.parse(localStorage['lhPageviews']);
		for (var page in lhPageviews){
			for (var i = lhPageviews[page].length-1; i>=0; i--){
				if (lhPageviews[page][i].length == 3 || (Date.now()-lhPageviews[page][i][1])>864000){
					data = lhPageviews[page][i];
					lhPageviews[page].splice(i, 1);
					endurance = data.length == 3 ? data[2] - data[1] : 0
					result.push(['VISIT', page, now - data[1], endurance, data[0]]);
				}
			}
			if (lhPageviews[page].length == 0)
				delete lhPageviews[page]
		}
		localStorage['lhPageviews'] = JSON.stringify(lhPageviews);*/

		/* simplified VISIT event tracking */
		//define visit source and save start mark
		if (referrer.substring(0, 5) == 'mail_')
			var source = 'FRM';
		else if ((referrer.substring(7,13) == 'yandex')
				 || (referrer.substring(7,13) == 'google'))
			var source ='ISR';
		else
			var source = 'ANY';
                result.push(['VISIT', document.location.toString(), now, 0, source]);

//		var catalogItemViews = JSON.parse(localStorage['lhCatalogItemViews']);
//		for (var i = 0; i < catalogItemViews.length; i++) {
//			result.push(['CATALOGITEMVIEW', catalogItemViews[i], 0, 1]);
//		}

		return JSON.stringify(result)
	}

	function newSendForm(leadhitForm, key, press) {
        var params = {};

        params['url'] = location.toString().split('#')[0];
        params['uid'] = getUID();
        params['vid'] = getVisitId();
        params['ref'] = getReferrer();
        params['formid'] = key.substring(11);
        params['f_cart_sum'] = ''

        try {
            params['f_cart_sum'] = getStringByQS(lh_vars['cart_sum_query']);
        } catch(e) {}

        params['f_cart_sum'].replace(/\u00a0/g, ' ');

        if (press || !leadhitForm['lh_order']) {
            params['action'] = (leadhitForm['action']) || ('http://' + document.location.host + document.location.pathname).replace('http://www.', 'http://');
        } else {
            params['action'] = 'lhAutoForms';
        }

        form_url = proto + baseTrackUrl + "stat/lead_form?" + leadhitForm['query'] + '&' + makeQuery(params) + '&clid=' + window.lh_clid + '&form_added_time=' + leadhitForm['form_added_time'];

        function _sendForm(key, leadhitForm) {
            loadScript(form_url, function(e) {
                console.log('callback');
                var leadhitSentForm = JSON.parse(localStorage.getItem('leadhitSentForm' + key.substring(11)));
                leadhitSentForm = leadhitForm;
                localStorage.setItem('leadhitSentForm' + key.substring(11), JSON.stringify(leadhitSentForm));
                localStorage.removeItem(key);
            })
        }

        _sendForm(key, leadhitForm);

    }

    function checkAndSentForm(press) {

        var localKeys = Object.keys(localStorage);
        for (i = 0; i < localKeys.length; i++) {
            if (localKeys[i].indexOf('leadhitForm') !== -1) {
                var leadhitForm = localStorage.getItem(localKeys[i]),
                    leadhitSentForm = localStorage.getItem('leadhitSentForm' + localKeys[i].substring(11));
                if (leadhitForm != '{}' && leadhitForm != leadhitSentForm) {
                    newSendForm(JSON.parse(leadhitForm), localKeys[i], press);
                }
            }
        }
    };


    function hashCode(str) {
        var hash = 0;

        if (str.length == 0) return hash;

        for (i = 0; i < str.length; i++) {
            char = str.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    }


	function newHandleForms() {

		var forms = document.getElementsByTagName("form");
		for (var i = 0; i < forms.length; i++) {
			form = forms[i];

			// Set unique id on form
			form._lh_formid = genUID();

			// count hash
			var hash_string = 'form' + form['id']
			for (j=0; j<form.elements.length; j++) {
			    var element = form.elements[j];
			    hash_string = hash_string + element['tag'] + element['id'];
			};

			var hash = hashCode(hash_string);

			// Set leadhit name for field
			for (j=0; j<form.elements.length; j++) {
				var element = form.elements[j];
				element.setAttribute('data-lh_name', hash.toString() + '_field_' + j.toString());
			}
		}
	}

	function checkformsWithHashedFields(form) {
        for (var formSelector in formsWithHashedFields) {
            if (form == document.querySelector(formSelector)) {
                return formSelector;
            }
        }
        return false;
    }

    function newSaveForm(form) {

        var lhDiffTime = localStorage.getItem('lhDiffTime');
        if (!form._lh_formid) newHandleForms();
        var formId = form._lh_formid,
            leadhitForm = JSON.parse(localStorage.getItem(formId));

        if (!leadhitForm) leadhitForm = {};

        var params = {};
        for (var i = 0; i < form.elements.length; i++) {
            el = form.elements[i];
            if (el.type != 'password') {
                if (el.name) {
                    el_name = 'f_' + el.name;
                } else {
                    el_name = 'f_' + el.getAttribute('data-lh_name');
                }
                params[el_name] = getElemValue(el);
            };
        };

        if (typeof(formsWithHashedFields) !== "undefined" && checkformsWithHashedFields(form)) {
            formSelector = checkformsWithHashedFields(form);
            for (var i = 0; i < form.elements.length; i++) {
                var el = form.elements[i];
                if (formsWithHashedFields[formSelector].indexOf(el.name) !== -1) {
                    fhashName = 'f_' + (el.name || el.getAttribute('data-lh_name')) + '_' + hashCode(getElemValue(el));
                    params[fhashName] = '';
                }
            }
        }

        leadhitForm['query'] = makeQuery(params);
        leadhitForm['action'] = (form.attributes['action'] || {value: ''}).value;
        if (orderButtons.length > 0) {
            for (var i = 0; i < orderButtons.length; i++) {
                if (form == nearest(orderButtons[i], 'form')){
                    leadhitForm['lh_order'] = true;
                }
            }
        };

        leadhitForm['form_added_time'] = parseInt(lhDiffTime) + Date.now();
        localStorage.setItem('leadhitForm' + formId, JSON.stringify(leadhitForm));
    };

  try {
    localStorage.setItem('leadhit_tracker', '1');
    var check = true;
  } catch (e) {var check = false;}

	if (!window.lh_sf && check) {
		try {
			init();
			loadLHVars();
			processVisit();
			startTicker();
			try {
				initPlugins();
			} catch(e) {}
		}
		catch (e) {
      sendError(e);
		}
	}

    function checkDisabledForms(){
	    for (var i = 0; i < disabledSelectors.length; i++) {
            var disabled_form = document.querySelector(disabledSelectors[i]);
            if (disabled_form  && disabledForms.indexOf(disabled_form) == -1) {
                disabledForms.push(disabled_form);
            }
        }
	}

    function addOrderButtons() {
        for (var i = 0; i < orderSelectors.length; i++) {
            var order_button = document.querySelector(orderSelectors[i]);
            if (order_button && orderButtons.indexOf(order_button) == -1) {
                orderButtons.push(order_button);
            }
        }
    }

    function callOrderButton(order_button) {
        var form = (nearest(order_button, 'form'));
        checkDisabledForms()
        if (disabledForms.indexOf(form) == -1 ){
            newSaveForm(form);
            checkAndSentForm(true);
        }
    }

	function choiceSendForm() {
        // enableStatistics();

        if (typeof(orderSelectors) !== "undefined") {
            newHandleForms();
            var leadhitForm = {};

            document.addEventListener('mouseup', function (event) {
                addOrderButtons();
                if (orderButtons.indexOf(event.target) !== -1) {
                    callOrderButton(event.target);
                }
            });

            if (typeof(disabledSelectors) !== "undefined") {
                disabledSelectors = disabledSelectors.concat(defaultDisabledForms);
            } else {
                disabledSelectors = defaultDisabledForms;
            }

            var localKeys = Object.keys(localStorage);
            for (var i = 0; i < localKeys.length; i++) {
                if (localKeys[i].indexOf('leadhitForm') !== -1) {
                    leadhitForm = JSON.parse(localStorage.getItem(localKeys[i]));
                    if (leadhitForm) {
                        checkAndSentForm(false);
                    }
                } else if (localKeys[i].indexOf('leadhitSentForm') !== -1) {
                    localStorage.removeItem(localKeys[i]);
                }
            }

            document.addEventListener('input', function(e) {
                var form = (nearest(e.target, 'form'));
                checkDisabledForms()
                if (disabledForms.indexOf(form) == -1 ){
                    newSaveForm(form);
                }
            });

            document.addEventListener('select', function(e) {
                var form = (nearest(e.target, 'form'));
                checkDisabledForms()
                if (disabledForms.indexOf(form) == -1 ){
                    newSaveForm(form);
                }
            });

            setInterval(function() {
                checkAndSentForm(false);
            }, 2000);

        } else {
            setInterval(sendQueuedForms, tickInterval);
            sendQueuedForms();
            addEvent(window, 'load', handleForms);
        }
    };

    Leadhit.addEvent = addEvent;
    Leadhit.addEventLive = addEventLive;
    Leadhit.addItemToOrder = addItemToOrder;
    Leadhit.addToCart = addToCart;
    Leadhit.createCookie = createCookie;
    Leadhit.getStringByQS = getStringByQS;
    Leadhit.getStringByXPath = getStringByXPath;
    Leadhit.getVisitId = getVisitId;
    Leadhit.getUID = getUID;
    Leadhit.genUID = genUID;
    Leadhit.loadScript = loadScript;
    Leadhit.log = log;
    Leadhit.makeQuery = makeQuery;
    Leadhit.nearest = nearest;
    Leadhit.newSaveForm = newSaveForm;
    Leadhit.readCookie = readCookie;
    Leadhit.saveForm = saveForm;
    Leadhit.saveItemUrl = saveItemUrl;
    Leadhit.sendFormAsync = sendFormAsync;
    Leadhit.wrapToForm = wrapToForm;

})();


