<?php


class ModelModuleManufacturerCategory extends Model {
    public function getCategories($manufacturer_id) {
        $sql = "SELECT DISTINCT ptc.category_id as category_id, c.parent_id as parent_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX ."product_to_category ptc ON (p.product_id = ptc.product_id) LEFT JOIN " . DB_PREFIX . "category c ON (ptc.category_id = c.category_id) WHERE p.status = '1' AND p.manufacturer_id = '" . $manufacturer_id . "'";
        $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "product_option_value pov WHERE pov.product_id = p.product_id AND pov.quantity > '0' ORDER BY pov.quantity ASC)";
       // $sql .=")";

        $result = array();
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $query_child = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id='" . $row['category_id'] . "'");
            if (!$query_child->row) {
                $result[] = $row['category_id'];
            }
        }

        return $result;
    }

    private $_categories = array();
    private function getParentCategories($category_id) {
        $query = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE category_id='" . (int)$category_id . "'");
        if ($query->row['parent_id'] != 0) {
            $this->_categories[] = $query->row['category_id'];
            $this->getParentCategories($query->row['parent_id']);
        } else {
            $this->_categories[] = $query->row['category_id'];
            return true;
        }
    }

    public function getSeoUrl($category_id, $manufacturer_id) {
        $this->getParentCategories($category_id);
        $result = array();
        $queryMan = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query LIKE('manufacturer_id=" . (int)$manufacturer_id . "')");
        $this->_categories = array_reverse($this->_categories);
        foreach ($this->_categories as $key => $item) {
            $queryCatSql = "SELECT keyword FROM " . DB_PREFIX . "url_alias ";
            $queryCatSql .= " WHERE query LIKE('category_id=" . (int)$item . "')";
            $queryCat = $this->db->query($queryCatSql);
            $result[] = $queryCat->row['keyword'];
        }

        $url = '/';
        foreach ($result as $item) {
            $url .= $item.'/';
        }
        $url .= $queryMan->row['keyword'] . '/';
        $this->_categories = array();

        return $url;
    }


}
