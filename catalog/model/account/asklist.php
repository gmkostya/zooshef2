<?php
class ModelAccountAsklist extends Model {
	public function addAsklist($product_id) {
		$this->event->trigger('pre.asklist.add');

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_asklist WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "'");

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_asklist SET customer_id = '" . (int)$this->customer->getId() . "', product_id = '" . (int)$product_id . "', date_added = NOW()");

		$this->event->trigger('post.asklist.add');
	}
	public function addAsklist2($product_id, $option = array(), $name, $email) {
		$this->event->trigger('pre.asklist.add');
        if ($this->customer->getId()){
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_asklist WHERE customer_id = '" .  (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "' AND  option_info = '" .json_encode($option). "'");
           $this->db->query("INSERT INTO " . DB_PREFIX . "customer_asklist SET customer_id = '" . (int)$this->customer->getId()  . "', product_id = '" . (int)$product_id . "', option_info = '" . json_encode($option) . "', name = '" . $name . "',  email='" . $email . "', date_added = NOW()");
            //echo "INSERT INTO " . DB_PREFIX . "customer_asklist SET customer_id = '" . (int)$this->customer->getId()  . "', product_id = '" . (int)$product_id . "', option_info = '" . json_encode($option) . "', name = '" . $name . "',  email='" . $email . "', date_added = NOW()";

        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_asklist SET customer_id = '0', product_id = '" . (int)$product_id . "', option_info = '" . json_encode($option) . "', date_added = NOW(), name = '" . $name . "',  email='" . $email . "'");
        }
        $this->event->trigger('post.asklist.add');
	}

	public function deleteAsklist($product_id) {
		$this->event->trigger('pre.asklist.delete');

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_asklist WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "'");

		$this->event->trigger('post.asklist.delete');
	}

	public function getAsklist() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_asklist WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->rows;
	}

	public function getTotalAsklist() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_asklist WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
}
